#include "sip_bnf_solver.hpp"

#include <fstream>   // for operator<<, basic_ostream, basic_ostream::operator<<, basic_ostream<>::__ostream_type, basic_ios::rdbuf, endl, basic_o...
#include <memory>    // for unique_ptr, allocator_traits<>::value_type
#include <utility>   // for pair, make_pair, move
#include <iostream>  // for cout
#include <limits>    // for numeric_limits
#include <stdexcept> // for invalid_argument

#include "common.hpp"        // for copy_variables, discretize, fix_variables
#include "symbol_table.hpp"  // for symbol_table
#include "output.hpp"        // to_json, from_json
#include "nlohmann/json.hpp" // for json

#ifdef DIPS_use_mpi
#include "mpi.h"
#endif

namespace dips {


sip_bnf_program_definition_parser::sip_bnf_program_definition_parser(std::istream& is, symbol_table& symbols) :
    programdefinition_parser_chain(is, symbols, { { "lbp", {} } }) { }

void sip_bnf_solver::program_info::add_llp(const program& llp, const discretization& disc) {
    llps.emplace_back(llp);
    lbp_discs.emplace_back(disc);
}

solve_record sip_bnf_solver::solve(program_info prog) {
    std::unique_ptr<solver> sub(make_subsolver(symbols, solver_base_name));

    for(auto& option : options) {
        sub->pass_option(option.first, option.second);
    }

    solve_record rec;
    solve_record lbp_rec;
    int n_llps = prog.llps.size();
    std::vector<solve_record> llp_rec(n_llps);

    enum { LBP,
        NDEF } next_problem
      = LBP;

    double feas_vio_lbp = std::numeric_limits<double>::infinity(); // feasibility violation of lbp


    for(int i = 0; i < max_iter; i++) {
        scoped_push sp_it(sub->solver_name, "it_" + std::to_string(i + 1));

        std::cout << "\nIteration " << i + 1 << ":\n";
        std::cout << "LBD           = " << rec.m_lbd << '\n';
        std::cout << "feas.vio. LBD = " << feas_vio_lbp << "\n\n";

        if(next_problem == LBP) {
            scoped_push sp_lbp(sub->solver_name, "_lbp");
            std::cout << "\nsip: solving lbp\n";

            sub->set_option("abs_tol", abs_tol_lbp);
            sub->set_option("rel_tol", rel_tol_lbp);
            sub->set_option("settings", settings_lbp);
            sub->set_option("max_time", max_time - rec.m_cpu);

            lbp_rec = sub->solve(prog.lbp);

            rec.m_cpu += lbp_rec.m_cpu;

            std::cout << "reporting status\n";
            report(lbp_rec);

            if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                return rec;
            }

            switch(lbp_rec.m_status) {
                case INFEASIBLE:
                    // lbp proves sip to be infeasible
                    rec.m_status = INFEASIBLE;
                    rec.m_lbd = std::numeric_limits<double>::infinity();
                    rec.m_ubd = std::numeric_limits<double>::infinity();
                    return rec;
                case GLOBAL:
                    // computed valid lbd
                    rec.m_lbd = lbp_rec.m_lbd;
                    break;
                default:
                    throw std::invalid_argument("lbp: unhandled solver status: "
                                                + to_string(lbp_rec.m_status));
            }

            bool lbp_feasible = true;
            double feas_vio_temp = -std::numeric_limits<double>::infinity();
            for(int j = 0; j < n_llps; ++j) {
                scoped_push sp_lbp_llp(sub->solver_name, "_llp_" + std::to_string(j + 1));
                std::cout << "sip: solving lbp_llp_" << j + 1 << '\n';
                // note: always minimizing. This leads to
                //  - flipped sign of objective
                //  - flipped lbd and ubd

                symbols.push_scope();
                fix_variables(lbp_rec, symbols);

                sub->set_option("abs_tol", abs_tol_lbp_llp);
                sub->set_option("rel_tol", rel_tol_lbp_llp);
                sub->set_option("settings", settings_lbp_llp);
                sub->set_option("max_time", max_time - rec.m_cpu);

                llp_rec[j] = sub->solve(prog.llps[j]);

                rec.m_cpu += llp_rec[j].m_cpu;
                symbols.pop_scope();

                std::cout << "reporting status\n";
                report(llp_rec[j]);


                if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                    return rec;
                }

                switch(llp_rec[j].m_status) {
                    case INFEASIBLE:
                        // by convention: llp_rec.m_ubd = llp_rec.m_lbd = + infty
                        // => -llp_rec.m_lbd <= feas_tol
                        // => incumbent point in llp[j] sip feasible
                        // => no tolerance update necessary
                        break;
                    case GLOBAL:
                        if(-llp_rec[j].m_lbd <= feas_tol) {
                            // => incumbent point in llp[j] sip feasible
                            // => nothing happens here
                            // => compute llp[j+1]
                        } else {
                            // -llp_rec.m_lbd > feas_tol
                            // incumbent point of lbp might be sip infeasible
                            lbp_feasible = false;
                        }
                        // tolerance update if necessary
                        if(-llp_rec[j].m_lbd > 0 && abs_tol_lbp_llp > -llp_rec[j].m_lbd / red_tol_lbp_llp) {
                            set_tolerance_robust("abs_tol_lbp_llp", -llp_rec[j].m_lbd / red_tol_lbp_llp);
                        }
                        break;
                    default:
                        throw std::invalid_argument("lbp_llp_" + std::to_string(j + 1)
                                                    + ": unhandled solver status: " + to_string(llp_rec[j].m_status));
                }

                // keeping track of max feasibility violation
                if(-llp_rec[j].m_lbd > feas_vio_temp) {
                    feas_vio_temp = -llp_rec[j].m_lbd;
                }
            }

            feas_vio_lbp = feas_vio_temp;

            // check termination
            if(lbp_feasible) {
                // => incumbent point in all llps sip feasible
                std::cout << "\nlbp furnished a point which is within llp feas. tolerance. Terminating.\n";
                std::cout << "Feas. vio.:   " << feas_vio_lbp << " <= feas_tol = " << feas_tol << std::endl;
                rec.m_status = GLOBAL;
                rec.m_lbd = lbp_rec.m_lbd;
                rec.m_ubd = lbp_rec.m_ubd;
                copy_variables(lbp_rec, rec);

                return rec;
            } else {
                // discretize
                for(int j = 0; j < n_llps; ++j) {
                    if(discr_all || -llp_rec[j].m_ubd > 0) {
                        // discretize always or only if llp[j] produced a valid cut
                        discretize(prog.lbp_discs[j], llp_rec[j], symbols);
                        std::cout << "discretized llp_" << j + 1 << ".\n";
                    }
                }
                next_problem = LBP;
            }
        }

        if(next_problem == NDEF) {
            throw std::invalid_argument("procedure not implemented");
        }


#ifdef DIPS_use_mpi
        int _rank;
        MPI_Comm_rank(MPI_COMM_WORLD, &_rank);
        if(_rank == 0) {
#endif
            // write discretization points to output file
            std::string disc_output_path = "./";
            for(auto& i : sub->solver_name) {
                disc_output_path += i;
            }
            std::ofstream disc_output(disc_output_path + "_disc_points.txt");
            std::streambuf* backup = std::cout.rdbuf();
            std::cout.rdbuf(disc_output.rdbuf());
            std::vector<std::vector<discretization>> discs;
            discs.emplace_back(prog.lbp_discs);
            report(symbols, discs);
            std::cout.rdbuf(backup); // reassign cout to its former buffer before that buffer no longer exists
            disc_output.close();     // close file output
#ifdef DIPS_use_mpi
        }
        MPI_Barrier(MPI_COMM_WORLD);
#endif
    }

    rec.m_status = EXCEEDED_MAX_ITERATIONS;
    std::cout << solver_base_name << ": total number of iterations exceeded limit."
              << "Rising max_it might help\n";
    return rec;
}


bool sip_bnf_solver::set_option(const std::string& option, double value) {
    if(option == "feas_tol") {
        if(value >= 0) {
            feas_tol = value;
            return true;
        }
    }
    if(option == "abs_tol_lbp") {
        if(value >= 1e-9) {
            abs_tol_lbp = value;
            return true;
        }
    }
    if(option == "rel_tol_lbp") {
        if(value >= 1e-9) {
            rel_tol_lbp = value;
            return true;
        }
    }
    if(option == "abs_tol_lbp_llp") {
        if(value >= 1e-9) {
            abs_tol_lbp_llp = value;
            return true;
        }
    }
    if(option == "rel_tol_lbp_llp") {
        if(value >= 1e-9) {
            rel_tol_lbp_llp = value;
            return true;
        }
    }
    if(option == "red_tol_lbp_llp") {
        if(value > 1) {
            red_tol_lbp_llp = value;
            return true;
        }
    }
    if(option == "max_iter") {
        if(value >= 1) {
            max_iter = (int)value;
            return true;
        }
    }
    if(option == "max_time") {
        if(value >= 0) {
            max_time = value;
            return true;
        }
    }
    std::cout << "Failed to set option " << option << " = " << value << '\n';
    return false;
}

bool sip_bnf_solver::set_option(const std::string& option, const std::string& value) {
    if(option == "settings_lbp") {
        settings_lbp = value;
        return true;
    }
    if(option == "settings_lbp_llp") {
        settings_lbp_llp = value;
        return true;
    }
    std::cout << "Failed to set option " << option << " = " << value << '\n';
    return false;
}

bool sip_bnf_solver::set_option(const std::string& option, bool value) {
    if(option == "discr_all") {
        discr_all = value;
        return true;
    }
    std::cout << "Failed to set option " << option << " = " << value << '\n';
    return false;
}

void sip_bnf_solver::pass_option(const std::string& option, double value) {
    options.emplace_back(option, value);
}

double sip_bnf_solver::get_option(const std::string& option) const {
    if(option == "feas_tol") {
        return feas_tol;
    }
    throw std::invalid_argument("Option '" + option + "' not supported or implemented.");
}

void sip_bnf_solver::set_solver_base_name(std::string name) {
    solver_base_name = std::move(name);
}

std::string sip_bnf_solver::get_solver_base_name() {
    return solver_base_name;
}

} // namespace dips
