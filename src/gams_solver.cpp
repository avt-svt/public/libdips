#include "gams_solver.hpp"
#if !defined(DIPS_use_gams_parser_force_unflattened)
#include "gams_parser_flattened.hpp"
#endif
#if !defined(DIPS_use_gams_parser_force_flattened)
#include "gams_parser_unflattened.hpp"
#endif

#include "program_printer.hpp"
#include "parameter_cloner.hpp" // for add_variable_to_solve_record
#include "symbol_finder.hpp"    // for find_variables
#include "QuadExpr.hpp"         // for PureQuadExpr, QuadExpr, LinExpr
#include "expression.hpp"       // for expression
#include "program.hpp"          // for program

#include <sstream>
#include <locale>
#include <iostream>
#include <filesystem>
#include <fstream>
#include <cstring>

namespace dips {



solve_record gams_solver::solve(program prog) {
    // prepare record
    solve_record rec;

    // check if constant constraints are infeasible due to rhs value
    for(auto it = prog.m_constraints.begin(); it != prog.m_constraints.end(); ++it) {
        if(is_tree_constant(it->get(), symbols)) {
            if(!evaluate_expression(it->get(), symbols)) {
                std::cout << "warning: detected constant constraint which is infeasible due to rhs value.\n"
                          << "Constraint " << std::distance(prog.m_constraints.begin(), it) + 1 << "/"
                          << prog.m_constraints.size() << " was determined infeasible with the ale evaluator.\n"
                          << "The program was not passed to GAMS because if GAMS detects an \"Equation [to be] infeasible due to rhs value\" ERROR 3 is thrown instead of reporting the problem to be infeasible.\n"
                          << "The constraint reads:\n"
                          << expression_to_string(it->get()) << '\n';
                rec.m_status = INFEASIBLE;
                rec.m_lbd = std::numeric_limits<double>::infinity();
                rec.m_ubd = std::numeric_limits<double>::infinity();
                return rec;
            }
        }
    }

    // find all variables that are used in the program statement
    auto used_variables = find_variables(prog, symbols);

    // add parameter of correct shape to solve_record
    for(const auto& var_name : used_variables) {
        add_variable_to_solve_record(var_name, symbols, rec);
    }

    // prepare gams input
    std::stringstream input;
#if defined(DIPS_use_gams_parser_force_unflattened) && defined(DIPS_use_gams_parser_force_flattened)
    static_assert(false, "both DIPS_use_gams_parser_force_unflattened and DIPS_use_gams_parser_force_unflattened selected");
#elif defined DIPS_use_gams_parser_force_unflattened
    flattening = false;
    gams_parser_unflattened par(prog, symbols, solver_type, problem_class, decimals);
    par.parse_gams(input);
#elif defined DIPS_use_gams_parser_force_flattened
    flattening = true;
    gams_parser_flattened par(prog, symbols, solver_type, problem_class, decimals);
    par.parse_gams(input);
#else
    // force neither unflattened nor flattened
    // first try unflattened. If this fails try flattened
    try {
        flattening = false;
        gams_parser_unflattened par(prog, symbols, solver_type, problem_class, decimals);
        par.parse_gams(input);

    } catch(unsupportedNodeParsingException& e) {
        std::cout << "Encountered an unsupported node while generating the gams input: " << e.what() << '\n';
        std::cout << "Trying to recover by switching to flattened interface instead.\n";
        flattening = true;
        input.str(std::string());
        input.clear();
        gams_parser_flattened par(prog, symbols, solver_type, problem_class, decimals);
        par.parse_gams(input);
    }
#endif

    // create output path
    std::string output_path = "";
    for(int i = 0; i < solver_name.size(); i++) {
        if(i == 0) {
            output_path += ".";
            output_path += std::filesystem::path::preferred_separator;
        }
        output_path += solver_name[i];
    }

    // create option file name
    std::filesystem::path opt_file_location;
    std::filesystem::path opt_file_destination;
    if(opt_file == 1) {
        opt_file_location = std::filesystem::path(solver_type + ".opt");
        opt_file_destination = output_path / std::filesystem::path(solver_type + ".opt");
    } else if(opt_file < 10) {
        opt_file_location = std::filesystem::path(solver_type + ".op" + std::to_string((int)opt_file));
        opt_file_destination = output_path / std::filesystem::path(solver_type + ".op" + std::to_string((int)opt_file));
    } else if(opt_file < 100) {
        opt_file_location = std::filesystem::path(solver_type + ".o" + std::to_string((int)opt_file));
        opt_file_destination = output_path / std::filesystem::path(solver_type + ".o" + std::to_string((int)opt_file));
    } else {
        opt_file_location = std::filesystem::path(solver_type + "." + std::to_string((int)opt_file));
        opt_file_destination = output_path / std::filesystem::path(solver_type + "." + std::to_string((int)opt_file));
    }

    std::locale::global(std::locale::classic());

    try {
        // bug in GAMS 42.2 under unix (reported):
        // initalizing the GAMSWorkspaceInfo without an empty systemDirectory leads to runtime error.
        // workarounds according to GAMS support:
        // 1. use GAMSEnum::DebugLevel::Off
        // 2. add the path of your gams installation directory to the environment variable "LD_LIBRARY_PATH" (not testet)
        // 3. add the gams installation directory in the lines below, e.g.,
        //    gams::GAMSWorkspaceInfo info(output_path, "/opt/gams/gams42.2_linux_x64_64_sfx",gams::GAMSEnum::DebugLevel::KeepFiles)
        gams::GAMSWorkspaceInfo info(output_path, "", gams::GAMSEnum::DebugLevel::Off);
        gams::GAMSWorkspace work(info);
        auto job = work.addJobFromString(input.str(), "problem");
        auto opt = work.addOptions();

        opt.setKeep(gams::GAMSOptions::EKeep::EKeepEnum::KeepProcDir);
        opt.setSolver(problem_class, solver_type);

        // set options
        opt.setOptCR(rel_tol);
        opt.setOptCA(abs_tol);
        opt.setResLim(max_time);
        opt.setThreads(gams_threads);
        //opt.setSysOut(gams::GAMSOptions::ESysOut::ESysOutEnum::IncludeAdditionalSolverOutput);

        // copy (if needed) and set option file
        if(opt_file_location != opt_file_destination) {
            try {
                std::filesystem::copy(opt_file_location, opt_file_destination);
            } catch(const std::exception& e) {
                std::cout << "exception occured:\n"
                          << "  Copying option file failed:\n"
                          << "  " << e.what() << '\n'
                          << "  Please ensure that the settings file is in same folder as the *.exe\n"
                          << "  Continuing with default settings.\n";
            }
        }

        opt.setOptFile(opt_file);
        job.run(opt);

        std::locale::global(std::locale::classic());

        // copy solution from gams into record
        check_gams_solver_status((int)job.outDB().getParameter("ss_gams_internal").findRecord().value());
        copy_gams_model_status((int)job.outDB().getParameter("ms_gams_internal").findRecord().value(), rec);
        if(rec.m_status == INFEASIBLE || rec.m_status == UNBOUNDED) {
            return rec;
        }
        rec.m_cpu = job.outDB().getParameter("cpu_gams_internal").findRecord().value();
        rec.m_lbd = job.outDB().getParameter("lbd_gams_internal").findRecord().value();
        rec.m_ubd = job.outDB().getParameter("ubd_gams_internal").findRecord().value();

        gams_symbol_reader sym_read(job, flattening);
        if(rec.m_status == GLOBAL && rec.m_lbd != rec.m_lbd) {
            rec.m_lbd = rec.m_ubd;
        }
        for(auto& it : rec.m_solution) {
            auto* sym = it.second.get();
            if(has_validshape(sym)) {
                sym_read.dispatch(sym);
            }
        }

    } catch(gams::GAMSExceptionExecution& ex) {
        print_GAMSExceptionExecution(ex);
    } catch(const gams::GAMSException& ex) {
        std::cout << "unknown GAMS exception occured:\n  " << ex.what() << '\n';
    } catch(const std::exception& ex) {
        std::cout << "unknown exception occured:\n  " << ex.what() << '\n';
    }
    return rec;
}

bool gams_solver::set_option(std::string option, double value) {
    if(option == "abs_tol") {
        abs_tol = value;
        return true;
    }
    if(option == "rel_tol") {
        rel_tol = value;
        return true;
    }
    if(option == "max_time") {
        max_time = value;
        return true;
    }
    if(option == "opt_file") {
        opt_file = value;
        return true;
    }
    if(option == "threads") {
        gams_threads = value;
        return true;
    }
    return false;
}

bool gams_solver::set_option(std::string option, std::string value) {
    if(option == "settings") {
        try {
            opt_file = std::stoi(value);
        } catch(std::exception& ex) {
            std::cout << "failed to set option '" << option << "' to '" << value << "':\n  " << ex.what() << '\n';
            return false;
        }
        return true;
    }
    return false;
}

double gams_solver::get_option(std::string option) {
    if(option == "abs_tol") {
        return abs_tol;
    }
    if(option == "rel_tol") {
        return rel_tol;
    }
    if(option == "flattening") {
        return static_cast<double>(flattening);
    }
    throw std::invalid_argument("Option '" + option + "' not supported or implemented.");
}

void gams_solver::print_GAMSExceptionExecution(gams::GAMSExceptionExecution& ex) {
    std::cout << "GAMS execution exception occured:\n";
    switch(ex.rc()) {
        case gams::GAMSEnum::GAMSExitCode::SolverToBeCalled:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::SolverToBeCalled) << ":\n"
                      << "  Solver is to be called, the system should never return this number.\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::CompilationError:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::CompilationError) << ":\n"
                                                                                                         "  There was a compilation error.\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::ExecutionError:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::ExecutionError) << ":\n"
                      << "  There was an execution error.\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::SystemLimitsReached:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::SystemLimitsReached) << ":\n"
                      << "  System limits were reached.\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::FileError:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::FileError) << ":\n"
                      << "  There was a file error.\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::ParameterError:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::ParameterError) << ":\n"
                      << "  There was a parameter error.\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::LicensingError:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::LicensingError) << ":\n"
                      << "  There was a licensing error.\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::GAMSSystemError:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::GAMSSystemError) << ":\n"
                      << "  There was a GAMS system error.\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::GAMSCouldNotBeStarted:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::GAMSCouldNotBeStarted) << ":\n"
                      << "  GAMS could not be started.\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::OutOfMemory:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::OutOfMemory) << ":\n"
                      << "  Out of memory.\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::OutOfDisk:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::OutOfDisk) << ":\n"
                      << "  Out of disk.\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::CouldNotCreateScratchDir:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::CouldNotCreateScratchDir) << ":\n"
                      << "  Could not create process/scratch directory.\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::TooManyScratchDirs:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::TooManyScratchDirs) << ":\n"
                      << "  Too many process/scratch directories.\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::CouldNotDeleteScratchDir:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::CouldNotDeleteScratchDir) << ":\n"
                      << "  Could not delete the process/scratch directory.\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::CouldNotWriteGamsNext:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::CouldNotWriteGamsNext) << ":\n"
                      << "  Could not write the script gamsnext.\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::CouldNotWriteParamFile:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::CouldNotWriteParamFile) << ":\n"
                      << "  Could not write the parameter file.\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::CouldNotReadEnvVar:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::CouldNotReadEnvVar) << ":\n"
                      << "Could not read environment variable.\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::CouldNotSpawnGAMScmex:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::CouldNotSpawnGAMScmex) << ":\n"
                      << "Could not spawn the GAMS language compiler (gamscmex).\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::CurDirNotFound:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::CurDirNotFound) << ":\n"
                      << "Current directory (curdir) does not exist.\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::CurDirNotSet:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::CurDirNotSet) << ":\n"
                      << "  Cannot set current directory (curdir).\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::BlankInSysDir:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::BlankInSysDir) << ":\n"
                      << "  Blank in system directory (UNIX only).\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::BlankInCurDir:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::BlankInCurDir) << ":\n"
                      << "  Blank in current directory (UNIX only).\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::BlankInScrExt:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::BlankInScrExt) << ":\n"
                      << "  Blank in scratch extension (scrext).\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::UnexpectedCmexRC:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::UnexpectedCmexRC) << ":\n"
                      << "  Unexpected cmexRC.\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::ProcDirNotFound:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::ProcDirNotFound) << ":\n"
                      << "  Could not find the process directory (procdir).\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::CmexLibNotFound:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::CmexLibNotFound) << ":\n"
                      << "  CMEX library not be found (experimental).\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::CmexLibEPNotFound:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::CmexLibEPNotFound) << ":\n"
                      << "  Entry point in CMEX library could not be found (experimental).\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::BlankInProcDir:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::BlankInProcDir) << ":\n"
                      << "  Blank in process directory (UNIX only).\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::BlankInScrDir:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::BlankInScrDir) << ":\n"
                      << "  Blank in scratch directory (UNIX only).\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::CannotAddPath:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::CannotAddPath) << ":\n"
                      << "  Cannot add path / unknown UNIX environment / cannot set environment variable.\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::MissingCommandLineParameter:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::MissingCommandLineParameter) << ":\n"
                      << "  Driver error: incorrect command line parameters for gams.\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::CannotInstallInterrupt:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::CannotInstallInterrupt) << ":\n"
                      << "  Driver error: internal error: cannot install interrupt handler.\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::CouldNotGetCurrentDir:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::CouldNotGetCurrentDir) << ":\n"
                      << "  Driver error: problems getting current directory.\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::CmexNotFound:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::CmexNotFound) << ":\n"
                      << "  Driver error: internal error: GAMS compile and execute module not found.\n";
            break;
        case gams::GAMSEnum::GAMSExitCode::OptNotFound:
            std::cout << "  " << gams::GAMSEnum::text(gams::GAMSEnum::GAMSExitCode::OptNotFound) << ":\n"
                      << "  Driver error: internal error: cannot load option handling library .\n";
            break;
        default:
            std::cout << "  Unknown GAMS exit code " << ex.rc() << '\n';
    }
}


bool gams_solver::check_gams_solver_status(int status) {
    switch(status) {
        case gams::GAMSEnum::SolveStat::Normal:
            return true;
        case gams::GAMSEnum::SolveStat::Iteration:
            throw std::invalid_argument("unhandled GAMS solverstatus (" + std::to_string(gams::GAMSEnum::SolveStat::Iteration) + "):\n"
                                        + "  ITERATION INTERRUPT.\n  The solver was interrupted because it used too many iterations. The option iterlim may be used to increase the iteration limit if everything seems normal.");
        case gams::GAMSEnum::SolveStat::Resource:
            throw std::invalid_argument("unhandled GAMS solverstatus (" + std::to_string(gams::GAMSEnum::SolveStat::Resource) + "):\n"
                                        + "  RESOURCE INTERRUPT.\n"
                                        + "  The solver was interrupted because it used too much time. The option reslim may be used to increase the time limit if everything seems normal.");
        case gams::GAMSEnum::SolveStat::Solver:
            throw std::invalid_argument("unhandled GAMS solverstatus (" + std::to_string(gams::GAMSEnum::SolveStat::Solver) + "):\n"
                                        + "  TERMINATED BY SOLVER.\n  The solver encountered some difficulty and was unable to continue. More details will appear following the message.");
        case gams::GAMSEnum::SolveStat::EvalError:
            throw std::invalid_argument("unhandled GAMS solverstatus (" + std::to_string(gams::GAMSEnum::SolveStat::EvalError) + "):\n"
                                        + "  EVALUATION INTERRUPT.\n"
                                        + "  Too many evaluations of nonlinear terms at undefined values. We recommend to use variable bounds to prevent forbidden operations, such as division by zero. The rows in which the errors occur are listed just before the solution.");
        case gams::GAMSEnum::SolveStat::Capability:
            throw std::invalid_argument("unhandled GAMS solverstatus (" + std::to_string(gams::GAMSEnum::SolveStat::Capability) + "):\n"
                                        + "  CAPABILITY PROBLEMS.\n"
                                        + "  The solver does not have the capability required by the model. For example, some solvers do not support certain types of discrete variables or support a more limited set of functions than other solvers.");
        case gams::GAMSEnum::SolveStat::License:
            throw std::invalid_argument("unhandled GAMS solverstatus (" + std::to_string(gams::GAMSEnum::SolveStat::License) + "):\n"
                                        + "  LICENSING PROBLEMS.\n"
                                        + "  The solver cannot find the appropriate license key needed to use a specific subsolver.");
        case gams::GAMSEnum::SolveStat::User:
            throw std::invalid_argument("unhandled GAMS solverstatus (" + std::to_string(gams::GAMSEnum::SolveStat::User) + "):\n"
                                        + "  USER INTERRUPT.\n"
                                        + "  The user has sent a message to interrupt the solver via the interrupt button in the IDE or sending a Control+C from a command line.");
        case gams::GAMSEnum::SolveStat::SetupErr:
            throw std::invalid_argument("unhandled GAMS solverstatus (" + std::to_string(gams::GAMSEnum::SolveStat::SetupErr) + "):\n"
                                        + "  ERROR SETUP FAILURE.\n"
                                        + "  The solver encountered a fatal failure during problem set-up time.");
        case gams::GAMSEnum::SolveStat::SolverErr:
            throw std::invalid_argument("unhandled GAMS solverstatus (" + std::to_string(gams::GAMSEnum::SolveStat::SolverErr) + "):\n"
                                        + "  ERROR SOLVER FAILURE.\n"
                                        + "  The solver encountered a fatal error.");
        case gams::GAMSEnum::SolveStat::InternalErr:
            throw std::invalid_argument("unhandled GAMS solverstatus (" + std::to_string(gams::GAMSEnum::SolveStat::InternalErr) + "):\n"
                                        + "  ERROR INTERNAL SOLVER FAILURE.\n"
                                        + "  The solver encountered an internal fatal error.");
        case gams::GAMSEnum::SolveStat::Skipped:
            throw std::invalid_argument("unhandled GAMS solverstatus (" + std::to_string(gams::GAMSEnum::SolveStat::Skipped) + "):\n"
                                        + "  SOLVE PROCESSING SKIPPED.\n"
                                        + "  The entire solve step has been skipped. This happens if execution errors were encountered and the GAMS parameter ExecErr has been set to a nonzero value or the property MaxExecError has a nonzero value.");
        case gams::GAMSEnum::SolveStat::SystemErr:
            throw std::invalid_argument("unhandled GAMS solverstatus (" + std::to_string(gams::GAMSEnum::SolveStat::SystemErr) + "):\n"
                                        + "  ERROR SYSTEM FAILURE.\n"
                                        + "  This indicates a completely unknown or unexpected error condition.");
        default:
            throw std::invalid_argument("unhandled GAMS solverstatus (" + std::to_string(status) + ")\n"
                                        + "  UNKNOWN SOLVERSTATUS.\n"
                                        + "  Please rever to GAMS manual.");
    }
    return false;
}


void gams_solver::copy_gams_model_status(int model_status, solve_record& rec) {
    switch(model_status) {
        case gams::GAMSEnum::ModelStat::Unbounded:
            rec.m_status = UNBOUNDED;
            rec.m_lbd = -std::numeric_limits<double>::infinity();
            rec.m_ubd = -std::numeric_limits<double>::infinity();
            break;
        case gams::GAMSEnum::ModelStat::OptimalGlobal:
            rec.m_status = GLOBAL;
            break;
        case gams::GAMSEnum::ModelStat::OptimalLocal:
            //rec.m_status = LOCAL;
            std::cout << "WARNING: GAMS reported local solution; Solution will be handled as global solution!\n";
            rec.m_status = GLOBAL;
            break;
        case gams::GAMSEnum::ModelStat::Feasible:
        case gams::GAMSEnum::ModelStat::IntegerSolution:
            //rec.m_status = FEASIBLE;
            std::cout << "WARNING: GAMS reported local solution; Solution will be handled as global solution!\n";
            rec.m_status = GLOBAL;
            rec.m_status = GLOBAL;
            break;
        case gams::GAMSEnum::ModelStat::InfeasibleGlobal:
        case gams::GAMSEnum::ModelStat::IntegerInfeasible:
        case gams::GAMSEnum::ModelStat::InfeasibleNoSolution:
            rec.m_status = INFEASIBLE;
            rec.m_lbd = std::numeric_limits<double>::infinity();
            rec.m_ubd = std::numeric_limits<double>::infinity();
            break;
        case gams::GAMSEnum::ModelStat::InfeasibleLocal:
            throw std::invalid_argument("unhandled GAMS modelstatus (" + std::to_string(gams::GAMSEnum::ModelStat::InfeasibleLocal) + "):\n"
                                        + "  LOCALLY INFEASIBLE.\n"
                                        + "  No feasible point could be found for the NLP problem from the given starting point. It does not necessarily mean that no feasible point exists.");
        case gams::GAMSEnum::ModelStat::InfeasibleIntermed:
            throw std::invalid_argument("unhandled GAMS modelstatus (" + std::to_string(gams::GAMSEnum::ModelStat::InfeasibleIntermed) + "):\n"
                                        + "  INTERMEDIATE INFEASIBLE.\n"
                                        + "  The current solution is not feasible, but the solver stopped, either because of a limit (for example, iteration or resource) or because of some sort of difficulty. The solver status will give more information.");
        case gams::GAMSEnum::ModelStat::NonIntegerIntermed:
            throw std::invalid_argument("unhandled GAMS modelstatus (" + std::to_string(gams::GAMSEnum::ModelStat::NonIntegerIntermed) + "):\n"
                                        + "  INTERMEDIATE NON-INTEGER.\n"
                                        + "  An incomplete solution to a problem with discrete variables. A feasible solution has not yet been found. See section Model Termination Conditions for MIPs for more information.");
        case gams::GAMSEnum::ModelStat::LicenseError:
            throw std::invalid_argument("unhandled GAMS modelstatus (" + std::to_string(gams::GAMSEnum::ModelStat::LicenseError) + "):\n"
                                        + "  LIC PROBLEM - NO SOLUTION.\n"
                                        + "  The solver cannot find the appropriate license key needed to use a specific subsolver.");
        case gams::GAMSEnum::ModelStat::ErrorUnknown:
            throw std::invalid_argument("unhandled GAMS modelstatus (" + std::to_string(gams::GAMSEnum::ModelStat::ErrorUnknown) + "):\n"
                                        + "  ERROR UNKNOWN.\n"
                                        + "  After a solver error the model status is unknown.");
        case gams::GAMSEnum::ModelStat::ErrorNoSolution:
            throw std::invalid_argument("unhandled GAMS modelstatus (" + std::to_string(gams::GAMSEnum::ModelStat::ErrorNoSolution) + "):\n"
                                        + "  ERROR NO SOLUTION.\n"
                                        + "  An error occurred and no solution has been returned. No solution will be returned to GAMS because of errors in the solution process.");
        case gams::GAMSEnum::ModelStat::NoSolutionReturned:
            throw std::invalid_argument("unhandled GAMS modelstatus (" + std::to_string(gams::GAMSEnum::ModelStat::NoSolutionReturned) + "):\n"
                                        + "  NO SOLUTION RETURNED.\n"
                                        + "  A solution is not expected for this solve. For example, the CONVERT solver only reformats the model but does not give a solution.");
        case gams::GAMSEnum::ModelStat::SolvedUnique:
            throw std::invalid_argument("unhandled GAMS modelstatus (" + std::to_string(gams::GAMSEnum::ModelStat::SolvedUnique) + "):\n"
                                        + "  SOLVED UNIQUE.\n"
                                        + "  Indicates the solution returned is unique, i.e. no other solution exists. Used for CNS models. Examples where this status could be returned include non-singular linear models, triangular models with constant non-zero elements on the diagonal, and triangular models where the functions are monotone in the variable on the diagonal.");
        case gams::GAMSEnum::ModelStat::Solved:
            throw std::invalid_argument("unhandled GAMS modelstatus (" + std::to_string(gams::GAMSEnum::ModelStat::Solved) + "):\n"
                                        + "  SOLVED.\n"
                                        + "  Indicates the model has been solved: used for CNS models. The solution might or might not be unique. If the solver uses status 17 - SOLVED SINGULAR wherever possible then this status implies that the Jacobian is non-singular, i.e. that the solution is at least locally unique.");
        case gams::GAMSEnum::ModelStat::SolvedSingular:
            throw std::invalid_argument("unhandled GAMS modelstatus (" + std::to_string(gams::GAMSEnum::ModelStat::SolvedSingular) + "):\n"
                                        + "  SOLVED SINGULAR.\n"
                                        + "  Indicates the CNS model has been solved, but the Jacobian is singular at the solution. This can indicate that other solutions exist, either along a line (for linear models) or a curve (for nonlinear models) including the solution returned.");
        case gams::GAMSEnum::ModelStat::UnboundedNoSolution:
            throw std::invalid_argument("unhandled GAMS modelstatus (" + std::to_string(gams::GAMSEnum::ModelStat::UnboundedNoSolution) + "):\n"
                                        + "  UNBOUNDED - NO SOLUTION.\n"
                                        + "  The model is unbounded and no solution can be provided.");
        default:
            throw std::invalid_argument("unhandled GAMS modelstatus (" + std::to_string(model_status) + "):\n"
                                        + "  UNKNOWN MODELSTATUS.\n"
                                        + "  Please rever to GAMS manual.");
    }
}




} // namespace dips
