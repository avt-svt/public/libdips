#include "gams_parser_flattened.hpp"

namespace dips {

void gams_parser_flattened::parse_gams(std::stringstream& input) {

    // define variables and parameters
    symbol_stringer_flattened sym_str(input, symbols);
    auto defined_symbols = find_variables(prog, symbols);
    for(auto it = defined_symbols.begin(); it != defined_symbols.end(); ++it) {
        base_symbol* sym = symbols.resolve(*it);
        if(sym) {
            sym_str.dispatch(sym);
        }
    }
    // if no parameters should be used, comment the following code and change the
    // corresponding code for the  parameter_node<real<IDim>> operator
    //auto fixed_symbols = find_parameters(prog, symbols);
    //for (auto it = fixed_symbols.begin(); it != fixed_symbols.end(); ++it) {
    //    auto sym = symbols.resolve(*it);
    //    if (sym) {
    //        sym_str.dispatch(sym);
    //    }
    //}

    // write equations
    expression_stringer_flattened expr_str(symbols);
    if(max_min_abs_ref) {
        if(!expr_str.get_nested_min_max()) {
            std::cout << "max_min_abs_ref = true -> setting nested_min_max = true\n";
            expr_str.set_nested_min_max(true);
        }
#ifdef DIPS_use_gams_macro_min_max_reformulation
        input << "$macro max(a,b) (((a)+(b))/2 + abs((a)-(b))/2)\n";
        input << "$macro min(a,b) (((a)+(b))/2 - abs((a)-(b))/2)\n";
#endif
    }
    input << "variable obj_gams_internal; equation eqobj; eqobj.. obj_gams_internal =e= " << expr_str.dispatch(*prog.m_objective.begin()) << ";\n";
    int k = 1;
    for(auto it = prog.m_constraints.begin(); it != prog.m_constraints.end(); ++it) {
        auto vec = expr_str.dispatch(*it);
        for(auto jt = vec.begin(); jt != vec.end(); ++jt) {
            input << "equation eqcon" << k << "; eqcon" << k << ".. " << *jt << ";\n";
            ++k;
        }
    }

    parse_gams_solve_statement(input);
}

} // namespace dips
