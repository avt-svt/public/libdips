#include "quad_expr_evaluator.hpp"

#include <stddef.h>  // for size_t
#include <iterator>  // for advance
#include <list>      // for list, operator!=
#include <string>    // for operator+, allocator, char_traits, to_string, basic_string, string
#include <utility>   // for move
#include <variant>   // for variant
#include <stdexcept> // for invalid_argument

#include "QuadExpr.hpp"
#include "util/evaluator.hpp"            // for evaluate_expression, util
#include "util/visitor_utils.hpp"        // for evaluate_children_iterated, call_visitor, evaluate_children, evaluate_children_tuple, evaluate_child, evalu...
#include "util/expression_to_string.hpp" // for expression_to_string, symbol_to_string
#include "util/expression_utils.hpp"     // for get_subtree_value
#include "scalar_evaluator.hpp"          // for ScalarInvalidEntryNodeError, scalar_evaluator, ScalarVariableIdManager
#include "node.hpp"                      // for value_node, addition_node, conjunction_node, disjunction_node, equal_node, exponentiation_node, greater_equ...
#include "symbol.hpp"                    // for expression_symbol, function_symbol, parameter_symbol, variable_symbol

namespace dips {

using namespace ale;
using namespace ale::util;

QuadConstraint::QuadConstraint(QuadExpr expr, QuadConstraintType type) :
    expr(std::move(expr)), type(type) { }

const QuadExpr& QuadConstraint::getExpr() const { return expr; }
QuadConstraintType QuadConstraint::getType() const { return type; }

QuadConstraint QuadConstraint::negate() const {
    switch(type) {
        case QuadConstraintType::EQUAL:
            return { expr, QuadConstraintType::NOT_EQUAL };
        case QuadConstraintType::NOT_EQUAL:
            return { expr, QuadConstraintType::EQUAL };
        case QuadConstraintType::LESS:
            return { expr, QuadConstraintType::GREATER_EQUAL };
        case QuadConstraintType::GREATER:
            return { expr, QuadConstraintType::LESS_EQUAL };
        case QuadConstraintType::LESS_EQUAL:
            return { expr, QuadConstraintType::GREATER };
        case QuadConstraintType::GREATER_EQUAL:
            return { expr, QuadConstraintType::LESS };
        default:
            throw std::invalid_argument("unexpected QuadConstraintType");
    }
}

class quad_expr_evaluator : private scalar_evaluator {
public:
    using scalar_evaluator::scalar_evaluator;

    /**
         * Visit for any node assuming all indexes are already specified
         * 
         * Say we have a symbol of type
         * set{index}[2, 3] x := ...
         * then the parameter_node "x" can only be evaluated if indexes is of size 2.
         * This would be the case for the expression "x[1, 2]"
         */
    template <typename TType>
    owning_ref<tensor_type<atom_type<TType>, 0>> operator()(value_node<TType>* node) {
        constexpr auto IDim = get_node_dimension<TType>;

        auto res = get_subtree_value(node, symbols);
        if(!res) {
            throw ExpressionNotQuadraticError("could not evaluate expression: " + expression_to_string(node));
        }

        if constexpr(IDim > 0) {
            assert_valid_index_for_shape(res->shape());

            auto out = *res[indexes.data()];
            indexes = {};
            return out;
        } else {
            assert_empty_indexes();
            return *res;
        }
    }

    /**
         * Default visit for real value_nodes
         * This visit is overridden by any of the following implementations
         * It tries to evaluate constant real expressions in case no other implementation is given
         */
    template <unsigned IDim>
    QuadExpr operator()(value_node<real<IDim>>* node) {
        auto res = get_subtree_value(node, symbols);
        if(!res) {
            throw ExpressionNotQuadraticError("could not evaluate expression: " + expression_to_string(node));
        }

        if constexpr(IDim > 0) {
            assert_valid_index_for_shape(res->shape());

            QuadExpr out((*res)[indexes.data()]);
            indexes = {};
            return out;
        } else {
            assert_empty_indexes();
            return QuadExpr(*res);
        }
    }

    /**
         * Default visit for boolean valued nodes
         * It tries to evaluate the expression using the evaluator and throws if that fails
         * This visit is overridden by any of the following implementations
         */
    template <unsigned IDim>
    QuadConstraintList operator()(value_node<ale::boolean<IDim>>* node) {
        auto res = get_subtree_value(node, symbols);
        if(!res) {
            throw ExpressionNotQuadraticError("could not evaluate expression: " + expression_to_string(node));
        }

        bool result = true;
        if constexpr(IDim > 0) {
            assert_valid_index_for_shape(res->shape());

            bool result = *res[indexes.data()];
            indexes = {};
        } else {
            assert_empty_indexes();
            result = *res;
        }

        if(result) {
            return { { QuadExpr(0), QuadConstraintType::EQUAL } }; // 0=0
        }
        return { { QuadExpr(1), QuadConstraintType::EQUAL } }; // 1=0
    }

    //
    // terminal visits
    //

    template <unsigned IDim>
    QuadExpr operator()(constant_node<real<IDim>>* node) {
        if constexpr(IDim == 0) {
            assert_empty_indexes();
            return QuadExpr(node->value);
        } else {
            assert_valid_index_for_shape(node->value.shape());

            QuadExpr out(node->value[indexes.data()]);
            indexes = {};
            return out;
        }
    }

    // parameter_node related visits
    template <unsigned IDim>
    QuadExpr operator()(parameter_node<real<IDim>>* node) {
        auto* sym = symbols.resolve<real<IDim>>(node->name);
        if(sym == nullptr) {
            if(!symbols.resolve(node->name)) {
                throw std::invalid_argument("symbol " + node->name + " not found");
            }
            throw std::invalid_argument("symbol " + node->name + " has unexpected type. Expected a real valued parameter of dimension " + std::to_string(IDim) + " but got " + symbol_to_string(sym));
        }
        return call_visitor(*this, sym);
    }

    // symbol visits
    template <unsigned IDim>
    QuadExpr operator()(parameter_symbol<real<IDim>>* sym) {
        if constexpr(IDim == 0) {
            assert_empty_indexes();
            return QuadExpr(sym->m_value);
        } else {
            assert_valid_index_for_shape(sym->m_value.shape());

            QuadExpr out(sym->m_value[indexes.data()]);
            indexes = {};
            return out;
        }
    }

    template <unsigned IDim>
    QuadExpr operator()(variable_symbol<real<IDim>>* sym) {
        LinExpr expr;

        if constexpr(IDim == 0) {
            assert_empty_indexes();
            expr.set_value(id_manager.get_id(sym->m_name), 1.0);
        } else {
            assert_valid_index_for_shape(sym->shape());

            expr.set_value(id_manager.get_id(sym->m_name, sym->shape(), indexes), 1.0);
            indexes = {};
        }

        return QuadExpr(expr);
    }

    template <unsigned IDim>
    QuadExpr operator()(expression_symbol<real<IDim>>* /*sym*/) {
        throw std::invalid_argument("visit of expression_symbol should not occur");
    }

    template <unsigned IDim>
    QuadExpr operator()(function_symbol<real<IDim>>* /*sym*/) {
        throw std::invalid_argument("visit of function_symbol should not occur");
    }

    //
    // non-terminal real node visits
    //

    QuadExpr operator()(minus_node* node) {
        return -evaluate_child(*this, node);
    }

    QuadExpr operator()(addition_node* node) {
        auto values = evaluate_children(*this, node);
        QuadExpr sum(0.0);
        for(auto& x : values) {
            sum += x;
        }
        return sum;
    }

    QuadExpr operator()(multiplication_node* node) {
        auto values = evaluate_children(*this, node);
        QuadExpr prod(1.0);
        for(const auto& x : values) {
            try {
                prod *= x;
            } catch(const OperationResultNotQuadratic& e) {
                std::string msg(e.what());
                throw ExpressionNotQuadraticError(msg + " in: " + expression_to_string(node));
            }
        }
        return prod;
    }

    QuadExpr operator()(inverse_node* node) {
        auto child_value = evaluate_child(*this, node);
        try {
            return 1.0 / child_value;
        } catch(const OperationResultNotQuadratic& e) {
            std::string msg(e.what());
            throw ExpressionNotQuadraticError(msg + " in: " + expression_to_string(node));
        }
    }

    QuadExpr operator()(exponentiation_node* node) {
        auto values = evaluate_children(*this, node);
        QuadExpr result = values.at(0);
        for(size_t i = 1; i < values.size(); ++i) {
            try {
                result.pow(values.at(i));
            } catch(const OperationResultNotQuadratic& e) {
                std::string msg(e.what());
                throw ExpressionNotQuadraticError(msg + " in: " + expression_to_string(node));
            }
        }
        return result;
    }

    template <typename TType>
    QuadExpr operator()(sum_node<TType>* node) {
        auto values = evaluate_children_iterated(*this, node, symbols);

        QuadExpr result(0.0);
        for(const auto& x : values) {
            result += x;
        }

        return result;
    }

    template <typename TType>
    QuadExpr operator()(product_node<TType>* node) {
        auto values = evaluate_children_iterated(*this, node, symbols);

        QuadExpr prod(1.0);
        for(const auto& x : values) {
            try {
                prod *= x;
            } catch(const OperationResultNotQuadratic& e) {
                std::string msg(e.what());
                throw ExpressionNotQuadraticError(msg + " in: " + expression_to_string(node));
            }
        }

        return prod;
    }

    //
    // non-terminal tensor node
    //

    template <unsigned IDim>
    QuadExpr operator()(function_node<real<IDim>>* node) {
        return evaluate_function(*this, node, symbols);
    }

    template <unsigned IDim>
    QuadExpr operator()(tensor_node<real<IDim>>* node) {
        // get first index
        size_t current_index = indexes.front();
        indexes.erase(indexes.begin());

        if(node->children.size() <= current_index) {
            throw std::invalid_argument("index " + std::to_string(current_index) + " is out of bounds for tensor " + expression_to_string(node));
        }

        // get child at current_index
        auto child_ptr = node->children.begin();
        std::advance(child_ptr, current_index);

        return call_visitor(*this, *child_ptr);
    }

    template <unsigned IDim>
    QuadExpr operator()(vector_node<real<IDim>>* /*node*/) {
        throw std::invalid_argument("visit of vector_node should not occur");
    }

    template <unsigned IDim>
    QuadExpr operator()(entry_node<real<IDim>>* node) {
        // store indexes in reversed order before evaluating tensor child
        auto index = util::evaluate_expression(node->template get_child<1>(), symbols);
        indexes.insert(indexes.begin(), index - 1);

        try {
            return call_visitor(*this, node->template get_child<0>());
        } catch(const ScalarInvalidEntryNodeError& e) {
            if(e.nesting_level > 0) {
                throw ScalarInvalidEntryNodeError(e.nesting_level - 1);
            }
            throw std::invalid_argument("index " + std::to_string(index) + " is out of bounds for tensor " + expression_to_string(node->template get_child<0>()));
        }
    }

    //
    // non-terminal boolean node visits
    //

    QuadConstraintList operator()(equal_node<real<0>>* node) {
        auto [lhs, rhs] = evaluate_children_tuple(*this, node);
        return { { lhs - rhs, QuadConstraintType::EQUAL } };
    }

    QuadConstraintList operator()(less_node<real<0>>* node) {
        auto [lhs, rhs] = evaluate_children_tuple(*this, node);
        return { { lhs - rhs, QuadConstraintType::LESS } };
    }

    QuadConstraintList operator()(greater_node<real<0>>* node) {
        auto [lhs, rhs] = evaluate_children_tuple(*this, node);
        return { { lhs - rhs, QuadConstraintType::GREATER } };
    }

    QuadConstraintList operator()(less_equal_node<real<0>>* node) {
        auto [lhs, rhs] = evaluate_children_tuple(*this, node);
        return { { lhs - rhs, QuadConstraintType::LESS_EQUAL } };
    }

    QuadConstraintList operator()(greater_equal_node<real<0>>* node) {
        auto [lhs, rhs] = evaluate_children_tuple(*this, node);
        return { { lhs - rhs, QuadConstraintType::GREATER_EQUAL } };
    }

    template <typename TType>
    QuadConstraintList operator()(forall_node<TType>* node) {

        QuadConstraintList constr;
        try {
            auto values = evaluate_children_iterated(*this, node, symbols);
            for(const auto& x : values) {
                concat(constr, x);
            }
        } catch(const std::invalid_argument& e) {
            std::string msg(e.what());
            throw std::invalid_argument(msg + " in: " + expression_to_string(node));
        }

        return constr;
    }

    QuadConstraintList operator()(disjunction_node* node) {
        QuadConstraintList constr;
        auto values = evaluate_children(*this, node);
        for(const auto& v : values) {
            concat(constr, negate(v));
        }
        return negate(constr);
    }

    QuadConstraintList operator()(conjunction_node* node) {
        QuadConstraintList constr;
        auto values = evaluate_children(*this, node);
        for(const auto& v : values) {
            concat(constr, v);
        }
        return constr;
    }

    QuadConstraintList operator()(negation_node* node) {
        return negate(evaluate_child(*this, node));
    }

private:
    static void concat(QuadConstraintList& list, const QuadConstraintList& other) {
        list.insert(list.end(), other.begin(), other.end());
    }

    static QuadConstraintList negate(const QuadConstraintList& list) {
        QuadConstraintList negatedConstraints;
        for(const auto& x : list) {
            negatedConstraints.push_back(x.negate());
        }
        return negatedConstraints;
    }
};

QuadExpr evaluate_quadratic(expression<real<0>>& expr, symbol_table& symbols, const ScalarVariableIdManager& id_manager) {
    quad_expr_evaluator eval(symbols, id_manager);
    return call_visitor(eval, expr);
}

QuadConstraintList evaluate_quadratic_constraint(expression<ale::boolean<0>>& expr, symbol_table& symbols, const ScalarVariableIdManager& id_manager) {
    quad_expr_evaluator eval(symbols, id_manager);
    return call_visitor(eval, expr);
}

} // namespace dips
