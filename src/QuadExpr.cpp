#include "QuadExpr.hpp"

#include <algorithm>   // for max, find, lower_bound, min
#include <cmath>       // for abs,pow
#include <utility>     // for pair, make_pair, move
#include <iterator>    // for end, reverse_iterator
#include <memory>      // for allocator_traits<>::value_type
#include <type_traits> // for __strip_reference_wrapper<>::__type
#include <stdexcept>   // for invalid_argument
#include <sstream>     // for stringstream

namespace dips {

LinExpr::LinExpr(double scalar) :
    _constant(scalar) { }

bool LinExpr::is_scalar() const { return _values.empty() && _ids.empty(); }

double& LinExpr::constant() { return _constant; }

double LinExpr::constant() const { return _constant; }

double LinExpr::get_value(unsigned index) const {
    auto it = std::find(_ids.begin(), _ids.end(), index);
    auto it_value = _values.begin() + (it - _ids.begin());
    if(it != _ids.end()) {
        return *it_value;
    }
    return 0;
}




void LinExpr::set_value(unsigned index, double value) {
    // insert at the end if we know this is were the value belongs
    // mostly this firt case is used
    if(_ids.empty() || index > _ids.back()) {

        _ids.push_back(index);
        _values.push_back(value);
    } else {
        // get iterator to first item which is >= index
        auto it = std::lower_bound(_ids.begin(), _ids.end(), index);
        // calculate iterator to same position in value vector
        auto it_val = _values.begin() + (it - _ids.begin());
        if(*it == index) {
            *it_val = value;
        } else {
            _ids.insert(it, index);
            _values.insert(it_val, value);
        }
    }
}

const std::vector<unsigned>& LinExpr::get_non_zero_ids() const { return _ids; }

const std::vector<double>& LinExpr::get_non_zero_values() const { return _values; }

std::string LinExpr::print() const {
    std::stringstream ss;

    for(unsigned i = 0; i < _ids.size(); i++) {
        ss << "(" << _ids[i] << "):" << _values[i] << ",";
    }
    ss << "(const):" << _constant << std::endl;
    return ss.str();
}

void LinExpr::for_all_coeffs(const std::function<void(unsigned, double, double)>& f, const LinExpr& a,
  const LinExpr& b) {
    int i1 = 0;
    int i2 = 0;
    // go from small id to large id through both id vectors
    // which are both sorted in ascending order
    // replace missing z[i] with zero if one of the vectors does not contain it
    while(i1 != a._ids.size() || i2 != b._ids.size()) {
        if(i1 == a._ids.size()) { // no more in a
            f(b._ids[i2], 0, b._values[i2]);
            i2++;
        } else if(i2 == b._ids.size()) { // no more in b
            f(a._ids[i1], a._values[i1], 0);
            i1++;
        } else if((a._ids[i1] < b._ids[i2])) { // a.ids[i1] has the next ID, b does not have a coefficient for that id.
            f(a._ids[i1], a._values[i1], 0);
            i1++;
        } else if((a._ids[i1] > b._ids[i2])) { // b.ids[i2] has the next ID, a does not have a coefficient for that id
            f(b._ids[i2], 0, b._values[i2]);
            i2++;
        } else { //(a._ids[i1]==b._ids[i2]) // current ID is contained in both id vectors
            f(a._ids[i1], a._values[i1], b._values[i2]);
            i1++;
            i2++;
        }
    }
}


void LinExpr::for_all_coeffs_plus(LinExpr& out, const LinExpr& a,
  const LinExpr& b) {
    int i1 = 0;
    int i2 = 0;
    // go from small id to large id through both id vectors
    // which are both sorted in ascending order
    // replace missing z[i] with zero if one of the vectors does not contain it
    while(i1 != a._ids.size() || i2 != b._ids.size()) {
        if(i1 == a._ids.size()) { // no more in a
            out.set_value(b._ids[i2], 0 + b._values[i2]);
            i2++;
        } else if(i2 == b._ids.size()) { // no more in b

            out.set_value(a._ids[i1], a._values[i1] + 0);
            i1++;
        } else if((a._ids[i1] < b._ids[i2])) { // a.ids[i1] has the next ID, b does not have a coefficient for that id.
            out.set_value(a._ids[i1], a._values[i1] + 0);
            i1++;
        } else if((a._ids[i1] > b._ids[i2])) { // b.ids[i2] has the next ID, a does not have a coefficient for that id
            out.set_value(b._ids[i2], 0 + b._values[i2]);
            i2++;
        } else { //(a._ids[i1]==b._ids[i2]) // current ID is contained in both id vectors
            out.set_value(a._ids[i1], a._values[i1] + b._values[i2]);
            i1++;
            i2++;
        }
    }
}

void LinExpr::for_all_coeffs_minus(LinExpr& out, const LinExpr& a,
  const LinExpr& b) {
    int i1 = 0;
    int i2 = 0;
    // go from small id to large id through both id vectors
    // which are both sorted in ascending order
    // replace missing z[i] with zero if one of the vectors does not contain it
    while(i1 != a._ids.size() || i2 != b._ids.size()) {
        if(i1 == a._ids.size()) { // no more in a
            out.set_value(b._ids[i2], 0 - b._values[i2]);
            i2++;
        } else if(i2 == b._ids.size()) { // no more in b

            out.set_value(a._ids[i1], a._values[i1] - 0);
            i1++;
        } else if((a._ids[i1] < b._ids[i2])) { // a.ids[i1] has the next ID, b does not have a coefficient for that id.
            out.set_value(a._ids[i1], a._values[i1] - 0);
            i1++;
        } else if((a._ids[i1] > b._ids[i2])) { // b.ids[i2] has the next ID, a does not have a coefficient for that id
            out.set_value(b._ids[i2], 0 - b._values[i2]);
            i2++;
        } else { //(a._ids[i1]==b._ids[i2]) // current ID is contained in both id vectors
            out.set_value(a._ids[i1], a._values[i1] - b._values[i2]);
            i1++;
            i2++;
        }
    }
}


void LinExpr::clear() {
    _values.clear();
    _ids.clear();
    _constant = 0.0;
}

std::ostream& operator<<(std::ostream& os, const LinExpr& dt) {
    os << dt.print();
    return os;
}

LinExpr calculate_binary_operation_element_wise(const LinExpr& LHS, const LinExpr& RHS,
  const std::function<double(double, double)>& op) {
    LinExpr Out(op(LHS.constant(), RHS.constant()));
    auto assingElementwiseOpToOut = [&Out, op](int id, double a, double b) { Out.set_value(id, op(a, b)); };
    LinExpr::for_all_coeffs(assingElementwiseOpToOut, LHS, RHS);
    return Out;
}

LinExpr operator+(const LinExpr& LHS, const LinExpr& RHS) {
    LinExpr Out(LHS.constant() + RHS.constant());
    LinExpr::for_all_coeffs_plus(Out, LHS, RHS);
    return Out;
}

LinExpr operator-(const LinExpr& LHS, const LinExpr& RHS) {
    LinExpr Out(LHS.constant() - RHS.constant());
    LinExpr::for_all_coeffs_minus(Out, LHS, RHS);
    return Out;
}

LinExpr operator*(const LinExpr& LHS, const LinExpr& RHS) {
    return calculate_binary_operation_element_wise(LHS, RHS, std::multiplies<double>());
}

LinExpr operator/(const LinExpr& LHS, const LinExpr& RHS) {
    return calculate_binary_operation_element_wise(LHS, RHS, std::divides<double>());
}

LinExpr scale(const LinExpr& LHS, double scale) {
    LinExpr result(LHS);
    result.constant() *= scale;

    for(double& val : result._values) {
        val *= scale;
    }

    return result;
}

LinExpr operator*(const LinExpr& LHS, double scalar) { return scale(LHS, scalar); }

LinExpr operator*(double scalar, const LinExpr& RHS) { return scale(RHS, scalar); }

LinExpr operator/(const LinExpr& LHS, double divisor) { return scale(LHS, 1 / divisor); }



void PureQuadExpr::add_term(unsigned int index1, unsigned int index2, double value) {
    this->_ids.emplace_back(index1, index2);
    this->_values.emplace_back(value);
}

void PureQuadExpr::clear() {
    this->_ids.clear();
    this->_values.clear();
}

std::string PureQuadExpr::print() const {
    std::stringstream ss;
    for(int i = 0; i < _ids.size(); i++) {
        ss << "(" << _ids[i].first << "," << _ids[i].second << "): " << _values[i] << ",";
    }
    ss << std::endl;
    return ss.str();
}

const std::vector<std::pair<unsigned, unsigned>>& PureQuadExpr::get_non_zero_ids() const {
    return _ids;
}

const std::vector<double>& PureQuadExpr::get_non_zero_values() const {
    return _values;
}


std::ostream& operator<<(std::ostream& os, const PureQuadExpr& dt) {
    os << dt.print();
    return os;
}

PureQuadExpr PureQuadExpr::operator+=(const PureQuadExpr& RHS) {
    this->_values.insert(this->_values.end(), RHS._values.begin(), RHS._values.end());
    this->_ids.insert(this->_ids.end(), RHS._ids.begin(), RHS._ids.end());
    return *this;
}

PureQuadExpr PureQuadExpr::operator-=(const PureQuadExpr& RHS) {
    this->_ids.insert(this->_ids.end(), RHS._ids.begin(), RHS._ids.end());
    this->_values.reserve(this->_ids.size());
    for(double _value : RHS._values) {
        this->_values.push_back(-_value);
    }
    return *this;
}

PureQuadExpr operator+(PureQuadExpr LHS, const PureQuadExpr& RHS) {
    LHS += RHS;
    return LHS;
}


PureQuadExpr operator-(PureQuadExpr LHS, const PureQuadExpr& RHS) {
    LHS -= RHS;
    return LHS;
}


PureQuadExpr scale(PureQuadExpr LHS, double scalar) {
    for(double& e : LHS._values) {
        e *= scalar;
    }
    return LHS;
}


PureQuadExpr operator*(PureQuadExpr LHS, double scalar) {
    return scale(std::move(LHS), scalar);
}


PureQuadExpr operator*(double scalar, PureQuadExpr RHS) {
    return scale(std::move(RHS), scalar);
}


double SparseMatrix::get_element(unsigned row, unsigned col) const {
    if(row > get_number_of_rows()) {
        return 0;
    }
    auto it = _matrix.find(std::make_pair(row, col));
    if(it == _matrix.end()) {
        return 0;
    }
    return it->second;
}

void SparseMatrix::setElement(unsigned row, unsigned col, double value) { _matrix[std::make_pair(row, col)] = value; }

LinExpr SparseMatrix::getRow(unsigned row) const {
    LinExpr result(0.0);
    for(std::map<std::pair<unsigned int, unsigned int>, double>::const_iterator it = _matrix.lower_bound(std::make_pair(row, 0)); it->first.first <= row && it != _matrix.end(); it++) {
        result.set_value(it->first.second, it->second);
    }
    return result;
}

unsigned SparseMatrix::get_number_of_rows() const {
    if(!_matrix.empty()) {
        unsigned highestCurrentRowIndex = _matrix.rbegin()->first.first;
        return highestCurrentRowIndex + 1;
    }
    return 0;
}

void SparseMatrix::append_row(const LinExpr& row) {
    unsigned newRowIndex = get_number_of_rows(); // known row with highest index is get_number_of_rows-1;
    append_row(row, newRowIndex);
}

void SparseMatrix::append_row(const LinExpr& row, unsigned rowNumber) {
    unsigned minNumber = get_number_of_rows(); // we only allow appending, not replacing.
    unsigned nnz = row.get_non_zero_ids().size();
    unsigned newRowIndex = rowNumber;
    if(newRowIndex < minNumber) {
        throw std::invalid_argument(
          "Tried to append a row to a sparse matrix, but given row index lead to an insertion. "
          "Requested row index to append: "
          + std::to_string(rowNumber) + " index of last row already in matrix: " + std::to_string(minNumber - 1));
    }
    for(unsigned i = 0; i < nnz; i++) {
        // efficently inserts at the end of the map!
        _matrix.insert(std::end(_matrix), std::make_pair(std::make_pair(newRowIndex, row.get_non_zero_ids()[i]),
                                            row.get_non_zero_values()[i]));
    }
}

std::string SparseMatrix::print() const {
    std::stringstream ss;
    if(!_matrix.empty()) {

        unsigned previousRow = _matrix.begin()->first.first;
        ss << "\n";
        for(const auto& t : _matrix) {
            unsigned row = t.first.first;
            unsigned col = t.first.second;
            double val = t.second;
            if(row != previousRow) {
                ss << "\n";
                previousRow = row;
            }
            ss << "(" << row << "," << col << "): " << val << ",";
        }
    }
    ss << std::endl;
    return ss.str();
}

void SparseMatrix::clear() { _matrix.clear(); }

std::ostream& operator<<(std::ostream& os, const SparseMatrix& dt) {
    os << dt.print();
    return os;
}

SparseMatrix operator+(const SparseMatrix& LHS, const SparseMatrix& RHS) {
    SparseMatrix result;
    unsigned maxRows = std::max(LHS.get_number_of_rows(), RHS.get_number_of_rows());
    for(unsigned i = 0; i < maxRows; i++) {
        result.append_row(LHS.getRow(i) + RHS.getRow(i), i); // if both rows are empty, this will do nothing
    }
    return result;
}

SparseMatrix operator-(const SparseMatrix& LHS, const SparseMatrix& RHS) {
    SparseMatrix result;
    unsigned maxRows = std::max(LHS.get_number_of_rows(), RHS.get_number_of_rows());
    for(unsigned i = 0; i < maxRows; i++) {
        result.append_row(LHS.getRow(i) - RHS.getRow(i), i);
    }
    return result;
}

SparseMatrix operator*(const SparseMatrix& LHS, const SparseMatrix& RHS) {
    SparseMatrix result;
    unsigned maxRows = std::max(LHS.get_number_of_rows(), RHS.get_number_of_rows());
    for(unsigned i = 0; i < maxRows; i++) {
        result.append_row(LHS.getRow(i) * RHS.getRow(i), i);
    }
    return result;
}

SparseMatrix add(SparseMatrix LHS, double scalar) {
    for(auto& p : LHS._matrix) {
        p.second += scalar;
    }
    return LHS;
}

SparseMatrix scale(SparseMatrix LHS, double scale) {
    for(auto& p : LHS._matrix) {
        p.second *= scale;
    }
    return LHS;
}

SparseMatrix operator+(const SparseMatrix& LHS, double scalar) { return add(LHS, scalar); }

SparseMatrix operator+(double scalar, const SparseMatrix& RHS) { return add(RHS, scalar); }

SparseMatrix operator-(const SparseMatrix& LHS, double scalar) { return add(LHS, -scalar); }

SparseMatrix operator-(const SparseMatrix& LHS) { return scale(LHS, -1.0); }

SparseMatrix operator-(double scalar, const SparseMatrix& RHS) { return scalar + (-RHS); }

SparseMatrix operator*(const SparseMatrix& LHS, double scalar) { return scale(LHS, scalar); }

SparseMatrix operator*(double scalar, const SparseMatrix& RHS) { return scale(RHS, scalar); }

bool QuadExpr::is_quadratic() const { return order == Order::QUADRATIC; }

bool QuadExpr::is_linear() const { return order == Order::LINEAR; }

bool QuadExpr::is_scalar() const { return order == Order::SCALAR; }

QuadExpr::QuadExpr(const LinExpr& linExpr) :
    linearPart(linExpr), order(linExpr.is_scalar() ? Order::SCALAR : Order::LINEAR) { }
QuadExpr::QuadExpr(double scalar) :
    QuadExpr(LinExpr(scalar)) { }
QuadExpr::QuadExpr() :
    QuadExpr(0.0) { }

const LinExpr& QuadExpr::getLinearPart() const { return linearPart; }
const PureQuadExpr& QuadExpr::getQuadraticPart() const { return quadraticPart; }
double QuadExpr::getConstant() const { return linearPart.constant(); }

void QuadExpr::pow(const QuadExpr& other) {
    if(!other.is_scalar()) {
        throw OperationResultNotQuadratic("Power has to be scalar");
    }

    if(is_scalar()) {
        linearPart.constant() = std::pow(linearPart.constant(), other.linearPart.constant());
    } else {
        double e = other.getConstant();

        if(std::abs(e) <= 1e-6) {
            *this = QuadExpr(1.0);
        } else if(std::abs(e - 1) <= 1e-6) {
            // nothing to be done here
        } else if(std::abs(e - 2) <= 1e-6) {
            *this *= *this;
        } else {
            throw OperationResultNotQuadratic("non-scalar QuadExpr can only be raised to the power of 0, 1 and 2");
        }
    }
}

QuadExpr& QuadExpr::operator=(double scalar) {
    *this = QuadExpr(scalar);
    return *this;
}

QuadExpr& QuadExpr::operator+=(const QuadExpr& RHS) {
    if(RHS.is_quadratic() || is_quadratic()) {
        order = Order::QUADRATIC;
    } else if(RHS.is_linear() || is_linear()) {
        order = Order::LINEAR;
    } else {
        order = Order::SCALAR;
    }
    linearPart = linearPart + RHS.linearPart;
    quadraticPart += RHS.quadraticPart;
    return *this;
}

QuadExpr& QuadExpr::operator-=(const QuadExpr& RHS) {
    if(RHS.is_quadratic() || is_quadratic()) {
        order = Order::QUADRATIC;
    } else if(RHS.is_linear() || is_linear()) {
        order = Order::LINEAR;
    } else {
        order = Order::SCALAR;
    }
    linearPart = linearPart - RHS.linearPart;
    quadraticPart -= RHS.quadraticPart;
    return *this;
}

QuadExpr& QuadExpr::operator*=(const QuadExpr& RHS) {
    if(is_scalar()) {
        double c = linearPart.constant();
        *this = RHS;
        *this *= c;
    } else if(RHS.is_scalar()) {
        *this *= RHS.linearPart.constant();
    } else if(is_quadratic() || RHS.is_quadratic()) {
        throw OperationResultNotQuadratic("Cannot multiply quadratic expression with non-scalar expression.");
    } else {
        // both sides must be linear
        order = Order::QUADRATIC;
        for(unsigned i = 0; i < linearPart.get_non_zero_ids().size(); i++) {
            for(unsigned j = 0; j < RHS.getLinearPart().get_non_zero_ids().size(); j++) {
                unsigned index1 = linearPart.get_non_zero_ids()[i];
                unsigned index2 = RHS.getLinearPart().get_non_zero_ids()[j];
                quadraticPart.add_term(std::min(index1, index2), std::max(index1, index2), linearPart.get_non_zero_values()[i] * RHS.getLinearPart().get_non_zero_values()[j]);
            }
        }

        double c = linearPart.constant();
        double cRHS = RHS.linearPart.constant();
        linearPart = linearPart * RHS.linearPart.constant() + RHS.linearPart * linearPart.constant();
        linearPart.constant() = c * cRHS;
    }
    return *this;
}

QuadExpr& QuadExpr::operator*=(double scalar) {
    linearPart = linearPart * scalar;
    quadraticPart = quadraticPart * scalar;
    return *this;
}

QuadExpr& QuadExpr::operator/=(double scalar) {
    (*this) *= 1 / scalar;
    return *this;
}

QuadExpr operator+(const QuadExpr& RHS) { return QuadExpr(RHS); }

QuadExpr operator+(const QuadExpr& LHS, const QuadExpr& RHS) {
    QuadExpr tmp = LHS;
    tmp += RHS;
    return tmp;
}

QuadExpr operator-(const QuadExpr& LHS, const QuadExpr& RHS) {
    QuadExpr tmp = LHS;
    tmp -= RHS;
    return tmp;
}

QuadExpr operator-(const QuadExpr& RHS) { return -1 * RHS; }

QuadExpr operator*(const QuadExpr& LHS, const QuadExpr& RHS) {
    QuadExpr tmp = LHS;
    tmp *= RHS;
    return tmp;
}

QuadExpr operator*(const QuadExpr& LHS, double RHS) {
    QuadExpr tmp = LHS;
    tmp *= RHS;
    return tmp;
}

QuadExpr operator*(double LHS, const QuadExpr& RHS) { return RHS * LHS; }

QuadExpr operator/(const QuadExpr& LHS, double scalar) {
    if(std::abs(scalar) < 1e-8) {
        throw std::invalid_argument("cannot divide by 0");
    }
    return (1 / scalar) * LHS;
}

QuadExpr operator/(double scalar, const QuadExpr& RHS) {
    if(!RHS.is_scalar()) {
        throw OperationResultNotQuadratic("cannot divide by non-scalar QuadExpr");
    }
    if(std::abs(RHS.getConstant()) < 1e-8) {
        throw std::invalid_argument("cannot divide by 0");
    }
    return QuadExpr(scalar / RHS.getConstant());
}

QuadExpr operator/(const QuadExpr& LHS, const QuadExpr& RHS) {
    if(!RHS.is_scalar()) {
        throw OperationResultNotQuadratic("cannot divide by non-scalar QuadExpr");
    }
    return LHS / RHS.getConstant();
}

} // namespace dips
