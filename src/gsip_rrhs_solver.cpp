#include "gsip_rrhs_solver.hpp"

#include <algorithm> // for min
#include <fstream>   // for operator<<, basic_ostream, basic_ostream::operator<<, basic_ostream<>::__ostream_type, basic_ios::rdbuf, endl, basic_o...
#include <memory>    // for unique_ptr, allocator_traits<>::value_type
#include <utility>   // for pair, make_pair, move
#include <iostream>  // for cout
#include <iterator>  // for distance
#include <limits>    // for numeric_limits
#include <list>      // for _List_iterator, list, list<>::iterator, operator!=, _List_iterator<>::_Self
#include <stdexcept> // for invalid_argument

#include "fadbad_evaluator.hpp"
#include "util/evaluator.hpp" // for evaluate_expression
#include "common.hpp"         // for fix_variables, set_real, discretize, copy_variables
#include "expression.hpp"     // for expression
#include "node.hpp"           // for less_equal_node, value_node, equal_node, greater_equal_node
#include "symbol_table.hpp"   // for symbol_table
#include "value.hpp"          // for real
#include "output.hpp"         // to_json, from_json
#include "nlohmann/json.hpp"  // for json

#ifdef DIPS_use_mpi
#include "mpi.h"
#endif

namespace dips {


gsip_rrhs_program_definition_parser::gsip_rrhs_program_definition_parser(std::istream& is, symbol_table& symbols) :
    programdefinition_parser_chain(is, symbols, { { "lbp", {} }, { "ubp", { "eps_res" } }, { "aux", { "alpha", "g_u" } } }) { }

void gsip_rrhs_solver::program_info::add_llp(const program& llp, const program& aux, const discretization& lbp_disc, const discretization& ubp_disc) {
    llps.emplace_back(llp);
    auxs.emplace_back(aux);
    lbp_discs.emplace_back(lbp_disc);
    ubp_discs.emplace_back(ubp_disc);
}


solve_record gsip_rrhs_solver::solve(program_info prog) {
    std::unique_ptr<solver> sub(make_subsolver(symbols, solver_base_name));

    for(auto& option : options) {
        sub->pass_option(option.first, option.second);
    }

    solve_record rec;
    solve_record lbp_rec;
    solve_record ubp_rec;
    int n_llps = prog.llps.size();
    std::vector<solve_record> llp_rec(n_llps);
    std::vector<solve_record> aux_rec(n_llps);
    std::vector<solve_record> disc_rec(n_llps);

    enum { LBP,
        UBP,
        RECOVER,
        NDEF } next_problem
      = LBP;

    if(ubp_guard <= 0 && init_res_dynamic == true) {
        throw std::invalid_argument("invalid combination of settings: ubp_guard <= 0 and init_res_dynamic = true.");
    }
    if(init_res_dynamic == true) {
        eps_res_l = std::numeric_limits<double>::quiet_NaN();
        eps_res_u = std::numeric_limits<double>::quiet_NaN();
    }
    double feas_vio_lbp = std::numeric_limits<double>::infinity(); // feasibility violation of lbp
    double feas_vio_ubp = std::numeric_limits<double>::infinity(); // feasibility violation of ubp (always negative)
    eps_res_u = init_res_u;                                        // set initial eps_res_u
    eps_res_l = init_res_l;                                        // set initial eps_res_l
    alpha = init_alpha;                                            // set initial alpha
    bool set_init_res_dynamic = init_res_dynamic;                  // flag to only set init_res dynamically once


    for(int i = 0; i < max_iter; i++) {
        scoped_push sp_it(sub->solver_name, "it_" + std::to_string(i + 1));

        std::cout << "\nIteration " << i + 1 << ":\n";
        std::cout << "LBD           = " << rec.m_lbd << '\n';
        std::cout << "feas.vio. LBD = " << feas_vio_lbp << '\n';
        std::cout << "UBD           = " << rec.m_ubd << '\n';
        std::cout << "feas.vio. UBD = " << feas_vio_ubp << '\n';
        std::cout << "eps_res_l     = " << eps_res_l << '\n';
        std::cout << "eps_res_u     = " << eps_res_u << "\n\n";

        // check termination
        if(check_termination(rec, feas_vio_ubp, abs_tol, rel_tol, feas_tol)) {
            return rec;
        }
        if(check_aborted(max_time, rec, solver_base_name, aborted)) {
            return rec;
        }

        if(next_problem == LBP) {
            scoped_push sp_lbp(sub->solver_name, "_lbp");
            std::cout << "\ngsip: solving lbp\n";

            sub->set_option("abs_tol", abs_tol_lbp);
            sub->set_option("rel_tol", rel_tol_lbp);
            sub->set_option("settings", settings_lbp);
            sub->set_option("max_time", max_time - rec.m_cpu);

            lbp_rec = sub->solve(prog.lbp);

            rec.m_cpu += lbp_rec.m_cpu;

            std::cout << "reporting status\n";
            report(lbp_rec);

            if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                return rec;
            }

            switch(lbp_rec.m_status) {
                case INFEASIBLE:
                    // lbp proves gsip to be infeasible
                    rec.m_status = INFEASIBLE;
                    rec.m_lbd = std::numeric_limits<double>::infinity();
                    rec.m_ubd = std::numeric_limits<double>::infinity();

                    return rec;
                case GLOBAL:
                    // computed valid lbd
                    rec.m_lbd = lbp_rec.m_lbd;
                    break;
                default:
                    throw std::invalid_argument("lbp: unhandled solver status: "
                                                + to_string(lbp_rec.m_status));
            }

            bool lbp_feasible = true;
            double feas_vio_temp = -std::numeric_limits<double>::infinity();
            for(int j = 0; j < n_llps; ++j) {
                if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                    return rec;
                }
                scoped_push sp_lbp_llp(sub->solver_name, "_llp_" + std::to_string(j + 1));
                std::cout << "gsip: solving lbp_llp_" << j + 1 << '\n';
                // note: always minimizing. This leads to
                //  - flipped sign of objective
                //  - flipped lbd and ubd

                symbols.push_scope();
                fix_variables(lbp_rec, symbols);

                sub->set_option("abs_tol", abs_tol_lbp_llp);
                sub->set_option("rel_tol", rel_tol_lbp_llp);
                sub->set_option("settings", settings_lbp_llp);
                sub->set_option("max_time", max_time - rec.m_cpu);

                llp_rec[j] = sub->solve(prog.llps[j]);

                rec.m_cpu += llp_rec[j].m_cpu;
                symbols.pop_scope();

                std::cout << "reporting status\n";
                report(llp_rec[j]);

                if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                    return rec;
                }

                bool compute_aux = true;

                switch(llp_rec[j].m_status) {
                    case INFEASIBLE:
                        // by convention: llp_rec.m_ubd = llp_rec.m_lbd = + infty
                        // => -llp_rec.m_lbd <= feas_tol
                        // => incumbent point in llp[j] gsip feasible
                        // => no tolerance update necessary
                        compute_aux = false;
                        break;
                    case GLOBAL:
                        if(-llp_rec[j].m_lbd <= feas_tol) {
                            // => incumbent point in llp[j] gsip feasible
                            // => nothing happens here
                            // => compute llp[j+1]
                            compute_aux = false;
                        }
                        // inconclusive llp result, zero is bracketed => tolerance update
                        // skip aux because assumption is that aux_g_u  (= objective value of llp at found point) > 0
                        else if(-llp_rec[j].m_lbd > 0 && -llp_rec[j].m_ubd < 0) {
                            if(abs_tol_lbp_llp > -llp_rec[j].m_lbd / red_tol_lbp_llp) {
                                set_tolerance_robust("abs_tol_lbp_llp", -llp_rec[j].m_lbd / red_tol_lbp_llp);
                            }
                            lbp_feasible = false;
                            compute_aux = false;
                            std::cout << " \n Skipping aux problem: LLP was inconclusive. " << std::endl;
                        } else {
                            // -llp_rec.m_lbd > feas_tol
                            // incumbent point of lbp might be gsip infeasible
                            lbp_feasible = false;
                            compute_aux = check_slater(lbp_rec, prog.llps[j], llp_rec[j]);
                        }
                        break;
                    default:
                        throw std::invalid_argument("lbp_llp_" + std::to_string(j + 1)
                                                    + ": unhandled solver status: " + to_string(llp_rec[j].m_status));
                }

                // keeping track of max feasibility violation
                if(-llp_rec[j].m_lbd > feas_vio_temp) {
                    feas_vio_temp = -llp_rec[j].m_lbd;
                }

                if(compute_aux) {
                    scoped_push sp_aux(sub->solver_name, "_aux" + std::to_string(j + 1));
                    std::cout << "gsip: solving lbp_aux_" << j + 1 << " with\n";

                    double cpu_time_all_alphas = 0; // Sum all cpu times in the following while loop for logging
                    while(compute_aux) {
                        scoped_push sp_alpha(sub->solver_name, "_alpha_" + std::to_string(alpha));
                        std::cout << "    alpha = " << alpha << '\n';

                        if(alpha * -llp_rec[j].m_ubd < min_alpha) {
                            std::cout << "\nError: alpha*gu_max = "
                                      << alpha << "*" << -llp_rec[j].m_ubd
                                      << " = " << alpha * -llp_rec[j].m_ubd
                                      << " < min_alpha = "
                                      << min_alpha << ". ";

                            if(init_alpha * -llp_rec[j].m_ubd < min_alpha) {
                                std::cout << "The problem is likely numerical sensitive because init_alpha*gu_max = "
                                          << init_alpha << "*" << -llp_rec[j].m_ubd
                                          << " = " << init_alpha * -llp_rec[j].m_ubd
                                          << " < min_alpha = "
                                          << min_alpha << ".\n";
                                rec.m_status = NUMERICAL_SENSITIVE;
                            } else {
                                std::cout << "An assumption is likely violated.\n";
                                rec.m_status = ASSUMPTION_VIOLATION_LIKELY;
                            }
                            std::cout << "Note: a reduction of min_alpha might help. Please ensure that the feasiblity tolerance of the used subsolver is smaller than alpha*gu_max.\n"
                                      << "  ... Aborting.\n ";
                            return rec;
                        }

                        symbols.push_scope();
                        fix_variables(lbp_rec, symbols);

                        set_real(prog.aux_g_u, -llp_rec[j].m_ubd, symbols);
                        set_real(prog.aux_alpha, alpha, symbols);

                        sub->set_option("abs_tol", abs_tol_lbp_aux);
                        sub->set_option("rel_tol", rel_tol_lbp_aux);
                        sub->set_option("settings", settings_lbp_aux);
                        sub->set_option("max_time", max_time - rec.m_cpu);

                        aux_rec[j] = sub->solve(prog.auxs[j]);

                        std::cout << "lbp aux time: " << aux_rec[j].m_cpu << std::endl;

                        cpu_time_all_alphas += aux_rec[j].m_cpu;
                        rec.m_cpu += aux_rec[j].m_cpu;
                        symbols.pop_scope();

                        if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                            return rec;
                        }

                        switch(aux_rec[j].m_status) {
                            case INFEASIBLE:
                                // by convention: llp_rec.m_ubd = llp_rec.m_lbd = + infty
                                // => will reduce alpha
                            case FEASIBLE:
                            case LOCAL:
                                // no global solution required
                            case GLOBAL:
                                if(aux_rec[j].m_ubd >= 0) {
                                    // aux has not furnished a llp Slater point
                                    // => reduce alpha and resolve
                                    alpha /= red_alpha;
                                    set_real(prog.aux_alpha, alpha, symbols);

                                    //std::cout << "reporting status\n";
                                    //report(aux_rec[j]);
                                } else {
                                    // aux has furnished a llp Slater point
                                    // => discretize
                                    // => reset alpha
                                    disc_rec[j] = aux_rec[j];
                                    // discretize(prog.lbp_discs[j], aux_rec[j], symbols);
                                    std::cout << "discretized aux_" << j + 1 << ".\n";

                                    alpha = init_alpha;
                                    compute_aux = false;

                                    std::cout << "reporting status\n";
                                    report(aux_rec[j]);
                                }
                                break;
                            default:
                                throw std::invalid_argument("lbp_aux_" + std::to_string(j + 1)
                                                            + ": unhandled solver status: " + to_string(aux_rec[j].m_status));
                        }
                    }
                } else {
                    // discretize
                    if(discr_all || -llp_rec[j].m_ubd > 0) {
                        // discretize always or only if llp[j] produced a valid cut
                        disc_rec[j] = llp_rec[j];
                        // discretize(prog.lbp_discs[j], llp_rec[j], symbols);
                        std::cout << "discretized llp_" << j + 1 << ".\n";
                    }
                }
            }

            feas_vio_lbp = feas_vio_temp;

            // check termination
            if(lbp_feasible) {
                // => incumbent point in all llps gsip feasible
                feas_vio_ubp = feas_vio_lbp;
                std::cout << "\nlbp furnished a point which is within llp feas. tolerance. Terminating.\n";
                std::cout << "Feas. vio.:   " << feas_vio_ubp << " <= feas_tol = " << feas_tol << std::endl;
                rec.m_status = GLOBAL;
                rec.m_lbd = lbp_rec.m_lbd;
                rec.m_ubd = lbp_rec.m_ubd;
                copy_variables(lbp_rec, rec);

                return rec;
            } else {
                // discretize
                for(int j = 0; j < n_llps; ++j) {
                    if(discr_all || -disc_rec[j].m_ubd > 0) {
                        // discretize always or only if llp[j] produced a valid cut
                        discretize(prog.lbp_discs[j], disc_rec[j], symbols);
                        if(ubp_guard > 0 && feas_vio_lbp > ubp_guard) {
                            discretize(prog.ubp_discs[j], disc_rec[j], symbols);
                        }
                    }
                }
                if(ubp_guard <= 0 || feas_vio_lbp <= ubp_guard) {
                    next_problem = UBP;
                    if(set_init_res_dynamic) {
                        eps_res_u = std::max(feas_vio_lbp, min_eps_res_u);
                        eps_res_l = std::max(feas_vio_lbp, min_eps_res_l);
                        std::cout << "setting eps_res_u = max(feas_vio_lbp, min_eps_res_u) = max(" << feas_vio_lbp << ", " << min_eps_res_u << ") = " << eps_res_u << '\n';
                        std::cout << "setting eps_res_l = max(feas_vio_lbp, min_eps_res_l) = max(" << feas_vio_lbp << ", " << min_eps_res_l << ") = " << eps_res_l << '\n';
                        set_init_res_dynamic = false;
                    }
                } else {
                    next_problem = LBP;
                    std::cout << "skipped UBP because feas_vio_lbp > ubp_guard";
                }
            }
        }

        else if(next_problem == UBP) {
            scoped_push sp_ubp(sub->solver_name, "_ubp");
            std::cout << "\ngsip: solving ubp\n";

            set_real(prog.ubp_restrict_u, eps_res_u, symbols);
            set_real(prog.ubp_restrict_l, eps_res_l, symbols);

            sub->set_option("abs_tol", abs_tol_ubp);
            sub->set_option("rel_tol", rel_tol_ubp);
            sub->set_option("settings", settings_ubp);
            sub->set_option("max_time", max_time - rec.m_cpu);

            ubp_rec = sub->solve(prog.ubp);

            rec.m_cpu += ubp_rec.m_cpu;

            std::cout << "reporting status\n";
            report(ubp_rec);

            if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                return rec;
            }

            switch(ubp_rec.m_status) {
                case INFEASIBLE:
                    // ubp infeasible, eps_res_* chosen too large
                    // => reduce eps_res_*
                    // => end iteration and go to LBP
                    eps_res_u /= red_res_u;
                    eps_res_l /= red_res_l;
                    if(eps_res_u < min_eps_res_u) {
                        std::cout << "failure: eps_res_u = " << eps_res_u
                                  << " reduced below min_eps_res_u = "
                                  << min_eps_res_u
                                  << ". A reduction of min_eps_res_u (however unlikely) might help."
                                  << " Trying to recover.\n";
                        next_problem = RECOVER;
                        continue;
                    }
                    if(eps_res_l < min_eps_res_l) {
                        std::cout << "failure: eps_res_l = " << eps_res_l
                                  << " reduced below min_eps_res_l = "
                                  << min_eps_res_l
                                  << ". A reduction of min_eps_res_l (however unlikely) might help."
                                  << " Trying to recover.\n";
                        next_problem = RECOVER;
                        continue;
                    }

                    next_problem = LBP;
                    continue;
                case GLOBAL:
                    // computed candidate point for ubd
                    // => nothing happens here
                    // => compute llps
                    break;
                default:
                    throw std::invalid_argument("ubp: unhandled solver status: "
                                                + to_string(ubp_rec.m_status));
            }

            // tolerance update for llp if necessary
            if(abs_tol_ubp_llp >= std::min(eps_res_u, eps_res_l)) {
                set_tolerance_robust("abs_tol_ubp_llp", std::min(eps_res_u, eps_res_l) / red_tol_ubp_llp);
            }

            bool ubp_feasible = true;
            double feas_vio_temp = -std::numeric_limits<double>::infinity();
            for(int j = 0; j < n_llps; ++j) {
                if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                    return rec;
                }
                scoped_push sp_ubp_llp(sub->solver_name, "_llp_" + std::to_string(j + 1));
                std::cout << "gsip: solving ubp_llp_" << j + 1 << '\n';
                // note: always minimizing. This leads to
                //  - flipped sign of objective
                //  - flipped lbd and ubd

                symbols.push_scope();
                fix_variables(ubp_rec, symbols);

                sub->set_option("abs_tol", abs_tol_ubp_llp);
                sub->set_option("rel_tol", rel_tol_ubp_llp);
                sub->set_option("settings", settings_lbp_llp);
                sub->set_option("max_time", max_time - rec.m_cpu);

                llp_rec[j] = sub->solve(prog.llps[j]);

                rec.m_cpu += llp_rec[j].m_cpu;
                symbols.pop_scope();

                std::cout << "reporting status\n";
                report(llp_rec[j]);

                if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                    return rec;
                }

                switch(llp_rec[j].m_status) {
                    case INFEASIBLE:
                        // by convention: llp_rec.m_ubd = llp_rec.m_lbd = + infty
                        // => -llp_rec.m_lbd <= feas_tol
                        // => incumbent point in llp[j] gsip feasible
                        break;
                    case GLOBAL:
                        if(-llp_rec[j].m_lbd <= feas_tol) {
                            // => incumbent point in llp[j] gsip feasible
                            // => nothing happens here
                            // => compute llp[j+1]
                        } else {
                            // -llp_rec.m_lbd > feas_tol
                            // incumbent point of lbp might be gsip infeasible
                            ubp_feasible = false;
                        }
                        break;
                    default:
                        throw std::invalid_argument("ubp_llp_" + std::to_string(j + 1)
                                                    + ": unhandled solver status: " + to_string(llp_rec[j].m_status));
                }

                // keeping track of max feasibility violation
                if(-llp_rec[j].m_lbd > feas_vio_temp) {
                    feas_vio_temp = -llp_rec[j].m_lbd;
                }
            }

            if(ubp_feasible) {
                feas_vio_ubp = feas_vio_temp;
                rec.m_status = FEASIBLE;
                if(ubp_rec.m_ubd < rec.m_ubd) {
                    rec.m_ubd = ubp_rec.m_ubd;
                    copy_variables(ubp_rec, rec);
                }
                eps_res_u /= red_res_u;
                eps_res_l /= red_res_l;
                if(eps_res_u < min_eps_res_u) {
                    std::cout << "failure: eps_res = " << eps_res_u
                              << " reduced below min_eps_res_u = "
                              << min_eps_res_u
                              << ". A reduction of min_eps_res_u (however unlikely) might help. "
                              << " Trying to recover.\n";
                    next_problem = RECOVER;
                    continue;
                }
                if(eps_res_l < min_eps_res_l) {
                    std::cout << "failure: eps_res = " << eps_res_l
                              << " reduced below min_eps_res_l = "
                              << min_eps_res_l
                              << ". A reduction of min_eps_res_l (however unlikely) might help. "
                              << " Trying to recover.\n";
                    next_problem = RECOVER;
                    continue;
                }
                next_problem = LBP;
            } else {
                // discretize
                for(int j = 0; j < n_llps; ++j) {
                    if(discr_all || !(-llp_rec[j].m_lbd <= 0)) {
                        // discretize always or only if llp[j] produced a valid cut
                        discretize(prog.ubp_discs[j], llp_rec[j], symbols);
                        std::cout << "discretized llp_" << j + 1 << ".\n";
                    }
                }
                next_problem = LBP;
            }
        }

        else if(next_problem == RECOVER) {
            // trying to recover by solving ubp once with eps_res_u = eps_res_l = 0
            // this is equivalent to solving the lbp with the discretization of the ubp
            scoped_push sp_recover(sub->solver_name, "_recover");
            std::cout << "\ngsip: solving recover (constructed from ubp)\n";

            std::cout << "setting eps_res_u = eps_res_l = 0.\n";
            set_real(prog.ubp_restrict_u, 0, symbols);
            set_real(prog.ubp_restrict_l, 0, symbols);

            sub->set_option("abs_tol", abs_tol_ubp);
            sub->set_option("rel_tol", rel_tol_ubp);
            sub->set_option("settings", settings_ubp);
            sub->set_option("max_time", max_time - rec.m_cpu);

            auto recover_rec = sub->solve(prog.ubp);

            rec.m_cpu += recover_rec.m_cpu;

            std::cout << "reporting status\n";
            report(recover_rec);

            if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                return rec;
            }

            switch(recover_rec.m_status) {
                case INFEASIBLE:
                    if(rec.m_status == FEASIBLE) {
                        // a feasible point has already been found but the recovery problem declared gsip to be infeasible -> this should not happen
                        std::cout << "error: a feasible point has been already found but the recovery step declared gsip to be infeasible. This should not happen!\n";
                        rec.m_status = ABNORMAL;
                        return rec;
                    } else {
                        std::cout << "success: the recovery step declared gsip to be infeasible.\n";
                        rec.m_status = INFEASIBLE;
                        return rec;
                    }
                case GLOBAL:
                    // computed candidate point
                    // => nothing happens here
                    // => compute llps
                    break;
                default:
                    throw std::invalid_argument("recover: unhandled solver status: "
                                                + to_string(recover_rec.m_status));
            }

            bool recover_feasible = true;
            double feas_vio_temp = -std::numeric_limits<double>::infinity();
            for(int j = 0; j < n_llps; ++j) {
                if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                    return rec;
                }
                scoped_push sp_recover_llp(sub->solver_name, "_llp_" + std::to_string(j + 1));
                std::cout << "gsip: solving recover_llp_" << j + 1 << '\n';
                // note: always minimizing. This leads to
                //  - flipped sign of objective
                //  - flipped lbd and ubd

                symbols.push_scope();
                fix_variables(recover_rec, symbols);

                sub->set_option("abs_tol", abs_tol_ubp_llp);
                sub->set_option("rel_tol", rel_tol_ubp_llp);
                sub->set_option("settings", settings_lbp_llp);
                sub->set_option("max_time", max_time - rec.m_cpu);

                llp_rec[j] = sub->solve(prog.llps[j]);

                rec.m_cpu += llp_rec[j].m_cpu;
                symbols.pop_scope();

                std::cout << "reporting status\n";
                report(llp_rec[j]);

                if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                    return rec;
                }

                switch(llp_rec[j].m_status) {
                    case INFEASIBLE:
                        // by convention: llp_rec.m_ubd = llp_rec.m_lbd = + infty
                        // => -llp_rec.m_lbd <= feas_tol
                        // => incumbent point in llp[j] gsip feasible
                        break;
                    case GLOBAL:
                        if(-llp_rec[j].m_lbd <= feas_tol) {
                            // => incumbent point in llp[j] gsip feasible
                            // => nothing happens here
                            // => compute llp[j+1]
                        } else {
                            // -llp_rec.m_lbd > feas_tol
                            // incumbent point of lbp might be gsip infeasible
                            recover_feasible = false;
                        }
                        break;
                    default:
                        throw std::invalid_argument("recover_llp_" + std::to_string(j + 1)
                                                    + ": unhandled solver status: " + to_string(llp_rec[j].m_status));
                }

                // keeping track of max feasibility violation
                if(-llp_rec[j].m_lbd > feas_vio_temp) {
                    feas_vio_temp = -llp_rec[j].m_lbd;
                }
            }


            // check termination
            if(recover_feasible) {
                // => incumbent point in all llps gsip feasible
                feas_vio_ubp = feas_vio_temp;
                std::cout << "\nsuccess: recover furnished a point which is within llp feas. tolerance. Terminating.\n";
                std::cout << "Feas. vio.:   " << feas_vio_ubp << " <= feas_tol = " << feas_tol << std::endl;
                rec.m_status = GLOBAL;
                rec.m_lbd = recover_rec.m_lbd;
                rec.m_ubd = recover_rec.m_ubd;
                copy_variables(recover_rec, rec);
                return rec;
            }

            // check termination
            if(check_termination(rec, feas_vio_ubp, abs_tol, rel_tol, feas_tol)) {
                return rec;
            }

            std::cout << "\nfailure: an assumption is likely violated. Aborting...\n";
            rec.m_status = ASSUMPTION_VIOLATION_LIKELY;
            return rec;
        }

        if(next_problem == NDEF) {
            throw std::invalid_argument("procedure not implemented");
        }

        // check termination
        if(check_termination(rec, feas_vio_ubp, abs_tol, rel_tol, feas_tol)) {
            return rec;
        }

#ifdef DIPS_use_mpi
        int _rank;
        MPI_Comm_rank(MPI_COMM_WORLD, &_rank);
        if(_rank == 0) {
#endif
            // write discretization points to output file
            std::string disc_output_path = "./";
            for(auto& i : sub->solver_name) {
                disc_output_path += i;
            }
            std::ofstream disc_output(disc_output_path + "_disc_points.txt");
            std::streambuf* backup = std::cout.rdbuf();
            std::cout.rdbuf(disc_output.rdbuf());
            std::vector<std::vector<discretization>> discs;
            discs.emplace_back(prog.lbp_discs);
            discs.emplace_back(prog.ubp_discs);
            report(symbols, discs);
            std::cout.rdbuf(backup); // reassign cout to its former buffer before that buffer no longer exists
            disc_output.close();     // close file output
#ifdef DIPS_use_mpi
        }
        MPI_Barrier(MPI_COMM_WORLD);
#endif
    }

    rec.m_status = EXCEEDED_MAX_ITERATIONS;
    std::cout << solver_base_name << ": total number of iterations exceeded limit."
              << "Rising max_it might help\n";
    return rec;
}




bool gsip_rrhs_solver::check_slater(const solve_record& lbp_rec, program& llp_prog, const solve_record& llp_rec) {
    bool compute_aux = false;
    scope_guard scope(symbols);

    fix_variables(lbp_rec, symbols);
    fix_variables(llp_rec, symbols);
    double gL = std::numeric_limits<double>::quiet_NaN(); // gL <= 0

    for(auto it = llp_prog.m_constraints.begin(); it != llp_prog.m_constraints.end(); ++it) {
        int constr = std::distance(llp_prog.m_constraints.begin(), it) + 1;
        ConstraintContainer<double> constraint = evaluate_fadbad_constraint<double>(*it, symbols, {}, {});
        if(!constraint.eq.empty()) {
            std::cout << "Slater condition NOT applicable to equality constraint g^L_" << std::to_string(constr) << "\n";
            return compute_aux = true;
        } else {
            // compute the maximum over constraint.ineq
            if(!empty(constraint.ineq)) {
                gL = *std::max_element(constraint.ineq.begin(), constraint.ineq.end());
                if(gL < -tol_llp_slater) {
                    std::cout << "Slater condition fullfilled for g^L_" << constr
                              << " = " << gL << " < tol_llp_slater = " << -tol_llp_slater << "\n";
                    //std::cout << ale::expression_to_string(it->get()) << std::endl;
                } else {
                    std::cout << "Slater condition NOT fullfilled for g^L_" << constr
                              << " = " << gL << " >= tol_llp_slater = " << -tol_llp_slater << "\n";
                    //std::cout << ale::expression_to_string(it->get()) << std::endl;
                    compute_aux = true;
                    return compute_aux;
                }
            }
        }
    }
    return compute_aux;
};
bool gsip_rrhs_solver::set_option(const std::string& option, double value) {
    if(option == "feas_tol") {
        if(value >= 0) {
            feas_tol = value;
            return true;
        }
    }
    if(option == "abs_tol") {
        if(value >= 1e-9) {
            abs_tol = value;
            return true;
        }
    }
    if(option == "rel_tol") {
        if(value >= 1e-9) {
            rel_tol = value;
            return true;
        }
    }
    if(option == "abs_tol_ubp") {
        if(value >= 1e-9) {
            abs_tol_ubp = value;
            return true;
        }
    }
    if(option == "rel_tol_ubp") {
        if(value >= 1e-9) {
            rel_tol_ubp = value;
            return true;
        }
    }
    if(option == "abs_tol_ubp_llp") {
        if(value >= 1e-9) {
            abs_tol_ubp_llp = value;
            return true;
        }
    }
    if(option == "rel_tol_ubp_llp") {
        if(value >= 1e-9) {
            rel_tol_ubp_llp = value;
            return true;
        }
    }
    if(option == "abs_tol_lbp") {
        if(value >= 1e-9) {
            abs_tol_lbp = value;
            return true;
        }
    }
    if(option == "rel_tol_lbp") {
        if(value >= 1e-9) {
            rel_tol_lbp = value;
            return true;
        }
    }
    if(option == "abs_tol_lbp_llp") {
        if(value >= 1e-9) {
            abs_tol_lbp_llp = value;
            return true;
        }
    }
    if(option == "rel_tol_lbp_llp") {
        if(value >= 1e-9) {
            rel_tol_lbp_llp = value;
            return true;
        }
    }
    if(option == "abs_tol_lbp_aux") {
        if(value >= 1e-9) {
            abs_tol_lbp_aux = value;
            return true;
        }
    }
    if(option == "rel_tol_lbp_aux") {
        if(value >= 1e-9) {
            rel_tol_lbp_aux = value;
            return true;
        }
    }
    if(option == "red_tol_lbp_llp") {
        if(value > 1) {
            red_tol_lbp_llp = value;
            return true;
        }
    }
    if(option == "red_tol_ubp_llp") {
        if(value > 1) {
            red_tol_ubp_llp = value;
            return true;
        }
    }
    if(option == "max_iter") {
        if(value >= 1) {
            max_iter = (int)value;
            return true;
        }
    }
    if(option == "max_time") {
        if(value >= 0) {
            max_time = value;
            return true;
        }
    }
    if(option == "init_res_l") {
        if(value > 0) {
            init_res_l = value;
            return true;
        }
    }
    if(option == "red_res_l") {
        if(value > 1) {
            red_res_l = value;
            return true;
        }
    }
    if(option == "min_eps_res_l") {
        if(value >= 0) {
            min_eps_res_l = value;
            return true;
        }
    }
    if(option == "init_res_u") {
        if(value > 0) {
            init_res_u = value;
            return true;
        }
    }
    if(option == "red_res_u") {
        if(value > 1) {
            red_res_u = value;
            return true;
        }
    }
    if(option == "min_eps_res_u") {
        if(value >= 0) {
            min_eps_res_u = value;
            return true;
        }
    }
    if(option == "init_alpha") {
        if(value < 1) {
            init_alpha = value;
            return true;
        }
    }
    if(option == "red_alpha") {
        if(value > 1) {
            red_alpha = value;
            return true;
        }
    }
    if(option == "min_alpha") {
        if(value >= 0) {
            min_alpha = value;
            return true;
        }
    }
    if(option == "tol_llp_slater") {
        if(value > 0) {
            tol_llp_slater = value;
            return true;
        }
    }
    if(option == "ubp_guard") {
        if(value > 0 || value == -1) {
            ubp_guard = value;
            return true;
        }
    }
    std::cout << "Failed to set option " << option << " = " << value << '\n';
    return false;
}

bool gsip_rrhs_solver::set_option(const std::string& option, const std::string& value) {
    if(option == "settings_lbp") {
        settings_lbp = value;
        return true;
    }
    if(option == "settings_ubp") {
        settings_ubp = value;
        return true;
    }
    if(option == "settings_lbp_llp") {
        settings_lbp_llp = value;
        return true;
    }
    if(option == "settings_lbp_aux") {
        settings_lbp_aux = value;
        return true;
    }
    if(option == "settings_ubp_llp") {
        settings_ubp_llp = value;
        return true;
    }
    std::cout << "Failed to set option " << option << " = " << value << '\n';
    return false;
}

bool gsip_rrhs_solver::set_option(const std::string& option, bool value) {
    if(option == "discr_all") {
        discr_all = value;
        return true;
    }
    if(option == "init_res_dynamic") {
        init_res_dynamic = value;
        return true;
    }
    std::cout << "Failed to set option " << option << " = " << value << '\n';
    return false;
}

void gsip_rrhs_solver::pass_option(const std::string& option, double value) {
    options.emplace_back(option, value);
}

double gsip_rrhs_solver::get_option(const std::string& option) const {
    if(option == "abs_tol") {
        return abs_tol;
    }
    if(option == "rel_tol") {
        return rel_tol;
    }
    if(option == "feas_tol") {
        return feas_tol;
    }
    throw std::invalid_argument("Option '" + option + "' not supported or implemented.");
}

void gsip_rrhs_solver::set_solver_base_name(std::string name) {
    solver_base_name = std::move(name);
}

std::string gsip_rrhs_solver::get_solver_base_name() {
    return solver_base_name;
}

} // namespace dips
