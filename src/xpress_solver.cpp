#include "xpress_solver.hpp"

#include <ext/alloc_traits.h> // for __alloc_traits<>::value_type
#include <cstdlib>            // for size_t, NULL
#include <iostream>           // for operator<<, basic_ostream, endl, cout, ostream
#include <string>             // for string, allocator, basic_string, operator==, char_traits, operator+, operator<<, to_string
#include <unordered_map>      // for unordered_map
#include <vector>             // for vector
#include <cmath>              // for ceil, round
#include <algorithm>          // for min
#include <array>              // for array
#include <exception>          // for exception
#include <limits>             // for numeric_limits
#include <list>               // for list, operator!=, _List_iterator, _List_iterator<>::_Self
#include <set>                // for operator!=, set, _Rb_tree_const_iterator, _Rb_tree_const_iterator<>::_Self
#include <stdexcept>          // for invalid_argument
#include <utility>            // for move, pair
#include <variant>            // for variant

#include "xprb_cpp.h"              // for XPRBvar, XPRBprob, XPRBexpr, operator*, XPRBdefcberr, XPRBseterrctrl, operator<=, operator==, operator>=, XPRB_BV
#include "xprs.h"                  // for XPRSsetintcontrol, XPRSsetdblcontrol, XPRSgetdblattrib, XPRS_PRESOLVE, XPRSsetlogfile, XPRSwriteprob, XPRS_BESTBOUND
#include "quad_expr_evaluator.hpp" // for QuadConstraintList, QuadConstraintType, evaluate_quadratic, evaluate_quadratic_constraint, QuadConstraint, QuadCo...
#include "get_time.hpp"            // for thread_clock
#include "parameter_cloner.hpp"    // for add_variable_to_solve_record
#include "symbol_finder.hpp"       // for find_variables
#include "QuadExpr.hpp"            // for PureQuadExpr, QuadExpr, LinExpr
#include "expression.hpp"          // for expression
#include "program.hpp"             // for program
#include "scalar_evaluator.hpp"    // for ScalarVariableIdManager

namespace ale {
struct symbol_table;
} // namespace ale

namespace dips {


/*class xpress_callback: public GRBCallback {
public:
    explicit xpress_callback(const std::atomic<bool>& abort_opt): abort_optimization(abort_opt) {}

protected:
    void callback() final {
        if (abort_optimization) {
            abort();
        }
    }

private:
    const std::atomic<bool>& abort_optimization;
};
*/

class bcl_exception {
public:
    std::string msg;
    int code;
    bcl_exception(int c, const char* m) {
        code = c;
        msg = std::string(m);
        std::cout << "EXCP:" << msg << "\n";
    }
};
/**** User error handling function ****/
void XPRB_CC usererror(xbprob* prob, void* vp, int num, int type,
  const char* t) {
    throw bcl_exception(num, t);
}


xpress_solver::xpress_solver(symbol_table& symbols, std::string name) :
    solver(name), symbols(symbols), solver_name(std::move(name)) { }

solver* xpress_solver::make_solver(symbol_table& symbols, std::string xpress_solver_name) {
    return new xpress_solver(symbols, std::move(xpress_solver_name));
}

// convert QuadExpr to dashoptimization::XPRBexpr
dashoptimization::XPRBexpr convert_expression(const QuadExpr& expr, const std::vector<dashoptimization::XPRBvar>& variables) {
    const LinExpr& linearPart = expr.getLinearPart();
    const PureQuadExpr& quadraticPart = expr.getQuadraticPart();

    // initialize expression with constant term
    dashoptimization::XPRBexpr xpress_expr(expr.getConstant());

    // add linear terms
    for(const unsigned& var_id : linearPart.get_non_zero_ids()) {
        double coeff = linearPart.get_value(var_id);
        xpress_expr.addTerm(coeff, variables.at(var_id));
    }


    // add quadratic terms
    for(unsigned i = 0; i < quadraticPart.get_non_zero_ids().size(); i++) {
        const auto& indices = quadraticPart.get_non_zero_ids().at(i);
        xpress_expr += variables.at(indices.first) * variables.at(indices.second) * quadraticPart.get_non_zero_values().at(i);
    }

    return xpress_expr;
}

// set objective of model to quadratic expression expr
void set_objective(dashoptimization::XPRBprob& model, const QuadExpr& expr, const std::vector<dashoptimization::XPRBvar>& variables) {
    model.setObj(convert_expression(expr, variables));
    model.setSense(0);
}

// add list of quadratic constraints to model
// an optional comment can be used to indentify the constraints in xpress
void add_quad_constraints(dashoptimization::XPRBprob& model, const QuadConstraintList& constr_list,
  const std::vector<dashoptimization::XPRBvar>& variables, const std::string& comment) {
    for(const QuadConstraint& constr : constr_list) {
        dashoptimization::XPRBexpr temp = convert_expression(constr.getExpr(), variables);

        switch(constr.getType()) {
            case QuadConstraintType::EQUAL:
                model.newCtr(temp == 0.0);
                break;
            case QuadConstraintType::LESS_EQUAL:
                model.newCtr(temp <= 0.0);
                break;
            case QuadConstraintType::GREATER_EQUAL:
                model.newCtr(temp >= 0.0);
                break;
            case QuadConstraintType::NOT_EQUAL:
                throw std::invalid_argument("Constraints of type '!=' are not allowed");
            case QuadConstraintType::LESS:
                throw std::invalid_argument("Constraints of type '<' are not allowed");
            case QuadConstraintType::GREATER:
                throw std::invalid_argument("Constraints of type '>' are not allowed");
        }
    }
}

// reads the optimized values from model and stores them into rec
void set_solution_variables(solve_record& rec, dashoptimization::XPRBprob& model, const std::vector<dashoptimization::XPRBvar>& variables, const ScalarVariableIdManager& id_manager) {
    id_manager.set_record<dashoptimization::XPRBvar>([](dashoptimization::XPRBvar x) {
        if(x.getType() == XPRB_BV || x.getType() == XPRB_UI) {
            return std::round(x.getSol());
        }
        return x.getSol();
    },
      rec, variables);
}

void set_record(dashoptimization::XPRBprob& model, solve_record& rec, const std::vector<dashoptimization::XPRBvar>& variables, const ScalarVariableIdManager& id_manager) {
    switch(model.getMIPStat()) {
        case XPRB_MIP_OPTIMAL: {
            rec.m_status = GLOBAL;
            XPRSgetdblattrib(model.getXPRSprob(), XPRS_BESTBOUND, &rec.m_lbd);
            rec.m_ubd = model.getObjVal();

            set_solution_variables(rec, model, variables, id_manager);

            break;
        }
        case XPRB_MIP_INFEAS: {
            rec.m_status = INFEASIBLE;
            rec.m_lbd = std::numeric_limits<double>::infinity();
            rec.m_ubd = std::numeric_limits<double>::infinity();

            // bool print_iis = false;
            // if (print_iis)
            // {

            //     model.exportProb(XPRB_MPS, "iis");
            //     model.print();

            //     int      numv, numc, numiis, len, ncol, nrow, scode, ct;
            //     int      s, i;
            //     char *   vnames, *cnames;
            //     int *    viis, *ciis, *vsizes, *csizes;
            //     char **  vindex, **cindex;
            //     double*  suminfeas = NULL;
            //     XPRSprob op        = model.getXPRSprob();

            //     XPRSgetintattrib(op, XPRS_ORIGINALCOLS, &ncol);
            //     XPRSgetnamelist(op, 2, NULL, 0, &len, 0, ncol - 1);
            //     // Get number of bytes required for retrieving names
            //     vnames = new char[len];
            //     vindex = new char*[ncol];
            //     XPRSgetnamelist(op, 2, vnames, len, NULL, 0, ncol - 1);
            //     vindex[0] = vnames;
            //     for (i = 1; i < ncol; i++) vindex[i] = vindex[i - 1] + strlen2(vindex[i - 1]) + 1;
            //     XPRSgetintattrib(op, XPRS_ORIGINALROWS, &nrow);
            //     XPRSgetnamelist(op, 1, NULL, 0, &len, 0, nrow - 1);
            //     cnames = new char[len];
            //     cindex = new char*[nrow];
            //     XPRSgetnamelist(op, 1, cnames, len, NULL, 0, nrow - 1);
            //     cindex[0] = cnames;
            //     for (i = 1; i < nrow; i++) cindex[i] = cindex[i - 1] + strlen2(cindex[i - 1]) + 1;
            //     numiis = model.getNumIIS();
            //     for (s = 1; s <= numiis; s++)
            //     {
            //         XPRSgetiisdata(op, s, &numc, &numv, NULL, NULL, NULL, NULL,
            //             NULL, NULL, NULL, NULL);

            //         ciis = new int[numc];
            //         viis = new int[numv];
            //         XPRSgetiisdata(op, s, &numc, &numv, ciis, viis, NULL, NULL,
            //             NULL, NULL, NULL, NULL);
            //         std::cout << "IIS " << s << ":  " << numv << " variables, ";
            //         std::cout << numc << " constraints" << std::endl;
            //         if (numv > 0)
            //         {
            //             std::cout << "  Variables: ";  // Print all variables in the IIS
            //             for (i = 0; i < numv; i++) std::cout << vindex[viis[i]] << " ";
            //             std::cout << std::endl;
            //             delete[] viis;  // Free the array of variables
            //         }
            //         if (numc > 0)
            //         {
            //             std::cout << "  Constraints: ";  // Print all constraints in the IIS
            //             for (i = 0; i < numc; i++) std::cout << cindex[ciis[i]] << " ";
            //             std::cout << std::endl;
            //             delete[] ciis;  // Free the array of constraints
            //         }
            //     }
            // }
            //model.computeIIS();
            //model.write("model.ilp");
            break;
        }
        case XPRB_MIP_UNBOUNDED: {
            rec.m_status = UNBOUNDED;
            rec.m_lbd = -std::numeric_limits<double>::infinity();
            rec.m_ubd = -std::numeric_limits<double>::infinity();
            break;
        }
        case XPRB_MIP_SOLUTION: {
            rec.m_status = FEASIBLE;
            XPRSgetdblattrib(model.getXPRSprob(), XPRS_BESTBOUND, &rec.m_lbd);
            rec.m_ubd = model.getObjVal();

            // check if solution was found before the limit was reached
            set_solution_variables(rec, model, variables, id_manager);
            break;
        }
        default:
            rec.m_status = ABNORMAL;
            break;
    }
}

solve_record xpress_solver::solve(program prog) {
    solve_record rec;

    // catch error in missing license
    try {
        dashoptimization::XPRBprob model("prob");
        XPRBseterrctrl(0);
        XPRBdefcberr(NULL, usererror, NULL);
        model.setMsgLevel(2);
        XPRSsetlogfile(model.getXPRSprob(), (solver_name + ".log").c_str());

        // find all variables that are used in the program statement
        auto used_variables = find_variables(prog, symbols);

        // add parameter of correct shape to solve_record
        for(const auto& var_name : used_variables) {
            add_variable_to_solve_record(var_name, symbols, rec);
        }

        // evaluate expressions
        ScalarVariableIdManager id_manager;
        id_manager.set_ids(used_variables, symbols);

        std::vector<QuadConstraintList> quad_constraint_lists {};
        std::vector<std::string> comments;

        auto obj = evaluate_quadratic(prog.m_objective.front(), symbols, id_manager);
        for(auto& constr : prog.m_constraints) {
            auto evaluated_constr = evaluate_quadratic_constraint(constr, symbols, id_manager);
            quad_constraint_lists.push_back(evaluated_constr);
            comments.push_back(constr.m_note);
        }

        // add variables to model
        auto variables = id_manager.map_over_variable_symbols<dashoptimization::XPRBvar>([&model](double lb, double ub, double init, bool integral, const std::string& name, const std::string& comment, const std::vector<size_t>& index) {
            char varType = XPRB_PL;
            if(integral) {
                if(-1 + 1e-6 <= lb && ub <= 2 - 1e-6) {
                    varType = XPRB_BV;
                } else {
                    varType = XPRB_UI;
                }
            }

            std::string var_name = name;
            if(!index.empty()) {
                for(size_t i : index) {
                    var_name += "_" + std::to_string(i);
                }
            }

            return model.newVar(name.c_str(), varType, lb, ub);
        },
          symbols);

        // set the objective and constraints of the model
        set_objective(model, obj, variables);
        for(size_t i = 0; i < quad_constraint_lists.size(); i++) {
            std::string comment;
            if(i < comments.size()) {
                comment = comments[i];
            }
            add_quad_constraints(model, quad_constraint_lists.at(i), variables, comment);
        }

        // try to read settings from file
        // try {
        //     env.readParams(settings_file);
        // } catch (const GRBException& e) {
        //     if (e.getErrorCode() != GRB_ERROR_INVALID_ARGUMENT) {
        //         std::cout << "Xpress Error code = " << e.getErrorCode() << std::endl;
        //         throw std::runtime_error(e.getMessage());
        //     }
        // }

        //set options saved in pass_option of calling context
        for(const auto& option : options) {
            set_option(option.first, option.second);
        }

        // set xpress model parameters
        // go to C-API
        XPRSsetdblcontrol(model.getXPRSprob(), XPRS_MIPABSSTOP, this->abs_tol);
        XPRSsetdblcontrol(model.getXPRSprob(), XPRS_MIPRELSTOP, this->rel_tol);
        XPRSsetdblcontrol(model.getXPRSprob(), XPRS_MIPTOL, this->int_tol);
        XPRSsetintcontrol(model.getXPRSprob(), XPRS_MAXTIME, -(int)std::ceil(this->time_limit));
        XPRSsetintcontrol(model.getXPRSprob(), XPRS_MIPTHREADS, this->num_threads);
        XPRSsetdblcontrol(model.getXPRSprob(), XPRS_MIPADDCUTOFF, -std::min(1e-5, this->abs_tol));

        // TODO: check if presolve error is fixed in newer XPRESS versions
        // prevent it from performing any reductions (lead to false infeasibility in my tests)
        XPRSsetintcontrol(model.getXPRSprob(), XPRS_PRESOLVE, -1);
        /// xpress_callback cb(abort_optimization);
        /// model.setCallback(&cb);

        // optimize model and measure cpu time (approximate, not respecting threads)
        auto time_start = thread_clock::now();
        model.mipOptimize("");
        if(model.getMIPStat() == XPRB_MIP_INFEAS) {
            XPRSsetintcontrol(model.getXPRSprob(), XPRS_PRESOLVE, 0);
            model.mipOptimize("");
            XPRSsetintcontrol(model.getXPRSprob(), XPRS_PRESOLVE, -1);
        }
        auto time_end = thread_clock::now();
        rec.m_cpu = std::chrono::duration<double>(time_end - time_start).count();

        // read solver status
        std::cout << "Sucessfully optimized" << std::endl;
        set_record(model, rec, variables, id_manager);
        XPRSwriteprob(model.getXPRSprob(), (solver_name + std::string(".mps")).c_str(), "");

        return rec;
    } catch(const std::exception& e) {
        std::cout << "Xpress Error code = " << e.what() << std::endl;
        throw e;
    }
}

void xpress_solver::abort() { abort_optimization = true; }

// size_t
// strlen2(const char *str)
// {
//         const char *s;

//         for (s = str; *s; ++s)
//                 ;
//         return (s - str);
// }

bool xpress_solver::set_option(std::string option, double value) {
    // TODO: abs_tol, rel_tol ranges could be bigger
    if(option == "abs_tol") {
        if(value >= 1e-9) {
            abs_tol = value;
            return true;
        }
    } else if(option == "rel_tol") {
        if(value >= 1e-9) {
            rel_tol = value;
            return true;
        }
    } else if(option == "num_threads") {
        if(value >= 0) {
            num_threads = static_cast<int>(value);
            return true;
        }
    } else if(option == "lp_alg") {
        if(value >= -1 && value <= 5) {
            lp_alg = static_cast<int>(value);
            return true;
        }
    } else if(option == "int_tol") {
        if(value >= 1e-9 && value <= 1e-1) {
            int_tol = value;
            return true;
        }
    } else if(option == "non_convex") {
        if(value >= -1 && value <= 2) {
            non_convex = static_cast<int>(value);
            return true;
        }
    } else if(option == "time_limit") {
        if(value >= 0) {
            time_limit = value;
            return true;
        }
    } else if(option == "solution_limit") {
        if(value >= 1 && value <= 2000000000) {
            solution_limit = static_cast<int>(value);
            return true;
        }
    } else if(option == "int_focus") {
        if(value == 1) {
            int_focus = true;
            return true;
        } else if(value == 0) {
            int_focus = false;
            return true;
        }
    }
    return false;
}

bool xpress_solver::set_option(std::string option, std::string value) {
    if(option == "settings") {
        settings_file = std::move(value);
        return true;
    }
    return false;
}

double xpress_solver::get_option(std::string option) {
    throw std::invalid_argument("Option '" + option + "' not supported or implemented.");
}

} // namespace dips
