#include "ipopt_solver.hpp"

#include <cmath>

#include "IpIpoptApplication.hpp"
#include "fadiff.h"

#include "ipopt_problem.hpp"
#include "parameter_cloner.hpp"


namespace dips {

using namespace ale;

using DiffVarType = fadbad::F<fadbad::F<double>>;

ipopt_solver::ipopt_solver(symbol_table& symbols, const std::string& name) :
    solver(name),
    symbols(symbols),
    tol(1.0e-7),
    max_iter(5000),
    mu_strategy("adaptive"),
    output_file("ipopt.out"),
    derivative_test("none"),
    scaling_factor(1.0) {};

//TODO: named ipopt to avoid clash with Highs
void set_solution_variables_ipopt(solve_record& rec, const ScalarVariableIdManager& id_manager, const std::vector<double>& variables) {
    id_manager.set_record<double>([](double x) {
        return x;
    },
      rec, variables);
}

void set_record(const Ipopt::ApplicationReturnStatus& status, solve_record& rec,
  const Ipopt::SmartPtr<IpoptProblem<DiffVarType>>& mynlp, std::vector<double>& variables_out) {
    switch(status) {
        case Ipopt::Solve_Succeeded: {
            // TODO: is this intended behaviour for this solver or was it task-specific
            // LOCAL would be correct, but we want to pretent that Ipopt solves globally
            rec.m_status = GLOBAL;

            auto [obj_value, id_manager, variables] = mynlp->get_solution();
            rec.m_lbd = obj_value;
            rec.m_ubd = obj_value;
            set_solution_variables_ipopt(rec, id_manager, variables);

            variables_out = variables;

            break;
        }
        case Ipopt::Infeasible_Problem_Detected: {
            rec.m_status = INFEASIBLE;
            rec.m_lbd = std::numeric_limits<double>::infinity();
            rec.m_ubd = std::numeric_limits<double>::infinity();
            break;
        }
        default:
            rec.m_status = ABNORMAL;
            break;
    }
}

solve_record ipopt_solver::solve(program prog) {
    solve_record rec;

    // find all variables that are used in the program statement
    auto used_variables = find_variables(prog, symbols);

    // add parameter of correct shape to solve_record
    for(const auto& var_name : used_variables) {
        add_variable_to_solve_record(var_name, symbols, rec);
    }

    // construct model
    Ipopt::SmartPtr mynlp(new IpoptProblem<DiffVarType>(prog, symbols, convex_qcqp));
    Ipopt::SmartPtr app(IpoptApplicationFactory());

    // set options saved in pass_option of calling context
    for(auto it = options.begin(); it != options.end(); ++it) {
        this->set_option(it->first, it->second);
    }

    // configure Ipopt app
    app->Options()->SetNumericValue("tol", tol);
    app->Options()->SetIntegerValue("max_iter", max_iter);
    // app->Options()->SetStringValue("mu_strategy", mu_strategy);
    app->Options()->SetStringValue("output_file", output_file);
    app->Options()->SetStringValue("nlp_scaling_method", "none");
    app->Options()->SetStringValue("derivative_test", "second-order"); //derivative_test);
    app->Options()->SetNumericValue("obj_scaling_factor", scaling_factor);
    app->Options()->SetIntegerValue("print_level", 3);
    // app->Options()->SetStringValue("print_timing_statistics", "yes");

    // Making the following selectable will require implementation of eval_h in ipopt_problem!
    app->Options()->SetStringValue("hessian_approximation", "exact");
    //app->Options()->SetStringValue("hessian_approximation", "limited-memory");
    // app->Options()->SetStringValue("limited_memory_update_type", "bfgs");
    if(this->convex_qcqp) {
        app->Options()->SetStringValue("hessian_approximation", "exact");
        app->Options()->SetStringValue("jac_c_constant", "yes");
        app->Options()->SetStringValue("least_square_init_primal", "yes"); //ignore initial point
        app->Options()->SetStringValue("mehrotra_algorithm", "yes");       // TODO: test
    }

    auto status = app->Initialize();
    if(status != Ipopt::Solve_Succeeded) {
        std::cout << "\n\n*** Ipopt Error during initialization!" << std::endl;
        rec.m_status = ABNORMAL;
        return rec;
    }

    if(old_solution_set) {
        Ipopt::Index n;
        Ipopt::Index m;
        Ipopt::Index dummy;
        Ipopt::TNLP::IndexStyleEnum a;
        mynlp->get_nlp_info(n, m, dummy, dummy, a);
        std::vector<double> x_l(n);
        std::vector<double> x_u(n);
        std::vector<double> g(m);
        mynlp->get_bounds_info(n, x_l.data(), x_u.data(), m, g.data(), g.data());
        mynlp->set_bounds_and_starting_point(x_l, x_u, old_solution);
    }

    // optimize model
    status = app->OptimizeTNLP(mynlp);

    // set solver status
    std::vector<double> variables;
    set_record(status, rec, mynlp, variables);
    if(rec.m_status == GLOBAL || rec.m_status == LOCAL) {
        old_solution = variables;
        old_solution_set = true;
    }

    return rec;
}

bool ipopt_solver::set_option(std::string opt, double val) {
    if(opt == "tolerance") {
        tol = val;
        return true;
    }
    if(opt == "scaling_factor") {
        scaling_factor = val;
        return true;
    }
    if(opt == "max_iter") {
        if(std::trunc(val) != val) {
            return false;
        }
        max_iter = std::round(val);
        return true;
    }
    return false;
}

bool ipopt_solver::set_option(std::string opt, std::string val) {
    if(opt == "hessian_approximation") {
        throw std::runtime_error(
          " Error: Changing Ipopt Hessian Approximation will require changes to Ipopt_problem eval_h!\n");
        return false;
    }
    if(opt == "limited_memory_update_type") {
        throw std::runtime_error(
          " Error: Changing Ipopt Hessian Approximation will require changes to Ipopt_problem eval_h!\n");
        return false;
    }
    if(opt == "mu_strategy") {
        mu_strategy = val;
        return true;
    }
    if(opt == "output_file") {
        output_file = val;
        return true;
    }
    if(opt == "derivative_test") {
        derivative_test = val;
        return true;
    }
    if(opt == "problem_class") {
        if(val == "convex_qcqp") {
            convex_qcqp = true;
            return true;
        }
    }
    return false;
}

double ipopt_solver::get_option(std::string) {
    throw std::runtime_error(" Error: Ipopt solver currently does not implement method get_option!\n");
}

} // namespace dips
