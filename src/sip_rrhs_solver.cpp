#include "sip_rrhs_solver.hpp"

#include <fstream>   // for operator<<, basic_ostream, basic_ostream::operator<<, basic_ostream<>::__ostream_type, basic_ios::rdbuf, endl, basic_o...
#include <memory>    // for unique_ptr, allocator_traits<>::value_type
#include <utility>   // for pair, make_pair, move
#include <iostream>  // for cout
#include <limits>    // for numeric_limits
#include <stdexcept> // for invalid_argument

#include "common.hpp"        // for copy_variables, discretize, fix_variables, set_real, scoped_delayed_function_calls
#include "symbol_table.hpp"  // for symbol_table
#include "output.hpp"        // to_json, from_json
#include "nlohmann/json.hpp" // for json

#ifdef DIPS_use_mpi
#include "mpi.h"
#endif

namespace dips {


sip_rrhs_program_definition_parser::sip_rrhs_program_definition_parser(std::istream& is, symbol_table& symbols) :
    programdefinition_parser_chain(is, symbols, { { "lbp", {} }, { "ubp", { "eps_res" } } }) { }

void sip_rrhs_solver::program_info::add_llp(const program& llp, const discretization& lbp_disc, const discretization& ubp_disc) {
    llps.emplace_back(llp);
    lbp_discs.emplace_back(lbp_disc);
    ubp_discs.emplace_back(ubp_disc);
}


solve_record sip_rrhs_solver::solve(program_info prog) {
    std::unique_ptr<solver> sub(make_subsolver(symbols, solver_base_name));

    for(auto& option : options) {
        sub->pass_option(option.first, option.second);
    }

    abort_subsolver_callbacks.emplace_back([&sub = sub]() { sub->abort(); });
    scoped_callback_vector_clearer scope(abort_subsolver_callbacks);

    solve_record rec;
    solve_record lbp_rec;
    solve_record ubp_rec;
    size_t n_llps = prog.llps.size();
    std::vector<solve_record> llp_rec(n_llps);

    enum { LBP,
        UBP,
        RECOVER,
        NDEF } next_problem
      = LBP;

    if(ubp_guard <= 0 && init_res_dynamic == true) {
        throw std::invalid_argument("invalid combination of settings: ubp_guard <= 0 and init_res_dynamic = true.");
    }
    if(init_res_dynamic == true) {
        init_res = std::numeric_limits<double>::quiet_NaN();
    }
    double feas_vio_lbp = std::numeric_limits<double>::infinity(); // feasibility violation of lbp
    double feas_vio_ubp = std::numeric_limits<double>::infinity(); // feasibility violation of ubp (always negative)
    eps_res = init_res;                                            // set initial eps_res
    bool set_init_res_dynamic = init_res_dynamic;                  // flag to only set init_res dynamically once


    for(int i = 0; i < max_iter; i++) {
        scoped_push sp_it(sub->solver_name, "it_" + std::to_string(i + 1));

        std::cout << "\nIteration " << i + 1 << ":\n";
        std::cout << "LBD           = " << rec.m_lbd << '\n';
        std::cout << "feas.vio. LBD = " << feas_vio_lbp << '\n';
        std::cout << "UBD           = " << rec.m_ubd << '\n';
        std::cout << "feas.vio. UBD = " << feas_vio_ubp << '\n';
        std::cout << "eps_res       = " << eps_res << "\n\n";

        // check termination
        if(check_termination(rec, feas_vio_ubp, abs_tol, rel_tol, feas_tol)) {
            return rec;
        }


        if(check_aborted(max_time, rec, solver_base_name, aborted)) {

            return rec;
        }

        if(next_problem == LBP) {
            scoped_push sp_lbp(sub->solver_name, "_lbp");
            std::cout << "\nsip: solving lbp\n";

            sub->set_option("abs_tol", abs_tol_lbp);
            sub->set_option("rel_tol", rel_tol_lbp);
            sub->set_option("settings", settings_lbp);
            sub->set_option("max_time", max_time - rec.m_cpu);

            lbp_rec = sub->solve(prog.lbp);

            rec.m_cpu += lbp_rec.m_cpu;

            std::cout << "reporting status\n";
            report(lbp_rec);

            if(check_aborted(max_time, rec, solver_base_name, aborted)) {

                return rec;
            }

            switch(lbp_rec.m_status) {
                case INFEASIBLE:
                    // lbp proves sip to be infeasible
                    rec.m_status = INFEASIBLE;
                    rec.m_lbd = std::numeric_limits<double>::infinity();
                    rec.m_ubd = std::numeric_limits<double>::infinity();

                    return rec;
                case GLOBAL:
                    // computed valid lbd
                    rec.m_lbd = lbp_rec.m_lbd;
                    break;
                default:
                    throw std::invalid_argument("lbp: unhandled solver status: "
                                                + to_string(lbp_rec.m_status));
            }

            bool lbp_feasible = true;
            double feas_vio_temp = -std::numeric_limits<double>::infinity();
            for(int j = 0; j < n_llps; ++j) {

                if(check_aborted(max_time, rec, solver_base_name, aborted)) {

                    return rec;
                }
                scoped_push sp_lbp_llp(sub->solver_name, "_llp_" + std::to_string(j + 1));
                std::cout << "sip: solving lbp_llp_" << j + 1 << '\n';
                // note: always minimizing. This leads to
                //  - flipped sign of objective
                //  - flipped lbd and ubd

                symbols.push_scope();
                fix_variables(lbp_rec, symbols);

                sub->set_option("abs_tol", abs_tol_lbp_llp);
                sub->set_option("rel_tol", rel_tol_lbp_llp);
                sub->set_option("settings", settings_lbp_llp);
                sub->set_option("max_time", max_time - rec.m_cpu);

                llp_rec[j] = sub->solve(prog.llps[j]);

                rec.m_cpu += llp_rec[j].m_cpu;
                symbols.pop_scope();

                std::cout << "reporting status\n";
                report(llp_rec[j]);

                if(check_aborted(max_time, rec, solver_base_name, aborted)) {

                    return rec;
                }

                switch(llp_rec[j].m_status) {
                    case INFEASIBLE:
                        // by convention: llp_rec.m_ubd = llp_rec.m_lbd = + infty
                        // => -llp_rec.m_lbd <= feas_tol
                        // => incumbent point in llp[j] sip feasible
                        // => no tolerance update necessary
                        break;
                    case GLOBAL:
                        if(-llp_rec[j].m_lbd <= feas_tol) {
                            // => incumbent point in llp[j] sip feasible
                            // => nothing happens here
                            // => compute llp[j+1]
                        } else {
                            // -llp_rec.m_lbd > feas_tol
                            // incumbent point of lbp might be sip infeasible
                            lbp_feasible = false;
                        }
                        // tolerance update if necessary
                        if(-llp_rec[j].m_lbd > 0 && abs_tol_lbp_llp > -llp_rec[j].m_lbd / red_tol_lbp_llp) {
                            set_tolerance_robust("abs_tol_lbp_llp", -llp_rec[j].m_lbd / red_tol_lbp_llp);
                        }
                        break;
                    default:
                        throw std::invalid_argument("lbp_llp_" + std::to_string(j + 1)
                                                    + ": unhandled solver status: " + to_string(llp_rec[j].m_status));
                }

                // keeping track of max feasibility violation
                if(-llp_rec[j].m_lbd > feas_vio_temp) {
                    feas_vio_temp = -llp_rec[j].m_lbd;
                }
            }

            feas_vio_lbp = feas_vio_temp;

            // check termination
            if(lbp_feasible) {
                // => incumbent point in all llps sip feasible
                feas_vio_ubp = feas_vio_lbp;
                std::cout << "\nlbp furnished a point which is within llp feas. tolerance. Terminating.\n";
                std::cout << "Feas. vio.:   " << feas_vio_ubp << " <= feas_tol = " << feas_tol << std::endl;
                rec.m_status = GLOBAL;
                rec.m_lbd = lbp_rec.m_lbd;
                rec.m_ubd = lbp_rec.m_ubd;
                copy_variables(lbp_rec, rec);

                return rec;
            } else {
                // discretize
                for(int j = 0; j < n_llps; ++j) {
                    if(discr_all || -llp_rec[j].m_ubd > 0) {
                        // discretize always or only if llp[j] produced a valid cut
                        discretize(prog.lbp_discs[j], llp_rec[j], symbols);
                        if(ubp_guard > 0 && feas_vio_lbp > ubp_guard) {
                            discretize(prog.ubp_discs[j], llp_rec[j], symbols);
                        }
                        std::cout << "discretized llp_" << j + 1 << ".\n";
                    }
                }
                if(ubp_guard <= 0 || feas_vio_lbp <= ubp_guard) {
                    next_problem = UBP;
                    if(set_init_res_dynamic) {
                        eps_res = feas_vio_lbp;
                        std::cout << "setting eps_res = max(feas_vio_lbp, min_eps_res) = max(" << feas_vio_lbp << ", " << min_eps_res << ") = " << eps_res << '\n';
                        set_init_res_dynamic = false;
                    }
                } else {
                    next_problem = LBP;
                    std::cout << "skipped UBP because feas_vio_lbp > ubp_guard";
                }
            }

        } else if(next_problem == UBP) {
            scoped_push sp_upb(sub->solver_name, "_ubp");
            std::cout << "\nsip: solving ubp\n";

            set_real(prog.ubp_restrict, eps_res, symbols);

            sub->set_option("abs_tol", abs_tol_ubp);
            sub->set_option("rel_tol", rel_tol_ubp);
            sub->set_option("settings", settings_ubp);
            sub->set_option("max_time", max_time - rec.m_cpu);

            ubp_rec = sub->solve(prog.ubp);

            rec.m_cpu += ubp_rec.m_cpu;

            std::cout << "reporting status\n";
            report(ubp_rec);


            if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                return rec;
            }

            switch(ubp_rec.m_status) {
                case INFEASIBLE:
                    // ubp infeasible, eps_res chosen too large
                    // => reduce eps_res
                    // => end iteration and go to LBP
                    eps_res /= red_res;
                    if(eps_res < min_eps_res) {
                        std::cout << "failure: eps_res = " << eps_res
                                  << " reduced below min_eps_res = "
                                  << min_eps_res
                                  << ". A reduction of min_eps_res (however unlikely) might help."
                                  << " Trying to recover.\n";
                        next_problem = RECOVER;
                        continue;
                    }
                    next_problem = LBP;
                    continue;
                case GLOBAL:
                    // computed candidate point for ubd
                    // => nothing happens here
                    // => compute llps
                    break;
                default:
                    throw std::invalid_argument("ubp: unhandled solver status: "
                                                + to_string(ubp_rec.m_status));
            }

            // tolerance update for llp if necessary
            if(abs_tol_ubp_llp >= eps_res) {
                set_tolerance_robust("abs_tol_ubp_llp", eps_res / red_tol_ubp_llp);
            }

            bool ubp_feasible = true;
            double feas_vio_temp = -std::numeric_limits<double>::infinity();
            for(int j = 0; j < n_llps; ++j) {

                if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                    return rec;
                }
                scoped_push sp_ubp_llp(sub->solver_name, "_llp_" + std::to_string(j + 1));
                std::cout << "sip: solving ubp_llp_" << j + 1 << '\n';
                // note: always minimizing. This leads to
                //  - flipped sign of objective
                //  - flipped lbd and ubd

                symbols.push_scope();
                fix_variables(ubp_rec, symbols);

                sub->set_option("abs_tol", abs_tol_ubp_llp);
                sub->set_option("rel_tol", rel_tol_ubp_llp);
                sub->set_option("settings", settings_lbp_llp);
                sub->set_option("max_time", max_time - rec.m_cpu);

                llp_rec[j] = sub->solve(prog.llps[j]);

                rec.m_cpu += llp_rec[j].m_cpu;
                symbols.pop_scope();

                std::cout << "reporting status\n";
                report(llp_rec[j]);


                if(check_aborted(max_time, rec, solver_base_name, aborted)) {

                    return rec;
                }

                switch(llp_rec[j].m_status) {
                    case INFEASIBLE:
                        // by convention: llp_rec.m_ubd = llp_rec.m_lbd = + infty
                        // => -llp_rec.m_lbd <= feas_tol
                        // => incumbent point in llp[j] sip feasible
                        break;
                    case GLOBAL:
                        if(-llp_rec[j].m_lbd <= feas_tol) {
                            // => incumbent point in llp[j] sip feasible
                            // => nothing happens here
                            // => compute llp[j+1]
                        } else {
                            // -llp_rec.m_lbd > feas_tol
                            // incumbent point of lbp might be sip infeasible
                            ubp_feasible = false;
                            // tolerance update if necessary
                            if(-llp_rec[j].m_lbd > 0 && abs_tol_ubp_llp > -llp_rec[j].m_lbd / red_tol_ubp_llp) {
                                set_tolerance_robust("abs_tol_ubp_llp", -llp_rec[j].m_lbd / red_tol_ubp_llp);
                            }
                        }
                        break;
                    default:
                        throw std::invalid_argument("ubp_llp_" + std::to_string(j + 1)
                                                    + ": unhandled solver status: " + to_string(llp_rec[j].m_status));
                }

                // keeping track of max feasibility violation
                if(-llp_rec[j].m_lbd > feas_vio_temp) {
                    feas_vio_temp = -llp_rec[j].m_lbd;
                }
            }

            if(ubp_feasible) {
                feas_vio_ubp = feas_vio_temp;
                rec.m_status = FEASIBLE;
                if(ubp_rec.m_ubd < rec.m_ubd) {
                    rec.m_ubd = ubp_rec.m_ubd;
                    copy_variables(ubp_rec, rec);
                }
                eps_res /= red_res;
                if(eps_res < min_eps_res) {
                    std::cout << "failure: eps_res = " << eps_res
                              << " reduced below min_eps_res = "
                              << min_eps_res
                              << ". A reduction of min_eps_res (however unlikely) might help."
                              << " Trying to recover.\n";
                    next_problem = RECOVER;
                    continue;
                }
                next_problem = LBP;
            } else {
                // discretize
                for(int j = 0; j < n_llps; ++j) {
                    if(discr_all || !(-llp_rec[j].m_lbd <= 0)) {
                        // discretize always or only if llp[j] produced a valid cut
                        discretize(prog.ubp_discs[j], llp_rec[j], symbols);
                        std::cout << "discretized llp_" << j + 1 << ".\n";
                    }
                }
                next_problem = LBP;
            }
        }

        else if(next_problem == RECOVER) {
            // trying to recover by solving ubp once with eps_res = 0
            // this is equivalent to solving the lbp with the discretization of the ubp
            scoped_push sp_recover(sub->solver_name, "_recover");
            std::cout << "\nsip: solving recover (constructed from ubp)\n";

            std::cout << "setting eps_res = 0.\n";
            set_real(prog.ubp_restrict, 0, symbols);

            sub->set_option("abs_tol", abs_tol_ubp);
            sub->set_option("rel_tol", rel_tol_ubp);
            sub->set_option("settings", settings_ubp);
            sub->set_option("max_time", max_time - rec.m_cpu);

            auto recover_rec = sub->solve(prog.ubp);

            rec.m_cpu += recover_rec.m_cpu;

            std::cout << "reporting status\n";
            report(recover_rec);

            if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                return rec;
            }

            switch(recover_rec.m_status) {
                case INFEASIBLE:
                    if(rec.m_status == FEASIBLE) {
                        // a feasible point has already been found but the recovery problem declared sip to be infeasible -> this should not happen
                        std::cout << "error: a feasible point has been already found but the recovery step declared sip to be infeasible. This should not happen!\n";
                        rec.m_status = ABNORMAL;
                        return rec;
                    } else {
                        std::cout << "success: the recovery step declared sip to be infeasible.\n";
                        rec.m_status = INFEASIBLE;
                        return rec;
                    }
                case GLOBAL:
                    // computed candidate point
                    // => nothing happens here
                    // => compute llps
                    break;
                default:
                    throw std::invalid_argument("recover: unhandled solver status: "
                                                + to_string(recover_rec.m_status));
            }

            bool recover_feasible = true;
            double feas_vio_temp = -std::numeric_limits<double>::infinity();
            for(int j = 0; j < n_llps; ++j) {
                if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                    return rec;
                }
                scoped_push sp_recover_llp(sub->solver_name, "_llp_" + std::to_string(j + 1));
                std::cout << "sip: solving recover_llp_" << j + 1 << '\n';
                // note: always minimizing. This leads to
                //  - flipped sign of objective
                //  - flipped lbd and ubd

                symbols.push_scope();
                fix_variables(recover_rec, symbols);

                sub->set_option("abs_tol", abs_tol_ubp_llp);
                sub->set_option("rel_tol", rel_tol_ubp_llp);
                sub->set_option("settings", settings_lbp_llp);
                sub->set_option("max_time", max_time - rec.m_cpu);

                llp_rec[j] = sub->solve(prog.llps[j]);

                rec.m_cpu += llp_rec[j].m_cpu;
                symbols.pop_scope();

                std::cout << "reporting status\n";
                report(llp_rec[j]);

                if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                    return rec;
                }

                switch(llp_rec[j].m_status) {
                    case INFEASIBLE:
                        // by convention: llp_rec.m_ubd = llp_rec.m_lbd = + infty
                        // => -llp_rec.m_lbd <= feas_tol
                        // => incumbent point in llp[j] sip feasible
                        break;
                    case GLOBAL:
                        if(-llp_rec[j].m_lbd <= feas_tol) {
                            // => incumbent point in llp[j] sip feasible
                            // => nothing happens here
                            // => compute llp[j+1]
                        } else {
                            // -llp_rec.m_lbd > feas_tol
                            // incumbent point of lbp might be sip infeasible
                            recover_feasible = false;
                        }
                        break;
                    default:
                        throw std::invalid_argument("recover_llp_" + std::to_string(j + 1)
                                                    + ": unhandled solver status: " + to_string(llp_rec[j].m_status));
                }

                // keeping track of max feasibility violation
                if(-llp_rec[j].m_lbd > feas_vio_temp) {
                    feas_vio_temp = -llp_rec[j].m_lbd;
                }
            }


            // check termination
            if(recover_feasible) {
                // => incumbent point in all llps sip feasible
                feas_vio_ubp = feas_vio_temp;
                std::cout << "\nsuccess: recover furnished a point which is within llp feas. tolerance. Terminating.\n";
                std::cout << "Feas. vio.:   " << feas_vio_ubp << " <= feas_tol = " << feas_tol << std::endl;
                rec.m_status = GLOBAL;
                rec.m_lbd = recover_rec.m_lbd;
                rec.m_ubd = recover_rec.m_ubd;
                copy_variables(recover_rec, rec);
                return rec;
            }

            // check termination
            if(check_termination(rec, feas_vio_ubp, abs_tol, rel_tol, feas_tol)) {
                return rec;
            }

            std::cout << "\nfailure: an assumption is likely violated. Aborting...\n";
            rec.m_status = ASSUMPTION_VIOLATION_LIKELY;
            return rec;
        }

        if(next_problem == NDEF) {
            throw std::invalid_argument("procedure not implemented");
        }


        // check termination
        if(check_termination(rec, feas_vio_ubp, abs_tol, rel_tol, feas_tol)) {
            return rec;
        }

#ifdef DIPS_use_mpi
        int _rank;
        MPI_Comm_rank(MPI_COMM_WORLD, &_rank);
        if(_rank == 0) {
#endif
            // write discretization points to output file
            std::string disc_output_path = "./";
            for(auto& i : sub->solver_name) {
                disc_output_path += i;
            }
            std::ofstream disc_output(disc_output_path + "_disc_points.txt");
            std::streambuf* backup = std::cout.rdbuf();
            std::cout.rdbuf(disc_output.rdbuf());
            std::vector<std::vector<discretization>> discs;
            discs.emplace_back(prog.lbp_discs);
            discs.emplace_back(prog.ubp_discs);
            report(symbols, discs);
            std::cout.rdbuf(backup); // reassign cout to its former buffer before that buffer no longer exists
            disc_output.close();     // close file output
#ifdef DIPS_use_mpi
        }
        MPI_Barrier(MPI_COMM_WORLD);
#endif
    }

    rec.m_status = EXCEEDED_MAX_ITERATIONS;
    std::cout << solver_base_name << ": total number of iterations exceeded limit."
              << "Rising max_it might help\n";
    return rec;
}


bool sip_rrhs_solver::set_option(const std::string& option, double value) {
    if(option == "feas_tol") {
        if(value >= 0) {
            feas_tol = value;
            return true;
        }
    }
    if(option == "abs_tol") {
        if(value >= 1e-9) {
            abs_tol = value;
            return true;
        }
    }
    if(option == "rel_tol") {
        if(value >= 1e-9) {
            rel_tol = value;
            return true;
        }
    }
    if(option == "abs_tol_ubp") {
        if(value >= 1e-9) {
            abs_tol_ubp = value;
            return true;
        }
    }
    if(option == "rel_tol_ubp") {
        if(value >= 1e-9) {
            rel_tol_ubp = value;
            return true;
        }
    }
    if(option == "abs_tol_ubp_llp") {
        if(value >= 1e-9) {
            abs_tol_ubp_llp = value;
            return true;
        }
    }
    if(option == "rel_tol_ubp_llp") {
        if(value >= 1e-9) {
            rel_tol_ubp_llp = value;
            return true;
        }
    }
    if(option == "abs_tol_lbp") {
        if(value >= 1e-9) {
            abs_tol_lbp = value;
            return true;
        }
    }
    if(option == "rel_tol_lbp") {
        if(value >= 1e-9) {
            rel_tol_lbp = value;
            return true;
        }
    }
    if(option == "abs_tol_lbp_llp") {
        if(value >= 1e-9) {
            abs_tol_lbp_llp = value;
            return true;
        }
    }
    if(option == "rel_tol_lbp_llp") {
        if(value >= 1e-9) {
            rel_tol_lbp_llp = value;
            return true;
        }
    }
    if(option == "red_tol_lbp_llp") {
        if(value > 1) {
            red_tol_lbp_llp = value;
            return true;
        }
    }
    if(option == "red_tol_ubp_llp") {
        if(value > 1) {
            red_tol_ubp_llp = value;
            return true;
        }
    }
    if(option == "max_iter") {
        if(value >= 1) {
            max_iter = (int)value;
            return true;
        }
    }
    if(option == "max_time") {
        if(value >= 0) {
            max_time = value;
            return true;
        }
    }
    if(option == "init_res") {
        if(value > 0) {
            init_res = value;
            return true;
        }
    }
    if(option == "red_res") {
        if(value > 1) {
            red_res = value;
            return true;
        }
    }
    if(option == "min_eps_res") {
        if(value >= 0) {
            min_eps_res = value;
            return true;
        }
    }
    if(option == "ubp_guard") {
        if(value > 0 || value == -1) {
            ubp_guard = value;
            return true;
        }
    }
    std::cout << "Failed to set option " << option << " = " << value << '\n';
    return false;
}

bool sip_rrhs_solver::set_option(const std::string& option, const std::string& value) {
    if(option == "settings_lbp") {
        settings_lbp = value;
        return true;
    }
    if(option == "settings_ubp") {
        settings_ubp = value;
        return true;
    }
    if(option == "settings_lbp_llp") {
        settings_lbp_llp = value;
        return true;
    }
    if(option == "settings_ubp_llp") {
        settings_ubp_llp = value;
        return true;
    }
    std::cout << "Failed to set option " << option << " = " << value << '\n';
    return false;
}

bool sip_rrhs_solver::set_option(const std::string& option, bool value) {
    if(option == "discr_all") {
        discr_all = value;
        return true;
    }
    if(option == "init_res_dynamic") {
        init_res_dynamic = value;
        return true;
    }
    std::cout << "Failed to set option " << option << " = " << value << '\n';
    return false;
}
void sip_rrhs_solver::pass_option(const std::string& option, double value) {
    options.emplace_back(std::make_pair(option, value));
}

double sip_rrhs_solver::get_option(const std::string& option) const {
    if(option == "abs_tol") {
        return abs_tol;
    }
    if(option == "rel_tol") {
        return rel_tol;
    }
    if(option == "feas_tol") {
        return feas_tol;
    }
    throw std::invalid_argument("Option '" + option + "' not supported or implemented.");
}

void sip_rrhs_solver::set_solver_base_name(std::string name) {
    solver_base_name = std::move(name);
}

std::string sip_rrhs_solver::get_solver_base_name() {
    return solver_base_name;
}

} // namespace dips
