#include "cplex_solver.hpp"

#include <stddef.h>      // for size_t
#include <array>         // for array
#include <iostream>      // for operator<<, endl, basic_ostream, basic_ostream<>::__ostream_type, cout, ostream
#include <limits>        // for numeric_limits
#include <list>          // for list, operator!=, _List_iterator, _List_iterator<>::_Self
#include <set>           // for set, operator!=, _Rb_tree_const_iterator, _Rb_tree_const_iterator<>::_Self
#include <stdexcept>     // for invalid_argument, runtime_error
#include <unordered_map> // for unordered_map
#include <variant>       // for variant
#include <vector>        // for vector

#include "quad_expr_evaluator.hpp" // for QuadConstraintList, QuadConstraintType, QuadConstraint, evaluate_quadratic, evaluate_quadratic_constraint, Quad...
#include "parameter_cloner.hpp"    // for add_variable_to_solve_record
#include "symbol_finder.hpp"       // for find_variables
#include "QuadExpr.hpp"            // for QuadExpr, PureQuadExpr, LinExpr
#include "scalar_evaluator.hpp"    // for ScalarVariableIdManager

namespace dips {

void set_solution_variables(solve_record& rec, const ScalarVariableIdManager& id_manager, const std::vector<IloNumVar>& ilo_variable_list, IloCplex& cplex) {
    id_manager.set_record<IloNumVar>([&cplex](IloNumVar var) {
        return cplex.isExtracted(var) ? cplex.getValue(var) : 0.0;
    },
      rec, ilo_variable_list);
}

IloExpr convert_expression(IloModel& model, const QuadExpr& expr, const std::vector<IloNumVar>& ilo_variable_list) {
    const LinExpr& linearPart = expr.getLinearPart();
    const PureQuadExpr& quadraticPart = expr.getQuadraticPart();

    // initialize expression with constant term
    IloExpr ilo_expr(model.getEnv(), expr.getConstant());

    // add linear terms
    for(const unsigned& var_id : linearPart.get_non_zero_ids()) {
        double coeff = linearPart.get_value(var_id);
        ilo_expr.setLinearCoef(ilo_variable_list.at(var_id), coeff);
    }

    // add quadratic terms
    for(unsigned i = 0; i < quadraticPart.get_non_zero_ids().size(); i++) {
        const auto& indices = quadraticPart.get_non_zero_ids().at(i);
        ilo_expr.setQuadCoef(ilo_variable_list.at(indices.first), ilo_variable_list.at(indices.second), quadraticPart.get_non_zero_values().at(i));
    }

    return ilo_expr;
}

IloRange convert_constraint(IloModel& model, const QuadConstraint& constr, const std::vector<IloNumVar>& ilo_variable_list) {
    auto expr = convert_expression(model, constr.getExpr(), ilo_variable_list);

    switch(constr.getType()) {
        case QuadConstraintType::EQUAL:
            return IloRange(model.getEnv(), 0, expr, 0);
        case QuadConstraintType::LESS_EQUAL:
            return IloRange(model.getEnv(), -IloInfinity, expr, 0);
        case QuadConstraintType::GREATER_EQUAL:
            return IloRange(model.getEnv(), 0, expr, IloInfinity);
        case QuadConstraintType::NOT_EQUAL:
            throw std::invalid_argument("Constraints of type '!=' are not allowed");
        case QuadConstraintType::LESS:
            throw std::invalid_argument("Constraints of type '<' are not allowed");
        case QuadConstraintType::GREATER:
            throw std::invalid_argument("Constraints of type '>' are not allowed");
        default:
            throw std::invalid_argument("unexpected QuadConstraintType");
    }
}

void set_objective(IloModel& model, const QuadExpr& expr, const std::vector<IloNumVar>& ilo_variable_list) {
    model.add(IloObjective(model.getEnv(), convert_expression(model, expr, ilo_variable_list), IloObjective::Minimize));
}

void add_quad_constraints(IloModel& model, const QuadConstraintList& constr_list, const std::vector<IloNumVar>& ilo_variable_list, const std::string& comment) {
    for(const QuadConstraint& constr : constr_list) {
        model.add(convert_constraint(model, constr, ilo_variable_list));
    }
}

solve_record cplex_solver::solve(program prog) {
    std::lock_guard<std::mutex> solve_lock(solve_mutex);

    solve_record rec;

    IloEnv env;

    // find all variables that are used in the program statement
    auto used_variables = find_variables(prog.m_objective.front(), symbols);
    for(auto& constr : prog.m_constraints) {
        auto constr_vars = find_variables(constr, symbols);
        used_variables.insert(constr_vars.begin(), constr_vars.end());
    }

    // add parameter of correct shape to solve_record
    for(const auto& var_name : used_variables) {
        add_variable_to_solve_record(var_name, symbols, rec);
    }

    // evaluate expressions
    ScalarVariableIdManager id_manager;
    id_manager.set_ids(used_variables, symbols);

    std::vector<QuadConstraintList> quad_constraint_lists;

    auto obj = evaluate_quadratic(prog.m_objective.front(), symbols, id_manager);
    for(auto& constr : prog.m_constraints) {
        auto evaluated_constr = evaluate_quadratic_constraint(constr, symbols, id_manager);
        quad_constraint_lists.push_back(evaluated_constr);
    }

    // add variables to environment
    auto add_variable = [&env](double lb, double ub, double init, bool isInteger, const std::string& name, const std::string& comment, const std::vector<size_t>& indexes) {
        return IloNumVar(env, lb, ub, isInteger ? IloNumVar::Int : IloNumVar::Float);
    };
    auto ilo_variable_list = id_manager.map_over_variable_symbols<IloNumVar>(add_variable, symbols);

    // add objective and constraints to a model
    IloModel model(env);
    set_objective(model, obj, ilo_variable_list);
    for(auto& constr_list : quad_constraint_lists) {
        add_quad_constraints(model, constr_list, ilo_variable_list, ""); // TODO: parse comment again
    }

    // solve model
    IloCplex cplex(model);

    try {
        cplex.readParam(settings_file.data());
    } catch(const IloCplex::Exception& e) {
        if(e.getStatus() != CPXERR_FAIL_OPEN_READ) {
            std::cout << "CPLEX error code = " << e.getStatus() << std::endl;
            throw std::runtime_error(e.getMessage());
        }
    }

    cplex.use(make_abort_callback(env, aborted));
    cplex.setOut(env.getNullStream());
    cplex.setWarning(env.getNullStream());
    cplex.setParam(IloCplex::Param::ClockType, 1);
    cplex.setParam(IloCplex::Param::Threads, num_threads);
    cplex.setParam(IloCplex::RootAlg, lp_alg);
    cplex.setParam(IloCplex::Param::MIP::Tolerances::AbsMIPGap, abs_tol);
    cplex.setParam(IloCplex::Param::MIP::Tolerances::MIPGap, rel_tol);
    cplex.setParam(IloCplex::Param::MIP::Tolerances::Integrality, int_tol);
    //cplex.setParam(IloCplex::Param::Emphasis::Numerical,true);

    if(aborted) {
        rec.m_status = ABORTED;
        env.end();
        return rec;
    }

    double start = cplex.getCplexTime();
    cplex.solve();
    double end = cplex.getCplexTime();

    rec.m_cpu = end - start;

    // read solver status
    IloAlgorithm::Status stat = cplex.getStatus();
    switch(stat) {
        case IloAlgorithm::Unbounded:
            rec.m_status = UNBOUNDED;
            rec.m_lbd = -std::numeric_limits<double>::infinity();
            rec.m_ubd = -std::numeric_limits<double>::infinity();
            break;
        case IloAlgorithm::Optimal:
            rec.m_status = GLOBAL;
            {
                if(cplex.isMIP()) {
                    rec.m_lbd = cplex.getBestObjValue();
                } else {
                    rec.m_lbd = cplex.getObjValue();
                }
                rec.m_ubd = cplex.getObjValue();

                set_solution_variables(rec, id_manager, ilo_variable_list, cplex);
                break;
            }
        case IloAlgorithm::Feasible:
            rec.m_status = FEASIBLE;
            {
                if(cplex.isMIP()) {
                    rec.m_lbd = cplex.getBestObjValue();
                } else {
                    rec.m_lbd = -std::numeric_limits<double>::infinity();
                }
                rec.m_ubd = cplex.getObjValue();

                set_solution_variables(rec, id_manager, ilo_variable_list, cplex);
                break;
            }
        case IloAlgorithm::Infeasible:
            rec.m_status = INFEASIBLE;
            rec.m_lbd = std::numeric_limits<double>::infinity();
            rec.m_ubd = std::numeric_limits<double>::infinity();
            break;
        case IloAlgorithm::Error:
            rec.m_status = ABNORMAL;
            break;
        default:
            rec.m_status = UNKNOWN;
            break;
    }

    env.end();
    return rec;
}

void cplex_solver::abort() { aborted = true; }

cplex_solver::abort_callback::abort_callback(IloEnv& env, std::atomic<bool>& abo) :
    IloCplex::MIPCallbackI(env), aborted(abo) { }

void cplex_solver::abort_callback::main() {
    if(aborted) {
        abort();
    }
}

IloCplex::CallbackI* cplex_solver::abort_callback::duplicateCallback() const {
    return (new(getEnv()) abort_callback(*this));
}

IloCplex::Callback cplex_solver::make_abort_callback(IloEnv& env, std::atomic<bool>& abo) {
    return IloCplex::Callback(new(env) abort_callback(env, abo));
}

bool cplex_solver::set_option(std::string option, double value) {
    if(option == "abs_tol") {
        if(value >= 1e-9) {
            abs_tol = value;
            return true;
        }
        return false;
    }
    if(option == "rel_tol") {
        if(value >= 1e-9) {
            rel_tol = value;
            return true;
        }
        return false;
    }
    if(option == "num_threads") {
        if(value >= 1) {
            num_threads = (int)value;
            return true;
        }
        return false;
    }
    if(option == "lp_alg") {
        if(value >= 0 && value <= 6) {
            lp_alg = (int)value;
            return true;
        }
        return false;
    }
    if(option == "int_tol") {
        if(value >= 0 && value <= 0.5) {
            int_tol = value;
            return true;
        }
        return false;
    }
    return false;
}

bool cplex_solver::set_option(std::string option, std::string value) {
    return static_cast<bool>(option == "settings");
}

double cplex_solver::get_option(std::string option) {
    if(option == "abs_tol") {
        return abs_tol;
    }
    if(option == "rel_tol") {
        return rel_tol;
    }
    throw std::invalid_argument("Option '" + option + "' not supported or implemented.");
}

} // namespace dips
