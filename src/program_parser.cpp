#include "program_parser.hpp"

#include <list>      // for list
#include <memory>    // for allocator, unique_ptr
#include <queue>     // for queue
#include <string>    // for string, operator==, basic_string
#include <stdexcept> // for runtime_error

#include "config.hpp"       // for LIBALE_MAX_DIM
#include "expression.hpp"   // for expression
#include "lexer.hpp"        // for lexer
#include "node.hpp"         // for value_node
#include "parser.hpp"       // for parser
#include "token.hpp"        // for token, token::END, token::COLON, token::KEYWORD, token::SEMICOL
#include "token_buffer.hpp" // for token_buffer
#include "value.hpp"        // for boolean, real


namespace dips {




program_parser::program_parser(std::istream& input, ale::symbol_table& symbols) :
    parser(input, symbols) {
    lex.reserve_keywords(
      { "definitions",
        "objective",
        "constraints",
        "outputs",
        "relaxation",
        "only",
        "squashing",
        "programdefinitions" });
}

program program_parser::parse() {
    std::queue<std::string>().swap(errors);
    program result;
    match(ale::token::END);
    if(match(ale::token::END)) {
        report_empty();
        recover();
    }

    while(!check(ale::token::END)) {
        // ingore any spurious semicolons
        if(match(ale::token::SEMICOL)) {
            continue;
        }
        if(match_keyword("definitions")) {
            if(!match(ale::token::COLON)) {
                report_syntactical();
                recover_block();
                continue;
            }
            parse_definitions();
            continue;
        }
        if(match_keyword("objective")) {
            if(!match(ale::token::COLON)) {
                report_syntactical();
                recover_block();
                continue;
            }
            parse_objective(result);
            continue;
        }

        if(match_keyword("constraints")) {
            if(!match(ale::token::COLON)) {
                report_syntactical();
                recover_block();
                continue;
            }
            parse_constraints(result);
            continue;
        }

        if(match_keyword("relaxation")) {
            if(!match_keyword("only") || !match_keyword("constraints") || !match(ale::token::COLON)) {
                report_syntactical();
                recover_block();
                continue;
            }
            parse_relaxations(result);
            continue;
        }

        if(match_keyword("squashing")) {
            if(!match_keyword("constraints") || !match(ale::token::COLON)) {
                report_syntactical();
                recover();
                continue;
            }
            parse_squashes(result);
            continue;
        }

        if(match_keyword("outputs")) {
            if(!match(ale::token::COLON)) {
                report_syntactical();
                recover_block();
                continue;
            }
            parse_outputs(result);
            continue;
        }

        if(match_keyword("programdefinitions")) {
            recover_block();
            continue;
        }
        report_syntactical();
        recover();
    }
    print_errors();
    return result;
}
void program_parser::parse_definitions() {
    while(!check(ale::token::END) && !check_any_keyword("definitions", "objective", "constraints", "relaxation", "squashing", "outputs", "programdefinitions")) {
        if(match_any_definition<LIBALE_MAX_DIM>()) {
            continue;
        }
        if(match_any_assignment<LIBALE_MAX_DIM>()) {
            continue;
        }
        report_syntactical();
        recover();
    }
}
void program_parser::parse_objective(program& prog) {
    while(!check(ale::token::END) && !check_any_keyword("definitions", "objective", "constraints", "relaxation", "squashing", "outputs", "programdefinitions")) {
        std::unique_ptr<ale::value_node<ale::real<0>>> expr;
        std::string note;
        if(match_expression(expr, note)) {
            prog.m_objective.emplace_back(expr.release(), note);
            if(prog.m_objective.size() > 1) {
                throw std::runtime_error("Error -- Parsed more than one objective.");
            }
            return;
        }
        report_syntactical();
        recover();
    }
}

void program_parser::parse_constraints(program& prog) {
    while(!check(ale::token::END) && !check_any_keyword("definitions", "objective", "constraints", "relaxation", "squashing", "outputs", "programdefinitions")) {
        std::unique_ptr<ale::value_node<ale::boolean<0>>> expr;
        std::string note;
        if(match_expression(expr, note)) {
            prog.m_constraints.emplace_back(expr.release(), note);
            continue;
        }
        report_syntactical();
        recover();
    }
}

void program_parser::parse_relaxations(program& prog) {
    while(!check(ale::token::END) && !check_any_keyword("definitions", "objective", "constraints", "relaxation", "squashing", "outputs", "programdefinitions")) {
        std::unique_ptr<ale::value_node<ale::boolean<0>>> expr;
        std::string note;
        if(match_expression(expr, note)) {
            prog.m_relaxations.emplace_back(expr.release(), note);
            continue;
        }
        report_syntactical();
        recover();
    }
}

void program_parser::parse_squashes(program& prog) {
    while(!check(ale::token::END) && !check_any_keyword("definitions", "objective", "constraints", "relaxation", "squashing", "outputs", "programdefinitions")) {
        std::unique_ptr<ale::value_node<ale::boolean<0>>> expr;
        std::string note;
        if(match_expression(expr, note)) {
            prog.m_squashes.emplace_back(expr.release(), note);
            continue;
        }
        report_syntactical();
        recover();
    }
}

void program_parser::parse_outputs(program& prog) {
    while(!check(ale::token::END) && !check_any_keyword("definitions", "objective", "constraints", "relaxation", "squashing", "outputs", "programdefinitions")) {
        std::unique_ptr<ale::value_node<ale::real<0>>> expr;
        std::string note;
        if(match_expression(expr, note)) {
            prog.m_outputs.emplace_back(expr.release(), note);
            continue;
        }
        report_syntactical();
        recover();
    }
}

void program_parser::recover_block() {
    while(current().type != ale::token::END && !(current().type == ale::token::KEYWORD && (current().lexeme == "definitions" || current().lexeme == "objective" || current().lexeme == "constraints" || current().lexeme == "relaxations" || current().lexeme == "squashing" || current().lexeme == "outputs" || current().lexeme == "programdefinitions"))) {
        consume();
    }
    buf.clear();
}



} // namespace dips
