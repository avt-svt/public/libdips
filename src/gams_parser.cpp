#include "gams_parser.hpp"
#include "symbol_finder.hpp"

namespace dips {

void gams_parser::parse_gams_solve_statement(std::stringstream& input) {
    input << "parameter ms_gams_internal, ss_gams_internal, lbd_gams_internal, ubd_gams_internal, cpu_gams_internal;\n";
    input << "model mod_gams_internal /all/;\n";
    if(has_prio_variable(prog, symbols)) {
        input << "mod_gams_internal.prioropt = 1"
              << ";\n";
    }
    input << "option decimals = " << decimals << ";\n";
    input << "solve mod_gams_internal minimizing obj_gams_internal using " << problem_class << ";\n";
    input << "lbd_gams_internal = mod_gams_internal.objest; ubd_gams_internal = obj_gams_internal.l;\n";
    input << "ms_gams_internal = mod_gams_internal.modelstat; ss_gams_internal = mod_gams_internal.solvestat; cpu_gams_internal = mod_gams_internal.resusd;\n";
}

} // namespace dips