#include "gurobi_solver.hpp"

#include <chrono>        // for duration, operator-, time_point
#include <cstdlib>       // for size_t
#include <iostream>      // for operator<<, basic_ostream::operator<<, endl, basic_ostream, cout, ostream, basic_ostream<>::__ostream_type
#include <string>        // for string, allocator, operator==, basic_string, operator+, char_traits, to_string
#include <vector>        // for vector
#include <unordered_map> // for unordered_map
#include <array>         // for array
#include <limits>        // for numeric_limits
#include <list>          // for list, operator!=, _List_iterator, _List_iterator<>::_Self
#include <set>           // for operator!=, set, _Rb_tree_const_iterator, _Rb_tree_const_iterator<>::_Self
#include <stdexcept>     // for invalid_argument, runtime_error
#include <utility>       // for move, pair
#include <variant>       // for variant, visit

#include "gurobi_c++.h"            // for GRBVar, GRBModel, GRBException, GRBQuadExpr, operator*, GRBEnv, GRB_DoubleAttr_ObjVal, GRB_DoubleParam_TimeLimit
#include "gurobi_evaluator.hpp"    // for GurobiConstraintList, GurobiExponentialConstraint, evaluate_gurobi, evaluate_gurobi_constraint
#include "get_time.hpp"            // for thread_clock
#include "parameter_cloner.hpp"    // for add_variable_to_solve_record
#include "symbol_finder.hpp"       // for find_variables
#include "QuadExpr.hpp"            // for QuadExpr, PureQuadExpr, LinExpr
#include "expression.hpp"          // for expression
#include "gurobi_c.h"              // for GRB_BINARY, GRB_CONTINUOUS, GRB_CUTOFF, GRB_EQUAL, GRB_ERROR_INVALID_ARGUMENT, GRB_GREATER_EQUAL, GRB_INFEASIBLE
#include "program.hpp"             // for program
#include "quad_expr_evaluator.hpp" // for QuadConstraintList, QuadConstraintType, QuadConstraint, QuadConstraintType::EQUAL, QuadConstraintType::GREATER
#include "scalar_evaluator.hpp"    // for ScalarVariableIdManager

namespace dips {

class gurobi_callback : public GRBCallback {
public:
    explicit gurobi_callback(const std::atomic<bool>& abort_opt) :
        abort_optimization(abort_opt) { }

protected:
    void callback() final {
        if(abort_optimization) {
            abort();
        }
    }

private:
    const std::atomic<bool>& abort_optimization;
};

gurobi_solver::gurobi_solver(symbol_table& symbols, std::string name) :
    solver(name), symbols(symbols), solver_name(std::move(name)) { }

solver* gurobi_solver::make_solver(symbol_table& symbols, std::string gurobi_solver_name) {
    return new gurobi_solver(symbols, std::move(gurobi_solver_name));
}

// convert QuadExpr to GRBQuadExpr
GRBQuadExpr convert_expression(const QuadExpr& expr, const std::vector<GRBVar>& grb_var_list) {
    const LinExpr& linearPart = expr.getLinearPart();
    const PureQuadExpr& quadraticPart = expr.getQuadraticPart();

    // initialize expression with constant term
    GRBQuadExpr gurobi_expr(expr.getConstant());

    // add linear terms
    for(const unsigned& var_id : linearPart.get_non_zero_ids()) {
        double coeff = linearPart.get_value(var_id);
        gurobi_expr.addTerm(coeff, grb_var_list.at(var_id));
    }

    // add quadratic terms

    for(unsigned i = 0; i < quadraticPart.get_non_zero_ids().size(); i++) {
        const auto& indices = quadraticPart.get_non_zero_ids().at(i);
        gurobi_expr += grb_var_list.at(indices.first) * grb_var_list.at(indices.second) * quadraticPart.get_non_zero_values().at(i);
    }

    return gurobi_expr;
}

// convert QuadConstraintType to Gurobi constraint flag (GRB_Equal,...)
char convert_constr_type(QuadConstraintType t) {
    switch(t) {
        case QuadConstraintType::EQUAL:
            return GRB_EQUAL;
        case QuadConstraintType::LESS_EQUAL:
            return GRB_LESS_EQUAL;
        case QuadConstraintType::GREATER_EQUAL:
            return GRB_GREATER_EQUAL;
        case QuadConstraintType::NOT_EQUAL:
            throw std::invalid_argument("Constraints of type '!=' are not allowed");
        case QuadConstraintType::LESS:
            throw std::invalid_argument("Constraints of type '<' are not allowed");
        case QuadConstraintType::GREATER:
            throw std::invalid_argument("Constraints of type '>' are not allowed");
        default:
            throw std::invalid_argument("unexpected QuadConstraintType");
    }
}

// set objective of model to quadratic expression expr
void set_objective(GRBModel& model, const QuadExpr& expr, const std::vector<GRBVar>& grb_var_list) {
    try {
        model.setObjective(convert_expression(expr, grb_var_list), GRB_MINIMIZE);
    } catch(const GRBException& e) {
        std::cout << "Gurobi Error code = " << e.getErrorCode() << std::endl;
        throw std::runtime_error(e.getMessage());
    }
}

// add list of quadratic constraints to model
// an optional comment can be used to indentify the constraints in gurobi
void add_quad_constraints(GRBModel& model, const QuadConstraintList& constr_list,
  const std::vector<GRBVar>& grb_var_list, const std::string& comment) {
    try {
        for(const QuadConstraint& constr : constr_list) {
            GRBQuadExpr temp = convert_expression(constr.getExpr(), grb_var_list);
            if(!constr.getExpr().is_quadratic()) {
                model.addConstr(temp.getLinExpr(), convert_constr_type(constr.getType()), 0.0,
                  comment);
            } else {
                model.addQConstr(temp, convert_constr_type(constr.getType()), 0.0,
                  comment);
            }
        }
    } catch(const GRBException& e) {
        std::cout << "Gurobi Error code = " << e.getErrorCode() << std::endl;
        throw std::runtime_error(e.getMessage());
    }
}

// add special gurobi constraints
void add_gurobi_constraint(GRBModel& model, const GurobiExponentialConstraint& constr, const std::vector<GRBVar>& grb_var_list) {
    model.addGenConstrExp(grb_var_list.at(constr.variable_id_x), grb_var_list.at(constr.variable_id_y), "", "");
}

// reads the optimized values from model and stores them into rec
void set_solution_variables(solve_record& rec, const ScalarVariableIdManager& id_manager, const std::vector<GRBVar>& grb_var_list) {
    id_manager.set_record<GRBVar>([](GRBVar x) {
        return x.get(GRB_DoubleAttr_X);
    },
      rec, grb_var_list);
}

// read status code from model and set rec accordingly
// if reoptimized is true it will not try to reoptimize on infeasible_or_unbound status code
void set_record(GRBModel& model, solve_record& rec, const ScalarVariableIdManager& id_manager, const std::vector<GRBVar>& grb_var_list, bool reoptimized = false) {
    switch(model.get(GRB_IntAttr_Status)) {
        case GRB_OPTIMAL: {
            rec.m_status = GLOBAL;

            // gurobi only gives a bound if we are mixed integer
            if(model.get(GRB_IntAttr_IsMIP)) {
                rec.m_lbd = model.get(GRB_DoubleAttr_ObjBound);
                rec.m_ubd = model.get(GRB_DoubleAttr_ObjVal);
            } else {
                rec.m_ubd = model.get(GRB_DoubleAttr_ObjVal);
                rec.m_lbd = rec.m_ubd;
            }

            set_solution_variables(rec, id_manager, grb_var_list);

            break;
        }
        case GRB_SUBOPTIMAL:
        case GRB_INFEASIBLE: {
            rec.m_status = INFEASIBLE;
            rec.m_lbd = std::numeric_limits<double>::infinity();
            rec.m_ubd = std::numeric_limits<double>::infinity();

            //model.computeIIS();
            //model.write("model.ilp");
            break;
        }
        case GRB_INF_OR_UNBD: {
            // try to reoptimize once with DualRestrictions set to 0 to find out if solution
            // is infeasible or unbounded
            if(!reoptimized) {
                try {
                    model.reset();
                    model.set(GRB_IntParam_DualReductions, 0);

                    // reduce timelimit
                    double timeLimit = model.get(GRB_DoubleParam_TimeLimit) - model.get(GRB_DoubleAttr_Runtime);
                    model.set(GRB_DoubleParam_TimeLimit, timeLimit);

                    model.optimize();
                } catch(const GRBException& e) {
                    std::cout << "Gurobi Error code = " << e.getErrorCode() << std::endl;
                    throw std::runtime_error(e.getMessage());
                }

                set_record(model, rec, id_manager, grb_var_list, true);
            } else {
                rec.m_status = UNKNOWN;
            }
            break;
        }
        case GRB_UNBOUNDED: {
            rec.m_status = UNBOUNDED;
            rec.m_lbd = -std::numeric_limits<double>::infinity();
            rec.m_ubd = -std::numeric_limits<double>::infinity();
            break;
        }
        case GRB_NODE_LIMIT:
        case GRB_TIME_LIMIT:
        case GRB_USER_OBJ_LIMIT:
        case GRB_SOLUTION_LIMIT:
        case GRB_ITERATION_LIMIT: {
            // check if solution was found before the limit was reached
            if(model.get(GRB_IntAttr_SolCount) >= 1) {
                rec.m_status = FEASIBLE;

                //gurobi only gives a bound if we are mixed integer
                if(model.get(GRB_IntAttr_IsMIP)) {
                    rec.m_lbd = model.get(GRB_DoubleAttr_ObjBound);
                    rec.m_ubd = model.get(GRB_DoubleAttr_ObjVal);
                } else {
                    rec.m_ubd = model.get(GRB_DoubleAttr_ObjVal);
                    rec.m_lbd = rec.m_ubd;
                }

                set_solution_variables(rec, id_manager, grb_var_list);

                break;
            }
            rec.m_status = UNKNOWN;
            break;
        }
        case GRB_INTERRUPTED:
            rec.m_status = ABORTED;
            break;
        case GRB_CUTOFF:
        case GRB_LOADED:
        case GRB_NUMERIC:
        case GRB_INPROGRESS:
        default:
            rec.m_status = ABNORMAL;
            break;
    }
}

solve_record gurobi_solver::solve(program prog) {
    solve_record rec;

    try {
        GRBEnv env;
        env.set(GRB_IntParam_LogToConsole, 0);
        env.set(GRB_StringParam_LogFile, "./gurobi.log");

        //set options saved in pass_option of calling context
        for(const auto& option : options) {
            set_option(option.first, option.second);
        }

        // try to read settings from file
        // must read settings file before instantiating model for settings to take effect
        try {
            env.readParams(settings_file);
        } catch(const GRBException& e) {
            if(e.getErrorCode() != GRB_ERROR_INVALID_ARGUMENT) {
                throw std::runtime_error(e.getMessage() + " (Gurobi Error Code: " + std::to_string(e.getErrorCode()) + ")");
            }
        }

        GRBModel model(env);

        // find all variables that are used in the program statement
        auto used_variables = find_variables(prog, symbols);

        // add parameter of correct shape to solve_record
        for(const auto& var_name : used_variables) {
            add_variable_to_solve_record(var_name, symbols, rec);
        }

        // evaluate expressions
        ScalarVariableIdManager id_manager;
        id_manager.set_ids(used_variables, symbols);

        GurobiConstraintList grb_constraints;

        std::vector<QuadConstraintList> quad_constraint_lists;
        std::vector<std::string> comments;

        auto obj = evaluate_gurobi(prog.m_objective.front(), symbols, id_manager, grb_constraints, quad_constraint_lists);
        for(auto& constr : prog.m_constraints) {
            auto evaluated_constr = evaluate_gurobi_constraint(constr, symbols, id_manager, grb_constraints, quad_constraint_lists);
            quad_constraint_lists.push_back(evaluated_constr);
            comments.push_back(constr.m_note);
        }

        // add variables to model

        auto add_variable = [&model](double lb, double ub, double init, bool integral, const std::string& name, const std::string& comment, const std::vector<size_t>& index) {
            std::string unique_name = name;
            for(size_t i : index) {
                unique_name += "_" + std::to_string(i);
            }

            char var_type = GRB_CONTINUOUS;
            if(integral) {
                if(-1 + 1e-6 <= lb && ub <= 2 - 1e-6) {
                    var_type = GRB_BINARY;
                } else {
                    var_type = GRB_INTEGER;
                }
            }

            return model.addVar(lb, ub, 0.0, var_type, unique_name);
        };
        auto grb_variable_list = id_manager.map_over_variable_symbols<GRBVar>(add_variable, symbols);

        // set the objective and constraints of the model
        set_objective(model, obj, grb_variable_list);
        for(size_t i = 0; i < quad_constraint_lists.size(); ++i) {
            std::string comment;
            if(i < comments.size()) {
                comment = comments.at(i);
            }
            add_quad_constraints(model, quad_constraint_lists.at(i), grb_variable_list, comment);
        }
        // delete quad_constraint_lists
        quad_constraint_lists.clear();

        // add gurobi helper constraints
        for(auto& constr : grb_constraints) {
            std::visit([&model, &grb_variable_list](auto c) { add_gurobi_constraint(model, c, grb_variable_list); }, constr);
        }

        // set gurobi model parameters
        model.set(GRB_StringAttr_ModelName, solver_name);
        model.set(GRB_DoubleParam_MIPGapAbs, abs_tol);
        model.set(GRB_DoubleParam_MIPGap, rel_tol);
        model.set(GRB_IntParam_Threads, num_threads);
        model.set(GRB_IntParam_Method, lp_alg);
        model.set(GRB_DoubleParam_IntFeasTol, int_tol);
        model.set(GRB_IntParam_IntegralityFocus, static_cast<int>(int_focus));
        model.set(GRB_IntParam_NonConvex, non_convex);
        model.set(GRB_DoubleParam_TimeLimit, time_limit);
        model.set(GRB_IntParam_SolutionLimit, solution_limit);
        model.write("Model.lp");

        gurobi_callback cb(abort_optimization);
        model.setCallback(&cb);

        // optimize model and measure cpu time (approximate, not respecting threads)
        auto time_start = thread_clock::now();
        model.optimize();
        auto time_end = thread_clock::now();
        rec.m_cpu = std::chrono::duration<double>(time_end - time_start).count();

        // read solver status
        std::cout << "Sucessfully optimized" << std::endl;
        set_record(model, rec, id_manager, grb_variable_list);

        return rec;
    } catch(const GRBException& e) {
        std::cout << "Gurobi Error code = " << e.getErrorCode() << std::endl;
        throw std::runtime_error(e.getMessage() + " (Gurobi Error Code: " + std::to_string(e.getErrorCode()) + ")");
    }
}

void gurobi_solver::abort() { abort_optimization = true; }

bool gurobi_solver::set_option(std::string option, double value) {
    // NOTE: abs_tol, rel_tol ranges could be bigger
    if(option == "abs_tol") {
        if(value >= 1e-9) {
            abs_tol = value;
            return true;
        }
    } else if(option == "rel_tol") {
        if(value >= 1e-9) {
            rel_tol = value;
            return true;
        }
    } else if(option == "num_threads") {
        if(value >= 0) {
            num_threads = static_cast<int>(value);
            return true;
        }
    } else if(option == "lp_alg") {
        if(value >= -1 && value <= 5) {
            lp_alg = static_cast<int>(value);
            return true;
        }
    } else if(option == "int_tol") {
        if(value >= 1e-9 && value <= 1e-1) {
            int_tol = value;
            return true;
        }
    } else if(option == "non_convex") {
        if(value >= -1 && value <= 2) {
            non_convex = static_cast<int>(value);
            return true;
        }
    } else if(option == "time_limit") {
        if(value >= 0) {
            time_limit = value;
            return true;
        }
    } else if(option == "solution_limit") {
        if(value >= 1 && value <= 2000000000) {
            solution_limit = static_cast<int>(value);
            return true;
        }
    } else if(option == "int_focus") {
        if(value == 1) {
            int_focus = true;
            return true;
        } else if(value == 0) {
            int_focus = false;
            return true;
        }
    }
    return false;
}

bool gurobi_solver::set_option(std::string option, std::string value) {
    if(option == "settings") {
        settings_file = std::move(value);
        return true;
    }
    return false;
}

double gurobi_solver::get_option(std::string option) {
    if(option == "abs_tol") {
        return abs_tol;
    } else if(option == "rel_tol") {
        return rel_tol;
    } else if(option == "num_threads") {
        return num_threads;
    } else if(option == "lp_alg") {
        return lp_alg;
    } else if(option == "int_tol") {
        return int_tol;
    } else if(option == "non_convex") {
        return non_convex;
    } else if(option == "time_limit") {
        return time_limit;
    } else if(option == "solution_limit") {
        return solution_limit;
    } else if(option == "int_focus") {
        return static_cast<double>(int_focus);
    }
    throw std::invalid_argument("Option '" + option + "' not supported or implemented.");
}



} // namespace dips
