#include "parameter_cloner.hpp"

#include <unordered_map> // for _Map_base<>::mapped_type, unordered_map

#include "util/visitor_utils.hpp" // for call_visitor
#include "solver.hpp"             // for solve_record, solution
#include "symbol_table.hpp"       // for symbol_table

namespace dips {

struct variable_to_parameter_visitor {
    base_symbol* operator()(base_symbol* /*sym*/) {
        throw std::invalid_argument("symbol has unexpected type");
    }

    template <typename TType>
    base_symbol* operator()(variable_symbol<TType>* sym) {
        auto* result = new parameter_symbol<TType>(sym->m_name);
        if constexpr(get_node_dimension<TType> != 0 && is_real_node<TType>) {
            result->m_value.resize(sym->shape(), std::numeric_limits<double>::quiet_NaN());
            return result;
        } else if constexpr(is_real_node<TType>) {
            result->m_value = std::numeric_limits<typename TType::basic_type>::quiet_NaN();
            return result;
        }
        throw std::invalid_argument("symbol \"" + sym->m_name + "\" has unexpected type \"" + typeid(TType).name() + "\"");
    }
};

void add_variable_to_solve_record(const std::string& var_name, symbol_table& symbols, solve_record& rec) {
    // get variable symbol
    auto* old_sym = symbols.resolve(var_name);
    if(old_sym == nullptr) {
        throw std::invalid_argument("unable to resolve: " + var_name);
    }

    // create new parameter symbol from it
    auto* new_sym = call_visitor(variable_to_parameter_visitor {}, old_sym);
    if(new_sym == nullptr) {
        throw std::invalid_argument("unable to generate parameter clone of variable");
    }

    // add it to the solution record
    rec.m_solution[var_name].reset(new_sym);
}
} // namespace dips
