#include "sip_oracle_solver.hpp"

#include "util/evaluator.hpp" // for evaluate_expression
#include "common.hpp"         // for discretize, fix_variables, copy_variables, set_real
#include "expression.hpp"     // for expression
#include "node.hpp"           // for minus_node, value_node
#include "symbol.hpp"         // for cast_parameter_symbol, parameter_symbol
#include "symbol_table.hpp"   // for symbol_table
#include "value.hpp"          // for index, real, set, tensor_type<>::basic_type
#include "output.hpp"         // to_json, from_json
#include "nlohmann/json.hpp"  // for json

//#include "program_printer.hpp"

#include <cmath>
#include <fstream>   // for operator<<, basic_ostream, basic_ostream<>::__ostream_type, basic_ostream::operator<<, basic_ios::rdbuf, endl, basic_o...
#include <memory>    // for unique_ptr, allocator_traits<>::value_type, make_unique
#include <utility>   // for pair, make_pair, move
#include <iostream>  // for cout
#include <list>      // for list, _List_iterator
#include <variant>   // for variant
#include <stdexcept> // for invalid_argument
#include <atomic>    // for atomic


#ifdef DIPS_use_mpi
#include "mpi.h"
#endif

namespace dips {


sip_oracle_program_definition_parser::sip_oracle_program_definition_parser(std::istream& is, symbol_table& symbols) :
    programdefinition_parser_chain(is, symbols, { { "lbp", {} }, { "ora", { "ora_target" } } }) { }

void sip_oracle_solver::program_info::add_llp(const program& llp, const discretization& lbp_disc, const discretization& ora_disc) {
    llps.emplace_back(llp);
    lbp_discs.emplace_back(lbp_disc);
    ora_discs.emplace_back(ora_disc);
}


solve_record sip_oracle_solver::solve(program_info prog) {

    std::unique_ptr<solver> sub(make_subsolver(symbols, solver_base_name));

    for(auto& option : options) {
        sub->pass_option(option.first, option.second);
    }

    solve_record rec;
    solve_record lbp_rec;
    solve_record ora_rec;
    int n_llps = prog.llps.size();
    std::vector<solve_record> llp_rec(n_llps);

    // set inital lbd and ubd provided by the user
    rec.m_lbd = prog.init_lbd;
    rec.m_ubd = prog.init_ubd;

    enum { LBP,
        ORA,
        UTOPIAN_UBP,
        NDEF } next_problem
      = ORA;

    double feas_vio_ubp = std::numeric_limits<double>::infinity(); // feasibility violation of ubp (always negative)


    // check if initial discretization set is empty
    for(int i = 0; i < n_llps; i++) {
        auto* indexes = cast_parameter_symbol<ale::set<ale::index<0>, 0>>(symbols.resolve(prog.ora_discs[i].indexes));
        if(indexes == nullptr) {
            throw std::invalid_argument("indexset has improper type");
        }
        if(indexes->m_value.empty()) {
            std::cout << "preprocessing: initial discretization empty. Generating discretization by solving lbp once.\n";
            next_problem = LBP;
            break;
        }
    }

    if(!(std::abs(rec.m_lbd) < std::numeric_limits<double>::infinity())) {
        std::cout << "preprocessing: initial LBD = " << rec.m_lbd << ". Generating LBD by solving lbp once.\n";
        next_problem = LBP;
    }

    bool compute_utopian_ubp = false;
    double utopian_ubd = std::numeric_limits<double>::infinity();
    if(next_problem != LBP && !(std::abs(rec.m_ubd) < std::numeric_limits<double>::infinity())) {
        compute_utopian_ubp = true;

        next_problem = UTOPIAN_UBP;
    }

    for(int i = 0; i < max_iter; i++) {
        scoped_push sp_it(sub->solver_name, "it_" + std::to_string(i + 1));

        std::cout << "\nIteration " << i + 1 << ":\n";
        std::cout << "LBD           = " << rec.m_lbd << '\n';
        std::cout << "UBD           = " << rec.m_ubd << '\n';
        std::cout << "feas.vio. UBD = " << feas_vio_ubp << "\n\n";

        // check termination
        if(check_termination(rec, feas_vio_ubp, abs_tol, rel_tol, feas_tol)) {
            return rec;
        }

        if(next_problem == LBP) {
            scoped_push sp_lbp(sub->solver_name, "_lbp");
            std::cout << "\nsip: solving lbp\n";

            sub->set_option("abs_tol", abs_tol_lbp);
            sub->set_option("rel_tol", rel_tol_lbp);
            sub->set_option("settings", settings_lbp);
            sub->set_option("max_time", max_time - rec.m_cpu);

            lbp_rec = sub->solve(prog.lbp);

            rec.m_cpu += lbp_rec.m_cpu;

            std::cout << "reporting status\n";
            report(lbp_rec);


            if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                return rec;
            }

            switch(lbp_rec.m_status) {
                case INFEASIBLE:
                    // lbp proves sip to be infeasible
                    rec.m_status = INFEASIBLE;
                    rec.m_lbd = std::numeric_limits<double>::infinity();
                    rec.m_ubd = std::numeric_limits<double>::infinity();

                    return rec;
                case GLOBAL:
                    // computed valid lbd
                    rec.m_lbd = lbp_rec.m_lbd;
                    break;
                default:
                    throw std::invalid_argument("lbp: unhandled solver status: "
                                                + to_string(lbp_rec.m_status));
            }


            bool lbp_feasible = true;
            double feas_vio_temp = -std::numeric_limits<double>::infinity();
            for(int j = 0; j < n_llps; ++j) {
                scoped_push sp_lbp_llp(sub->solver_name, "_llp_" + std::to_string(j + 1));
                std::cout << "sip: solving lbp_llp_" << j + 1 << '\n';
                // note: always minimizing. This leads to
                //  - flipped sign of objective
                //  - flipped lbd and ubd

                symbols.push_scope();
                fix_variables(lbp_rec, symbols);

                sub->set_option("abs_tol", abs_tol_lbp_llp);
                sub->set_option("rel_tol", rel_tol_lbp_llp);
                sub->set_option("settings", settings_lbp_llp);
                sub->set_option("max_time", max_time - rec.m_cpu);

                llp_rec[j] = sub->solve(prog.llps[j]);

                rec.m_cpu += llp_rec[j].m_cpu;
                symbols.pop_scope();

                std::cout << "reporting status\n";
                report(llp_rec[j]);


                if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                    return rec;
                }

                switch(llp_rec[j].m_status) {
                    case INFEASIBLE:
                        // by convention: llp_rec.m_ubd = llp_rec.m_lbd = + infty
                        // => -llp_rec.m_lbd <= feas_tol
                        // => incumbent point in llp[j] sip feasible
                        // => no tolerance update necessary
                        break;
                    case GLOBAL:
                        if(-llp_rec[j].m_lbd <= feas_tol) {
                            // => incumbent point in llp[j] sip feasible
                            // => nothing happens here
                            // => compute llp[j+1]
                        } else {
                            // -llp_rec.m_lbd > feas_tol
                            // incumbent point of lbp might be sip infeasible
                            lbp_feasible = false;
                        }
                        // tolerance update if necessary
                        if(-llp_rec[j].m_lbd > 0 && abs_tol_lbp_llp > -llp_rec[j].m_lbd / red_tol_lbp_llp) {
                            set_tolerance_robust("abs_tol_lbp_llp", -llp_rec[j].m_lbd / red_tol_lbp_llp);
                        }
                        break;
                    default:
                        throw std::invalid_argument("lbp_llp_" + std::to_string(j + 1)
                                                    + ": unhandled solver status: " + to_string(llp_rec[j].m_status));
                }

                // keeping track of max feasibility violation
                if(-llp_rec[j].m_lbd > feas_vio_temp) {
                    feas_vio_temp = -llp_rec[j].m_lbd;
                }
            }

            // check termination
            if(lbp_feasible) {
                // => incumbent point in all llps sip feasible
                std::cout << "\nlbp furnished a point which is within llp feas. tolerance. Terminating.\n";
                std::cout << "Feas. vio.:   " << feas_vio_temp << " <= feas_tol = " << feas_tol << std::endl;
                rec.m_status = GLOBAL;
                rec.m_ubd = lbp_rec.m_ubd;
                copy_variables(lbp_rec, rec);


                return rec;
            } else {
                // discretize
                for(int j = 0; j < n_llps; ++j) {
                    if(discr_all || -llp_rec[j].m_ubd > 0) {
                        // discretize always or only if llp[j] produced a valid cut
                        discretize(prog.lbp_discs[j], llp_rec[j], symbols);
                        discretize(prog.ora_discs[j], llp_rec[j], symbols);
                        std::cout << "discretized llp_" << j + 1 << ".\n";
                    }
                }

                if(!(std::abs(rec.m_ubd) < std::numeric_limits<double>::infinity())) {
                    compute_utopian_ubp = true;
                    next_problem = UTOPIAN_UBP;
                } else {
                    next_problem = ORA;
                }
            }
        }

        else if(next_problem == UTOPIAN_UBP) {
            std::cout << "preprocessing: initial UBD = " << rec.m_ubd << ". Generating utopian UBD by maximizing lbp objective.\n";
            std::cout << "               constructing auxillary program utopian_ubp ... ";
            program utopian_ubp = prog.lbp;
            std::unique_ptr<ale::value_node<ale::real<0>>> neg_lbp_objective;
            neg_lbp_objective = std::make_unique<minus_node>((*prog.lbp.m_objective.begin()).get()->clone());
            utopian_ubp.m_objective.front() = neg_lbp_objective.release();
            std::cout << "done\n";

            scoped_push sp_lbp(sub->solver_name, "_utopian_ubp");
            std::cout << "preprocessing: solving utopian_ubp\n";

            sub->set_option("abs_tol", abs_tol_lbp);
            sub->set_option("rel_tol", std::max(rel_tol_lbp, 0.5)); // only search for a coarse solution
            sub->set_option("settings", settings_lbp);
            sub->set_option("max_time", max_time - rec.m_cpu);

            auto utopian_ubp_rec = sub->solve(utopian_ubp);

            rec.m_cpu += utopian_ubp_rec.m_cpu;

            std::cout << "reporting status\n";
            report(utopian_ubp_rec);

            if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                return rec;
            }

            switch(utopian_ubp_rec.m_status) {
                case INFEASIBLE:
                    // utopien_ubp proves sip to be infeasible
                    std::cout << "utopian_ubp infeasible.\n";
                    rec.m_status = ABNORMAL;

                    return rec;
                case GLOBAL:
                    // computed valid utopian UBD
                    std::cout << "utopian UBD = " << -utopian_ubp_rec.m_lbd << '\n';
                    utopian_ubd = -utopian_ubp_rec.m_lbd;
                    break;
                default:
                    throw std::invalid_argument("utopian UBD: unhandled solver status: "
                                                + to_string(utopian_ubp_rec.m_status));
            }
            next_problem = ORA;
        }

        else if(next_problem == ORA) {
            scoped_push sp_ora(sub->solver_name, "_ora");
            std::cout << "\nsip: solving ora\n";

            double obj_target;
            if(compute_utopian_ubp) {
                obj_target = utopian_ubd;
                std::cout << "setting obj_target to utopian_ubd\n";
                std::cout << "obj_target   = " << utopian_ubd << '\n';
            } else {
                obj_target = 0.5 * (rec.m_ubd + rec.m_lbd);
                std::cout << "obj_target   = " << obj_target << '\n';
            }
            set_real(prog.ora_target, obj_target, symbols);

            // TODO: find good heuristic for tolerance

            sub->set_option("abs_tol", abs_tol_ora);
            sub->set_option("rel_tol", rel_tol_ora);
            sub->set_option("settings", settings_ora);
            sub->set_option("max_time", max_time - rec.m_cpu);

            ora_rec = sub->solve(prog.ora);

            rec.m_cpu += ora_rec.m_cpu;

            std::cout << "reporting status\n";
            report(ora_rec);


            if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                return rec;
            }

            switch(ora_rec.m_status) {
                case INFEASIBLE: {
                    // note that in the original algorithm statement,
                    // the following uncommented code is executed (this leads to
                    // looping of the algorithm if ora is infeasible):
                    // rec.m_lbd = obj_target;
                    // continue;
                    std::cout << "\nWarning: abnormal behaviour. Oracle problem reported infeasible.\n";
                    if(compute_utopian_ubp) {
                        std::cout << "utopian UBD = " << utopian_ubd << " is not attainable.\n";
                    }
                    std::cout << "This may be caused by\n"
                              << "  - a mistake in the formulation of the orcale problem\n"
                              << "  - too tight bounds of the auxilliary variable in the oracle problem (try to increase the bounds)\n"
                              << "  - an infeasible SIP\n";



                    scoped_push sp_lbp_feas(sub->solver_name, "_lbp_feasibility_check");
                    std::cout << "\nsip: checking feasiblity of the sip by solving lbp\n";

                    sub->set_option("abs_tol", abs_tol_lbp);
                    sub->set_option("rel_tol", rel_tol_lbp);
                    sub->set_option("settings", settings_lbp);
                    sub->set_option("max_time", max_time - rec.m_cpu);

                    lbp_rec = sub->solve(prog.lbp);

                    rec.m_cpu += lbp_rec.m_cpu;

                    std::cout << "reporting status\n";
                    report(lbp_rec);

                    if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                        return rec;
                    }

                    switch(lbp_rec.m_status) {
                        case INFEASIBLE:
                            // lbp proves sip to be infeasible
                            std::cout << "success: lbp proved sip to be infeasible\n";
                            rec.m_status = INFEASIBLE;
                            rec.m_lbd = std::numeric_limits<double>::infinity();
                            rec.m_ubd = std::numeric_limits<double>::infinity();
                            break;
                        case GLOBAL:
                            std::cout << "failure: lbp did not prove sip to be infeasible.\n...Aborting.\n";
                            rec.m_status = ABNORMAL;
                            return rec;
                        default:
                            throw std::invalid_argument("lbp: unhandled solver status: "
                                                        + to_string(lbp_rec.m_status));
                    }
                    return rec;
                }
                case GLOBAL: {
                    if(ora_rec.m_lbd > 0) {
                        // computed valid lbd
                        rec.m_lbd = obj_target;
                        if(compute_utopian_ubp) {
                            std::cout << "utopian UBD = " << utopian_ubd << " is not attainable.\n";
                            rec.m_status = INFEASIBLE;
                            rec.m_lbd = std::numeric_limits<double>::infinity();
                            rec.m_ubd = std::numeric_limits<double>::infinity();

                            return rec;
                        }
                        continue;
                    } else {
                        // ora_rec.m_lbd <= 0
                        // nothing happens here => solve llps
                    }
                    // tolerance update if necessary
                    if(ora_rec.m_ubd > 0 && abs_tol_ora > -ora_rec.m_lbd / red_tol_ora) {
                        set_tolerance_robust("abs_tol_ora", -ora_rec.m_lbd / red_tol_ora);
                    }
                    break;
                    default:
                        throw std::invalid_argument("ora: unhandled solver status: "
                                                    + to_string(ora_rec.m_status));
                }
            }

            // compute current obj value
            symbols.push_scope();
            fix_variables(ora_rec, symbols);
            double obj = util::evaluate_expression(prog.lbp.m_objective.front(), symbols);
            symbols.pop_scope();

            bool ora_feasible = true;
            double feas_vio_temp = -std::numeric_limits<double>::infinity();
            for(int j = 0; j < n_llps; ++j) {
                scoped_push sp_ora_llp(sub->solver_name, "_llp_" + std::to_string(j + 1));
                std::cout << "sip: solving ora_llp_" << j + 1 << '\n';
                // note: always minimizing. This leads to
                //  - flipped sign of objective
                //  - flipped lbd and ubd

                symbols.push_scope();
                fix_variables(ora_rec, symbols);

                sub->set_option("abs_tol", abs_tol_ora_llp);
                sub->set_option("rel_tol", rel_tol_ora_llp);
                sub->set_option("settings", settings_ora_llp);
                sub->set_option("max_time", max_time - rec.m_cpu);

                llp_rec[j] = sub->solve(prog.llps[j]);

                rec.m_cpu += llp_rec[j].m_cpu;
                symbols.pop_scope();

                std::cout << "reporting status\n";
                report(llp_rec[j]);


                if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                    return rec;
                }

                switch(llp_rec[j].m_status) {
                    case INFEASIBLE:
                        // by convention: llp_rec.m_ubd = llp_rec.m_lbd = + infty
                        // => -llp_rec.m_lbd <= feas_tol
                        // => incumbent point in llp[j] sip feasible
                        // => no tolerance update necessary
                        break;
                    case GLOBAL:
                        if(-llp_rec[j].m_lbd <= feas_tol) {
                            // => incumbent point in llp[j] sip feasible
                            // => nothing happens here
                            // => compute llp[j+1]
                        } else {
                            // -llp_rec.m_lbd > feas_tol
                            // incumbent point of ora might be sip infeasible
                            ora_feasible = false;
                        }
                        // tolerance update if necessary
                        if(llp_rec[j].m_lbd <= 0 && abs_tol_lbp_llp > -llp_rec[j].m_lbd / red_tol_ora_llp) {
                            set_tolerance_robust("abs_tol_ora_llp", -llp_rec[j].m_lbd / red_tol_ora_llp);
                        }
                        break;
                    default:
                        throw std::invalid_argument("ora_llp_" + std::to_string(j + 1)
                                                    + ": unhandled solver status: " + to_string(llp_rec[j].m_status));
                }

                // keeping track of max feasibility violation
                if(-llp_rec[j].m_lbd > feas_vio_temp) {
                    feas_vio_temp = -llp_rec[j].m_lbd;
                }
            }


            if(ora_feasible) {
                rec.m_status = FEASIBLE;
                compute_utopian_ubp = false;
                if(obj < rec.m_ubd) {
                    feas_vio_ubp = feas_vio_temp;
                    rec.m_ubd = obj;
                    copy_variables(ora_rec, rec);
                }
                next_problem = ORA;
            } else {
                // discretize
                for(int j = 0; j < n_llps; ++j) {
                    if(discr_all || !(-llp_rec[j].m_lbd <= 0)) {
                        // discretize always or only if llp[j] produced a valid cut
                        discretize(prog.ora_discs[j], llp_rec[j], symbols);
                        discretize(prog.lbp_discs[j], llp_rec[j], symbols);
                        std::cout << "discretized llp_" << j + 1 << ".\n";
                    }
                }
                next_problem = ORA;
            }
        }


        if(next_problem == NDEF) {
            throw std::invalid_argument("procedure not implemented");
        }


        // check termination
        if(compute_utopian_ubp && rec.m_lbd >= utopian_ubd) {
            std::cout << "utopian UBD = " << utopian_ubd << " is not attainable.\n";
            rec.m_status = INFEASIBLE;
            rec.m_lbd = std::numeric_limits<double>::infinity();
            rec.m_ubd = std::numeric_limits<double>::infinity();
            return rec;
        }
        if(check_termination(rec, feas_vio_ubp, abs_tol, rel_tol, feas_tol)) {
            return rec;
        }

#ifdef DIPS_use_mpi
        int _rank;
        MPI_Comm_rank(MPI_COMM_WORLD, &_rank);
        if(_rank == 0) {
#endif
            // write discretization points to output file
            std::string disc_output_path = "./";
            for(auto& i : sub->solver_name) {
                disc_output_path += i;
            }
            std::ofstream disc_output(disc_output_path + "_disc_points.txt");
            std::streambuf* backup = std::cout.rdbuf();
            std::cout.rdbuf(disc_output.rdbuf());
            std::vector<std::vector<discretization>> discs;
            discs.emplace_back(prog.ora_discs);
            report(symbols, discs);
            std::cout.rdbuf(backup); // reassign cout to its former buffer before that buffer no longer exists
            disc_output.close();     // close file output
#ifdef DIPS_use_mpi
        }
        MPI_Barrier(MPI_COMM_WORLD);
#endif
    }

    rec.m_status = EXCEEDED_MAX_ITERATIONS;
    std::cout << solver_base_name << ": total number of iterations exceeded limit."
              << "Rising max_it might help\n";

    return rec;
}

bool sip_oracle_solver::set_option(const std::string& option, double value) {
    if(option == "feas_tol") {
        if(value >= 0) {
            feas_tol = value;
            return true;
        }
    }
    if(option == "abs_tol") {
        if(value >= 1e-9) {
            abs_tol = value;
            return true;
        }
    }
    if(option == "rel_tol") {
        if(value >= 1e-9) {
            rel_tol = value;
            return true;
        }
    }
    if(option == "abs_tol_ora") {
        if(value >= 1e-9) {
            abs_tol_ora = value;
            return true;
        }
    }
    if(option == "rel_tol_ora") {
        if(value >= 1e-9) {
            rel_tol_ora = value;
            return true;
        }
    }
    if(option == "abs_tol_ora_llp") {
        if(value >= 1e-9) {
            abs_tol_ora_llp = value;
            return true;
        }
    }
    if(option == "rel_tol_ora_llp") {
        if(value >= 1e-9) {
            rel_tol_ora_llp = value;
            return true;
        }
    }
    if(option == "abs_tol_lbp") {
        if(value >= 1e-9) {
            abs_tol_lbp = value;
            return true;
        }
    }
    if(option == "rel_tol_lbp") {
        if(value >= 1e-9) {
            rel_tol_lbp = value;
            return true;
        }
    }
    if(option == "abs_tol_lbp_llp") {
        if(value >= 1e-9) {
            abs_tol_lbp_llp = value;
            return true;
        }
    }
    if(option == "rel_tol_lbp_llp") {
        if(value >= 1e-9) {
            rel_tol_lbp_llp = value;
            return true;
        }
    }
    if(option == "red_tol_ora") {
        if(value > 1) {
            red_tol_ora = value;
            return true;
        }
    }
    if(option == "red_tol_lbp_llp") {
        if(value > 1) {
            red_tol_lbp_llp = value;
            return true;
        }
    }
    if(option == "red_tol_ora_llp") {
        if(value > 1) {
            red_tol_ora_llp = value;
            return true;
        }
    }
    if(option == "max_iter") {
        if(value >= 1) {
            max_iter = (int)value;
            return true;
        }
    }
    if(option == "max_time") {
        if(value >= 0) {
            max_time = value;
            return true;
        }
    }
    std::cout << "Failed to set option " << option << " = " << value << '\n';
    return false;
}

bool sip_oracle_solver::set_option(const std::string& option, const std::string& value) {
    if(option == "settings_lbp") {
        settings_lbp = value;
        return true;
    }
    if(option == "settings_ora") {
        settings_ora = value;
        return true;
    }
    if(option == "settings_lbp_llp") {
        settings_lbp_llp = value;
        return true;
    }
    if(option == "settings_ora_llp") {
        settings_ora_llp = value;
        return true;
    }
    std::cout << "Failed to set option " << option << " = " << value << '\n';
    return false;
}

bool sip_oracle_solver::set_option(const std::string& option, bool value) {
    if(option == "discr_all") {
        discr_all = value;
        return true;
    }
    std::cout << "Failed to set option " << option << " = " << value << '\n';
    return false;
}

void sip_oracle_solver::pass_option(const std::string& option, double value) {
    options.emplace_back(option, value);
}

double sip_oracle_solver::get_option(const std::string& option) const {
    if(option == "abs_tol") {
        return abs_tol;
    }
    if(option == "rel_tol") {
        return rel_tol;
    }
    if(option == "feas_tol") {
        return feas_tol;
    }
    throw std::invalid_argument("Option '" + option + "' not supported or implemented.");
}

void sip_oracle_solver::set_solver_base_name(std::string name) {
    solver_base_name = std::move(name);
}

std::string sip_oracle_solver::get_solver_base_name() {
    return solver_base_name;
}

} // namespace dips
