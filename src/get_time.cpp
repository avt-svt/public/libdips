/**********************************************************************************
 * Copyright (c) 2019 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

#include "get_time.hpp"

#include <sys/types.h> // for __syscall_slong_t, clockid_t
#include <stdexcept>   // for runtime_error

#if defined(_WIN32)

#define NOMINMAX // The header windows.h has macros for min and max which interfer with our overloads of min and max
#include <windows.h>

#elif defined(__linux__)

#include <pthread.h> // for pthread_getcpuclockid, pthread_self, pthread_t
#include <ctime>     // for clock_gettime, timespec

#elif defined(__APPLE__) && defined(__MACH__)

#include <mach/thread_act.h>
#include <pthread.h> // for pthread_getcpuclockid, pthread_self, pthread_t

#else
#error "No routines for measuring CPU and wall-clock time are available for your system."
#endif

namespace dips {

thread_clock::time_point thread_clock::now() {

    using seconds = std::chrono::seconds;
    using microsec = std::chrono::microseconds;
    using nanosec = std::chrono::nanoseconds;

#if defined(_WIN32)

    // note that Windows uses 100 nanosecond ticks for FILETIME

    FILETIME time_creation, time_exit, time_user, time_system;


    if(GetThreadTimes(GetCurrentThread(), &time_creation, &time_exit, &time_system, &time_user) != 0) {
        // this is untested and might be wrong
        auto user = ((static_cast<rep>(time_user.dwHighDateTime) << 32) | time_user.dwLowDateTime) * 100;
        auto system = ((static_cast<rep>(time_system.dwHighDateTime) << 32) | time_system.dwLowDateTime) * 100;
        return time_point(nanosec(system) + nanosec(user));
    }

#elif defined(__linux__)


    struct timespec ts { };

    // TODO: boost checks for CLOCK_THREAD_CPUTIME_ID here, and uses it instead of pth if defined
    // get the current thread
    pthread_t pth = pthread_self();

    // get the clock_id associated to the current thread
    clockid_t clock_id;
    pthread_getcpuclockid(pth, &clock_id);

    // get the timespec associated to the thread clock
    if(clock_gettime(clock_id, &ts) == 0) {
        // convert timespec struct to time_point
        return time_point(seconds(ts.tv_sec) + nanosec(ts.tv_nsec));
    }

#elif defined(__APPLE__) && defined(__MACH__)

    // get the thread port (borrowing pthread's reference)
    mach_port_t port = pthread_mach_thread_np(pthread_self());

    // get the thread info
    thread_basic_info_data_t info;
    mach_msg_type_number_t count = THREAD_BASIC_INFO_COUNT;
    if(thread_info(port, THREAD_BASIC_INFO, (thread_info_t)&info, &count) == KERN_SUCCESS) {
        // calculate user and system time
        auto user = seconds(info.user_time.seconds) + microsec(info.user_time.microseconds);

        auto system = seconds(info.system_time.seconds) + microsec(info.system_time.microseconds);

        return time_point(user + system);
    }

#endif

    throw std::runtime_error("Could not get current thread time");
}

} // namespace dips
