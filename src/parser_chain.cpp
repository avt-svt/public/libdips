#include "parser_chain.hpp"

#include <iostream>  // for cout
#include <iterator>  // for istreambuf_iterator, operator!=
#include <map>       // for map, _Rb_tree_iterator
#include <utility>   // for move, pair
#include <stdexcept> // for invalid_argument

#include "discretization.hpp" // for discretization

//TODO:check & documentation
namespace dips {

programdefinition_parser_chain::programdefinition_parser_chain(std::istream& is,
  ale::symbol_table& symbols,
  const std::vector<parser_specification>& specifications) :
    specifications(specifications),
    symbols(symbols) {

    //save current position in istream
    std::streampos pos = is.tellg();
    //copy whole stream into string
    content = std::string(std::istreambuf_iterator<char>(is), {});
    //reset the position (we do not want to modify the passed istream, but reading from it would)
    is.seekg(pos);
}

void programdefinition_parser_chain::parse() {
    std::vector<std::string> missing_blocks;
    for(const parser_specification& spec : specifications) {
        std::stringstream input(content);
        {
            programdefinition_parser parser(input, symbols, spec.block_name, spec.required_internals);
            parser.parse();
            definition_parsers.emplace_back(std::move(parser));
        }
    }
}

bool programdefinition_parser_chain::fail() {
    bool definition_parser_failed = false;
    for(auto& parser : definition_parsers) {
        std::cout << "Checking \"" << parser.get_block_name() << "\" program definitions\n";
        if(parser.fail()) {
            definition_parser_failed = true;
        } else {
            std::cout << "  OK.\n";
        }
    }
    return definition_parser_failed;
}

std::vector<discretization> programdefinition_parser_chain::get_discretizations(const std::string& block) {
    for(auto& it : definition_parsers) {
        if(it.get_block_name() == block) {
            return it.get_discretizations();
        }
    }
    throw std::invalid_argument("Error: could not retrieve discretizations of block \"" + block + "\" because block does not exist");
}

std::string programdefinition_parser_chain::get_internal(const std::string& block, const std::string& internal) {
    for(auto& it : definition_parsers) {
        if(it.get_block_name() == block) {
            if(it.get_internals().count(internal) != 1) {
                throw std::invalid_argument("Error: could not retrieve internal \"" + internal + "\" in block \"" + block + "\" because internal does not exist");
            } else {
                return it.get_internals().find(internal)->second;
            }
        }
    }
    throw std::invalid_argument("Error: could not retrieve internals of block \"" + block + "\" because block does not exist");
}
std::vector<std::string> programdefinition_parser_chain::get_internals_list(const std::string& block, const std::string& internal) {
    for(auto& it : definition_parsers) {
        if(it.get_block_name() == block) {
            if(it.get_internals_list().count(internal) != 1) {
                throw std::invalid_argument("Error: could not retrieve internal \"" + internal + "\" in block \"" + block + "\" because internal does not exist");
            } else {
                return it.get_internals_list().find(internal)->second;
            }
        }
    }
    throw std::invalid_argument("Error: could not retrieve internals of block \"" + block + "\" because block does not exist");
}

std::string programdefinition_parser_chain::get_info(const std::string& block, const std::string& info_name) {
    for(auto& it : definition_parsers) {
        if(it.get_block_name() == block) {
            if(it.get_info().count(info_name) != 1) {
                throw std::invalid_argument("Error: could not retrieve info \"" + info_name + "\" in block \"" + block + "\" because info does not exist");
            } else {
                return it.get_info().find(info_name)->second;
            }
        }
    }
    throw std::invalid_argument("Error: could not retrieve info of block \"" + block + "\" because block does not exist");
}
std::vector<std::string> programdefinition_parser_chain::get_info_list(const std::string& block, const std::string& info_name) {
    for(auto& it : definition_parsers) {
        if(it.get_block_name() == block) {
            if(it.get_info_list().count(info_name) != 1) {
                throw std::invalid_argument("Error: could not retrieve info \"" + info_name + "\" in block \"" + block + "\" because info does not exist");
            } else {
                return it.get_info_list().find(info_name)->second;
            }
        }
    }
    throw std::invalid_argument("Error: could not retrieve info of block \"" + block + "\" because block does not exist");
}

std::map<std::string, std::string> programdefinition_parser_chain::get_info(const std::string& block) {
    for(auto& it : definition_parsers) {
        if(it.get_block_name() == block) {
            return it.get_info();
        }
    }
    throw std::invalid_argument("Error: could not retrieve info of block \"" + block + "\" because block does not exist");
}
std::map<std::string, std::vector<std::string>> programdefinition_parser_chain::get_info_list(const std::string& block) {
    for(auto& it : definition_parsers) {
        if(it.get_block_name() == block) {
            return it.get_info_list();
        }
    }
    throw std::invalid_argument("Error: could not retrieve info of block \"" + block + "\" because block does not exist");
}

} // namespace dips
