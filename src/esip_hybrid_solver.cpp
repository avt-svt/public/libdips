#include "esip_hybrid_solver.hpp"

#include <algorithm>  // for min
#include <memory>     // for allocator_traits<>::value_type, unique_ptr
#include <fstream>    // for operator<<, basic_ostream::operator<<, basic_ostream, basic_ostream<>::__ostream_type, basic_ios::rdbuf, endl, basi...
#include <string>     // for string, allocator, operator+, operator==, to_string, char_traits, basic_string, operator<<
#include <filesystem> // for create_directories
#include <iostream>   // for cout
#include <limits>     // for numeric_limits
#include <list>       // for list
#include <stdexcept>  // for invalid_argument, runtime_error

#include "minmax_bnf_solver.hpp" // for minmax_bnf_solver, minmax_bnf_solver::program_info
#include "util/evaluator.hpp"    // for evaluate_expression
#include "common.hpp"            // for discretize, fix_variables, copy_variables, set_real
#include "symbol_table.hpp"      // for symbol_table

#ifdef DIPS_use_mpi
#include "mpi.h"
#endif

namespace dips {

esip_hybrid_program_definition_parser::esip_hybrid_program_definition_parser(std::istream& is, symbol_table& symbols) :
    programdefinition_parser_chain(is, symbols, { { "lbp", {} }, { "ubp", { "eps_res" } }, { "res", { "res_target" } }, { "mlp", {} } }) { }

std::vector<discretization> esip_hybrid_program_definition_parser::get_lbp_discretizations() {
    return definition_parsers.front().get_discretizations();
}
std::vector<discretization> esip_hybrid_program_definition_parser::get_ubp_discretizations() {
    return definition_parsers.at(1).get_discretizations();
}
std::vector<discretization> esip_hybrid_program_definition_parser::get_res_discretizations() {
    return definition_parsers.at(2).get_discretizations();
}
std::vector<discretization> esip_hybrid_program_definition_parser::get_mlp_discretizations() {
    return definition_parsers.back().get_discretizations();
}

void esip_hybrid_solver::program_info::add_mlp_and_llp(const program& mlp, const program& llp, const discretization& lbp_disc, const discretization& ubp_disc, const discretization& res_disc, const discretization& mlp_disc) {

    mlp_llp_pairs.emplace_back(mlp, llp);
    ubp_discs.emplace_back(ubp_disc);
    lbp_discs.emplace_back(lbp_disc);
    res_discs.emplace_back(res_disc);
    mlp_discs.emplace_back(mlp_disc);
}

solve_record esip_hybrid_solver::solve(program_info prog) {
    std::unique_ptr<solver> sub(make_subsolver(symbols, solver_base_name));
    minmax_bnf_solver minmax(symbols, make_subsolver, solver_base_name);

    for(auto& option : options) {
        sub->pass_option(option.first, option.second);
        minmax.pass_option(option.first, option.second);
    }
    solve_record rec;

    int n_mlp_llp_pairs = prog.mlp_llp_pairs.size();
    std::vector<solve_record> mlp_rec(n_mlp_llp_pairs);
    double eps_res = init_res;

    enum { LBP,
        UBP,
        RES } next
      = LBP;

    double feas_vio_lbp = std::numeric_limits<double>::infinity(); // feasibility violation of lbp
    double feas_vio_ubp = std::numeric_limits<double>::infinity(); // feasibility violation of ubp (always negative)

    for(int i = 0; i < max_iter; ++i) {
        scoped_push sp_it(sub->solver_name, "it_" + std::to_string(i + 1));
        std::cout << "\nIteration " << std::to_string(i + 1) << ":\n";
        std::cout << "LBD           = " << rec.m_lbd << '\n';
        std::cout << "UBD           = " << rec.m_ubd << "\n\n";

        // check termination
        if(check_termination(rec, feas_vio_ubp, abs_tol, rel_tol, feas_tol)) {
            return rec;
        }
        abs_tol_lbp = std::min(0.5 * abs_tol, abs_tol_lbp);
        abs_tol_ubp = std::min(0.5 * abs_tol, abs_tol_ubp);
        abs_tol_res = std::min(0.5 * abs_tol, abs_tol_res);
        switch(next) {
            case LBP: {
                scoped_push sp_lbp(sub->solver_name, "_lbp");
                std::cout << "\nESIP: solving LBP ";

                sub->set_option("abs_tol", abs_tol_lbp);
                sub->set_option("rel_tol", rel_tol_lbp);
                sub->set_option("settings", settings_lbp);
                sub->set_option("max_time", max_time - rec.m_cpu);

                auto lbp_rec = sub->solve(prog.lbp);

                rec.m_cpu += lbp_rec.m_cpu;

                std::cout << "reporting status\n";
                report(lbp_rec);

                if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                    return rec;
                }

                switch(lbp_rec.m_status) {
                    case GLOBAL:
                        rec.m_lbd = lbp_rec.m_lbd;
                        break;
                    case INFEASIBLE:
                        rec.m_status = INFEASIBLE;
                        rec.m_lbd = std::numeric_limits<double>::infinity();
                        rec.m_ubd = std::numeric_limits<double>::infinity();
                        return rec;
                    default:
                        throw std::invalid_argument("unhandled solver status: " + to_string(lbp_rec.m_status));
                }

                scoped_push sp_mlp_it(minmax.solver_name, "it_" + std::to_string(i + 1));
                bool ulp_feasible = true;
                double feas_vio_temp = -std::numeric_limits<double>::infinity();
                for(int j = 0; j < n_mlp_llp_pairs; ++j) {

                    std::cout << "\nESIP: solving mlp_" << j + 1 << '\n';
                    // note: always minimizing. This leads to
                    //  - flipped sign of objective
                    //  - flipped lbd and ubd
                    scoped_push sp_mlp_mlp(minmax.solver_name, "_mlp" + std::to_string(i + 1));
                    scoped_push sp_mlp_jth(minmax.solver_name, "_" + std::to_string(j + 1) + "/");

                    // create output folder name and folder for minmax solver
                    std::filesystem::create_directories(minmax.get_solver_base_name());

                    symbols.push_scope();
                    fix_variables(lbp_rec, symbols);

                    minmax_bnf_solver::program_info minmax_program;
                    minmax_program.lbp = prog.mlp_llp_pairs[j].first;
                    minmax_program.add_llp(prog.mlp_llp_pairs[j].second, prog.mlp_discs[j]);

                    minmax.set_option("rel_tol", rel_tol_lbp_minmax);
                    minmax.set_option("abs_tol", abs_tol_lbp_minmax);

                    minmax.set_option("settings_lbp", settings_lbp_minmax_ulp);
                    minmax.set_option("settings_lbp_llp", settings_lbp_minmax_llp);
                    minmax.set_option("max_time", max_time - rec.m_cpu);

                    mlp_rec[j] = minmax.solve(minmax_program);

                    rec.m_cpu += mlp_rec[j].m_cpu;
                    symbols.pop_scope();

                    std::cout << "reporting status\n";
                    report(mlp_rec[j]);

                    if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                        return rec;
                    }

                    switch(mlp_rec[j].m_status) {
                        case INFEASIBLE:
                            // by convention: mlp_rec.m_ubd = mlp_rec.m_lbd = + infty
                            // => -mlp_rec.m_lbd <= feas_tol
                            // => incumbent point in mlp[j] sip feasible
                            // => no tolerance update necessary
                            break;
                        case GLOBAL:
                            if(-mlp_rec[j].m_lbd <= feas_tol) {
                                // => incumbent point in mlp[j] sip feasible
                                // => nothing happens here
                                // => compute mlp[j+1]
                            } else {
                                // -mlp_rec.m_lbd > feas_tol
                                // incumbent point of lbp might be sip infeasible
                                ulp_feasible = false;
                            }
                            // tolerance update if necessary
                            // tolerance update if necessary
                            if(-mlp_rec[j].m_lbd > 0 && abs_tol_lbp_minmax > -mlp_rec[j].m_lbd / red_tol_lbp_minmax) {
                                set_tolerance_robust("abs_tol_lbp_llp", -mlp_rec[j].m_lbd / red_tol_lbp_minmax);
                            }
                            if(-mlp_rec[j].m_lbd > 0 && abs_tol_ubp_minmax > -mlp_rec[j].m_lbd / red_tol_ubp_minmax) {
                                set_tolerance_robust("abs_tol_ubp_llp", -mlp_rec[j].m_lbd / red_tol_ubp_minmax);
                            }
                            if(-mlp_rec[j].m_lbd > 0 && abs_tol_res_minmax > -mlp_rec[j].m_lbd / red_tol_res_minmax) {
                                set_tolerance_robust("abs_tol_res_llp", -mlp_rec[j].m_lbd / red_tol_res_minmax);
                            }
                            break;
                        default:
                            throw std::invalid_argument("lbp_mlp" + std::to_string(j + 1) + ": unhandled solver status: " + to_string(mlp_rec[j].m_status));
                    }
                    // keeping track of max feasibility violation
                    if(-mlp_rec[j].m_lbd > feas_vio_temp) {
                        feas_vio_temp = -mlp_rec[j].m_lbd;
                    }
                }
                // check sip-feasibility
                if(ulp_feasible) {
                    // => incumbent point in all llps sip feasible
                    feas_vio_ubp = feas_vio_lbp;
                    rec.m_ubd = lbp_rec.m_ubd;
                    copy_variables(lbp_rec, rec);
                }
                // check termination
                if(check_termination(rec, feas_vio_temp, abs_tol, rel_tol, feas_tol)) {
                    return rec;
                }

                // discretize
                for(int j = 0; j < n_mlp_llp_pairs; ++j) {
                    if(discr_all || -mlp_rec[j].m_ubd > 0) {
                        // discretize always or only if llp[j] produced a valid cut
                        discretize(prog.lbp_discs[j], mlp_rec[j], symbols);
                        discretize(prog.ubp_discs[j], mlp_rec[j], symbols);
                        discretize(prog.res_discs[j], mlp_rec[j], symbols);
                        std::cout << "discretized mlp_" << j + 1 << ".\n";
                    }
                }
                next = UBP;
                break;
            }
            case UBP: {
                std::cout << "\nESIP: solving UBP ";

                set_real(prog.ubp_restrict, eps_res, symbols);

                sub->set_option("abs_tol", abs_tol_ubp);
                sub->set_option("rel_tol", rel_tol_ubp);
                sub->set_option("settings", settings_ubp);
                sub->set_option("max_time", max_time - rec.m_cpu);

                auto ubp_rec = sub->solve(prog.ubp);

                std::cout << ubp_rec.m_cpu << '\n';
                rec.m_cpu += ubp_rec.m_cpu;
                std::cout << "reporting status\n";
                report(ubp_rec);
                switch(ubp_rec.m_status) {
                    case INFEASIBLE:
                        // ubp infeasible, eps_res chosen too large
                        // => reduce eps_res
                        // => end iteration and go to LBP
                        eps_res /= red_res;
                        next = LBP;
                        continue;

                    case GLOBAL:
                        // computed candidate point for ubd
                        // => nothing happens here
                        // => compute llps
                        break;
                    default:
                        throw std::invalid_argument("ubp: unhandled solver status: " + to_string(ubp_rec.m_status));
                }
                // tolerance update for minmax if necessary
                if(abs_tol_lbp_minmax >= eps_res) {
                    set_tolerance_robust("abs_tol_lbp_llp", eps_res / red_tol_lbp_minmax);
                }
                if(abs_tol_ubp_minmax >= eps_res) {
                    set_tolerance_robust("abs_tol_ubp_llp", eps_res / red_tol_ubp_minmax);
                }
                if(abs_tol_res_minmax >= eps_res) {
                    set_tolerance_robust("abs_tol_res_llp", eps_res / red_tol_res_minmax);
                }

                scoped_push sp_mlp_it(minmax.solver_name, "it_" + std::to_string(i + 1));
                bool ulp_feasible = true;
                double feas_vio_temp = -std::numeric_limits<double>::infinity();
                for(int j = 0; j < n_mlp_llp_pairs; ++j) {

                    std::cout << "\nESIP: solving mlp_" << j + 1 << '\n';
                    // note: always minimizing. This leads to
                    //  - flipped sign of objective
                    //  - flipped lbd and ubd
                    scoped_push sp_mlp_mlp(minmax.solver_name, "_mlp" + std::to_string(i + 1));
                    scoped_push sp_mlp_jth(minmax.solver_name, "_" + std::to_string(j + 1) + "/");

                    // create output folder name and folder for minmax solver
                    std::filesystem::create_directories(minmax.get_solver_base_name());

                    symbols.push_scope();
                    fix_variables(ubp_rec, symbols);

                    minmax_bnf_solver::program_info minmax_program;
                    minmax_program.lbp = prog.mlp_llp_pairs[j].first;
                    minmax_program.add_llp(prog.mlp_llp_pairs[j].second, prog.mlp_discs[j]);

                    minmax.set_option("rel_tol", rel_tol_ubp_minmax);
                    minmax.set_option("abs_tol", abs_tol_ubp_minmax);


                    minmax.set_option("settings_lbp", settings_ubp_minmax_ulp);
                    minmax.set_option("settings_lbp_llp", settings_ubp_minmax_llp);
                    minmax.set_option("max_time", max_time - rec.m_cpu);

                    mlp_rec[j] = minmax.solve(minmax_program);

                    rec.m_cpu += mlp_rec[j].m_cpu;
                    symbols.pop_scope();

                    std::cout << "reporting status\n";
                    report(mlp_rec[j]);

                    if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                        return rec;
                    }

                    switch(mlp_rec[j].m_status) {
                        case INFEASIBLE:
                            // by convention: mlp_rec.m_ubd = mlp_rec.m_lbd = + infty
                            // => -mlp_rec.m_lbd <= feas_tol
                            // => incumbent point in mlp[j] sip feasible
                            // => no tolerance update necessary
                            break;
                        case GLOBAL:
                            if(-mlp_rec[j].m_lbd <= feas_tol) {
                                // => incumbent point in mlp[j] sip feasible
                                // => nothing happens here
                                // => compute mlp[j+1]
                            } else {
                                // -mlp_rec.m_lbd > feas_tol
                                // incumbent point of lbp might be sip infeasible
                                ulp_feasible = false;
                            }
                            break;
                        default:
                            throw std::invalid_argument("ubp_mlp" + std::to_string(j + 1) + ": unhandled solver status: " + to_string(mlp_rec[j].m_status));
                    }
                    // keeping track of max feasibility violation
                    if(-mlp_rec[j].m_lbd > feas_vio_temp) {
                        feas_vio_temp = -mlp_rec[j].m_lbd;
                    }
                }
                // check sip-feasibility
                if(ulp_feasible) {
                    std::cout << "UBP obtained a feasible point with value " << ubp_rec.m_ubd << " eps_res: " << eps_res << std::endl;
                    // => incumbent point in all llps sip feasible
                    if(ubp_rec.m_ubd < rec.m_ubd) {
                        rec.m_ubd = ubp_rec.m_ubd;
                        copy_variables(ubp_rec, rec);
                    }
                    eps_res /= red_res;
                    next = RES;

                } else {
                    // discretize
                    for(int j = 0; j < n_mlp_llp_pairs; ++j) {
                        if(discr_all || -mlp_rec[j].m_ubd > 0) {
                            // discretize always or only if llp[j] produced a valid cut
                            discretize(prog.ubp_discs[j], mlp_rec[j], symbols);
                            discretize(prog.lbp_discs[j], mlp_rec[j], symbols);
                            discretize(prog.res_discs[j], mlp_rec[j], symbols);
                            std::cout << "discretized mlp_" << j + 1 << ".\n";
                        } else if(!(-mlp_rec[j].m_lbd <= 0)) {
                            // discretize always in ubp and res if llp[j] signals SIP-infeasibility and thus can be a valid cut with restriction
                            discretize(prog.ubp_discs[j], mlp_rec[j], symbols);
                            discretize(prog.res_discs[j], mlp_rec[j], symbols);
                            std::cout << "discretized mlp_" << j + 1 << ".\n";
                        }
                    }
                    next = UBP;
                }
                // check termination
                if(check_termination(rec, feas_vio_temp, abs_tol, rel_tol, feas_tol)) {
                    return rec;
                }
                break;
            }

            case RES: {
                // check termination
                if(check_termination(rec, feas_vio_ubp, abs_tol, rel_tol, feas_tol)) {
                    return rec;
                }
                scoped_push sp_res(sub->solver_name, "_res");
                std::cout << "\nsip: solving res\n";
                // note: objective of res = - eta

                double obj_target = 0.5 * (rec.m_ubd + rec.m_lbd);
                set_real(prog.res_target, obj_target, symbols);

                sub->set_option("abs_tol", abs_tol_res);
                sub->set_option("rel_tol", rel_tol_res);
                sub->set_option("settings", settings_res);
                sub->set_option("max_time", max_time - rec.m_cpu);

                auto res_rec = sub->solve(prog.res);

                rec.m_cpu += res_rec.m_cpu;

                std::cout << "reporting status\n";
                report(res_rec);

                if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                    return rec;
                }

                switch(res_rec.m_status) {
                    case INFEASIBLE:
                        // by convention: llp_rec.m_ubd = llp_rec.m_lbd = + infty
                        // => res computed a valid lower bound
                        rec.m_lbd = obj_target;

                        next = RES;
                        continue;
                    case GLOBAL:
                        if(-res_rec.m_lbd < 0) {
                            // => res computed a valid lower bound
                            rec.m_lbd = obj_target;

                            next = RES;

                            continue;
                        } else if(-res_rec.m_ubd <= 0) {

                            next = LBP;

                            continue;
                        }
                        break;
                    default:
                        throw std::invalid_argument("res: unhandled solver status: " + to_string(res_rec.m_status));
                }

                symbols.push_scope();
                fix_variables(res_rec, symbols);
                double obj = util::evaluate_expression(prog.lbp.m_objective.front(), symbols);
                symbols.pop_scope();
                std::cout << "\nESIP: solving MLP\n";

                scoped_push sp_mlp_it(minmax.solver_name, "it_" + std::to_string(i + 1));
                bool res_feasible = true;
                bool res_restricted = false;
                for(int j = 0; j < n_mlp_llp_pairs; ++j) {

                    std::cout << "\nESIP: solving mlp_" << j + 1 << '\n';
                    // note: always minimizing. This leads to
                    //  - flipped sign of objective
                    //  - flipped lbd and ubd
                    scoped_push sp_mlp_mlp(minmax.solver_name, "_mlp" + std::to_string(i + 1));
                    scoped_push sp_mlp_jth(minmax.solver_name, "_" + std::to_string(j + 1) + "/");

                    // create output folder name and folder for minmax solver
                    std::filesystem::create_directories(minmax.get_solver_base_name());

                    symbols.push_scope();
                    fix_variables(res_rec, symbols);

                    minmax_bnf_solver::program_info minmax_program;
                    minmax_program.lbp = prog.mlp_llp_pairs[j].first;
                    minmax_program.add_llp(prog.mlp_llp_pairs[j].second, prog.mlp_discs[j]);

                    minmax.set_option("abs_tol", abs_tol_res_minmax);
                    minmax.set_option("rel_tol", rel_tol_res_minmax);


                    minmax.set_option("settings_lbp", settings_res_minmax_ulp);
                    minmax.set_option("settings_lbp_llp", settings_res_minmax_llp);
                    minmax.set_option("max_time", max_time - rec.m_cpu);

                    mlp_rec[j] = minmax.solve(minmax_program);

                    rec.m_cpu += mlp_rec[j].m_cpu;
                    symbols.pop_scope();

                    std::cout << "reporting status\n";
                    report(mlp_rec[j]);

                    if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                        return rec;
                    }

                    switch(mlp_rec[j].m_status) {
                        case INFEASIBLE:
                            // by convention: mlp_rec.m_ubd = mlp_rec.m_lbd = + infty
                            // => -mlp_rec.m_lbd <= feas_tol
                            // => incumbent point in mlp[j] sip feasible
                            // => no tolerance update necessary
                            break;
                        case GLOBAL:
                            if(-mlp_rec[j].m_lbd > feas_tol) {
                                // -llp_rec.m_lbd > feas_tol
                                // incumbent point of res might be sip infeasible
                                res_feasible = false;
                            }
                            // used to to be -res_rec.m_ubd > feas_tol
                            else if(-mlp_rec[j].m_ubd > feas_tol) {
                                res_restricted = true;
                            }
                            break;
                        default:
                            throw std::invalid_argument("res_mlp" + std::to_string(j + 1) + ": unhandled solver status: " + to_string(mlp_rec[j].m_status));
                    }
                }
                // check sip-feasibility
                if(res_feasible) {

                    eps_res = std::min(eps_res, -res_rec.m_lbd / red_res);
                    rec.m_status = FEASIBLE;
                    if(obj < rec.m_ubd) {
                        rec.m_ubd = obj;
                        copy_variables(res_rec, rec);
                    } else if(obj > rec.m_ubd) {
                        throw std::runtime_error("res: invalid behaviour: obj > rec.m_ubd.");
                    }

                    next = UBP;

                    continue;
                } else if(res_restricted) {
                    // discretize
                    for(int j = 0; j < n_mlp_llp_pairs; ++j) {
                        if(discr_all || -res_rec.m_ubd > 0) {
                            // discretize always or only if llp[j] produced a valid cut
                            discretize(prog.lbp_discs[j], mlp_rec[j], symbols);
                            discretize(prog.ubp_discs[j], mlp_rec[j], symbols);
                            discretize(prog.res_discs[j], mlp_rec[j], symbols);
                            std::cout << "discretized llp_" << j + 1 << ".\n";
                        }
                    }
                }

                next = LBP;

                break;
            }

            default: {
                throw std::invalid_argument("procedure not implemented");
            }
        } //end switch
#ifdef DIPS_use_mpi
        int _rank;
        MPI_Comm_rank(MPI_COMM_WORLD, &_rank);
        if(_rank == 0) {
#endif
            // write discretization points to output file
            std::string disc_output_path = "./";
            for(auto& i : sub->solver_name) {
                disc_output_path += i;
            }
            std::ofstream disc_output(disc_output_path + "_disc_points.txt");
            std::streambuf* backup = std::cout.rdbuf();
            std::cout.rdbuf(disc_output.rdbuf());
            std::vector<std::vector<discretization>> discs;
            discs.emplace_back(prog.lbp_discs);
            discs.emplace_back(prog.ubp_discs);
            discs.emplace_back(prog.res_discs);
            report(symbols, discs);
            std::cout.rdbuf(backup); // reassign cout to its former buffer before that buffer no longer exists
            disc_output.close();     // close file output
#ifdef DIPS_use_mpi
        }
        MPI_Barrier(MPI_COMM_WORLD);
#endif
    }

    rec.m_status = EXCEEDED_MAX_ITERATIONS;
    std::cout << solver_base_name << ": total number of iterations exceeded limit."
              << "Rising max_it might help\n";
    return rec;
}

bool esip_hybrid_solver::set_option(const std::string& option, double value) {
    if(option == "feas_tol") {
        if(value >= 0) {
            feas_tol = value;
            return true;
        }
    }
    if(option == "abs_tol") {
        if(value >= 1e-9) {
            abs_tol = value;
            return true;
        }
        return false;
    }
    if(option == "rel_tol") {
        if(value >= 1e-9) {
            rel_tol = value;
            return true;
        }
        return false;
    }
    if(option == "max_iter") {
        if(value >= 1) {
            max_iter = (int)value;
            return true;
        }
        return false;
    }
    if(option == "init_res") {
        if(value > 0) {
            init_res = value;
            return true;
        }
        return false;
    }
    if(option == "red_res") {
        if(value > 1) {
            red_res = value;
            return true;
        }
        return false;
    }
    if(option == "abs_tol_lbp_llp") {
        if(value >= 1e-9) {
            abs_tol_lbp_minmax = value;
            return true;
        }
    }
    if(option == "rel_tol_lbp_llp") {
        if(value >= 1e-9) {
            rel_tol_lbp_minmax = value;
            return true;
        }
    }
    if(option == "red_tol_lbp_llp") {
        if(value > 1) {
            red_tol_lbp_minmax = value;
            return true;
        }
    }
    if(option == "abs_tol_ubp_llp") {
        if(value >= 1e-9) {
            abs_tol_ubp_minmax = value;
            return true;
        }
    }
    if(option == "rel_tol_ubp_llp") {
        if(value >= 1e-9) {
            rel_tol_ubp_minmax = value;
            return true;
        }
    }
    if(option == "red_tol_ubp_llp") {
        if(value > 1) {
            red_tol_ubp_minmax = value;
            return true;
        }
    }
    if(option == "abs_tol_res_llp") {
        if(value >= 1e-9) {
            abs_tol_res_minmax = value;
            return true;
        }
    }
    if(option == "rel_tol_res_llp") {
        if(value >= 1e-9) {
            rel_tol_res_minmax = value;
            return true;
        }
    }
    if(option == "red_tol_res_llp") {
        if(value > 1) {
            red_tol_res_minmax = value;
            return true;
        }
    }
    return false;
}

bool esip_hybrid_solver::set_option(const std::string& option, const std::string& value) {
    if(option == "settings_lbp") {
        settings_lbp = value;
        return true;
    }
    if(option == "settings_ubp") {
        settings_ubp = value;
        return true;
    }
    if(option == "settings_res") {
        settings_res = value;
        return true;
    }
    if(option == "settings_lbp_minmax_ulp") {
        settings_lbp_minmax_ulp = value;
        return true;
    }
    if(option == "settings_lbp_minmax_llp") {
        settings_lbp_minmax_llp = value;
        return true;
    }
    if(option == "settings_ubp_minmax_ulp") {
        settings_ubp_minmax_ulp = value;
        return true;
    }
    if(option == "settings_ubp_minmax_llp") {
        settings_ubp_minmax_llp = value;
        return true;
    }
    if(option == "settings_res_minmax_ulp") {
        settings_res_minmax_ulp = value;
        return true;
    }
    if(option == "settings_res_minmax_llp") {
        settings_res_minmax_llp = value;
        return true;
    }


    return false;
}

double esip_hybrid_solver::get_option(const std::string& option) const {
    if(option == "abs_tol") {
        return abs_tol;
    }
    if(option == "rel_tol") {
        return rel_tol;
    }
    if(option == "feas_tol") {
        return feas_tol;
    }
    throw std::invalid_argument("Option '" + option + "' not supported or implemented.");
}

} // namespace dips
