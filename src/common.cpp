#include "common.hpp"

#include <string>        // for operator+, allocator, char_traits, string, basic_string, to_string
#include <cassert>       // for assert
#include <algorithm>     // for max_element
#include <array>         // for array
#include <list>          // for operator!=, operator==, _List_iterator
#include <memory>        // for unique_ptr
#include <unordered_map> // for _Node_const_iterator, operator!=, operator==, _Node_iterator_base, _Map_base<>::mapped_type, unordered_map, unorde...
#include <utility>       // for pair
#include <variant>       // for variant
#include <stdexcept>     // for invalid_argument

#include "symbol.hpp"             // for base_symbol, cast_parameter_symbol, parameter_symbol, variable_symbol, cast_variable_symbol, value_symbol<>::variant
#include "util/visitor_utils.hpp" // for call_visitor
#include "config.hpp"             // for LIBALE_MAX_DIM, LIBALE_MAX_SET_DIM
#include "discretization.hpp"     // for discretization
#include "symbol_table.hpp"       // for symbol_table
#include "value.hpp"              // for tensor_type<>::basic_type, index, set, atom_type, real, tensor_type


namespace dips {



using namespace ale;

class symbol_appender_visitor {
public:
    symbol_appender_visitor(base_symbol* dest, size_t index) :
        dest(dest), index(index) { }

    void operator()(base_symbol* /*sym*/) {
        throw std::invalid_argument("destination symbol cannot be extender or destination and source symbol do not match");
    }

    template <typename TType>
    void operator()(parameter_symbol<TType>* source) {
        constexpr auto IDim = get_node_dimension<TType>;
        using dest_type = tensor_type<atom_type<TType>, IDim + 1>;

        if constexpr(IDim >= LIBALE_MAX_DIM || (is_set_node<TType> && IDim >= LIBALE_MAX_SET_DIM)) {
            throw std::invalid_argument("dimension of destination symbol bigger than LIBALE_MAX_DIM");
        } else {
            auto* cast_dest = cast_parameter_symbol<dest_type>(dest);
            if(cast_dest == nullptr) {
                throw std::invalid_argument("types of source \"" + source->m_name + "\" and dest do not match");
            }

            std::array<size_t, IDim + 1> shape = cast_dest->m_value.shape();
            shape.at(0) = index;
            cast_dest->m_value.resize(shape);

            if constexpr(IDim == 0) {
                cast_dest->m_value[index - 1] = source->m_value;
            } else {
                cast_dest->m_value[index - 1].assign(source->m_value);
            }
        }
    }

    template <typename TType>
    void operator()(variable_symbol<TType>* source) {
        constexpr auto IDim = get_node_dimension<TType>;
        using dest_type = tensor_type<atom_type<TType>, IDim + 1>;

        if constexpr(IDim == LIBALE_MAX_DIM) {
            throw std::invalid_argument("dimension of destination symbol bigger than LIBALE_MAX_DIM");
        } else {
            auto* cast_dest = cast_variable_symbol<dest_type>(dest);
            if(cast_dest == nullptr) {
                throw std::invalid_argument("types of source \"" + source->m_name + "\" and dest do not match");
            }

            std::array<size_t, IDim + 1> shape = cast_dest->shape();
            shape.at(0) = index;
            cast_dest->resize(shape);

            if constexpr(IDim == 0) {
                cast_dest->lower()[index - 1] = source->lower();
                cast_dest->upper()[index - 1] = source->upper();
            } else {
                cast_dest->lower()[index - 1].assign(source->lower());
                cast_dest->upper()[index - 1].assign(source->upper());
            }
        }
    }

private:
    base_symbol* dest;
    size_t index;
};

void append_symbol(base_symbol* source, base_symbol* dest, size_t index) {
    symbol_appender_visitor appender(dest, index);
    call_visitor(appender, source);
}

class symbol_resetter_visitor {
public:
    void operator()(base_symbol* /*sym*/) {
        throw std::invalid_argument("can only reset parameters and variables");
    }

    template <typename TType>
    void operator()(parameter_symbol<TType>* sym) {
        if constexpr(get_node_dimension<TType> == 0) {
            throw std::invalid_argument("cannot reset scalar parameter");
        } else {
            std::array<size_t, TType::dim> shape = sym->m_value.shape();
            shape[0] = 0;
            sym->m_value.resize(shape);
        }
    }

    template <typename TType>
    void operator()(variable_symbol<TType>* sym) {
        if constexpr(get_node_dimension<TType> == 0) {
            throw std::invalid_argument("cannot reset scalar variable");
        } else {
            std::array<size_t, TType::dim> shape = sym->shape();
            shape[0] = 0;
            sym->resize(shape);
        }
    }
};

void reset_symbol(base_symbol* sym) {
    call_visitor(symbol_resetter_visitor {}, sym);
}

void fix_variables(const solve_record& rec, symbol_table& symbols) {
    fix_variables(rec.m_solution, symbols);
}
void fix_variables(const solution& sol, symbol_table& symbols) {
    for(const auto& it : sol) {
        symbols.define(it.first, it.second->clone());
    }
}

void fix_variables(const solve_record& rec, symbol_table& symbols, std::vector<std::string> variables) {
    for(auto& variable : variables) {
        auto target = rec.m_solution.find(variable);
        if(target == rec.m_solution.end()) {
            throw std::invalid_argument("variable \"" + variable + "\" not found in solve record");
        }
        symbols.define(variable, target->second->clone());
    }
}

class fix_initial_visitor {
public:
    fix_initial_visitor(symbol_table& symbols) :
        symbols(symbols) { }

    void operator()(base_symbol* /*sym*/) { }

    template <unsigned IDim>
    void operator()(parameter_symbol<real<IDim>>* sym) {
        auto* new_variable = cast_variable_symbol<real<IDim>>(symbols.resolve(sym->m_name)->clone());
        new_variable->init() = sym->m_value;

        symbols.define(sym->m_name, new_variable);
    }

    symbol_table& symbols;
};

void fix_initial(const solve_record& rec, symbol_table& symbols) {
    fix_initial(rec.m_solution, symbols);
}
void fix_initial(const solution& sol, symbol_table& symbols) {
    for(const auto& it : sol) {
        fix_initial_visitor visitor(symbols);
        call_visitor(visitor, it.second.get());
    }
}

void fix_initial(const solve_record& rec, symbol_table& symbols, const std::vector<std::string>& variables) {
    for(const auto& variable : variables) {
        auto target = rec.m_solution.find(variable);
        if(target == rec.m_solution.end()) {
            throw std::invalid_argument("variable " + variable + " not found in solve record");
        }

        fix_initial_visitor visitor(symbols);
        call_visitor(visitor, target->second.get());
    }
}

void set_real(const std::string& name, double value, symbol_table& symbols) {

    auto* sym = cast_parameter_symbol<real<0>>(symbols.resolve(name));
    if(sym != nullptr) {
        symbols.define(name, new parameter_symbol<real<0>>(name, value));

    } else {
        throw std::invalid_argument("could not set parameter \"" + name + "\" to " + std::to_string(value) + ". Parameter has wrong size or name does not exist.");
    }
}

void copy_variables(const solve_record& source, solve_record& dest) {
    copy_variables(source.m_solution, dest.m_solution);
}
void copy_variables(const solution& source, solution& dest) {
    for(const auto& it : source) {
        dest[it.first].reset(it.second->clone());
    }
}

void add_renamed_symbol_to_record(const std::string& src_name, const std::string& dest_name, symbol_table& symbols, solve_record& dest) {
    base_symbol* parameter_copy = symbols.resolve(src_name)->clone();
    call_visitor([&dest_name](auto* sym) { sym->m_name = dest_name; }, parameter_copy);
    dest.m_solution[dest_name].reset(parameter_copy);
}

//solve_record usually only record variables not discretization parameters, we have to extract it from the symbol table manually
void add_discretization_parameters(const discretization& disc, symbol_table& symbols, solve_record& dest) {
    for(const auto& it : disc.parameters) {
        //save under ulp+first, because using it.second would be different depending if called from ubp or lbp, and just it.first would collide with mlp_variable name.
        add_renamed_symbol_to_record(it.second, "disc" + it.first, symbols, dest);
        assert(dest.m_solution.count("disc" + it.first) == 1);
    }
}

void reset_symbols(const discretization& disc, symbol_table& symbols) {
    auto* indexes = cast_parameter_symbol<set<index<0>, 0>>(symbols.resolve(disc.indexes));
    if(indexes == nullptr) {
        throw std::invalid_argument("indexset \"" + disc.indexes + "\" has improper type");
    }
    indexes->m_value.clear();

    for(const auto& parameter : disc.parameters) {
        auto* sym = symbols.resolve(parameter.second);
        if(sym == nullptr) {
            throw std::invalid_argument("target is undefined");
        }
        reset_symbol(sym);
    }
    for(const auto& variable : disc.variables) {
        auto* sym = symbols.resolve(variable.second);
        if(sym == nullptr) {
            throw std::invalid_argument("target is undefined");
        }
        reset_symbol(sym);
    }
}

void discretize(const discretization& disc, const solve_record& rec, symbol_table& symbols) {
    int ind = 0;
    auto* indexes_old = cast_parameter_symbol<set<index<0>, 0>>(symbols.resolve(disc.indexes));

    if(indexes_old == nullptr) {
        throw std::invalid_argument("indexset \"" + disc.indexes + "\" has improper type");
    }
    symbols.define(disc.indexes, indexes_old->clone());
    auto* indexes = cast_parameter_symbol<set<index<0>, 0>>(symbols.resolve(disc.indexes));

    if(indexes->m_value.empty()) {
        ind = 1;
    } else {
        ind = *std::max_element(indexes->m_value.begin(), indexes->m_value.end()) + 1;
    }
    indexes->m_value.push_back(ind);

    //add a discretization point to the upper level problem
    //extract the values from the solve_record of the lower level problem
    //add it as a collum of parameters to the upper level problem
    for(const auto& parameter : disc.parameters) {
        auto jt = rec.m_solution.find(parameter.first);
        if(jt == rec.m_solution.end()) {
            throw std::invalid_argument("discretization member \"" + parameter.first + "\" not found");
        }
        auto* dest_old = symbols.resolve(parameter.second);
        if(dest_old == nullptr) {
            throw std::invalid_argument("discretization variable \"" + parameter.second + "\" is undefined");
        }
        symbols.define(parameter.second, dest_old->clone());
        auto* dest = symbols.resolve(parameter.second);

        append_symbol(jt->second.get(), dest, ind);
    }

    //add additional variables to the upper level problem for cases where additional variables are needed per discretization point
    for(const auto& variable : disc.variables) {
        auto* source = symbols.resolve(variable.first);
        if(source == nullptr) {
            throw std::invalid_argument("variable \"" + variable.first + "\" is undefined");
        }
        auto* dest_old = symbols.resolve(variable.second);
        if(dest_old == nullptr) {
            throw std::invalid_argument("discretization variable \"" + variable.second + "\" is undefined");
        }
        auto* dest = symbols.resolve(variable.second);
        append_symbol(source, dest, ind);
    }
}

void discretize(const discretization& disc, const solve_record& /*rec_ulp*/, const solve_record& rec_llp, symbol_table& symbols) {
    discretize(disc, rec_llp, symbols);
}


scoped_callback_vector_clearer::~scoped_callback_vector_clearer() {
    managed.clear();
};

} // namespace dips
