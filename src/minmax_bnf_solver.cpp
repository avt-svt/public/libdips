#include "minmax_bnf_solver.hpp"

#include <algorithm> // for min, max
#include <memory>    // for allocator_traits<>::value_type, unique_ptr
#include <cmath>     // for fabs
#include <fstream>   // for operator<<, basic_ostream, basic_ostream<>::__ostream_type, basic_ostream::operator<<, basic_ios::rdbuf, endl, basic_o...
#include <iostream>  // for cout
#include <limits>    // for numeric_limits
#include <stdexcept> // for invalid_argument, runtime_error

#include "common.hpp"        // for copy_variables, discretize, fix_variables
#include "symbol_table.hpp"  // for symbol_table, scope_guard
#include "output.hpp"        // to_json, from_json
#include "nlohmann/json.hpp" // for json

#ifdef DIPS_use_mpi
#include "mpi.h"
#endif

namespace dips {


minmax_bnf_program_definition_parser::minmax_bnf_program_definition_parser(std::istream& is, symbol_table& symbols) :
    programdefinition_parser_chain(is, symbols, { { "lbp", {} } }) { }

void minmax_bnf_solver::program_info::add_llp(const program& llp, const discretization& disc) {
    llps.emplace_back(llp);
    lbp_discs.emplace_back(disc);
}

solve_record minmax_bnf_solver::solve(program_info prog) {
    std::unique_ptr<solver> sub(make_subsolver(symbols, get_solver_base_name()));

    for(auto& option : options) {
        sub->pass_option(option.first, option.second);
    }
    solve_record rec;
    solve_record lbp_rec;
    int n_llps = prog.llps.size();
    if(n_llps > 1) {
        std::cout << "warning: number of llps = " << n_llps << " > 1.\n";
    }
    std::vector<solve_record> llp_rec(n_llps);

    enum { LBP,
        NDEF } next_problem
      = LBP;

    double feas_vio_lbp = std::numeric_limits<double>::infinity(); // feasibility violation of lbp; in minmax this corresponds to the ubd


    //make sure not to interfere with the symbols of the calling context
    //this might be caused by discretization or appending (see issue 36 )
    scope_guard minmax_scope(symbols); // push minmax_scope
    /*
        // copy contents of discretization symbols over (needed?)
        for(auto disc: prog.lbp_discs)
        {
            symbols.define(disc.indexes, symbols.resolve(disc.indexes)->clone());
            for (auto& disc_pair: disc.parameters) {
                symbols.define(disc_pair.second, symbols.resolve(disc_pair.second)->clone());
            }
            for (auto& disc_pair: disc.variables) {
                symbols.define(disc_pair.second, symbols.resolve(disc_pair.second)->clone());
            }
        }
        */
    abs_tol_lbp = std::min(abs_tol_lbp, 0.4 * abs_tol);
    abs_tol_lbp_llp = std::min(abs_tol_lbp_llp, 0.4 * abs_tol);

    for(int i = 0; i < max_iter; i++) {
        scoped_push sp_it(sub->solver_name, "it_" + std::to_string(i + 1));

        std::cout << "\nIteration " << i + 1 << ":\n";
        std::cout << "LBD           = " << rec.m_lbd << '\n';
        std::cout << "UBD           = " << rec.m_ubd << "\n\n";


        //make sure that rel_tol of next iteration is more stringent than abs_tol
        if(i >= 1) {
            if((lbp_rec.m_ubd - lbp_rec.m_lbd) >= 0.5 * (rec.m_ubd - rec.m_lbd)) {
                double used_rel_tol = (lbp_rec.m_ubd == 0 ? lbp_rec.m_lbd : (lbp_rec.m_ubd - lbp_rec.m_lbd) / std::fabs(lbp_rec.m_ubd));
                if(rel_tol_lbp >= used_rel_tol) {
                    // terminated in lbp due to rel_tol
                    // => need to reduce rel_tol
                    set_tolerance_robust("rel_tol_lbp", used_rel_tol / 2.0);
                }
            }
        }

        if(next_problem == LBP) {
            scoped_push sp_lbp(sub->solver_name, "_lbp");
            std::cout << "\nminmax: solving lbp\n";

            sub->set_option("abs_tol", abs_tol_lbp);
            sub->set_option("rel_tol", rel_tol_lbp);
            sub->set_option("settings", settings_lbp);
            sub->set_option("max_time", max_time - rec.m_cpu);

            lbp_rec = sub->solve(prog.lbp);

            rec.m_cpu += lbp_rec.m_cpu;

            std::cout << "reporting status\n";
            report(lbp_rec);

            if(check_aborted(max_time, rec, get_solver_base_name(), aborted)) {
                return rec;
            }

            switch(lbp_rec.m_status) {
                case INFEASIBLE:
                    // lbp proves minmax to be infeasible
                    rec.m_status = INFEASIBLE;
                    rec.m_lbd = std::numeric_limits<double>::infinity();
                    rec.m_ubd = std::numeric_limits<double>::infinity();
                    return rec;
                case GLOBAL:
                    // computed valid lbd
                    rec.m_lbd = lbp_rec.m_lbd;
                    break;
                default:
                    throw std::invalid_argument("lbp: unhandled solver status: "
                                                + to_string(lbp_rec.m_status));
            }

            bool lbp_feasible = true;                                        // in minmax if lbp is feasible, it is optimal
            double feas_vio_temp = -std::numeric_limits<double>::infinity(); // temporary variable to find the worst found ubd for the incumbent point
            for(int j = 0; j < n_llps; ++j) {
                scoped_push sp_llp(sub->solver_name, "_llp_" + std::to_string(j + 1));
                std::cout << "minmax: solving lbp_llp_" << j + 1 << '\n';
                // note: always minimizing. This leads to
                //  - flipped sign of objective
                //  - flipped lbd and ubd

                symbols.push_scope();
                fix_variables(lbp_rec, symbols);

                sub->set_option("abs_tol", abs_tol_lbp_llp);
                sub->set_option("rel_tol", rel_tol_lbp_llp);
                sub->set_option("settings", settings_lbp_llp);
                sub->set_option("max_time", max_time - rec.m_cpu);

                llp_rec[j] = sub->solve(prog.llps[j]);

                rec.m_cpu += llp_rec[j].m_cpu;

                symbols.pop_scope();

                std::cout << "reporting status\n";
                report(llp_rec[j]);


                if(check_aborted(max_time, rec, get_solver_base_name(), aborted)) {
                    return rec;
                }

                switch(llp_rec[j].m_status) {
                    case INFEASIBLE:
                        // by convention: llp_rec.m_ubd = llp_rec.m_lbd = + infty
                        // => -llp_rec.m_lbd <= abs_tol
                        // => incumbent point in llp[j] minmax globally optimal
                        // => no tolerance update necessary
                        break;
                    case GLOBAL:
                        if((-llp_rec[j].m_lbd <= rec.m_lbd + abs_tol) ||                                                                                            // incumbent point is abs_tol optimal
                           (-llp_rec[j].m_lbd < std::numeric_limits<double>::infinity() && -llp_rec[j].m_lbd <= rec.m_lbd + rel_tol * std::fabs(-llp_rec[j].m_lbd)) // incumbent point is rel_tol optimal
                        ) {
                            // => incumbent point in llp[j] minmax optimal
                            // => nothing happens here
                            // => compute llp[j+1]
                        } else {
                            // incumbent point of lbp might be minmax sub-optimal
                            lbp_feasible = false;
                        }
                        // tolerance update if necessary
                        if(abs_tol_lbp_llp > (-llp_rec[j].m_lbd - rec.m_lbd) / red_tol_lbp_llp) {
                            set_tolerance_robust("abs_tol_lbp_llp", (-llp_rec[j].m_lbd - rec.m_lbd) / red_tol_lbp_llp);
                        }
                        //make sure that rel_tol of next iteration is more stringent than abs_tol
                        if((llp_rec[j].m_ubd - llp_rec[j].m_lbd) >= 0.5 * (rec.m_ubd - rec.m_lbd)) {
                            double used_rel_tol = (llp_rec[j].m_ubd == 0 ? llp_rec[j].m_lbd : (llp_rec[j].m_ubd - llp_rec[j].m_lbd) / std::fabs(llp_rec[j].m_ubd));
                            if(rel_tol_lbp_llp >= used_rel_tol) {
                                // terminated in lbp due to rel_tol
                                // => need to reduce rel_tol
                                set_tolerance_robust("rel_tol_lbp_llp", used_rel_tol / 2.0);
                            }
                        }

                        break;
                    default:
                        throw std::invalid_argument("lbp_llp_" + std::to_string(j + 1)
                                                    + ": unhandled solver status: " + to_string(llp_rec[j].m_status));
                }

                // keeping track of max feasibility violation
                // this corresponds to keeping track of worst ubd produced by the llps
                feas_vio_temp = std::max(feas_vio_temp, -llp_rec[j].m_lbd);
            }

            feas_vio_lbp = feas_vio_temp - rec.m_ubd;

            // check termination
            if(lbp_feasible) {
                // => incumbent point in all llps sip feasible
                rec.m_ubd = feas_vio_temp;

                std::cout << "\nlbp furnished a point which is within abs_tol and/or rel_tol. Terminating.\n";
                std::cout << "LBD/UBD: " << rec.m_lbd << " / " << rec.m_ubd << std::endl;
                if(!check_termination(rec, std::numeric_limits<double>::quiet_NaN(), abs_tol, rel_tol, std::numeric_limits<double>::quiet_NaN())) {
                    throw std::runtime_error("incumbent lbp solution reported to be feasible but check_termination failed");
                }
                rec.m_status = GLOBAL;
                rec.m_lbd = lbp_rec.m_lbd;
                copy_variables(lbp_rec, rec);
                for(int j = 0; j < n_llps; ++j) {
                    copy_variables(llp_rec[j], rec);
                }

                return rec;
            }

            // check if better ubd is obtained
            if(feas_vio_temp < rec.m_ubd) {
                rec.m_ubd = feas_vio_temp;
                rec.m_status = FEASIBLE;
                copy_variables(lbp_rec, rec);
                for(int j = 0; j < n_llps; ++j) {
                    copy_variables(llp_rec[j], rec);
                }
                std::cout << "minmax: new UBD produced by llps = " << rec.m_ubd << '\n';
                next_problem = LBP;
            } else {
                std::cout << "\nminmax: llp did not produce better UBD."
                          << "\n        incumbent UBD = " << rec.m_ubd << " <= " << feas_vio_temp << " UBD produced by llps\n";
            }
            // discretize
            for(int j = 0; j < n_llps; ++j) {
                if(discr_all ||                                                                                                                              // discretize all
                   !(-llp_rec[j].m_lbd <= rec.m_lbd + abs_tol) ||                                                                                            // discretize only if not abs_tol optimal
                   !(-llp_rec[j].m_lbd < std::numeric_limits<double>::infinity() && -llp_rec[j].m_lbd <= rec.m_lbd + rel_tol * std::fabs(-llp_rec[j].m_lbd)) // discretize only if not rel_tol optimal
                ) {
                    // discretize always or only if llp[j] produced a valid cut
                    discretize(prog.lbp_discs[j], llp_rec[j], symbols);
                    std::cout << "discretized llp_" << j + 1 << ".\n";
                }
            }
            next_problem = LBP;
        }

        if(next_problem == NDEF) {
            throw std::invalid_argument("procedure not implemented");
        }


        // check termination
        if(check_termination(rec, std::numeric_limits<float>::quiet_NaN(), abs_tol, rel_tol, 0)) {
            return rec;
        }

#ifdef DIPS_use_mpi
        int _rank;
        MPI_Comm_rank(MPI_COMM_WORLD, &_rank);
        if(_rank == 0) {
#endif
            // write discretization points to output file
            std::string disc_output_path = "./";
            for(auto& i : sub->solver_name) {
                disc_output_path += i;
            }
            std::ofstream disc_output(disc_output_path + "_disc_points.txt");
            std::streambuf* backup = std::cout.rdbuf();
            std::cout.rdbuf(disc_output.rdbuf());
            std::vector<std::vector<discretization>> discs;
            discs.emplace_back(prog.lbp_discs);
            report(symbols, discs);
            std::cout.rdbuf(backup); // reassign cout to its former buffer before that buffer no longer exists
            disc_output.close();     // close file output
#ifdef DIPS_use_mpi
        }
        MPI_Barrier(MPI_COMM_WORLD);
#endif
    }

    rec.m_status = EXCEEDED_MAX_ITERATIONS;
    std::cout << get_solver_base_name() << ": total number of iterations exceeded limit."
              << "Rising max_it might help\n";
    minmax_scope.trigger(); //manual trigger ensures that minmax_scope is in highest level C++ scope
    return rec;
}


bool minmax_bnf_solver::set_option(const std::string& option, double value) {
    if(option == "abs_tol") {
        if(value >= 1e-9) {
            abs_tol = value;
            return true;
        }
    }
    if(option == "rel_tol") {
        if(value >= 1e-9) {
            rel_tol = value;
            return true;
        }
    }
    if(option == "abs_tol_lbp") {
        if(value >= 1e-9) {
            abs_tol_lbp = value;
            return true;
        }
    }
    if(option == "rel_tol_lbp") {
        if(value >= 1e-9) {
            rel_tol_lbp = value;
            return true;
        }
    }
    if(option == "abs_tol_lbp_llp") {
        if(value >= 1e-9) {
            abs_tol_lbp_llp = value;
            return true;
        }
    }
    if(option == "rel_tol_lbp_llp") {
        if(value >= 1e-9) {
            rel_tol_lbp_llp = value;
            return true;
        }
    }
    if(option == "red_tol_lbp_llp") {
        if(value >= 1e-9) {
            red_tol_lbp_llp = value;
            return true;
        }
    }
    if(option == "max_iter") {
        if(value >= 1) {
            max_iter = (int)value;
            return true;
        }
    }
    if(option == "max_time") {
        if(value >= 0) {
            max_time = value;
            return true;
        }
    }
    std::cout << "Failed to set option " << option << " = " << value << '\n';
    return false;
}

bool minmax_bnf_solver::set_option(const std::string& option, const std::string& value) {
    if(option == "settings_lbp") {
        settings_lbp = value;
        return true;
    }
    if(option == "settings_lbp_llp") {
        settings_lbp_llp = value;
        return true;
    }
    std::cout << "Failed to set option " << option << " = " << value << '\n';
    return false;
}

bool minmax_bnf_solver::set_option(const std::string& option, bool value) {
    if(option == "discr_all") {
        discr_all = value;
        return true;
    }
    std::cout << "Failed to set option " << option << " = " << value << '\n';
    return false;
}

void minmax_bnf_solver::pass_option(const std::string& option, double value) {
    options.emplace_back(option, value);
}

double minmax_bnf_solver::get_option(const std::string& option) const {
    if(option == "rel_tol") {
        return rel_tol;
    }
    if(option == "abs_tol") {
        return abs_tol;
    }
    if(option == "feas_tol") {
        std::cout << "tried to get feas_tol from min-max solver. feas_tol corresponds to abs_tol!\nReturning abs_tol.\n";
        return abs_tol;
    }
    throw std::invalid_argument("Option '" + option + "' not supported or implemented.");
}

void minmax_bnf_solver::set_solver_base_name(const std::string& name) {
    solver_name.clear();
    solver_name.push_back(name);
}
std::string minmax_bnf_solver::get_solver_base_name() {
    std::string name;
    for(const std::string& a : solver_name) {
        name += a;
    }
    return name;
}

} // namespace dips
