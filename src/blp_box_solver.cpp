#include "blp_box_solver.hpp"
#include "solver.hpp"
#include "util/evaluator.hpp"
#include "program_printer.hpp"

#include <exception>
#include <algorithm>
#include <memory>
#include <cmath>
#include <fstream> // for writing discretization points to output file
#include <sstream> // for printing in exponential format

#ifdef DIPS_use_mpi
#include "mpi.h"
#endif

namespace dips {


blp_box_program_definition_parser::blp_box_program_definition_parser(std::istream& is, symbol_table& symbols) :
    programdefinition_parser_chain(is, symbols, { { "lbp", { "u_cont_var", "box_parameter", "l_obj_max" } }, { "aux", { "l_obj_star", "eps2_l_obj" } }, { "ubp", { "current_lbd", "eps_l_obj" } } }) { }

void blp_box_solver::program_info::add_llp(program llp, discretization lbp_disc) {
    llps.emplace_back(llp);
    lbp_discs.emplace_back(lbp_disc);
}

solve_record blp_box_solver::solve(program_info prog) {
    std::unique_ptr<solver> sub(make_subsolver(symbols, solver_base_name));

    for(auto& option : options) {
        sub->pass_option(option.first, option.second);
    }

    abort_subsolver_callbacks.emplace_back([&sub = sub]() { sub->abort(); });
    scoped_callback_vector_clearer scope(abort_subsolver_callbacks);
    solve_record rec;
    solve_record lbp_rec;
    solve_record ubp_rec;
    int n_llps = prog.llps.size();
    if(prog.llps.size() != prog.aux.size() || prog.llps.size() != prog.aux_v.size()) {
        throw std::invalid_argument("number of llp, aux and/or aux_v does not match");
    }
    if(prog.llps.size() != 1) {
        throw std::invalid_argument("multiple llps are not supported!!!");
    }
    std::vector<solve_record> llp_rec(n_llps);
    std::vector<solve_record> aux_rec(n_llps);
    std::vector<solve_record> aux_v_rec(n_llps);

    enum { LBP,
        UBP,
        RES,
        NDEF } next_problem
      = LBP;

    //opt_tol + ubp_tol = feas_tol (=llp optimality tol)
    //ubp_tol >= aux_tol >= opt_tol
    //aux_tol < ubp_tol - opt_tol
    abs_tol_ubp_eps_h = 0.9 * feas_tol;
    abs_tol_aux_eps_h2 = 0.5 * feas_tol;
    double opt_tol = abs_tol_lbp_llp = std::min(0.5 * abs_tol, 1e-2 * feas_tol); //original paper assumes all opt-tolerances are equal, TODO: make configurable

    double feas_vio_lbp = std::numeric_limits<double>::infinity(); // feasibility violation of lbp
    // double feas_vio_ubp = std::numeric_limits<double>::infinity();  // feasibility violation of ubp (always negative)
    d_box = init_d_box; // set initial d_box


    std::cout << "\npreprocessing: computing h_max values\n";
    for(int i = 0; i < n_llps; ++i) {
        program& h_max_prog = prog.h_max[i];

        scoped_push sp_h_max(sub->solver_name, "h_max_" + std::to_string(i + 1));
        std::cout << "solving auxillary program constructed from llp_" << i + 1 << " to compute h_max\n";

        //solve with loose tolerances
        sub->set_option("abs_tol", std::max(opt_tol, 1.0));
        sub->set_option("rel_tol", 0.9); //max a single order of magnitude

        //this will only do something for MAiNGO
        sub->pass_option("targetLowerBound", -1.0e+50);

        sub->set_option("max_time", max_time - rec.m_cpu);
        sub->set_option("settings", settings_lbp);

        auto h_max_rec = sub->solve(h_max_prog);
        sub->pass_option("targetLowerBound", std::numeric_limits<double>::max());
        rec.m_cpu += h_max_rec.m_cpu;

        std::cout << "reporting status\n";
        report(h_max_rec);
        if(check_aborted(max_time, rec, solver_base_name, aborted)) {
            return rec;
        }

        std::cout << "setting h_max of llp_" << i + 1 << " to " << -h_max_rec.m_lbd << '\n';
        set_real(prog.h_max_names[i], -h_max_rec.m_lbd, symbols);
    }



    for(int i = 0; i < max_iter; i++) {
        scoped_push sp_it(sub->solver_name, "it_" + std::to_string(i + 1));

        std::cout << "\nIteration " << i + 1 << ":\n";
        std::cout << "LBD           = " << rec.m_lbd << '\n';
        std::cout << "feas.vio. LBD = " << feas_vio_lbp << '\n';
        std::cout << "UBD           = " << rec.m_ubd << "\n\n";

        // check termination
        if(check_termination(rec, 0, abs_tol, rel_tol, 0)) {
            return rec;
        }
        if(check_aborted(max_time, rec, solver_base_name, aborted)) {
            return rec;
        }


        if(next_problem == LBP) {
            scoped_push sp_lbp(sub->solver_name, "_lbp");
            std::cout << "\nblp: solving lbp\n";

            sub->set_option("abs_tol", opt_tol);
            sub->set_option("rel_tol", 1e-9);
            sub->set_option("max_time", max_time - rec.m_cpu);
            sub->set_option("settings", settings_lbp);

            lbp_rec = sub->solve(prog.lbp);

            rec.m_cpu += lbp_rec.m_cpu;

            std::cout << "reporting status\n";
            report(lbp_rec);

            if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                return rec;
            }

            //std::cout << "lbp prorgram\n";
            //print(prog.lbp);

            switch(lbp_rec.m_status) {
                case INFEASIBLE:
                    // lbp proves blp to be infeasible
                    rec.m_status = INFEASIBLE;
                    rec.m_lbd = std::numeric_limits<double>::infinity();
                    rec.m_ubd = std::numeric_limits<double>::infinity();
                    return rec;
                case GLOBAL:
                    // computed valid lbd
                    rec.m_lbd = lbp_rec.m_lbd;
                    break;
                default:
                    throw std::invalid_argument("lbp: unhandled solver status: "
                                                + to_string(lbp_rec.m_status));
            }

            for(int j = 0; j < n_llps; ++j) {
                scoped_push sp_llp(sub->solver_name, "_llp_" + std::to_string(j + 1));
                std::cout << "blp: solving lbp_llp_" << j + 1 << '\n';

                symbols.push_scope();
                fix_variables(lbp_rec, symbols, prog.lbp_variable);

                sub->set_option("abs_tol", abs_tol_lbp_llp);
                sub->set_option("rel_tol", 1e-9);
                sub->set_option("max_time", max_time - rec.m_cpu);
                sub->set_option("settings", settings_lbp_llp);

                llp_rec[j] = sub->solve(prog.llps[j]);

                rec.m_cpu += llp_rec[j].m_cpu;

                symbols.pop_scope();

                std::cout << "reporting status\n";
                report(llp_rec[j]);

                if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                    return rec;
                }

                //std::cout << "llp_" + std::to_string(j + 1) + " prorgram\n";
                //print(prog.llps[j]);

                switch(llp_rec[j].m_status) {
                    case GLOBAL:
                        break;
                    case INFEASIBLE:
                        throw std::invalid_argument("lbp_llp_" + std::to_string(j + 1)
                                                    + ": is infeasible. However, feasibility is guaranteed by construction");
                    default:
                        throw std::invalid_argument("lbp_llp_" + std::to_string(j + 1)
                                                    + ": unhandled solver status: " + to_string(llp_rec[j].m_status));
                }

                // compute feasibility violation of lbp
                symbols.push_scope();
                fix_variables(lbp_rec, symbols);
                feas_vio_lbp = util::evaluate_expression(prog.llps[j].m_objective.front(), symbols) - llp_rec[j].m_lbd;
                symbols.pop_scope();

                scoped_push sp_aux(sub->solver_name, "_aux_" + std::to_string(j + 1));
                std::cout << "blp: solving lbp_llp_" << j + 1 << "_aux\n";

                symbols.push_scope();
                fix_variables(lbp_rec, symbols, prog.lbp_variable);
                set_real(prog.aux_hstar[j], llp_rec[j].m_lbd, symbols);
                set_real(prog.aux_eps_h2[j], abs_tol_aux_eps_h2, symbols);

                sub->set_option("abs_tol", abs_tol_aux);
                sub->set_option("rel_tol", rel_tol_aux);
                sub->set_option("max_time", max_time - rec.m_cpu);
                sub->set_option("settings", settings_lbp_llp_aux);

                aux_rec[j] = sub->solve(prog.aux[j]);

                rec.m_cpu += aux_rec[j].m_cpu;

                symbols.pop_scope();

                std::cout << "reporting status\n";
                report(aux_rec[j]);

                if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                    return rec;
                }

                //std::cout << "llp_" + std::to_string(j + 1) + "_aux prorgram\n";
                //print(prog.aux[j]);

                switch(aux_rec[j].m_status) {
                    case GLOBAL:
                        if(aux_rec[j].m_lbd > 0) {
                            throw std::invalid_argument("lbp_aux_" + std::to_string(j + 1)
                                                        + ": with negative objective is infeasible. However, feasibility is guaranteed by construction");
                        }
                        if(aux_rec[j].m_ubd >= 0) {
                            if(aux_rec[j].m_ubd == 0 && aux_rec[j].m_lbd == 0) {
                                std::cout << "ubd and lbd of aux is equal 0. check whether there is no constraint p?\n";
                            } else {
                                //ubd of aux is positiv.  reducing the optimality tolerances might help
                                set_tolerance_robust("abs_tol_aux", -aux_rec[j].m_lbd / 0.5);
                            }
                        }
                        break;
                    case INFEASIBLE:
                        throw std::invalid_argument("lbp_aux_" + std::to_string(j + 1)
                                                    + ": is infeasible. However, feasibility is guaranteed by construction");
                    default:
                        throw std::invalid_argument("lbp_aux_" + std::to_string(j + 1)
                                                    + ": unhandled solver status: " + to_string(aux_rec[j].m_status));
                }

                // discretize and compute box V
                discretize(prog.lbp_discs[j], aux_rec[j], symbols);
                double cpu_time_all_aux_v = 0; // Sum all cpu times in the following while loop for logging
                bool valid_box = false;
                while(!valid_box) {

                    auto to_string_scientific = [](double value) {std::stringstream out;   out << value; return out.str(); };
                    scoped_push sp_v_d(sub->solver_name, "_v_d_" + to_string_scientific(d_box));
                    std::cout << "blp: solving lbp_llp_aux_" + std::to_string(j + 1) + "_v with d = " << d_box << '\n';
                    // note: always minimizing. This leads to
                    //  - flipped sign of objective
                    //  - flipped lbd and ubd

                    // to compute the boxes V we do a two step procedure:
                    // first we employ subroutine 1 part (a)
                    // note that we also set bounds of the lbp_variable to bounds of box which we have computed
                    // -> push_scope necessary

                    // generate copy of the lbp variables
                    symbols.push_scope();
                    for(auto it = prog.lbp_variable.begin(); it != prog.lbp_variable.end(); ++it) {
                        symbols.define(*it, symbols.resolve(*it)->clone());
                    }
                    box_generator(prog.lbp_variable[j], prog.box_parameter[j], d_box, prog.lbp_discs[j], lbp_rec, symbols);

                    // compute boxes V; subroutine 1 part (b)
                    fix_variables(aux_rec[j], symbols);

                    sub->set_option("abs_tol", abs_tol_aux_v);
                    sub->set_option("rel_tol", rel_tol_aux_v);
                    sub->set_option("max_time", max_time - rec.m_cpu);
                    sub->set_option("settings", settings_lbp_llp_aux_v);

                    aux_v_rec[j] = sub->solve(prog.aux_v[j]);

                    cpu_time_all_aux_v += aux_v_rec[j].m_cpu;
                    rec.m_cpu += aux_v_rec[j].m_cpu;

                    symbols.pop_scope();

                    std::cout << "reporting status\n";
                    report(aux_v_rec[j]);

                    if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                        return rec;
                    }

                    //std::cout << "llp_" + std::to_string(j + 1) + "_aux_v prorgram\n";
                    //print(prog.aux_v[j]);

                    switch(aux_v_rec[j].m_status) {
                        case GLOBAL:
                            if(aux_v_rec[j].m_lbd >= 0) {
                                // computed valid box violation
                                valid_box = true;
                                d_box = init_d_box;
                                continue;
                            } else {
                                // box too big; reduce d and recompute
                                d_box *= red_d;
                                if(d_box < min_d_box) {
                                    std::cout << "failure: d_box = " << d_box
                                              << " reduced below min_d_box = "
                                              << min_d_box
                                              << ". A reduction of min_d_box (however unlikely) might help.\n... Aborting.\n ";
                                    rec.m_status = ASSUMPTION_VIOLATION_LIKELY;
                                    return rec;
                                }
                            }
                            break;
                        case INFEASIBLE:
                        default:
                            throw std::invalid_argument("lbp: unhandled solver status: "
                                                        + to_string(aux_v_rec[j].m_status));
                    }
                }
            }
            next_problem = UBP;
        }

        else if(next_problem == UBP) {

            scoped_push sp_ubp(sub->solver_name, "_ubp");
            std::cout << "\nblp: solving ubp\n";

            symbols.push_scope();
            for(int j = 0; j < n_llps; ++j) {
                set_real(prog.aux_hstar[j], llp_rec[j].m_ubd, symbols);
            }
            set_real(prog.ubp_lbd, rec.m_lbd, symbols);
            set_real(prog.ubp_eps_h, abs_tol_ubp_eps_h, symbols);
            fix_variables(lbp_rec, symbols, prog.lbp_variable);


            sub->set_option("abs_tol", opt_tol);
            sub->set_option("rel_tol", 1e-9);
            sub->set_option("max_time", max_time - rec.m_cpu);
            sub->set_option("settings", settings_ubp);

            ubp_rec = sub->solve(prog.ubp);

            rec.m_cpu += ubp_rec.m_cpu;

            symbols.pop_scope();

            std::cout << "reporting status\n";
            report(ubp_rec);

            if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                return rec;
            }

            switch(ubp_rec.m_status) {
                case INFEASIBLE:
                    // ubp infeasible, probing failed
                    // do nothing
                    break;
                case GLOBAL:
                    // computed candidate point for ubd
                    // check whether better ubd is obtained
                    if(ubp_rec.m_ubd < rec.m_ubd) {
                        rec.m_ubd = ubp_rec.m_ubd;
                        copy_variables(lbp_rec, rec); // copy lbp_record to obtain upper level variables
                        copy_variables(ubp_rec, rec); // overwrite lower level variables with correct values
                    }
                    break;
                default:
                    throw std::invalid_argument("ubp: unhandled solver status: "
                                                + to_string(ubp_rec.m_status));
            }

            next_problem = LBP;
        }

        else if(next_problem == NDEF) {
            throw std::invalid_argument("procedure not implemented");
        }


        // check termination
        if(check_termination(rec, 0, abs_tol, rel_tol, 0)) {
            symbols.print("lbp_y_disc");
            return rec;
        }

        //#ifdef DIPS_use_mpi
        //            int _rank;
        //            MPI_Comm_rank(MPI_COMM_WORLD, &_rank);
        //            if (_rank == 0) {
        //#endif
        //                // write discretization points to output file
        //                std::string disc_output_path = "./";
        //                for (int i = 0; i < sub->solver_name.size(); i++) {
        //                    disc_output_path += sub->solver_name[i];
        //                }
        //                std::ofstream disc_output(disc_output_path + "_disc_points.txt");
        //                std::streambuf* backup = std::cout.rdbuf();
        //                std::cout.rdbuf(disc_output.rdbuf());
        //                std::vector<std::vector<discretization>> discs;
        //                discs.emplace_back(prog.lbp_discs);
        //                report(symbols, discs);
        //                std::cout.rdbuf(backup);    // reassign cout to its former buffer before that buffer no longer exists
        //                disc_output.close();        // close file output
        //#ifdef DIPS_use_mpi
        //            }
        //            MPI_Barrier(MPI_COMM_WORLD);
        //#endif
    }

    rec.m_status = EXCEEDED_MAX_ITERATIONS;
    std::cout << solver_base_name << ": total number of iterations exceeded limit."
              << "Rising max_it might help\n";
    return rec;
}


bool blp_box_solver::set_option(std::string option, double value) {
    if(option == "abs_tol") {
        if(value >= 1e-9) {
            abs_tol = value;
            return true;
        }
    }
    if(option == "rel_tol") {
        if(value >= 1e-9) {
            rel_tol = value;
            return true;
        }
    }
    if(option == "feas_tol") {
        if(value >= 1e-9) {
            feas_tol = value;
            return true;
        }
    }
    if(option == "abs_tol_aux") {
        if(value >= 1e-9) {
            abs_tol_aux = value;
            return true;
        }
    }
    if(option == "rel_tol_aux") {
        if(value >= 1e-9) {
            rel_tol_aux = value;
            return true;
        }
    }
    if(option == "abs_tol_aux_v") {
        if(value >= 1e-9) {
            abs_tol_aux_v = value;
            return true;
        }
    }
    if(option == "rel_tol_aux_v") {
        if(value >= 1e-9) {
            rel_tol_aux_v = value;
            return true;
        }
    }
    if(option == "abs_tol_lbp_llp") {
        if(value >= 1e-9) {
            abs_tol_lbp_llp = value;
            return true;
        }
    }
    if(option == "abs_tol_ubp_eps_h") {
        if(value >= 1e-9) {
            abs_tol_ubp_eps_h = value;
            return true;
        }
    }
    if(option == "abs_tol_aux_eps_h2") {
        if(value >= 1e-9) {
            abs_tol_aux_eps_h2 = value;
            return true;
        }
    }
    if(option == "max_time") {
        if(value >= 0) {
            max_time = value;
            return true;
        }
    }
    if(option == "max_iter") {
        if(value >= 1) {
            max_iter = (int)value;
            return true;
        }
    }
    if(option == "init_d_box") {
        if(value >= 0 && value <= 1) {
            init_d_box = value;
            return true;
        }
    }
    if(option == "red_d") {
        if(value < 1) {
            red_d = value;
            return true;
        }
    }
    if(option == "min_d_box") {
        if(value >= 0) {
            min_d_box = value;
            return true;
        }
    }
    std::cout << "Failed to set option " << option << " = " << value << '\n';
    return false;
}

bool blp_box_solver::set_option(std::string option, std::string value) {
    if(option == "settings_lbp") {
        settings_lbp = value;
        return true;
    }
    if(option == "settings_ubp") {
        settings_ubp = value;
        return true;
    }
    if(option == "settings_lbp_llp") {
        settings_lbp_llp = value;
        return true;
    }
    if(option == "settings_lbp_llp_aux") {
        settings_lbp_llp_aux = value;
        return true;
    }
    if(option == "settings_lbp_llp_aux_v") {
        settings_lbp_llp_aux_v = value;
        return true;
    }
    std::cout << "Failed to set option " << option << " = " << value << '\n';
    return false;
}

bool blp_box_solver::set_option(std::string option, bool value) {
    if(option == "discr_all") {
        discr_all = value;
        return true;
    }
    std::cout << "Failed to set option " << option << " = " << value << '\n';
    return false;
}

void blp_box_solver::pass_option(std::string option, double value) {
    options.emplace_back(option, value);
}

double blp_box_solver::get_option(std::string option) const {
    if(option == "abs_tol") {
        return abs_tol;
    }
    if(option == "rel_tol") {
        return rel_tol;
    }
    if(option == "feas_tol") {
        return feas_tol;
    }
    if(option == "max_time") {
        return max_time;
    }
    throw std::invalid_argument("Option '" + option + "' not supported or implemented.");
}

void blp_box_solver::set_solver_base_name(std::string name) {
    solver_base_name = name;
}


// box generator and visitor
class box_generator_visitor {
public:
    box_generator_visitor(base_symbol* dest, base_symbol* src_var, size_t index, double red_d) :
        dest(dest), src_var(src_var), index(index), red_d(red_d) {};

    void operator()(base_symbol* src) {
        throw std::invalid_argument("src is not of correct type");
    }

    template <typename TType>
    void operator()(value_symbol<TType>* src) {
        std::visit(*this, src->get_value_variant());
    }


    template <unsigned IDim>
    void operator()(parameter_symbol<tensor_type<base_real, IDim>>* src) {
        constexpr auto IDimss = get_node_dimension<tensor_type<base_real, IDim>>;

        using dest_type = tensor_type<base_real, IDim + 2>;
        using src_var_type = tensor_type<base_real, IDim>;

        if constexpr(IDim + 2 > LIBALE_MAX_DIM) {
            throw std::invalid_argument("Dimension of destination symbol larger than LIBALE_MAX_DIM. Please increase the LIBALE_MAX_DIM");
        } else {
            auto* cast_dest = cast_parameter_symbol<dest_type>(dest);
            if(cast_dest == nullptr) {
                throw std::invalid_argument("types of source" + src->m_name + "and dest do not match");
            }

            auto* cast_src_var = cast_variable_symbol<src_var_type>(src_var);
            if(cast_src_var == nullptr) {
                throw std::invalid_argument("types of source and dest do not match");
            }

            // resizing dest
            std::array<size_t, IDim + 2> shape = cast_dest->m_value.shape();
            shape.at(0) = index;
            cast_dest->m_value.resize(shape);

            //std::cout << "\ndestination box:\n";
            //std::cout << symbol_to_string(dest);
            //std::cout << "\nsource from record:\n";
            //std::cout << symbol_to_string(src);
            //std::cout << "\nsource variable:\n";
            //std::cout << symbol_to_string(src_var);
            //std::cout << "\n\n";

            if constexpr(IDim == 0) {
                throw std::invalid_argument("IDim == 0 not implemented");
                //cast_dest->m_value[1]
                //cast_dest->m_value[index - 1] = source->m_value;
            } else {
                // TODO: check if dimensions of src and cast_src_var are the same
                std::cout << "TODO: check if dimensions of src and cast_src_var are the same\n";

                // create indexes for destination
                // has the for of [ number of discretization_index , lower/upper bound, size of src_var]
                size_t indexes_dest[IDim + 2];
                std::fill_n(indexes_dest, IDim + 2, (size_t)0);
                indexes_dest[0] = index - 1; // only need to consider latest entries

                size_t indexes_src[IDim];
                std::fill_n(indexes_src, IDim, (size_t)0);
                while(indexes_src[0] < cast_src_var->shape(0)) {
                    // fill indexes_dest with corresponding indexes_src
                    std::copy(std::begin(indexes_src), std::end(indexes_src), std::begin(indexes_dest) + 2);

                    // get lower bound and upper bound of cast_src_var; get solution point
                    double src_var_lb = cast_src_var->lower()[indexes_src];
                    double src_var_ub = cast_src_var->upper()[indexes_src];
                    double src_sol = src->m_value[indexes_src];

                    // setting bounds on V, c.f., subroutine 1 (a)
                    if(src_sol - (red_d / 2) * (src_var_ub - src_var_lb) < src_var_lb) {
                        // set lower-bound of box
                        indexes_dest[1] = 0;
                        cast_dest->m_value[indexes_dest] = src_var_lb - oversizing;
                        // set upper-bound of box
                        indexes_dest[1] = 1;
                        cast_dest->m_value[indexes_dest] = src_var_lb + red_d * (src_var_ub - src_var_lb);
                    } else if(src_sol + (red_d / 2) * (src_var_ub - src_var_lb) > src_var_ub) {
                        // set lower-bound of box
                        indexes_dest[1] = 0;
                        cast_dest->m_value[indexes_dest] = src_var_ub - red_d * (src_var_ub - src_var_lb);
                        // set upper-bound of box
                        indexes_dest[1] = 1;
                        cast_dest->m_value[indexes_dest] = src_var_ub + oversizing;
                    } else {
                        // set lower-bound of box
                        indexes_dest[1] = 0;
                        cast_dest->m_value[indexes_dest] = src_sol - (red_d / 2) * (src_var_ub - src_var_lb);
                        // set upper-bound of box
                        indexes_dest[1] = 1;
                        cast_dest->m_value[indexes_dest] = src_sol + (red_d / 2) * (src_var_ub - src_var_lb);
                    }

                    for(int i = IDim - 1; i >= 0; --i) {
                        if(++indexes_src[i] < cast_src_var->shape(i)) {
                            break;
                        } else if(i != 0) {
                            indexes_src[i] = 0;
                        }
                    }
                }

                //std::cout << "\ndestination box after settings bounds:\n";
                //std::cout << symbol_to_string(dest);
                //std::cout << "\n\n";

                // update bounds of variable
                std::fill_n(indexes_src, IDim, (size_t)0);
                while(indexes_src[0] < cast_src_var->shape(0)) {

                    // fill indexes_dest with corresponding indexes_src
                    std::copy(std::begin(indexes_src), std::end(indexes_src), std::begin(indexes_dest) + 2);

                    // set lower bound
                    indexes_dest[1] = 0;
                    // check whether we have oversized the box
                    if(cast_dest->m_value[indexes_dest] < cast_src_var->lower()[indexes_src]) {
                        // we have oversized the box -> need to account for oversizing (undoing)
                        cast_src_var->lower()[indexes_src] = cast_dest->m_value[indexes_dest] + oversizing;
                    } else {
                        cast_src_var->lower()[indexes_src] = cast_dest->m_value[indexes_dest];
                    }

                    // set upper bound
                    indexes_dest[1] = 1;
                    // check whether we have oversized the box
                    if(cast_dest->m_value[indexes_dest] > cast_src_var->upper()[indexes_src]) {
                        // we have oversized the box -> need to account for oversizing (undoing)
                        cast_src_var->upper()[indexes_src] = cast_dest->m_value[indexes_dest] - oversizing;
                    } else {
                        cast_src_var->upper()[indexes_src] = cast_dest->m_value[indexes_dest];
                    }

                    for(int i = IDim - 1; i >= 0; --i) {
                        if(++indexes_src[i] < cast_src_var->shape(i)) {
                            break;
                        } else if(i != 0) {
                            indexes_src[i] = 0;
                        }
                    }
                }

                //std::cout << "\nsource variable after updating the bounds:\n";
                //std::cout << symbol_to_string(src_var);
            }
        }
    }



private:
    base_symbol* dest;    // box parameter
    base_symbol* src_var; // ulp variable
    size_t index;
    double red_d;
    double oversizing = 1e-3; // oversizing parameter to be able to avoid additional checks whether the binary variables should be introduced
};



void blp_box_solver::box_generator(
  std::string& ulp_vars,
  std::string& box_vars,
  double red_d,
  const discretization& disc,
  const solve_record& rec,
  symbol_table& symbols) {


    if(symbols.resolve(ulp_vars) == nullptr || symbols.resolve(box_vars) == nullptr || rec.m_solution.find(ulp_vars) == rec.m_solution.end()) {
        if(symbols.resolve(ulp_vars) == nullptr) {
            throw std::runtime_error(ulp_vars + " is an undefined symbol");
        }
        if(symbols.resolve(box_vars) == nullptr) {
            throw std::runtime_error(box_vars + " is an undefined symbol");
        }
        if(rec.m_solution.find(ulp_vars) == rec.m_solution.end()) {
            throw std::runtime_error(ulp_vars + " cannot be found in solve record");
        }
    }

    // get current index
    int ind = 0;
    auto* indexes = cast_parameter_symbol<ale::set<ale::index<0>, 0>>(symbols.resolve(disc.indexes));
    if(indexes == nullptr) {
        throw std::invalid_argument("indexset " + disc.indexes + " has improper type");
    }
    if(indexes->m_value.empty()) {
        throw std::runtime_error("indexset " + disc.indexes + " empty.");
    } else {
        ind = *std::max_element(indexes->m_value.begin(), indexes->m_value.end());
    }

    // generate box
    box_generator_visitor gen(symbols.resolve(box_vars), symbols.resolve(ulp_vars), ind, red_d);
    std::visit(gen, rec.m_solution.find(ulp_vars)->second->get_base_variant());
}

} // namespace dips
