#include "blp_adapt_oracle_solver.hpp"
#include "util/evaluator.hpp"
#include "program_printer.hpp"

#include <exception>
#include <algorithm>
#include <memory>
#include <cmath>
#include <fstream> // for writing discretization points to output file

#ifdef DIPS_use_mpi
#include "mpi.h"
#endif

namespace dips {


blp_adapt_oracle_program_definition_parser::blp_adapt_oracle_program_definition_parser(std::istream& is, symbol_table& symbols) :
    programdefinition_parser_chain(is, symbols, { { "lbp", { "u_cont_var" } }, { "adapt_ora", { "u_obj_target" } }, { "adapt_ora_aux", { "l_obj_star", "alpha" } } }) { }

void blp_adapt_oracle_solver::program_info::add_llp(program llp, program ora_aux, discretization ora_disc) {
    llps.emplace_back(llp);
    ora_auxs.emplace_back(ora_aux);
    ora_discs.emplace_back(ora_disc);
}


solve_record blp_adapt_oracle_solver::solve(program_info prog) {
    std::unique_ptr<solver> sub(make_subsolver(symbols, solver_base_name));

    for(auto it = options.begin(); it != options.end(); ++it) {
        sub->pass_option(it->first, it->second);
    }

    solve_record rec;
    solve_record lbp_rec;
    solve_record ora_rec;
    int n_llps = prog.llps.size();
    std::vector<solve_record> llp_rec(n_llps);
    std::vector<solve_record> ora_aux_rec(n_llps);

    // set inital lbd and ubd provided by the user
    rec.m_lbd = prog.init_lbd;
    rec.m_ubd = prog.init_ubd;

    enum { LBP,
        ORA,
        UTOPIAN_UBP,
        NDEF } next_problem
      = LBP;

    double feas_vio_ubp = std::numeric_limits<double>::infinity(); // feasibility violation of ubd (always negative)


    // check if initial discretization set is empty
    for(int i = 0; i < n_llps; i++) {
        auto* indexes = cast_parameter_symbol<ale::set<ale::index<0>, 0>>(symbols.resolve(prog.ora_discs[i].indexes));
        if(indexes == nullptr) {
            throw std::invalid_argument("indexset has improper type");
        }
        if(indexes->m_value.empty()) {
            std::cout << "preprocessing: initial discretization empty. Generating discretization by solving lbp once.\n";
            next_problem = LBP;
            break;
        }
    }

    if(!(std::abs(rec.m_lbd) < std::numeric_limits<double>::infinity())) {
        std::cout << "preprocessing: initial LBD = " << rec.m_lbd << ". Generating LBD by solving lbp once.\n";
        next_problem = LBP;
    }

    bool compute_utopian_ubp = false;
    double utopian_ubd = std::numeric_limits<double>::infinity();
    if(next_problem != LBP && !(std::abs(rec.m_ubd) < std::numeric_limits<double>::infinity())) {
        compute_utopian_ubp = true;

        next_problem = UTOPIAN_UBP;
    }

    for(int i = 0; i < max_iter; i++) {
        scoped_push sp_it(sub->solver_name, "it_" + std::to_string(i + 1));

        std::cout << "\nIteration " << i + 1 << ":\n";
        std::cout << "LBD           = " << rec.m_lbd << '\n';
        std::cout << "UBD           = " << rec.m_ubd << "\n\n";
        std::cout << "feas.vio. UBD = " << feas_vio_ubp << "\n\n";

        // check termination
        if(check_termination(rec, feas_vio_ubp, abs_tol, rel_tol, feas_tol)) {
            return rec;
        }


        if(next_problem == LBP) {
            scoped_push sp_lbp(sub->solver_name, "_lbp");
            std::cout << "\nblp: solving lbp\n";

            sub->set_option("abs_tol", abs_tol_lbp);
            sub->set_option("rel_tol", rel_tol_lbp);
            sub->set_option("settings", settings_lbp);
            sub->set_option("max_time", max_time - rec.m_cpu);

            lbp_rec = sub->solve(prog.lbp);

            rec.m_cpu += lbp_rec.m_cpu;

            std::cout << "reporting status\n";
            report(lbp_rec);

            if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                return rec;
            }

            switch(lbp_rec.m_status) {
                case INFEASIBLE:
                    // lbp proves blp to be infeasible
                    rec.m_status = INFEASIBLE;
                    rec.m_lbd = std::numeric_limits<double>::infinity();
                    rec.m_ubd = std::numeric_limits<double>::infinity();

                    return rec;
                case GLOBAL:
                    // computed valid lbd
                    rec.m_lbd = lbp_rec.m_lbd;
                    break;
                default:
                    throw std::invalid_argument("lbp: unhandled solver status: "
                                                + to_string(lbp_rec.m_status));
            }

            bool lbp_feasible = true;
            double feas_vio_temp = -std::numeric_limits<double>::infinity();

            for(int j = 0; j < n_llps; ++j) {
                scoped_push sp_lbp_llp(sub->solver_name, "_llp_" + std::to_string(j + 1));
                std::cout << "blp: solving lbp_llp_" << j + 1 << '\n';
                // note: always minimizing. This leads to
                //  - flipped sign of objective
                //  - flipped lbd and ubd

                symbols.push_scope();
                fix_variables(lbp_rec, symbols, prog.ulp_variables);

                sub->set_option("abs_tol", abs_tol_lbp_llp);
                sub->set_option("rel_tol", rel_tol_lbp_llp);
                sub->set_option("settings", settings_lbp_llp);
                sub->set_option("max_time", max_time - rec.m_cpu);

                llp_rec[j] = sub->solve(prog.llps[j]);

                rec.m_cpu += llp_rec[j].m_cpu;

                symbols.pop_scope();

                std::cout << "reporting status\n";
                report(llp_rec[j]);

                if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                    return rec;
                }

                double u_obj, l_obj; // upper- and lower-level objective value
                switch(llp_rec[j].m_status) {
                    case GLOBAL:
                        // compute lower-level objective value for fixed lbp variables
                        symbols.push_scope();

                        fix_variables(lbp_rec, symbols);
                        l_obj = util::evaluate_expression(prog.llps[j].m_objective.front(), symbols);

                        symbols.pop_scope();

                        if(l_obj <= llp_rec[j].m_lbd + abs_tol_ora_eps_h) {
                            // => incumbent point computed by lbp in llp[j] blp eps_h-feasible
                            // => nothing happens here
                            // => compute llp[j+1]
                        } else {
                            // incumbent point of lbp might be blp infeasible
                            lbp_feasible = false;
                        }
                        // tolerance update if necessary
                        // TODO: implement
                        break;
                    case INFEASIBLE:
                    default:
                        throw std::invalid_argument("lbp_llp_" + std::to_string(j + 1)
                                                    + ": unhandled solver status: " + to_string(llp_rec[j].m_status));
                }

                // keeping track of max feasibility violation
                if(l_obj - llp_rec[j].m_lbd > feas_vio_temp) {
                    feas_vio_temp = llp_rec[j].m_lbd;
                }
            }

            // check termination
            if(lbp_feasible) {
                // => incumbent point in all llps blp eps_h-feasible
                std::cout << "\nlbp furnished a point which is within abs_tol_ora_eps_h-feas. tolerance. Terminating.\n";
                std::cout << "Feas. vio.:   " << feas_vio_temp << " <= abs_tol_ora_eps_h = " << abs_tol_ora_eps_h << std::endl;
                rec.m_status = GLOBAL;
                rec.m_lbd = lbp_rec.m_lbd;
                rec.m_ubd = lbp_rec.m_ubd;
                copy_variables(lbp_rec, rec);
                for(int j = 0; j < n_llps; ++j) {
                    copy_variables(llp_rec[j], rec);
                }

                return rec;
            } else {
                // check whether point is feasible in BLP and better than incumbent
                std::cout << "\nblp: probing (llp solution feasible in ulp?)\n";

                symbols.push_scope();

                fix_variables(lbp_rec, symbols, prog.ulp_variables); // fix upper-level variables and ...
                for(int j = 0; j < n_llps; ++j) {                    // fix lower-level variables
                    fix_variables(llp_rec[j], symbols);
                }

                bool probing_success = true;
                for(auto it = prog.lbp.m_constraints.begin(); it != prog.lbp.m_constraints.end(); ++it) {
                    if(!is_tree_constant(it->get(), symbols)) {
                        throw std::runtime_error("encountered non-constant constraint");
                    }
                    if(!util::evaluate_expression(it->get(), symbols)) {
                        std::cout << "probing failed. llp solution not feasible in ulp.\n";
                        probing_success = false;
                        break;
                    }
                }

                if(probing_success) {
                    double u_obj = util::evaluate_expression(prog.lbp.m_objective.begin()->get(), symbols);
                    if(rec.m_ubd > u_obj) {
                        std::cout << "probing succeeded and produced better upper bound:\n";
                        std::cout << "computed ubd = " << u_obj << " < "
                                  << " incumbend UBD = " << rec.m_ubd << '\n';
                        copy_variables(lbp_rec, rec);
                        for(int j = 0; j < n_llps; ++j) {
                            copy_variables(llp_rec[j], rec);
                        }
                        rec.m_ubd = u_obj;
                        compute_utopian_ubp = false;
                    } else {
                        std::cout << "probing succeeded but did not produce better upper bound:\n";
                        std::cout << "computed ubd = " << u_obj << " >= "
                                  << " incumbend UBD = " << rec.m_ubd << '\n';
                    }
                }

                symbols.pop_scope();
            }

            if(!(std::abs(rec.m_ubd) < std::numeric_limits<double>::infinity())) {
                compute_utopian_ubp = true;
                next_problem = UTOPIAN_UBP;
            } else {
                next_problem = ORA;
            }
        }

        else if(next_problem == UTOPIAN_UBP) {
            std::cout << "preprocessing: initial UBD = " << rec.m_ubd << ". Generating utopian UBD by maximizing lbp objective.\n";
            std::cout << "               constructing auxillary program utopian_ubp ... ";
            program utopian_ubp = prog.lbp;
            std::unique_ptr<ale::value_node<ale::real<0>>> neg_lbp_objective;
            neg_lbp_objective = std::make_unique<minus_node>((*prog.lbp.m_objective.begin()).get()->clone());
            utopian_ubp.m_objective.front() = neg_lbp_objective.release();
            std::cout << "done\n";

            scoped_push sp_lbp(sub->solver_name, "_utopian_ubp");
            std::cout << "preprocessing: solving utopian_ubp\n";

            sub->set_option("abs_tol", abs_tol_lbp);
            sub->set_option("rel_tol", std::max(rel_tol_lbp, 0.5)); // only search for a coarse solution
            sub->set_option("settings", settings_lbp);
            sub->set_option("max_time", max_time - rec.m_cpu);

            auto utopian_ubp_rec = sub->solve(utopian_ubp);

            rec.m_cpu += utopian_ubp_rec.m_cpu;
            std::cout << "reporting status\n";
            report(utopian_ubp_rec);

            if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                return rec;
            }

            switch(utopian_ubp_rec.m_status) {
                case INFEASIBLE:
                    // utopien_ubp proves sip to be infeasible
                    std::cout << "utopian_ubp infeasible.\n";
                    rec.m_status = INFEASIBLE;

                    return rec;
                case GLOBAL:
                    // computed valid utopian UBD
                    std::cout << "utopian UBD = " << -utopian_ubp_rec.m_lbd << '\n';
                    utopian_ubd = -utopian_ubp_rec.m_lbd;
                    break;
                default:
                    throw std::invalid_argument("utopian UBD: unhandled solver status: "
                                                + to_string(utopian_ubp_rec.m_status));
            }
            next_problem = ORA;
        }

        else if(next_problem == ORA) {
            scoped_push sp_ora(sub->solver_name, "_ora");
            std::cout << "\nblp: solving ora\n";

            if(compute_utopian_ubp) {
                obj_target = utopian_ubd;
                std::cout << "setting obj_target to utopian_ubd\n";
                std::cout << "obj_target   = " << utopian_ubd << '\n';
            } else {
                obj_target = 0.5 * (rec.m_ubd + rec.m_lbd);
                std::cout << "obj_target   = " << obj_target << '\n';
            }
            set_real(prog.obj_target_name, obj_target, symbols);

            sub->set_option("abs_tol", abs_tol_ora);
            sub->set_option("rel_tol", rel_tol_ora);
            sub->set_option("max_time", max_time - rec.m_cpu);
            sub->set_option("settings", settings_ora);

            ora_rec = sub->solve(prog.ora);

            rec.m_cpu += ora_rec.m_cpu;

            std::cout << "reporting status\n";
            report(ora_rec);

            if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                return rec;
            }

            switch(ora_rec.m_status) {
                case INFEASIBLE:
                    // ora infeasible, obj_target not attainable
                    // => set LBD equal obj_target
                    // => increase obj_target
                    // => end iteration and go to ORA
                    rec.m_lbd = obj_target;

                    if(compute_utopian_ubp) {
                        std::cout << "utopian UBD = " << utopian_ubd << " is not attainable.\n";
                        rec.m_status = INFEASIBLE;
                        rec.m_lbd = std::numeric_limits<double>::infinity();
                        rec.m_ubd = std::numeric_limits<double>::infinity();

                        return rec;
                    }

                    next_problem = ORA;
                    continue;
                case GLOBAL:
                    // computed candidate point for ubd
                    // => nothing happens here
                    // => compute llps
                    break;
                default:
                    throw std::invalid_argument("ora: unhandled solver status: "
                                                + to_string(ora_rec.m_status));
            }

            // tolerance update for llp if necessary
            //if (abs_tol_lbp_llp >= eps_res) {
            //    set_tolerance_robust("abs_tol_lbp_llp", eps_res / red_tol_lbp_llp);
            //}

            bool ora_feasible = true;
            for(int j = 0; j < n_llps; ++j) {
                scoped_push sp_ora_llp(sub->solver_name, "_llp_" + std::to_string(j + 1));
                std::cout << "blp: solving ora_llp_" << j + 1 << '\n';

                symbols.push_scope();
                try {
                    fix_variables(ora_rec, symbols, prog.ulp_variables);
                } catch(std::invalid_argument& e) {
                    std::cout << e.what();
                }
                sub->set_option("abs_tol", abs_tol_ora_llp);
                sub->set_option("rel_tol", rel_tol_ora_llp);
                sub->set_option("max_time", max_time - rec.m_cpu);
                sub->set_option("settings", settings_ora_llp);

                llp_rec[j] = sub->solve(prog.llps[j]);

                rec.m_cpu += llp_rec[j].m_cpu;

                symbols.pop_scope();

                std::cout << "reporting status\n";
                report(llp_rec[j]);

                if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                    return rec;
                }

                bool compute_ora_aux = false;
                switch(llp_rec[j].m_status) {
                    case GLOBAL:
                        if(ora_rec.m_ubd <= llp_rec[j].m_lbd + abs_tol_ora_eps_h) {
                            // => incumbent point in llp[j] blp feasible
                            // => nothing happens here
                            // => compute llp[j+1]
                        } else {
                            // incumbent point of ora might be in llp suboptimal
                            ora_feasible = false;
                            compute_ora_aux = true;
                        }
                        break;
                    case INFEASIBLE:
                    default:
                        throw std::invalid_argument("ora_llp_" + std::to_string(j + 1)
                                                    + ": unhandled solver status: " + to_string(llp_rec[j].m_status));
                }

                if(compute_ora_aux) {
                    scoped_push sp_ora_aux(sub->solver_name, "_ora_aux" + std::to_string(j + 1));
                    std::cout << "blp: solving lbp_ora_aux_" << j + 1 << " with\n";

                    double cpu_time_all_alphas = 0; // Sum all cpu times in the following while loop for logging
                    while(compute_ora_aux) {
                        scoped_push sp_alpha(sub->solver_name, "_alpha_" + std::to_string(j + 1));
                        std::cout << "    alpha = " << alpha << '\n';

                        if(alpha < min_alpha) {
                            std::cout << "\nError: alpha = " << alpha
                                      << " < min_alpha = " << min_alpha << ". ";
                            std::cout << "An assumption is likely violated.\n";
                            rec.m_status = ASSUMPTION_VIOLATION_LIKELY;

                            std::cout << "Note: a reduction of min_alpha might help.\n"
                                      << "  ... Aborting.\n ";
                            return rec;
                        }

                        symbols.push_scope();
                        fix_variables(lbp_rec, symbols, prog.ulp_variables);

                        set_real(prog.ora_aux_hstar[j], llp_rec[j].m_lbd, symbols);
                        set_real(prog.ora_aux_alpha[j], alpha, symbols);

                        sub->set_option("abs_tol", abs_tol_llp_ora_aux);
                        sub->set_option("rel_tol", rel_tol_llp_ora_aux);
                        sub->set_option("settings", settings_llp_ora_aux);
                        sub->set_option("max_time", max_time - rec.m_cpu);

                        ora_aux_rec[j] = sub->solve(prog.ora_auxs[j]);

                        std::cout << "aux time: " << ora_aux_rec[j].m_cpu << std::endl;

                        cpu_time_all_alphas += ora_aux_rec[j].m_cpu;
                        rec.m_cpu += ora_aux_rec[j].m_cpu;

                        symbols.pop_scope();

                        report(ora_aux_rec[j]);
                        if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                            return rec;
                        }

                        switch(ora_aux_rec[j].m_status) {
                            case INFEASIBLE:
                                // by convention: ora_aux_rec.m_ubd = ora_aux_rec.m_lbd = + infty
                                // => will reduce alpha
                                std::cout << "ora_aux infeasible!\n";
                            case FEASIBLE:
                            case LOCAL:
                                // no global solution required
                            case GLOBAL:
                                if(ora_aux_rec[j].m_ubd >= 0) {
                                    // ora_aux has not furnished a llp Slater point
                                    // => reduce alpha and resolve
                                    alpha /= red_alpha;
                                    set_real(prog.ora_aux_alpha[j], alpha, symbols);
                                } else {
                                    // ora_aux has furnished a llp Slater point
                                    // => discretize
                                    // => reset alpha
                                    discretize(prog.ora_discs[j], ora_aux_rec[j], symbols);
                                    alpha = init_alpha;
                                    compute_ora_aux = false;

                                    std::cout << "reporting status\n";
                                    report(ora_aux_rec[j]);
                                }
                                break;
                            default:
                                throw std::invalid_argument("lbp_ora_aux_" + std::to_string(j + 1)
                                                            + ": unhandled solver status: " + to_string(ora_aux_rec[j].m_status));
                        }
                    }
                }
            }

            if(ora_feasible) {
                rec.m_status = FEASIBLE;
                compute_utopian_ubp = false;
                rec.m_ubd = obj_target;
                copy_variables(ora_rec, rec);
                next_problem = ORA;
            } else {
                // discretize
                for(int j = 0; j < n_llps; ++j) {
                    //if(discr_all || !(-llp_rec[j].m_lbd <= 0)) {
                    // discretize always or only if llp[j] produced a valid cut
                    discretize(prog.ora_discs[j], llp_rec[j], symbols);
                    std::cout << "discretized llp_" << j + 1 << ".\n";
                    //}
                }
            }


            // check whether a better ubd has been computed; do probing because it is basically for free
            // use upper-level variable values from ora and lower-level variables values from llp
            symbols.push_scope();
            fix_variables(ora_rec, symbols, prog.ulp_variables);
            for(int j = 0; j < n_llps; ++j) {
                fix_variables(llp_rec[j], symbols);
            }
            bool feasible = true;
            for(auto it = prog.lbp.m_constraints.begin(); it != prog.lbp.m_constraints.end(); ++it) {
                if(!util::evaluate_expression(*it, symbols)) {
                    feasible = false;
                    break;
                };
            }
            if(feasible && rec.m_ubd > util::evaluate_expression(prog.lbp.m_objective.front(), symbols)) {
                std::cout << "produced better ubd by probing\n";
                compute_utopian_ubp = false;
                rec.m_ubd = util::evaluate_expression(prog.lbp.m_objective.front(), symbols);
                rec.m_status = FEASIBLE;
                copy_variables(ora_rec, rec);
                for(int j = 0; j < n_llps; ++j) {
                    copy_variables(llp_rec[j], rec);
                }
            }
            symbols.pop_scope();
        }


        if(next_problem == NDEF) {
            throw std::invalid_argument("procedure not implemented");
        }


        // check termination
        if(compute_utopian_ubp && rec.m_lbd >= utopian_ubd) {
            std::cout << "utopian UBD = " << utopian_ubd << " is not attainable.\n";
            rec.m_status = INFEASIBLE;
            rec.m_lbd = std::numeric_limits<double>::infinity();
            rec.m_ubd = std::numeric_limits<double>::infinity();
            return rec;
        }
        if(check_termination(rec, feas_vio_ubp, abs_tol, rel_tol, feas_tol)) {
            return rec;
        }

#ifdef DIPS_use_mpi
        int _rank;
        MPI_Comm_rank(MPI_COMM_WORLD, &_rank);
        if(_rank == 0) {
#endif
            // write discretization points to output file
            std::string disc_output_path = "./";
            for(auto& i : sub->solver_name) {
                disc_output_path += i;
            }
            std::ofstream disc_output(disc_output_path + "_disc_points.txt");
            std::streambuf* backup = std::cout.rdbuf();
            std::cout.rdbuf(disc_output.rdbuf());
            std::vector<std::vector<discretization>> discs;
            discs.emplace_back(prog.ora_discs);
            report(symbols, discs);
            std::cout.rdbuf(backup); // reassign cout to its former buffer before that buffer no longer exists
            disc_output.close();     // close file output
#ifdef DIPS_use_mpi
        }
        MPI_Barrier(MPI_COMM_WORLD);
#endif
    }

    rec.m_status = EXCEEDED_MAX_ITERATIONS;
    std::cout << solver_base_name << ": total number of iterations exceeded limit."
              << "Rising max_it might help\n";

    return rec;
}


bool blp_adapt_oracle_solver::set_option(std::string option, double value) {
    if(option == "feas_tol") {
        if(value >= 0) {
            feas_tol = value;
            return true;
        }
    }
    if(option == "abs_tol") {
        if(value >= 1e-9) {
            abs_tol = value;
            return true;
        }
    }
    if(option == "rel_tol") {
        if(value >= 1e-9) {
            rel_tol = value;
            return true;
        }
    }

    if(option == "abs_tol_lbp") {
        if(value >= 1e-9) {
            abs_tol_lbp = value;
            return true;
        }
    }
    if(option == "rel_tol_lbp") {
        if(value >= 1e-9) {
            rel_tol_lbp = value;
            return true;
        }
    }
    if(option == "abs_tol_lbp_llp") {
        if(value >= 1e-9) {
            abs_tol_lbp_llp = value;
            return true;
        }
    }
    if(option == "rel_tol_lbp_llp") {
        if(value >= 1e-9) {
            rel_tol_lbp_llp = value;
            return true;
        }
    }
    if(option == "abs_tol_ora") {
        if(value >= 1e-9) {
            abs_tol_ora = value;
            return true;
        }
    }
    if(option == "rel_tol_ora") {
        if(value >= 1e-9) {
            rel_tol_ora = value;
            return true;
        }
    }
    if(option == "abs_tol_ora_llp") {
        if(value >= 1e-9) {
            abs_tol_ora_llp = value;
            return true;
        }
    }
    if(option == "rel_tol_ora_llp") {
        if(value >= 1e-9) {
            rel_tol_ora_llp = value;
            return true;
        }
    }
    if(option == "abs_tol_llp_ora_aux") {
        if(value >= 1e-9) {
            abs_tol_llp_ora_aux = value;
            return true;
        }
    }
    if(option == "rel_tol_llp_ora_aux") {
        if(value >= 1e-9) {
            rel_tol_llp_ora_aux = value;
            return true;
        }
    }
    if(option == "abs_tol_ora_eps_h") {
        if(value >= 1e-9) {
            abs_tol_ora_eps_h = value;
            return true;
        }
    }
    if(option == "init_alpha") {
        if(value < 1) {
            init_alpha = value;
            return true;
        }
    }
    if(option == "red_alpha") {
        if(value > 1) {
            red_alpha = value;
            return true;
        }
    }
    if(option == "max_iter") {
        if(value >= 1) {
            max_iter = (int)value;
            return true;
        }
    }
    if(option == "max_time") {
        if(value >= 0) {
            max_time = value;
            return true;
        }
    }
    std::cout << "Failed to set option " << option << " = " << value << '\n';
    return false;
}

bool blp_adapt_oracle_solver::set_option(std::string option, std::string value) {
    if(option == "settings_lbp") {
        settings_lbp = value;
        return true;
    }
    if(option == "settings_lbp_llp") {
        settings_lbp_llp = value;
        return true;
    }
    if(option == "settings_ora") {
        settings_ora = value;
        return true;
    }
    if(option == "settings_ora_llp") {
        settings_ora_llp = value;
        return true;
    }
    if(option == "settings_ora_aux") {
        settings_llp_ora_aux = value;
        return true;
    }
    std::cout << "Failed to set option " << option << " = " << value << '\n';
    return false;
}

bool blp_adapt_oracle_solver::set_option(std::string option, bool value) {
    if(option == "discr_all") {
        discr_all = value;
        return true;
    }
    std::cout << "Failed to set option " << option << " = " << value << '\n';
    return false;
}

void blp_adapt_oracle_solver::pass_option(std::string option, double value) {
    options.emplace_back(option, value);
}

double blp_adapt_oracle_solver::get_option(std::string option) const {
    if(option == "abs_tol") {
        return abs_tol;
    }
    if(option == "rel_tol") {
        return rel_tol;
    }
    if(option == "feas_tol") {
        return feas_tol;
    }
    throw std::invalid_argument("Option '" + option + "' not supported or implemented.");
}

void blp_adapt_oracle_solver::set_solver_base_name(std::string name) {
    solver_base_name = name;
}


} // namespace dips
