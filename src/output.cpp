#include "output.hpp"

#include "nlohmann/json.hpp" // for basic_json<>::object_t, json, basic_json, operator==, basic_json<>::value_type

//define serialization of base_symbol (only valid for parameter symbol)
void ale::to_json(nlohmann::json& j, ale::base_symbol* const& s) {
    ale::parameter_symbol_serializer vis;
    j = vis.dispatch(s);
}
//define serialization of base_symbol unique_ptr
void ale::to_json(nlohmann::json& j, const std::unique_ptr<ale::base_symbol>& s) {
    //
    ale::base_symbol* p = s.get();
    to_json(j, p);
}
void ale::from_json(const nlohmann::json& j, std::unique_ptr<ale::base_symbol>& s) {
    std::vector<unsigned> shape = j.at("shape").get<std::vector<unsigned>>();
    if(j.at("type") == "real") {
        if(shape.empty()) {
            s = std::make_unique<ale::parameter_symbol<real<0>>>(j.at("name").get<std::string>(), j.at("value").get<double>());
        } else if(shape.size() == 1) {
            std::array<size_t, 1> new_shape = { shape.at(0) };
            std::unique_ptr<ale::parameter_symbol<real<1>>> p = std::make_unique<ale::parameter_symbol<real<1>>>(j.at("name"), new_shape);
            p->m_value = tensor_from_nested_vector<base_real, 1>(j.at("value").get<std::vector<double>>(), new_shape);
        } else if(shape.size() == 2) {
            std::array<size_t, 2> new_shape = { shape.at(0), shape.at(1) };
            std::unique_ptr<ale::parameter_symbol<real<2>>> p = std::make_unique<ale::parameter_symbol<real<2>>>(j.at("name"), new_shape);
            std::vector<std::vector<double>> data = j.at("value").get<std::vector<std::vector<double>>>();
            p->m_value = tensor_from_nested_vector<base_real, 2>(data, new_shape);
        } else if(shape.size() == 3) {
            std::array<size_t, 3> new_shape = { shape.at(0), shape.at(1), shape.at(2) };
            std::unique_ptr<ale::parameter_symbol<real<3>>> p = std::make_unique<ale::parameter_symbol<real<3>>>(j.at("name"), new_shape);
            nested_vector<double, 3> data = j.at("value").get<nested_vector<double, 3>>();
            p->m_value = tensor_from_nested_vector<base_real, 3>(data, new_shape);
        } else {
            throw std::invalid_argument("Tensor output only supported up to 3 dimensions");
        }
    } else if(j.at("type") == "index") {
        if(shape.empty()) {
            s = std::make_unique<ale::parameter_symbol<index<0>>>(j.at("name").get<std::string>(), j.at("value").get<double>());
        } else if(shape.size() == 1) {
            std::array<size_t, 1> new_shape = { shape.at(0) };
            std::unique_ptr<ale::parameter_symbol<index<1>>> p = std::make_unique<ale::parameter_symbol<index<1>>>(j.at("name"), new_shape);
            p->m_value = tensor_from_nested_vector<base_index, 1>(j.at("value").get<std::vector<int>>(), new_shape);
        } else if(shape.size() == 2) {
            std::array<size_t, 2> new_shape = { shape.at(0), shape.at(1) };
            std::unique_ptr<ale::parameter_symbol<index<2>>> p = std::make_unique<ale::parameter_symbol<index<2>>>(j.at("name"), new_shape);
            std::vector<std::vector<int>> data = j.at("value").get<std::vector<std::vector<int>>>();
            p->m_value = tensor_from_nested_vector<base_index, 2>(data, new_shape);
        } else if(shape.size() == 3) {
            std::array<size_t, 3> new_shape = { shape.at(0), shape.at(1), shape.at(2) };
            std::unique_ptr<ale::parameter_symbol<index<3>>> p = std::make_unique<ale::parameter_symbol<index<3>>>(j.at("name"), new_shape);
            nested_vector<int, 3> data = j.at("value").get<nested_vector<int, 3>>();
            p->m_value = tensor_from_nested_vector<base_index, 3>(data, new_shape);
        } else {
            throw std::invalid_argument("Tensor output only supported up to 3 dimensions");
        }
    } else {
        throw std::invalid_argument("JSON has invalid type field.");
    }
}
void dips::to_json(nlohmann::json& j, const dips::solve_record& s) {
    j = nlohmann::json();
    j["solution"] = s.m_solution;
    j["status"] = s.m_status;
    j["ubd"] = s.m_ubd;
    j["lbd"] = s.m_lbd;
    j["cpu_time"] = s.m_cpu;
}

void dips::from_json(const nlohmann::json& j, dips::solve_record& s) {
    j.at("solution").get_to(s.m_solution);
    j.at("status").get_to(s.m_status);
    j.at("ubd").get_to(s.m_ubd);
    j.at("lbd").get_to(s.m_lbd);
    j.at("cpu").get_to(s.m_cpu);
}
