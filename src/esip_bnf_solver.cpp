#include "esip_bnf_solver.hpp"

#include <filesystem> // for create_directories
#include <fstream>    // for operator<<, basic_ostream, basic_ostream::operator<<, basic_ostream<>::__ostream_type, basic_ios::rdbuf, basic_ofst...
#include <memory>     // for allocator_traits<>::value_type, unique_ptr
#include <utility>    // for pair, make_pair, move
#include <iostream>   // for cout
#include <limits>     // for numeric_limits
#include <stdexcept>  // for invalid_argument

#include "minmax_bnf_solver.hpp" // for minmax_bnf_solver, minmax_bnf_solver::program_info
#include "common.hpp"            // for copy_variables, discretize, fix_variables
#include "symbol_table.hpp"      // for symbol_table

#ifdef DIPS_use_mpi
#include "mpi.h"
#endif

namespace dips {

esip_bnf_program_definition_parser::esip_bnf_program_definition_parser(std::istream& is, symbol_table& symbols) :
    programdefinition_parser_chain(is, symbols, { { "lbp", {} }, { "mlp", {} } }) { }

std::vector<discretization> esip_bnf_program_definition_parser::get_ulp_discretizations() {
    return definition_parsers.front().get_discretizations();
}
std::vector<discretization> esip_bnf_program_definition_parser::get_mlp_discretizations() {
    return definition_parsers.back().get_discretizations();
}

void esip_bnf_solver::program_info::add_mlp_and_llp(const program& mlp, const program& llp, const discretization& ulp_disc, const discretization& mlp_disc) {

    mlp_llp_pairs.emplace_back(mlp, llp);
    ulp_discs.emplace_back(ulp_disc);
    mlp_discs.emplace_back(mlp_disc);
}

solve_record esip_bnf_solver::solve(program_info prog) {

    std::unique_ptr<solver> sub(make_subsolver(symbols, solver_base_name));
    minmax_bnf_solver minmax(symbols, make_subsolver, solver_base_name);

    for(auto& option : options) {
        sub->pass_option(option.first, option.second);
        minmax.pass_option(option.first, option.second);
    }
    solve_record rec;
    solve_record ulp_rec;
    int n_mlp_llp_pairs = prog.mlp_llp_pairs.size();
    std::vector<solve_record> mlp_rec(n_mlp_llp_pairs);

    enum { ULP,
        MLP,
        NDEF } next_problem
      = ULP;

    double feas_vio_ulp = std::numeric_limits<double>::infinity(); // feasibility violation of ulp (always negative)

    for(int i = 0; i < max_iter; ++i) {

        std::cout << "\nIteration " << std::to_string(i + 1) << ":\n";
        std::cout << "LBD           = " << rec.m_lbd << '\n';
        std::cout << "UBD           = " << rec.m_ubd << "\n\n";

        // check termination
        if(check_termination(rec, feas_vio_ulp, abs_tol, rel_tol, feas_tol)) {
            return rec;
        }

        if(next_problem == ULP) {

            scoped_push sp_it(sub->solver_name, "it_" + std::to_string(i + 1));
            scoped_push sp_ulp(sub->solver_name, "_ulp");
            std::cout << "\nesip: solving ulp\n";

            sub->set_option("abs_tol", abs_tol_ulp);
            sub->set_option("rel_tol", rel_tol_ulp);
            sub->set_option("settings", settings_ulp);
            sub->set_option("max_time", max_time - rec.m_cpu);

            ulp_rec = sub->solve(prog.ulp);

            rec.m_cpu += ulp_rec.m_cpu;

            std::cout << "reporting status\n";
            report(ulp_rec);

            if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                return rec;
            }

            switch(ulp_rec.m_status) {
                case INFEASIBLE:
                    // ulp proves sip to be infeasible
                    rec.m_status = INFEASIBLE;
                    rec.m_lbd = std::numeric_limits<double>::infinity();
                    rec.m_ubd = std::numeric_limits<double>::infinity();
                    return rec;
                case GLOBAL:
                    // computed valid lbd
                    rec.m_lbd = ulp_rec.m_lbd;
                    break;
                default:
                    throw std::invalid_argument("unhandled solver status: "
                                                + to_string(ulp_rec.m_status));
            }
            next_problem = MLP;
        }

        if(next_problem == MLP) {
            scoped_push sp_mlp_it(minmax.solver_name, "it_" + std::to_string(i + 1));
            bool ulp_feasible = true;
            double feas_vio_temp = -std::numeric_limits<double>::infinity();
            for(int j = 0; j < n_mlp_llp_pairs; ++j) {

                std::cout << "\nesip: solving mlp_" << j + 1 << '\n';
                // note: always minimizing. This leads to
                //  - flipped sign of objective
                //  - flipped lbd and ubd
                scoped_push sp_mlp_mlp(minmax.solver_name, "_mlp" + std::to_string(i + 1));
                scoped_push sp_mlp_jth(minmax.solver_name, "_" + std::to_string(j + 1) + "/");

                // create output folder name and folder for minmax solver
                std::filesystem::create_directories(minmax.get_solver_base_name());

                symbols.push_scope();
                fix_variables(ulp_rec, symbols);

                minmax_bnf_solver::program_info minmax_program;
                minmax_program.lbp = prog.mlp_llp_pairs[j].first;
                minmax_program.add_llp(prog.mlp_llp_pairs[j].second, prog.mlp_discs[j]);

                minmax.set_option("rel_tol", rel_tol_minmax);
                minmax.set_option("abs_tol", abs_tol_minmax);


                minmax.set_option("settings_lbp", settings_minmax_ulp);
                minmax.set_option("settings_lbp_llp", settings_minmax_llp);
                minmax.set_option("max_time", max_time - rec.m_cpu);

                mlp_rec[j] = minmax.solve(minmax_program);

                rec.m_cpu += mlp_rec[j].m_cpu;
                symbols.pop_scope();

                std::cout << "reporting status\n";
                report(mlp_rec[j]);

                if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                    return rec;
                }

                switch(mlp_rec[j].m_status) {
                    case INFEASIBLE:
                        // by convention: mlp_rec.m_ubd = mlp_rec.m_lbd = + infty
                        // => -mlp_rec.m_lbd <= feas_tol
                        // => incumbent point in mlp[j] sip feasible
                        // => no tolerance update necessary
                        break;
                    case GLOBAL:
                        if(-mlp_rec[j].m_lbd <= feas_tol) {
                            // => incumbent point in mlp[j] sip feasible
                            // => nothing happens here
                            // => compute mlp[j+1]
                        } else {
                            // -mlp_rec.m_lbd > feas_tol
                            // incumbent point of lbp might be sip infeasible
                            ulp_feasible = false;
                        }
                        // tolerance update if necessary
                        if(-mlp_rec[j].m_lbd > 0 && abs_tol_minmax > -mlp_rec[j].m_lbd / red_tol_minmax) {
                            set_tolerance_robust("abs_tol_minmax", -mlp_rec[j].m_lbd / red_tol_minmax);
                        }
                        break;
                    default:
                        throw std::invalid_argument("mlp" + std::to_string(j + 1) + ": unhandled solver status: " + to_string(mlp_rec[j].m_status));
                }
                // keeping track of max feasibility violation
                if(-mlp_rec[j].m_lbd > feas_vio_temp) {
                    feas_vio_temp = -mlp_rec[j].m_lbd;
                }
            }
            //check sip-feasibility
            if(ulp_feasible) {
                // => incumbent point in all llps sip feasible
                rec.m_ubd = ulp_rec.m_ubd;
                copy_variables(ulp_rec, rec);
            }
            // check termination
            if(check_termination(rec, feas_vio_ulp, abs_tol, rel_tol, feas_tol)) {
                return rec;
            }

            // discretize
            for(int j = 0; j < n_mlp_llp_pairs; ++j) {
                if(discr_all || -mlp_rec[j].m_ubd > 0) {
                    // discretize always or only if llp[j] produced a valid cut
                    discretize(prog.ulp_discs[j], mlp_rec[j], symbols);
                    std::cout << "discretized mlp_" << j + 1 << ".\n";
                }
            }
            next_problem = ULP;
        }

        if(next_problem == NDEF) {
            throw std::invalid_argument("procedure not implemented");
        }


        // check termination
        if(check_termination(rec, feas_vio_ulp, abs_tol, rel_tol, feas_tol)) {
            return rec;
        }
#ifdef DIPS_use_mpi
        int _rank;
        MPI_Comm_rank(MPI_COMM_WORLD, &_rank);
        if(_rank == 0) {
#endif
            // write discretization points to output file
            std::string disc_output_path = "./";
            for(auto& i : sub->solver_name) {
                disc_output_path += i;
            }
            std::ofstream disc_output(disc_output_path + "_disc_points.txt");
            std::streambuf* backup = std::cout.rdbuf();
            std::cout.rdbuf(disc_output.rdbuf());
            std::vector<std::vector<discretization>> discs;
            discs.emplace_back(prog.ulp_discs);
            discs.emplace_back(prog.mlp_discs);
            report(symbols, discs);
            std::cout.rdbuf(backup); // reassign cout to its former buffer before that buffer no longer exists
            disc_output.close();     // close file output
#ifdef DIPS_use_mpi
        }
        MPI_Barrier(MPI_COMM_WORLD);
#endif
    }
    rec.m_status = EXCEEDED_MAX_ITERATIONS;
    std::cout << solver_base_name << ": total number of iterations exceeded limit."
              << "Rising max_it might help\n";
    return rec;
}

bool esip_bnf_solver::set_option(const std::string& option, double value) {
    if(option == "feas_tol") {
        if(value >= 0) {
            feas_tol = value;
            return true;
        }
        return false;
    }
    if(option == "abs_tol") {
        if(value >= 1e-9) {
            abs_tol = value;
            return true;
        }
        return false;
    }
    if(option == "rel_tol") {
        if(value >= 1e-9) {
            rel_tol = value;
            return true;
        }
        return false;
    }
    if(option == "abs_tol_ulp") {
        if(value >= 1e-9) {
            abs_tol_ulp = value;
            return true;
        }
    }
    if(option == "rel_tol_ulp") {
        if(value >= 1e-9) {
            rel_tol_ulp = value;
            return true;
        }
    }
    if(option == "abs_tol_minmax") {
        if(value >= 1e-9) {
            abs_tol_minmax = value;
            return true;
        }
    }
    if(option == "rel_tol_minmax") {
        if(value >= 1e-9) {
            rel_tol_minmax = value;
            return true;
        }
    }
    if(option == "red_tol_minmax") {
        if(value > 1) {
            red_tol_minmax = value;
            return true;
        }
    }
    if(option == "max_iter") {
        if(value >= 1) {
            max_iter = (int)value;
            return true;
        }
        return false;
    }
    std::cout << "Failed to set option " << option << " = " << value << '\n';
    return false;
}

bool esip_bnf_solver::set_option(const std::string& option, const std::string& value) {
    if(option == "settings_ulp") {
        settings_ulp = value;
        return true;
    }
    if(option == "settings_minmax_ulp") {
        settings_minmax_ulp = value;
        return true;
    }
    if(option == "settings_minmax_llp") {
        settings_minmax_llp = value;
        return true;
    }
    return false;
}

void esip_bnf_solver::pass_option(const std::string& option, double value) {
    options.emplace_back(option, value);
}

double esip_bnf_solver::get_option(const std::string& option) const {
    if(option == "abs_tol") {
        return abs_tol;
    }
    if(option == "rel_tol") {
        return rel_tol;
    }
    if(option == "feas_tol") {
        return feas_tol;
    }
    throw std::invalid_argument("Option '" + option + "' not supported or implemented.");
}

void esip_bnf_solver::set_solver_base_name(std::string name) {
    solver_base_name = std::move(name);
}

} // namespace dips
