#include "gurobi_evaluator.hpp"

#include <cstdlib>   // for rand, size_t
#include <list>      // for operator!=, list
#include <stdexcept> // for invalid_argument
#include <string>    // for operator+, to_string

#include "util/visitor_utils.hpp" // for call_visitor, reset_value_node_ptr_variant, traverse_children
#include "expression.hpp"         // for expression
#include "node.hpp"               // for value_node, value_node_ptr_variant, exp_node, parameter_node, equal_node
#include "scalar_evaluator.hpp"   // for ScalarVariableIdManager
#include "symbol.hpp"             // for variable_symbol
#include "symbol_table.hpp"       // for symbol_table

namespace dips {

/**
     * Transforms an expression containing exp into epxressions which can be evaluated using the quad_expr evaluators
     * To do this additional constraints/variables are added
     */
class gurobi_expr_transformer {
public:
    gurobi_expr_transformer(value_node_ptr_variant root, symbol_table& symbols, ScalarVariableIdManager& id_manager, GurobiConstraintList& grb_constraints, std::vector<QuadConstraintList>& quad_constraint_lists) :
        current_node(root), symbols(symbols), id_manager(id_manager), grb_constraints(grb_constraints), quad_constraint_lists(quad_constraint_lists) { }

    template <typename TType>
    void operator()(value_node<TType>* node) {
        traverse_children(*this, node, {}, current_node);
    }

    void operator()(exp_node* node) {
        // exp_node is of the form: exp(f(a))
        // this is split into y=e^x and x=f(a)

        // create varible names for x, y
        auto name_x = "__helper_var_x" + std::to_string(std::rand());
        auto name_y = "__helper_var_y" + std::to_string(std::rand());

        // check that they are unique
        if(symbols.resolve(name_x) != nullptr || symbols.resolve(name_y) != nullptr) {
            throw std::invalid_argument("failed to generate unique variable names");
        }

        // create variables and define them (this is needed for evaluation in quad_expr_evaluator)
        auto* var_x = new variable_symbol<real<0>>(name_x);
        auto* var_y = new variable_symbol<real<0>>(name_y);
        symbols.define(name_x, var_x);
        symbols.define(name_y, var_y);

        // add constraint y=e^x
        grb_constraints.push_back({ GurobiExponentialConstraint { id_manager.get_or_assign_id(name_x), id_manager.get_or_assign_id(name_y) } });

        // add constraint x=f(a)
        expression<ale::boolean<0>> constr_x(new equal_node<real<0>>(new parameter_node<real<0>>(name_x), node->get_child<0>()->clone()));
        auto eval_constr = evaluate_gurobi_constraint(constr_x, symbols, id_manager, grb_constraints, quad_constraint_lists);
        quad_constraint_lists.push_back(eval_constr);

        // replace e^x with y
        reset_value_node_ptr_variant(current_node, new parameter_node<real<0>>(name_y));
    }

private:
    symbol_table& symbols;
    value_node_ptr_variant current_node;
    GurobiConstraintList& grb_constraints;
    std::vector<QuadConstraintList>& quad_constraint_lists;
    ScalarVariableIdManager& id_manager;
    size_t next_free_id = 0;
};

QuadExpr evaluate_gurobi(expression<real<0>>& expr, symbol_table& symbols, ScalarVariableIdManager& id_manager, GurobiConstraintList& grb_constraints, std::vector<QuadConstraintList>& quad_constraint_lists) {
    gurobi_expr_transformer transformer(expr.get_root(), symbols, id_manager, grb_constraints, quad_constraint_lists);
    call_visitor(transformer, expr);
    return evaluate_quadratic(expr, symbols, id_manager);
}

QuadConstraintList evaluate_gurobi_constraint(expression<ale::boolean<0>>& expr, symbol_table& symbols, ScalarVariableIdManager& id_manager, GurobiConstraintList& grb_constraints, std::vector<QuadConstraintList>& quad_constraint_lists) {
    gurobi_expr_transformer transformer(expr.get_root(), symbols, id_manager, grb_constraints, quad_constraint_lists);
    call_visitor(transformer, expr);
    return evaluate_quadratic_constraint(expr, symbols, id_manager);
}

} // namespace dips
