#include "highs_solver.hpp"

#include <chrono>        // for duration, operator-, time_point
#include <cstdlib>       // for size_t
#include <iostream>      // for operator<<, endl, basic_ostream, cout, ostream
#include <string>        // for allocator, operator==, char_traits, string, basic_string, operator+
#include <unordered_map> // for unordered_map
#include <vector>        // for vector
#include <mutex>         // for mutex, lock_guard
#include <algorithm>     // for copy, max, min
#include <array>         // for array
#include <exception>     // for exception
#include <limits>        // for numeric_limits
#include <list>          // for list, operator!=, _List_iterator, _List_iterator<>::_Self
#include <memory>        // for allocator_traits<>::value_type
#include <set>           // for operator!=, set, _Rb_tree_const_iterator, _Rb_tree_const_iterator<>::_Self
#include <stdexcept>     // for invalid_argument, runtime_error
#include <utility>       // for move, pair
#include <variant>       // for variant

#include "Highs.h"                 // for Highs
#include "quad_expr_evaluator.hpp" // for QuadConstraintList, QuadConstraintType, QuadConstraint, evaluate_quadratic, evaluate_quadratic_constraint, QuadCo...
#include "get_time.hpp"            // for thread_clock
#include "parameter_cloner.hpp"    // for add_variable_to_solve_record
#include "symbol_finder.hpp"       // for find_variables
#include "QuadExpr.hpp"            // for LinExpr, QuadExpr, SparseMatrix, PureQuadExpr
#include "program.hpp"             // for program
#include "scalar_evaluator.hpp"    // for ScalarVariableIdManager

namespace dips {

/*class highs_callback: public GRBCallback {
public:
    explicit highs_callback(const std::atomic<bool>& abort_opt): abort_optimization(abort_opt) {}

protected:
    void callback() final {
        if (abort_optimization) {
            abort();
        }
    }

private:
    const std::atomic<bool>& abort_optimization;
};
*/

highs_solver::highs_solver(symbol_table& symbols, std::string name) :
    solver(name), symbols(symbols), solver_name(std::move(name)) { }

solver* highs_solver::make_solver(symbol_table& symbols, std::string highs_solver_name) {
    return new highs_solver(symbols, std::move(highs_solver_name));
}

// set objective of model to quadratic ehighsion expr
void set_objective(Highs& model, const QuadExpr& expr) {
    // add linear part
    const LinExpr& linearPart = expr.getLinearPart();
    for(const unsigned& var_id : linearPart.get_non_zero_ids()) {
        double coeff = linearPart.get_value(var_id);
        model.changeColCost(static_cast<HighsInt>(var_id), coeff);
    }

    if(expr.is_quadratic()) {
        std::vector<HighsInt> columns;
        std::vector<HighsInt> start;
        std::vector<double> values = { 0 };

        // QuadExpr can represent x0*x1 either as
        // 0 0  or  0 1  or  0.0 0.2  or  ...
        // 1 0      0 0      0.8 0.0
        // so we add the the values of each pair ((i, j), (j, i))
        // to get an upper traingular matrix
        SparseMatrix upperTriangular;
        const PureQuadExpr& quadPart = expr.getQuadraticPart();
        for(int k = 0; k < quadPart.get_non_zero_ids().size(); k++) {
            int i = quadPart.get_non_zero_ids().at(k).first;
            int j = quadPart.get_non_zero_ids().at(k).second;

            double value = quadPart.get_non_zero_values().at(k);

            if(i == j) {
                upperTriangular.setElement(i, j, 2 * value);
            } else {
                int x = std::min(i, j);
                int y = std::max(i, j);

                double old_val = upperTriangular.get_element(x, y); // value of (j, i) or 0
                upperTriangular.setElement(x, y, old_val + value);
            }
        }


        // convert upperTriangular into the form expected by HiGHS
        for(int i = 0; i < upperTriangular.get_number_of_rows(); ++i) {
            const auto& row = upperTriangular.getRow(i);

            const auto& ids = row.get_non_zero_ids();
            columns.insert(columns.end(), ids.begin(), ids.end());

            const auto& vals = row.get_non_zero_values();
            values.insert(values.end(), vals.begin(), vals.end());

            start.push_back(static_cast<HighsInt>(vals.size()));
        }

        HighsInt hessformat = static_cast<HighsInt>(HessianFormat::kTriangular);
        model.passHessian(model.getNumCol(), static_cast<HighsInt>(values.size()), hessformat, start.data(), columns.data(), values.data());
    }

    model.changeObjectiveSense(ObjSense::kMinimize);
    model.changeObjectiveOffset(expr.getConstant());
}

// add list of quadratic constraints to model
// an optional comment can be used to indentify the constraints in highs
void add_quad_constraints(Highs& model, const QuadConstraintList& constr_list, const std::string& comment) {
    for(const QuadConstraint& constr : constr_list) {
        QuadConstraintType t = constr.getType();
        if(constr.getExpr().is_quadratic()) {
            throw std::runtime_error("Only linear constraints implemented in HiGHS solver");
        }

        const LinExpr& linearPart = constr.getExpr().getLinearPart();
        std::vector<HighsInt> indices(linearPart.get_non_zero_ids().begin(), linearPart.get_non_zero_ids().end());
        double c = linearPart.constant();

        // a1*x1+a2*x2+c \in [l,u] => a1*x1+a2*x2 \in [l-c,u-c]
        switch(t) {
            case QuadConstraintType::EQUAL:
                model.addRow(0.0 - c, 0.0 - c, static_cast<HighsInt>(linearPart.get_non_zero_values().size()),
                  indices.data(), linearPart.get_non_zero_values().data());
                break;
            case QuadConstraintType::LESS_EQUAL:
                model.addRow(-1e30 - c, 0.0 - c, static_cast<HighsInt>(linearPart.get_non_zero_values().size()),
                  indices.data(), linearPart.get_non_zero_values().data());
                break;
            case QuadConstraintType::GREATER_EQUAL:
                model.addRow(0.0 - c, 1e30 - c, static_cast<HighsInt>(linearPart.get_non_zero_values().size()),
                  indices.data(), linearPart.get_non_zero_values().data());
                break;
            case QuadConstraintType::NOT_EQUAL:
                throw std::invalid_argument("Constraints of type '!=' are not allowed");
            case QuadConstraintType::LESS:
                throw std::invalid_argument("Constraints of type '<' are not allowed");
            case QuadConstraintType::GREATER:
                throw std::invalid_argument("Constraints of type '>' are not allowed");
        }
    }
}

// reads the optimized values from model and stores them into rec
void set_solution_variables(solve_record& rec, const ScalarVariableIdManager& id_manager, const std::vector<double>& variable_solutions) {
    id_manager.set_record<double>([](double x) {
        return x;
    },
      rec, variable_solutions);
}

// read status code from model and set rec accordingly
void set_record(const Highs& model, solve_record& rec, const ScalarVariableIdManager& id_manager) {
    const HighsInfo& info = model.getInfo();

    switch(model.getModelStatus()) {
        case HighsModelStatus::kOptimal: {
            rec.m_status = GLOBAL;
            rec.m_lbd = info.objective_function_value;

            if(model.getModel().isMip()) {
                rec.m_ubd = rec.m_lbd + info.mip_gap;
            } else {
                rec.m_ubd = rec.m_lbd;
            }
            set_solution_variables(rec, id_manager, model.getSolution().col_value);

            break;
        }

        case HighsModelStatus::kInfeasible: {
            rec.m_status = INFEASIBLE;
            rec.m_lbd = std::numeric_limits<double>::infinity();
            rec.m_ubd = std::numeric_limits<double>::infinity();

            break;
        }
        case HighsModelStatus::kUnbounded: {
            rec.m_status = UNBOUNDED;
            rec.m_lbd = -std::numeric_limits<double>::infinity();
            rec.m_ubd = -std::numeric_limits<double>::infinity();
            break;
        }
        case HighsModelStatus::kTimeLimit: {
            if(info.max_primal_infeasibility <= 1e-6) {
                rec.m_lbd = info.objective_function_value;

                if(model.getModel().isMip()) {
                    rec.m_ubd = rec.m_lbd + info.mip_gap;
                } else {
                    rec.m_ubd = rec.m_lbd;
                }
                set_solution_variables(rec, id_manager, model.getSolution().col_value);
                rec.m_status = FEASIBLE;
            } else {
                rec.m_status = UNKNOWN;
            }
            break;
        }
        default:
            rec.m_status = ABNORMAL;
            break;
    }
}

struct HighsVariable {
    HighsVarType type {};
    double lb {};
    double ub {};
};

solve_record highs_solver::solve(program prog) {


    solve_record rec;

    //catch errors
    try {

        Highs model;
        model.setOptionValue("output_flag", false); // comment out for debug output

        // find all variables that are used in the program statement
        auto used_variables = find_variables(prog, symbols);

        // add parameter of correct shape to solve_record
        for(const auto& var_name : used_variables) {
            add_variable_to_solve_record(var_name, symbols, rec);
        }

        // evaluate expressions
        ScalarVariableIdManager id_manager;
        id_manager.set_ids(used_variables, symbols);

        std::vector<QuadConstraintList> quad_constraint_lists {};

        auto obj = evaluate_quadratic(prog.m_objective.front(), symbols, id_manager);
        for(auto& constr : prog.m_constraints) {
            auto evaluated_constr = evaluate_quadratic_constraint(constr, symbols, id_manager);
            quad_constraint_lists.push_back(evaluated_constr);
        }

        // add variables to model
        auto highs_vars = id_manager.map_over_variable_symbols<HighsVariable>([&model](double lb, double ub, double init, bool integral, const std::string& name, const std::string& comment, const std::vector<size_t>& index) {
            return HighsVariable { integral ? HighsVarType::kInteger : HighsVarType::kContinuous, lb, ub };
        },
          symbols);

        // we assume that addCol is only called here, since we do not get the column number back from HiGHS
        // only this way, the column number in changeColIntegrality (and other function calls) will match with the variable set by addCol
        for(HighsInt i = 0; i < highs_vars.size(); ++i) {
            const auto& var = highs_vars.at(i);
            model.addCol(0.0, var.lb, var.ub, 0, nullptr, nullptr);
            model.changeColIntegrality(i, var.type);
        }

        // set the objective and constraints of the model
        set_objective(model, obj);
        for(auto& constr_list : quad_constraint_lists) {
            add_quad_constraints(model, constr_list, ""); // TODO: parse comment again
        }
        model.writeModel("out.lp");

        // try to read settings from file
        model.readOptions(this->settings_file);

        //set options saved in pass_option of calling context
        for(const auto& option : options) {
            set_option(option.first, option.second);
        }

        // set  model parameters
        model.setOptionValue("mip_rel_gap", this->rel_tol);
        model.setOptionValue("mip_abs_gap", this->abs_tol);
        //model.setOptionValue("threads", this->num_threads); //currently changing this variable seemst to break HiGHS
        model.setOptionValue("time_limit", this->time_limit); // in seconds
        model.setOptionValue("mip_feasibility_tolerance", this->int_tol);
        model.setOptionValue("allow_unbounded_or_infeasible", false);
        //model.setOptionValue("parallel","off");
        //model.setOptionValue("use_implied_bounds_from_presolve", false);
        ///highs_callback cb(abort_optimization);
        ///model.setCallback(&cb);


        // optimize model and measure cpu time (approximate, not respecting threads)
        auto time_start = thread_clock::now();
        model.run();
        auto time_end = thread_clock::now();
        rec.m_cpu = std::chrono::duration<double>(time_end - time_start).count();

        // read solver status
        std::cout << "Sucessfully optimized" << std::endl;
        set_record(model, rec, id_manager);

        return rec;
    } catch(const std::exception& e) {
        std::cout << "HiGHS Error code = " << e.what() << std::endl;
        throw e;
    }
}

void highs_solver::abort() { abort_optimization = true; }

bool highs_solver::set_option(std::string option, double value) {
    // TODO: abs_tol, rel_tol ranges could be bigger
    if(option == "abs_tol") {
        if(value >= 1e-9) {
            abs_tol = value;
            return true;
        }
    } else if(option == "rel_tol") {
        if(value >= 1e-9) {
            rel_tol = value;
            return true;
        }
    } else if(option == "num_threads") {
        if(value >= 0) {
            num_threads = static_cast<int>(value);
            return true;
        }
    } else if(option == "lp_alg") {
        if(value >= -1 && value <= 5) {
            lp_alg = static_cast<int>(value);
            return true;
        }
    } else if(option == "int_tol") {
        if(value >= 1e-9 && value <= 1e-1) {
            int_tol = value;
            return true;
        }
    } else if(option == "time_limit") {
        if(value >= 0) {
            time_limit = value;
            return true;
        }
    }
    return false;
}

bool highs_solver::set_option(std::string option, std::string value) {
    if(option == "settings") {
        settings_file = std::move(value);
        return true;
    }
    return false;
}

double highs_solver::get_option(std::string option) {
    throw std::invalid_argument("Option '" + option + "' not supported or implemented.");
}

} // namespace dips
