#include "maingo_solver.hpp"

#include "MAiNGO.h"             // for MAiNGO
#include "aleModel.h"           // for AleModel
#include "symbol_finder.hpp"    // for find_variables
#include "parameter_cloner.hpp" // for add_variable_to_solve_record
#include "expression.hpp"       // for expression
#include "program.h"            // for Program
#include "program.hpp"          // for program
#include "returnCodes.h"        // for FEASIBLE_POINT, GLOBALLY_OPTIMAL, INFEASIBLE, NO_FEASIBLE_POINT_FOUND
#ifdef DIPS_use_mpi
#include "mpi.h"
#endif

#include <memory> // for shared_ptr, __shared_ptr_access, make_shared, unique_ptr
#include <limits> // for numeric_limits
#include <list>   // for list
#include <set>    // for operator!=, set, _Rb_tree_const_iterator, _Rb_tree_const_iterator<>::_Self


namespace dips {



solve_record maingo_solver::solve(program prog) {
#ifdef DIPS_use_mpi
    int _rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &_rank);
#endif
    solve_record rec;

    // find all variables that are used in the program statement
    auto used_variables = find_variables(prog, symbols);

    // add parameter of correct shape to solve_record
    for(const auto& var_name : used_variables) {
        add_variable_to_solve_record(var_name, symbols, rec);
    }

    maingo::Program maingo_prog;
    maingo_prog.mObjective = prog.m_objective;
    maingo_prog.mConstraints = prog.m_constraints;
    maingo_prog.mRelaxations = prog.m_relaxations;
    maingo_prog.mOutputs = prog.m_outputs;

    std::shared_ptr<maingo::AleModel> mymodel;
    std::shared_ptr<maingo::MAiNGO> solver;
    mymodel = std::make_shared<maingo::AleModel>(maingo_prog, symbols);
    solver = std::make_shared<maingo::MAiNGO>(mymodel);

    // setting default values and/or pass options if provided
    solver->set_option("epsilonA", abs_tol);
    solver->set_option("epsilonR", rel_tol);
    solver->set_option("maxTime", max_time);
    for(auto& option : options) {
        solver->set_option(option.first, option.second);
    }
    // overwritting default values and/or options if setting file is provided
    solver->read_settings(settings_file);

    // set output file path and names
    std::string output_path = "";
    for(auto& i : solver_name) {
        output_path += i;
    }
    solver->set_result_file_name(output_path + "_result.txt");
    solver->set_log_file_name(output_path + "_log.txt");
    solver->set_iterations_csv_file_name(output_path + "_iterations.csv");
    solver->set_solution_and_statistics_csv_file_name(output_path + "_statistics_and_solution.csv");
    solver->set_json_file_name(output_path + ".json");

    // solve program
#ifdef DIPS_use_mpi
    MPI_Barrier(MPI_COMM_WORLD);
#endif
    auto stat = solver->solve();
#ifdef DIPS_use_mpi
    MPI_Barrier(MPI_COMM_WORLD);
#endif

    rec.m_cpu = solver->get_cpu_solution_time();

    std::vector<double> variables;
    int varSize = 0;
#ifdef DIPS_use_mpi
    if(_rank == 0) {
#endif
        switch(stat) {
            case maingo::GLOBALLY_OPTIMAL:
                rec.m_status = GLOBAL;
                rec.m_lbd = solver->get_final_LBD();
                rec.m_ubd = solver->get_objective_value();
                variables = solver->get_solution_point();
                varSize = variables.size();
                break;
            case maingo::FEASIBLE_POINT:
                rec.m_status = FEASIBLE;
                rec.m_lbd = solver->get_final_LBD();
                rec.m_ubd = solver->get_objective_value();
                variables = solver->get_solution_point();
                varSize = variables.size();
                break;
            case maingo::INFEASIBLE:
                rec.m_status = INFEASIBLE;
                rec.m_lbd = std::numeric_limits<double>::infinity();
                rec.m_ubd = std::numeric_limits<double>::infinity();
                varSize = 0;
                break;
            case maingo::BOUND_TARGETS:
                rec.m_lbd = solver->get_final_LBD();
                rec.m_status = UNKNOWN;
                varSize = 0;
                break;
            case maingo::NO_FEASIBLE_POINT_FOUND:
            case maingo::NOT_SOLVED_YET:
                rec.m_status = UNKNOWN;
                varSize = 0;
                break;
            default:
                throw std::invalid_argument("unhandled MAiNGO return code " + std::to_string(stat));
        }
#ifdef DIPS_use_mpi
    }
#endif

#ifdef DIPS_use_mpi
    // broadcast to workers
    MPI_Bcast(&rec.m_cpu, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&rec.m_status, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&rec.m_lbd, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&rec.m_ubd, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&varSize, 1, MPI_INT, 0, MPI_COMM_WORLD);
    variables.resize(varSize);
    MPI_Bcast(variables.data(), variables.size(), MPI_DOUBLE, 0, MPI_COMM_WORLD);
#endif

    if(rec.m_status == INFEASIBLE || rec.m_status == UNKNOWN) {
        return rec;
    }

    auto positions = mymodel->get_positions();
    maingo_symbol_reader sym_read(variables, positions);
    for(auto& it : rec.m_solution) {
        sym_read.dispatch(it.second.get());
    }

    return rec;
}

bool maingo_solver::set_option(std::string option, double value) {
    if(option == "abs_tol") {
        if(value >= 1e-9) {
            abs_tol = value;
            return true;
        }
        return false;
    }
    if(option == "rel_tol") {
        if(value >= 1e-9) {
            rel_tol = value;
            return true;
        }
        return false;
    }
    if(option == "max_time") {
        if(value >= 0) {
            max_time = value;
            return true;
        }
        return false;
    }
    return false;
}

bool maingo_solver::set_option(std::string option, std::string value) {
    if(option == "settings") {
        settings_file = value;
        return true;
    }
    return false;
}

double maingo_solver::get_option(std::string option) {
    if(option == "abs_tol") {
        return abs_tol;
    }
    if(option == "rel_tol") {
        return rel_tol;
    }
    if(option == "max_time") {
        return max_time;
    }
    throw std::invalid_argument("Option '" + option + "' not supported or implemented.");
}

} // namespace dips
