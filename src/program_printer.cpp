#include "program_printer.hpp"

#include <iostream> // for endl, basic_ostream, cout, ostream
#include <list>     // for list, operator!=, _List_iterator, _List_iterator<>::_Self

#include "util/expression_to_string.hpp" // for expression_to_string
#include "program.hpp"                   // for program

namespace dips {

std::string program_to_string(program& prog) {
    std::string prog_str;
    prog_str += "objective:\n";
    prog_str += expression_to_string(prog.m_objective.front()) + "\n";
    prog_str += "constraints:\n";
    for(auto& constr : prog.m_constraints) {
        prog_str += expression_to_string(constr) + "\n";
    }
    return prog_str;
}



} // namespace dips
