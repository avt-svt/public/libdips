#include "solver.hpp"

#include <stdint.h>  // for uint8_t
#include <cassert>   // for assert
#include <cmath>     // for fabs
#include <algorithm> // for sort
#include <fstream>   // for ofstream

#include "util/expression_to_string.hpp" // for symbol_to_string
#include "nlohmann/json.hpp"             // for basic_json<>::object_t, json, basic_json<>::value_type
#include "symbol_table.hpp"              // for symbol_table
#include "output.hpp"                    // for to_json

namespace dips {



std::string
to_string(solve_status status) {
    switch(status) {
        case ABNORMAL: return "ABNORMAL";
        case ABORTED: return "ABORTED";
        case EXCEEDED_MAX_TIME: return "EXCEEDED_MAX_TIME";
        case EXCEEDED_MAX_ITERATIONS: return "EXCEEDED_MAX_ITERATIONS";
        case ASSUMPTION_VIOLATION_LIKELY: return "ASSUMPTION_VIOLATION_LIKELY";
        case NUMERICAL_SENSITIVE: return "NUMERICAL_SENSITIVE";
        case INFEASIBLE: return "INFEASIBLE";
        case FEASIBLE: return "FEASIBLE";
        case LOCAL: return "LOCAL";
        case GLOBAL: return "GLOBAL";
        case UNBOUNDED: return "UNBOUNDED";
        case UNKNOWN: return "UNKNOWN";
        default: return "UNKNOWN (developer info: solve_status not implemented in to_string)";
    }
}

void dump_json(const solve_record& rec, const std::filesystem::path& path) {
    nlohmann::json j;
    j["record"] = rec;
    std::filesystem::create_directories(path.parent_path());
    std::ofstream output(path);
    if(output.is_open()) {
        output << j.dump(2);
        output.close();
        assert(std::filesystem::is_regular_file(path));
    } else {
        std::cout << "Can not open path for json dump: " << path.string() << std::endl;
    }
};

void report(const solve_record& rec) {


    std::vector<std::string> keys;

    keys.reserve(rec.m_solution.size());
    for(const auto& it : rec.m_solution) {
        keys.push_back(it.first);
    }
    std::sort(keys.begin(), keys.end());

    std::cout << "solver status: " << to_string(rec.m_status) << '\n';
    std::cout << "cpu time: " << rec.m_cpu << '\n';
    std::cout << "lower bound: " << rec.m_lbd << '\n';
    std::cout << "upper bound: " << rec.m_ubd << '\n';
    std::cout << "variable values:\n";
    for(auto& it : keys) {
        std::cout << "  " << symbol_to_string(rec.m_solution.find(it)->second.get()) << std::endl;
    }
}

void report(const solve_record& rec, std::vector<std::string> names) {
    std::cout << "solver status: " << to_string(rec.m_status) << '\n';
    std::cout << "cpu time: " << rec.m_cpu << '\n';
    std::cout << "lower bound: " << rec.m_lbd << '\n';
    std::cout << "upper bound: " << rec.m_ubd << '\n';
    std::cout << "variable values:\n";
    for(auto& name : names) {
        auto jt = rec.m_solution.find(name);
        if(jt != rec.m_solution.end()) {
            std::cout << "  " << symbol_to_string(jt->second.get()) << std::endl;
        } else {
            std::cout << "  no record found for " << name << '\n';
        }
    }
}

void report(ale::symbol_table& symbols, const std::vector<std::vector<discretization>>& discs_vector) {
    for(const auto& j : discs_vector) {
        for(int i = 0; i < j.size(); i++) {
            auto* indexes = symbols.resolve(j[i].indexes);
            std::cout << symbol_to_string(indexes) << "\n\n";
            for(int k = 0; k < j[i].parameters.size(); k++) {
                auto* param = symbols.resolve(std::get<1>(j[i].parameters[k]));
                std::cout << symbol_to_string(param) << "\n";
            }
            std::cout << std::endl;
        }
    }
}


void solver::pass_option(const std::string& option, double value) {
    options[option] = value;
}

bool check_termination(solve_record& rec, double feas_vio_ubp, double abs_tol, double rel_tol, double feas_tol) {
    if(rec.m_ubd <= rec.m_lbd + abs_tol) {
        std::cout << "\nTermination criterion reached.\n";
        std::cout << "Absolute gap: " << rec.m_ubd - rec.m_lbd << " <= abs_tol = " << abs_tol << std::endl;
        std::cout << "Relative gap: " << ((rec.m_ubd == 0) ? (rec.m_lbd) : ((rec.m_ubd - rec.m_lbd) / std::fabs(rec.m_ubd))) << std::endl;
        std::cout << "Feas. vio:    " << feas_vio_ubp << " <= feas_tol = " << feas_tol << std::endl;
        rec.m_status = GLOBAL;
        return true;
    }
    if(rec.m_ubd < std::numeric_limits<double>::infinity() && rec.m_ubd <= rec.m_lbd + rel_tol * std::fabs(rec.m_ubd)) {
        std::cout << "\nTermination criterion reached.\n";
        std::cout << "Absolute gap: " << rec.m_ubd - rec.m_lbd << std::endl;
        std::cout << "Relative gap: " << ((rec.m_ubd == 0) ? (rec.m_lbd) : ((rec.m_ubd - rec.m_lbd) / std::fabs(rec.m_ubd))) << " <= rel_tol = " << rel_tol << std::endl;
        std::cout << "Feas. vio:    " << feas_vio_ubp << " <= feas_tol = " << feas_tol << std::endl;
        rec.m_status = GLOBAL;
        return true;
    }
    return false;
}
bool check_aborted(double max_time, solve_record& rec, const std::string& solver_base_name, bool aborted) {
    if(max_time - rec.m_cpu <= 0) {
        std::cout << solver_base_name << ": cpu time exceeded time limit."
                  << "Rising max_time might help\n";
        rec.m_status = EXCEEDED_MAX_TIME;
        return true;
    }
    if(aborted) {
        rec.m_status = ABORTED;
        return true;
    }
    return false;
}


void scoped_push::dismiss() {
    enabled = false;
}
void scoped_push::trigger() {
    if(enabled) {
        guarded_vector.pop_back();
    }
    dismiss();
}
} // namespace dips
