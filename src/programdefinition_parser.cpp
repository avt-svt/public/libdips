#include "programdefinition_parser.hpp"

#include <iostream>  // for operator<<, basic_ostream, cout, endl, cerr
#include <utility>   // for pair
#include <stdexcept> // for invalid_argument

#include "lexer.hpp"        // for lexer
#include "parser.hpp"       // for parser
#include "token.hpp"        // for token, token::LPAREN, token::RPAREN, token::SEMICOL, token::EQUAL, token::END, token::COMMA, token::COLON, token::KEYWORD
#include "token_buffer.hpp" // for token_buffer


namespace dips {

programdefinition_parser::programdefinition_parser(std::istream& is, ale::symbol_table& symbols, const std::string& block_name,
  const std::vector<std::string>& required_internals) :
    parser(is, symbols),
    block_name(block_name), required_internals(required_internals) {
    lex.reserve_keywords({ "programdefinitions",
      "set_disc",
      "set_disc_parameter_src",
      "set_disc_variable_src",
      "set_internal",
      "push_internal",
      "set_info",
      "push_info",
      //other block headings
      "definitions",
      "objective",
      "constraints",
      "outputs",
      "relaxation",
      "only",
      "squashing" });
}


//checks for "programdefinitions(<block_name>):"
//for each found it parses
bool programdefinition_parser::parse() {
    bool verbose = false;
    bool found_block_name = false;
    if(verbose) std::cout << "Desired block \"programdefinitions(\"" << block_name << "\")\"\n";
    while(!check(ale::token::END)) {
        if(check_keyword("programdefinitions")) {
            consume();

            if(!match(ale::token::LPAREN)) {
                report_syntactical();
                recover_block();
                continue; // check for next programdefinitions block
            } else {

                std::string actual_name;
                if(!match_literal(actual_name)) {
                    report_syntactical();
                    recover_block();
                    continue; // check for next programdefinitions block
                }

                if(!match(ale::token::RPAREN) || !match(ale::token::COLON)) {
                    report_syntactical();
                    recover_block();
                    continue; // check for next programdefinitions block
                }

                if(actual_name != block_name) {
                    if(verbose) std::cout << "  Skipping block \"programdefinitions(\"" << actual_name << "\")\"\n";
                    continue; // check for next programdefinitions block
                } else {
                    if(verbose) std::cout << "  Found    block \"programdefinitions(\"" << block_name << "\")\"\n";
                    parse_programdefinition();
                    found_block_name = true;
                    if(verbose) std::cout << "  Parsed   block \"programdefinitions(\"" << block_name << "\")\"\n";
                }
            }
        } else {
            // just ignore the keyword and try again
            consume();
            recover_block();
        }
    }
    print_errors();
    if(!found_block_name) {
        std::cout << "  Missing  block \"programdefinitions(\"" << block_name << "\")\"\n";
    }
    return found_block_name;
}

void programdefinition_parser::parse_programdefinition() {
    while(!check(ale::token::END) && !check_any_keyword("definitions", "objective", "constraints", "relaxation", "squashing", "outputs", "programdefinitions")) {
        // ingore any spurious semicolons
        if(match(ale::token::SEMICOL)) {
            continue;
        }
        if(check_keyword("set_disc")) {
            consume();

            if(!match(ale::token::LPAREN)) {
                report_syntactical();
                recover();
            }

            std::string disc_name = current().lexeme;

            if(discretization_mapping.find(disc_name) != discretization_mapping.end()) {
                set_semantic("ERROR: discretization index already defined");
                report_syntactical();
                recover();
                continue;
            }


            consume();

            if(!match(ale::token::RPAREN) || !match(ale::token::EQUAL)) {
                report_syntactical();
                recover();
                continue;
            }
            discretization new_disc;
            new_disc.indexes = current().lexeme;
            discretization_mapping[disc_name] = new_disc;
            consume();

            if(!match(ale::token::SEMICOL)) {
                report_syntactical();
                recover();
                continue;
            }

        } else if(check_keyword("set_disc_parameter_src")) {
            consume();

            if(!match(ale::token::LPAREN)) {
                report_syntactical();
                recover();
                continue;
            }

            std::string disc_name = current().lexeme;
            consume();

            if(discretization_mapping.find(disc_name) == discretization_mapping.end()) {
                // TODO:Ideally order should not matter
                set_expected_symbol();
                set_semantic("ERROR: discretization " + disc_name + " used before defined");
                recover();
                continue;
            }

            if(!match(ale::token::COMMA)) {
                report_syntactical();
                recover();
                continue;
            }

            std::string disc_param = current().lexeme;
            consume();

            if(!match(ale::token::RPAREN) || !match(ale::token::EQUAL)) {
                report_syntactical();
                recover();
                continue;
            }

            std::string disc_source = current().lexeme;
            consume();

            if(!match(ale::token::SEMICOL)) {
                report_syntactical();
                recover();
                continue;
            }

            discretization_mapping[disc_name].parameters.emplace_back(disc_source, disc_param);

        } else if(check_keyword("set_disc_variable_src")) {
            consume();

            if(!match(ale::token::LPAREN)) {
                report_syntactical();
                recover();
                continue;
            }

            std::string disc_name = current().lexeme;
            consume();

            if(discretization_mapping.find(disc_name) == discretization_mapping.end()) {
                // TODO:Ideally order should not matter
                set_expected_symbol();
                set_semantic("ERROR: discretization " + disc_name + " used before defined");
                recover();
                continue;
            }

            if(!match(ale::token::COMMA)) {
                report_syntactical();
                recover();
                continue;
            }

            std::string disc_param = current().lexeme;
            consume();

            if(!match(ale::token::RPAREN) || !match(ale::token::EQUAL)) {
                report_syntactical();
                recover();
                continue;
            }

            std::string disc_source = current().lexeme;
            consume();

            if(!match(ale::token::SEMICOL)) {
                report_syntactical();
                recover();
                continue;
            }

            discretization_mapping[disc_name].variables.emplace_back(disc_source, disc_param);

        } else if(check_keyword("set_internal")) {
            consume();

            if(!match(ale::token::LPAREN)) {
                report_syntactical();
                recover();
                continue;
            }

            std::string internal_name;
            if(!match_literal(internal_name)) {
                report_syntactical();
                recover();
                continue;
            }

            if(internals.find(internal_name) != internals.end()) {
                std::cout << "Warning: internal variable \"" << internal_name << "\" has already been set" << std::endl;
            }
            if(internals_list.find(internal_name) != internals_list.end()) {
                std::cout << "Warning: internal \"" << internal_name << "\" used as both list and parameter " << std::endl;
            }

            if(!match(ale::token::RPAREN) || !match(ale::token::EQUAL)) {
                report_syntactical();
                recover();
                continue;
            }

            std::string var_name = current().lexeme;
            consume();

            if(!match(ale::token::SEMICOL)) {
                report_syntactical();
                recover();
                continue;
            }

            internals[internal_name] = var_name;
        } else if(check_keyword("push_internal")) {
            consume();

            if(!match(ale::token::LPAREN)) {
                report_syntactical();
                recover();
                continue;
            }

            std::string internal_name;
            if(!match_literal(internal_name)) {
                report_syntactical();
                recover();
                continue;
            }

            if(internals.find(internal_name) != internals.end()) {
                std::cout << "Warning: internal \"" << internal_name << "\" used as both list and parameter " << std::endl;
            }

            if(!match(ale::token::RPAREN) || !match(ale::token::EQUAL)) {
                report_syntactical();
                recover();
                continue;
            }

            std::string var_name = current().lexeme;
            consume();

            if(!match(ale::token::SEMICOL)) {
                report_syntactical();
                recover();
                continue;
            }

            internals_list[internal_name].push_back(var_name);
        } else if(check_keyword("set_info")) {
            consume();

            if(!match(ale::token::LPAREN)) {
                report_syntactical();
                recover();
                continue;
            }

            std::string info_name;
            if(!match_literal(info_name)) {
                report_syntactical();
                recover();
                continue;
            }

            if(!match(ale::token::RPAREN) || !match(ale::token::EQUAL)) {
                report_syntactical();
                recover();
                continue;
            }

            // this accepst everything between "";
            std::string content;
            if(!match_literal(content)) {
                report_syntactical();
                recover();
                continue;
            }

            if(!match(ale::token::SEMICOL)) {
                report_syntactical();
                recover();
                continue;
            }

            const auto [dummy, did_not_exist] = info.insert_or_assign(info_name, content);
            if(!did_not_exist) {
                std::cout << "Warning: info \"" << info_name << "\" has already been set" << std::endl;
            }
            if(info_list.find(info_name) != info_list.end()) {
                std::cout << "Warning: info \"" << info_name << "\" used as both list and parameter " << std::endl;
            }

        } else if(check_keyword("push_info")) {
            consume();

            if(!match(ale::token::LPAREN)) {
                report_syntactical();
                recover();
                continue;
            }

            std::string info_name;
            if(!match_literal(info_name)) {
                report_syntactical();
                recover();
                continue;
            }

            if(info.find(info_name) != info.end()) {
                std::cout << "Warning: info \"" << info_name << "\" used as both list and parameter " << std::endl;
            }

            if(!match(ale::token::RPAREN) || !match(ale::token::EQUAL)) {
                report_syntactical();
                recover();
                continue;
            }

            // this accepst everything between "";
            std::string content;
            if(!match_literal(content)) {
                report_syntactical();
                recover();
                continue;
            }

            if(!match(ale::token::SEMICOL)) {
                report_syntactical();
                recover();
                continue;
            }

            info_list[info_name].push_back(content);
        } else {
            report_syntactical();
            recover();
            continue;
        }
    }
}

std::vector<discretization> programdefinition_parser::get_discretizations() const {

    std::vector<discretization> return_vector;
    for(const auto& it : discretization_mapping) {
        return_vector.push_back(it.second);
    }
    if(return_vector.empty()) {
        throw std::invalid_argument("Error: could not retrieve discretizations for \"" + block_name + "\" because discretization vector is empty");
    }
    return return_vector;
}
std::map<std::string, discretization> programdefinition_parser::get_discretization_map() const {
    return discretization_mapping;
}
std::map<std::string, std::string> programdefinition_parser::get_info() const {
    return info;
}

std::map<std::string, std::string> programdefinition_parser::get_internals() const {
    return internals;
}

std::string programdefinition_parser::get_block_name() const {
    return block_name;
}

std::map<std::string, std::vector<std::string>> programdefinition_parser::get_internals_list() const {
    return internals_list;
}

std::map<std::string, std::vector<std::string>> programdefinition_parser::get_info_list() const {
    return info_list;
}

bool programdefinition_parser::required_internals_read() const {
    bool internals_exist = true;
    for(const auto& s : required_internals) {
        if(internals.find(s) == internals.end() && internals_list.find(s) == internals_list.end()) {
            std::cerr << "  Required internal \"" << s << "\" not found\n";
            internals_exist = false;
        }
    }
    return internals_exist;
}

//TODO: duplication in program_parser.cpp that also needs to be keept up to date
void programdefinition_parser::recover_block() {
    while(current().type != ale::token::END && !(current().type == ale::token::KEYWORD && (current().lexeme == "definitions" || current().lexeme == "objective" || current().lexeme == "constraints" || current().lexeme == "relaxations" || current().lexeme == "squashing" || current().lexeme == "outputs" || current().lexeme == "programdefinitions"))) {
        consume();
    }
    buf.clear();
}

} // namespace dips
