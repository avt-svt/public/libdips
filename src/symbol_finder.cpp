#include "symbol_finder.hpp"

#include <vector>    // for vector
#include <algorithm> // for find
#include <cmath>     // for isnan
#include <list>      // for list, operator!=
#include <variant>   // for variant
#include <stdexcept> // for invalid_argument

#include "util/visitor_utils.hpp" // for call_visitor, traverse_children, evaluate_function
#include "util/evaluator.hpp"
#include "program.hpp" // for program
#include "symbol.hpp"  // for expression_symbol, parameter_symbol, variable_symbol, cast_function_symbol, function_symbol
#include "tensor.hpp"  // for decrease_tensor_index, last_tensor_index

namespace dips {

using namespace ale;

struct find_symbol_visitor {
    find_symbol_visitor(symbol_table& symbols, bool ignore_attribute_node) :
        symbols(symbols), ignore_attribute_node(ignore_attribute_node) { }

    template <typename TType>
    void operator()(value_node<TType>* node) {
        // push scope in case node is an iterator_node
        try {
            call_visitor([this](auto* node) { push_scoped_symbol(node); }, node);

            traverse_children(*this, node, symbols);

            // pop scope in case node is an iterator_node
            call_visitor([this](auto* node) { pop_scoped_symbol(node); }, node);
        } catch(const std::exception& e) {
            std::cout << "DEF: " << expression_to_string(node) << std::endl;
            throw;
        }
    }

    template <typename TType>
    void operator()(parameter_node<TType>* node) {
        // check if the symbol is only an iteration symbol
        if(std::find(scoped_symbols.begin(), scoped_symbols.end(), node->name) == scoped_symbols.end()) {
            auto* sym = symbols.resolve<TType>(node->name);
            if(sym != nullptr) {
                call_visitor(*this, sym);
            } else {
                throw std::invalid_argument("could not resolve symbol name: " + node->name);
            }
        }
    }

    template <typename TType>
    void operator()(attribute_node<TType>* node) {
        auto* sym = cast_value_symbol<TType>(symbols.resolve(node->variable_name));
        if(sym != nullptr) {
            if(ignore_attribute_node) {
                return;
            }
            call_visitor(*this, sym);
        } else {
            std::string type = typeid(TType).name();
            throw std::invalid_argument("symbol " + node->variable_name + " has unexpected type \"" + type + "\" in attribute call.");
        }
    }

    template <typename TType>
    void operator()(sum_node<TType>* node) {
        // skip empty sum_nodes
        auto val = ale::util::evaluate_expression(node->template get_child<0>(), symbols);
        if(val.empty()) {
            return;
        }
        // sum_node not empty -> do the same as in value_node<TType>* node

        // push scope in case node is an iterator_node
        call_visitor([this](auto* node) { push_scoped_symbol(node); }, node);

        traverse_children(*this, node, symbols);

        // pop scope in case node is an iterator_node
        call_visitor([this](auto* node) { pop_scoped_symbol(node); }, node);
    }

    template <typename TType>
    void operator()(function_node<TType>* node) {
        auto* sym = cast_function_symbol<TType>(symbols.resolve(node->name));
        if(sym == nullptr) {
            throw std::invalid_argument("could not resolve symbol name: " + node->name);
        }

        defined_functions.insert(node->name);

        // search in the function arguments
        traverse_children(*this, node);

        // search in the function expression (ignoring parameters)
        for(auto& arg : sym->arg_names) {
            scoped_symbols.push_back(arg);
        }

        evaluate_function(*this, node, symbols);

        for(auto& arg : sym->arg_names) {
            scoped_symbols.pop_back();
        }
    }

    template <typename TType>
    void operator()(parameter_symbol<TType>* sym) {
        defined_parameters.insert(sym->m_name);
    }

    template <typename TType>
    void operator()(variable_symbol<TType>* sym) {
        defined_variables.insert(sym->m_name);
    }

    template <typename TType>
    void operator()(expression_symbol<TType>* sym) {
        call_visitor(*this, sym->m_value);
    }

    std::set<std::string> defined_variables;  // contains all defined variables
    std::set<std::string> defined_parameters; // contains all defined parameters
    std::set<std::string> defined_functions;  // contains all defined functions
private:
    // push_scope (in order for push_scope(iterator_node) to be found only other base classes can be matched here)
    template <typename TType>
    void push_scoped_symbol(nary_node<TType>* node) { }

    template <typename... TTypes>
    void push_scoped_symbol(kary_node<TTypes...>* node) { }

    void push_scoped_symbol(terminal_node* node) { }

    template <typename IteratorType, typename TType>
    void push_scoped_symbol(iterator_node<IteratorType, TType>* node) {
        scoped_symbols.push_back(node->name);
    }

    // pop_scope
    template <typename TType>
    void pop_scoped_symbol(nary_node<TType>* node) { }

    template <typename... TTypes>
    void pop_scoped_symbol(kary_node<TTypes...>* node) { }

    void pop_scoped_symbol(terminal_node* node) { }

    template <typename IteratorType, typename TType>
    void pop_scoped_symbol(iterator_node<IteratorType, TType>* /*node*/) {
        scoped_symbols.pop_back();
    }

    std::vector<std::string> scoped_symbols; // contains iteration variables and function parameters
    symbol_table& symbols;
    bool ignore_attribute_node = false;
};

std::set<std::string> find_variables(value_node_variant expr, symbol_table& symbols, bool ignore_attribute_node) {
    find_symbol_visitor visitor(symbols, ignore_attribute_node);
    call_visitor(visitor, expr);
    return visitor.defined_variables;
}

std::set<std::string> find_parameters(value_node_variant expr, symbol_table& symbols) {
    find_symbol_visitor visitor(symbols, false);
    call_visitor(visitor, expr);
    return visitor.defined_parameters;
}

std::set<std::string> find_functions(value_node_variant expr, symbol_table& symbols) {
    find_symbol_visitor visitor(symbols, false);
    call_visitor(visitor, expr);
    return visitor.defined_functions;
}

struct has_prio_visitor {
    has_prio_visitor(symbol_table& symbols) :
        symbols(symbols) { }

    template <typename TType>
    void operator()(value_node<TType>* node) {
        traverse_children(*this, node);
    }

    template <typename TType>
    void operator()(variable_symbol<TType>* sym) {
        if constexpr(get_node_dimension<TType> == 0) {
            if(!std::isnan(sym->prio())) {
                has_prio = true;
            }
        } else {
            auto index = last_tensor_index(sym->shape());
            do {
                if(!std::isnan(sym->prio()[index.data()])) {
                    has_prio = true;
                }
            } while(decrease_tensor_index(sym->shape(), index));
        }
    }

    template <typename TType>
    void operator()(parameter_node<TType>* node) { }

    template <typename TType>
    void operator()(expression_symbol<TType>* sym) { }

    template <typename TType>
    void operator()(function_symbol<TType>* sym) { }

    template <typename TType>
    void operator()(parameter_symbol<TType>* sym) { }

    bool has_prio = false;
    symbol_table& symbols;
};

bool has_prio_variable(value_node_variant expr, symbol_table& symbols) {
    has_prio_visitor visitor(symbols);
    call_visitor(visitor, expr);
    return visitor.has_prio;
}

template <typename TType>
void add_vars(std::set<std::string>& vars, std::list<expression<TType>>& exprs, symbol_table& symbols) {
    for(auto& e : exprs) {
        auto new_vars = find_variables(e, symbols);
        vars.insert(new_vars.begin(), new_vars.end());
    }
}

std::set<std::string> find_variables(program& prog, symbol_table& symbols, bool ignore_attribute_nodes) {
    std::set<std::string> vars;

    add_vars(vars, prog.m_objective, symbols);
    add_vars(vars, prog.m_constraints, symbols);
    add_vars(vars, prog.m_relaxations, symbols);
    add_vars(vars, prog.m_squashes, symbols);
    add_vars(vars, prog.m_outputs, symbols);

    return vars;
}

template <typename TType>
void add_params(std::set<std::string>& vars, std::list<expression<TType>>& exprs, symbol_table& symbols) {
    for(auto& e : exprs) {
        auto new_vars = find_parameters(e, symbols);
        vars.insert(new_vars.begin(), new_vars.end());
    }
}

std::set<std::string> find_parameters(program& prog, symbol_table& symbols) {
    std::set<std::string> vars;

    add_params(vars, prog.m_objective, symbols);
    add_params(vars, prog.m_constraints, symbols);
    add_params(vars, prog.m_relaxations, symbols);
    add_params(vars, prog.m_squashes, symbols);
    add_params(vars, prog.m_outputs, symbols);

    return vars;
}

template <typename TType>
void add_functions(std::set<std::string>& vars, std::list<expression<TType>>& exprs, symbol_table& symbols) {
    for(auto& e : exprs) {
        auto new_vars = find_functions(e, symbols);
        vars.insert(new_vars.begin(), new_vars.end());
    }
}

std::set<std::string> find_functions(program& prog, symbol_table& symbols) {
    std::set<std::string> vars;

    add_functions(vars, prog.m_objective, symbols);
    add_functions(vars, prog.m_constraints, symbols);
    add_functions(vars, prog.m_relaxations, symbols);
    add_functions(vars, prog.m_squashes, symbols);
    add_functions(vars, prog.m_outputs, symbols);

    return vars;
}

template <typename TType>
bool has_prio_list(std::list<expression<TType>>& exprs, symbol_table& symbols) {
    bool has_prio = false;
    for(auto& e : exprs) {
        has_prio |= has_prio_variable(e, symbols);
    }

    return has_prio;
}

bool has_prio_variable(program& prog, symbol_table& symbols) {
    bool has_prio = false;

    has_prio |= has_prio_list(prog.m_objective, symbols);
    has_prio |= has_prio_list(prog.m_constraints, symbols);
    has_prio |= has_prio_list(prog.m_relaxations, symbols);
    has_prio |= has_prio_list(prog.m_squashes, symbols);
    has_prio |= has_prio_list(prog.m_outputs, symbols);

    return has_prio;
}

} // namespace dips
