#include "blp_no_box_solver.hpp"

#include <algorithm> // for min, max
#include <fstream>   // for operator<<, basic_ostream, basic_ios::rdbuf, basic_ofstream<>::__filebuf_type, basic_ostream<>::__ostream_type
#include <memory>    // for unique_ptr
#include <utility>   // for pair, move
#include <iostream>  // for cout
#include <limits>    // for numeric_limits
#include <stdexcept> // for invalid_argument

#include "common.hpp"       // for set_real, fix_variables, copy_variables, discretize, scoped_delayed_function_calls
#include "symbol_table.hpp" // for symbol_table


#ifdef DIPS_use_mpi
#include "mpi.h"
#endif

namespace dips {


blp_no_box_program_definition_parser::blp_no_box_program_definition_parser(std::istream& is, symbol_table& symbols) :
    programdefinition_parser_chain(is, symbols, { { "lbp", { "u_cont_var" } }, { "ubp", { "ubp_target", "eps_ubp", "current_lbd" } }, { "aux", { "aux_target", "eps_aux", "l_obj_val" } } }) {};


solve_record blp_no_box_solver::solve(const program_info prog) {
    std::unique_ptr<solver> sub(make_subsolver(symbols, solver_base_name));

    for(auto& option : options) {
        sub->pass_option(option.first, option.second);
    }
    abort_subsolver_callbacks.emplace_back([&sub = sub]() { sub->abort(); });

    scoped_callback_vector_clearer scope(abort_subsolver_callbacks);

    solve_record rec;
    solve_record lbp_rec;
    solve_record llp_rec;
    double Psi_hat = -100.0; //maximum of obtained aux objectives
    double l_obj_val;        // lower level objective value after the lbp is solved

    enum { LBP,
        UBP,
        AUX,
        LLP,
        NDEF } next_problem
      = LBP;

    //opt_tol + ubp_tol = feas_tol (=llp optimality tol)
    //ubp_tol >= aux_tol >= opt_tol
    //aux_tol < ubp_tol - opt_tol
    double ubp_tol = 0.9 * feas_tol;
    double aux_tol = 0.5 * feas_tol;
    double opt_tol = std::min(0.5 * abs_tol, 1e-2 * feas_tol);


    for(int i = 0; i < max_iter; ++i) {
        opt_tol = std::min(-Psi_hat, opt_tol);
        scoped_push sp_it(sub->solver_name, "it_" + std::to_string(i + 1));

        std::cout << "\nIteration " << std::to_string(i + 1) << ":\n";
        std::cout << "UBD     = " << std::to_string(rec.m_ubd) << "\n";
        std::cout << "LBD     = " << std::to_string(rec.m_lbd) << "\n";

        if(check_termination(rec, std::numeric_limits<double>::quiet_NaN(), abs_tol, rel_tol, 0.0)) {
            return rec;
        }
        if(check_aborted(max_time, rec, solver_base_name, aborted)) {
            return rec;
        }

        if(next_problem == LBP) {
            scoped_push sp_lbp(sub->solver_name, "_lbp");
            std::cout << "blp: solving lbp\n";

            sub->set_option("abs_tol", opt_tol);
            sub->set_option("rel_tol", 1e-9);
            sub->set_option("max_time", max_time - rec.m_cpu);
            sub->set_option("settings", settings_lbp);

            lbp_rec = sub->solve(prog.lbp);
            rec.m_cpu += lbp_rec.m_cpu;

            std::cout << "reporting status\n";
            report(lbp_rec);

            if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                return rec;
            }
            switch(lbp_rec.m_status) {
                //case FEASIBLE:
                //    std::cout << "WARNING: lbp not solved globally\n";
                case GLOBAL:
                    rec.m_lbd = lbp_rec.m_lbd;
                    break;
                case INFEASIBLE:
                    rec.m_status = INFEASIBLE;
                    rec.m_lbd = std::numeric_limits<double>::infinity();
                    rec.m_ubd = std::numeric_limits<double>::infinity();
                    return rec;
                default:
                    throw std::invalid_argument("unhandled solver status: " + to_string(lbp_rec.m_status));
            }

            symbols.push_scope();
            fix_variables(lbp_rec, symbols);
            auto a = prog.llp.m_objective.front();
            l_obj_val = util::evaluate_expression(a, symbols);
            symbols.pop_scope();

            next_problem = LLP;
        } else if(next_problem == LLP) {
            scoped_push sp_lbp(sub->solver_name, "_llp");
            std::cout << "blp: solving llp\n";

            symbols.push_scope();
            fix_variables(lbp_rec, symbols, prog.ul_variables);

            sub->set_option("abs_tol", opt_tol);
            sub->set_option("rel_tol", 1e-9);
            sub->set_option("max_time", max_time - rec.m_cpu);
            sub->set_option("settings", settings_llp);

            llp_rec = sub->solve(prog.llp);
            symbols.pop_scope();
            rec.m_cpu += llp_rec.m_cpu;
            std::cout << "reporting status\n";
            report(llp_rec);

            if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                return rec;
            }

            switch(llp_rec.m_status) {
                case GLOBAL:
                    break;
                case INFEASIBLE:
                    throw std::invalid_argument("unexpected infeasible LLP");
                default:
                    throw std::invalid_argument("unhandled solver status: " + to_string(llp_rec.m_status));
            }
            next_problem = AUX;
        } else if(next_problem == AUX) {
            scoped_push sp_lbp(sub->solver_name, "_aux");
            std::cout << "blp: solving aux\n";

            std::cout << "setting aux_target and aux_eps to: " << l_obj_val << " " << aux_tol << '\n';
            set_real(prog.aux_target, llp_rec.m_lbd, symbols);
            set_real(prog.aux_eps, aux_tol, symbols);
            //should test: set_real(prog.aux_eps, llp_rec.m_ubd - llp_rec.m_lbd, symbols);
            set_real(prog.l_obj_val, l_obj_val, symbols);

            symbols.push_scope();
            fix_variables(lbp_rec, symbols, prog.ul_variables);

            sub->set_option("abs_tol", abs_tol_aux);
            sub->set_option("rel_tol", rel_tol_aux);
            sub->set_option("max_time", max_time - rec.m_cpu);
            sub->set_option("settings", settings_aux);

            auto aux_rec = sub->solve(prog.aux);
            symbols.pop_scope();
            rec.m_cpu += aux_rec.m_cpu;

            std::cout << "reporting status\n";
            report(aux_rec);

            if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                return rec;
            }

            switch(aux_rec.m_status) {
                case GLOBAL:
                    if(aux_rec.m_lbd > 0) {
                        throw std::invalid_argument("lbp_aux with negative objective is infeasible. However, feasibility is guaranteed by construction");
                    }
                    if(aux_rec.m_ubd >= 0) {
                        if(aux_rec.m_ubd == 0 && aux_rec.m_lbd == 0) {
                            std::cout << "ubd and lbd of aux is equal 0. check whether there is no constraint p?\n";
                        } else {
                            //ubd of aux is positiv.  reducing the optimality tolerances might help
                            set_tolerance_robust("abs_tol_aux", -aux_rec.m_lbd / 0.5);

                            ;
                        }
                    }
                    break;
                case INFEASIBLE:
                    throw std::invalid_argument("lbp_aux is infeasible. However, feasibility is guaranteed by construction");
                default:
                    throw std::invalid_argument("lbp_aux: unhandled solver status: " + to_string(aux_rec.m_status));
            }
            discretize(prog.disc, aux_rec, symbols);
            Psi_hat = std::max(Psi_hat, aux_rec.m_lbd);
            next_problem = UBP;
        } else if(next_problem == UBP) {
            scoped_push sp_lbp(sub->solver_name, "_ubp");
            std::cout << "blp: solving ubp\n";

            set_real(prog.ubp_target, llp_rec.m_lbd, symbols);
            set_real(prog.ubp_eps, ubp_tol, symbols);
            set_real(prog.current_lbd, rec.m_lbd, symbols);
            symbols.push_scope();
            fix_variables(lbp_rec, symbols, prog.ul_variables);

            sub->set_option("abs_tol", opt_tol);
            sub->set_option("rel_tol", 1e-9);
            sub->set_option("max_time", max_time - rec.m_cpu);
            sub->set_option("settings", settings_ubp);

            auto ubp_rec = sub->solve(prog.ubp);
            symbols.pop_scope();
            rec.m_cpu += ubp_rec.m_cpu;

            std::cout << "reporting status\n";
            report(ubp_rec);

            if(check_aborted(max_time, rec, solver_base_name, aborted)) {
                return rec;
            }

            switch(ubp_rec.m_status) {
                case GLOBAL:
                    break;
                case INFEASIBLE:
                    next_problem = LBP;
                    continue;
                default:
                    throw std::invalid_argument("unhandled solver status: " + to_string(ubp_rec.m_status));
            }
            if(ubp_rec.m_ubd < rec.m_ubd) {
                rec.m_ubd = ubp_rec.m_ubd;
                copy_variables(lbp_rec, rec);
                copy_variables(ubp_rec, rec);
            }
            next_problem = LBP;
        }
#ifdef DIPS_use_mpi
        int _rank;
        MPI_Comm_rank(MPI_COMM_WORLD, &_rank);
        if(_rank == 0) {
#endif
            // write discretization points to output file
            std::string disc_output_path = "./";
            for(auto& i : sub->solver_name) {
                disc_output_path += i;
            }
            std::ofstream disc_output(disc_output_path + "_disc_points.txt");
            std::streambuf* backup = std::cout.rdbuf();
            std::cout.rdbuf(disc_output.rdbuf());
            std::vector<std::vector<discretization>> discs;
            discs.emplace_back(std::vector<discretization>({ prog.disc }));
            report(symbols, discs);
            std::cout.rdbuf(backup); // reassign cout to its former buffer before that buffer no longer exists
            disc_output.close();     // close file output
#ifdef DIPS_use_mpi
        }
        MPI_Barrier(MPI_COMM_WORLD);
#endif
    }

    rec.m_status = EXCEEDED_MAX_ITERATIONS;
    std::cout << solver_base_name << ": total number of iterations exceeded limit."
              << "Rising max_it might help\n";
    return rec;
}

bool blp_no_box_solver::set_option(const std::string& option, double value) {
    if(option == "abs_tol") {
        if(value >= 1e-9) {
            abs_tol = value;
            return true;
        }
    }
    if(option == "rel_tol") {
        if(value >= 1e-9) {
            rel_tol = value;
            return true;
        }
    }
    if(option == "feas_tol") {
        if(value >= 1e-9) {
            feas_tol = value;
            return true;
        }
    }
    if(option == "abs_tol_aux") {
        if(value >= 1e-9) {
            abs_tol_aux = value;
            return true;
        }
    }
    if(option == "rel_tol_aux") {
        if(value >= 1e-9) {
            rel_tol_aux = value;
            return true;
        }
    }
    if(option == "max_time") {
        if(value >= 0) {
            max_time = value;
            return true;
        }
    }
    if(option == "max_iter") {
        if(value >= 1) {
            max_iter = (int)value;
            return true;
        }
    }
    std::cout << "Failed to set option " << option << " = " << value << '\n';
    return false;
}

bool blp_no_box_solver::set_option(const std::string& option, const std::string& value) {
    if(option == "settings_lbp") {
        settings_lbp = value;
        return true;
    }
    if(option == "settings_ubp") {
        settings_ubp = value;
        return true;
    }
    if(option == "settings_llp") {
        settings_llp = value;
        return true;
    }
    if(option == "settings_aux") {
        settings_aux = value;
        return true;
    }
    std::cout << "Failed to set option " << option << " = " << value << '\n';
    return false;
}

double blp_no_box_solver::get_option(const std::string& option) const {
    if(option == "abs_tol") {
        return abs_tol;
    }
    if(option == "rel_tol") {
        return rel_tol;
    }
    if(option == "feas_tol") {
        return feas_tol;
    }
    if(option == "max_time") {
        return max_time;
    }
    throw std::invalid_argument("Option '" + option + "' not supported or implemented.");
}

void blp_no_box_solver::pass_option(const std::string& option, double value) {
    options.emplace_back(option, value);
}

void blp_no_box_solver::set_solver_base_name(std::string name) {
    solver_base_name = std::move(name);
}

std::string blp_no_box_solver::get_solver_base_name() {
    return solver_base_name;
}

} // namespace dips
