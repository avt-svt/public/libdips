#include "gams_parser_unflattened.hpp"

namespace dips {

void gams_parser_unflattened::parse_gams(std::stringstream& input) {

    // define variables and parameters
    symbol_stringer_unflattened sym_str(input, symbols);
    auto defined_symbols = find_variables(prog, symbols);
    for(auto it = defined_symbols.begin(); it != defined_symbols.end(); ++it) {
        base_symbol* sym = symbols.resolve(*it);
        if(sym) {
            sym_str.dispatch(sym);
        }
    }

    auto fixed_symbols = find_parameters(prog, symbols);
    for(auto it = fixed_symbols.begin(); it != fixed_symbols.end(); ++it) {
        auto sym = symbols.resolve(*it);
        if(sym) {
            sym_str.dispatch(sym);
        }
    }

    // write equations
    expression_stringer_unflattened expr_str(symbols);
    function_stringer func_str(symbols);

    auto function_symbols = find_functions(prog, symbols);
    auto it_functions = function_symbols.begin();
    auto it_objective = prog.m_objective.begin();
    auto it_constraints = prog.m_constraints.begin();
    for(int k = 0; k < function_symbols.size() + prog.m_objective.size() + prog.m_constraints.size(); ++k) {

        std::vector<std::string> vec;
        if(k < function_symbols.size()) {
            // parse function definitions/makros
            if(k != 0) {
                // increment functions iterator if not first loop over functions
                ++it_functions;
            }
            vec = func_str.dispatch(symbols.resolve(*it_functions)); //returns: vector; first entr: function macro; last n-1 entries: local sets
        } else if(k < function_symbols.size() + prog.m_objective.size()) {
            // parse objective
            if(k != function_symbols.size()) {
                // increment objectives iterator if not first loop over objectives
                ++it_objective;
                throw std::invalid_argument("only one objective allowed.");
            }
            vec = expr_str.dispatch(*it_objective); //returns: vector; first entr: objective equation; last n-1 entries: local sets
        } else {
            // parse constraints
            if(k != function_symbols.size() + prog.m_objective.size()) {
                // increment iterator if not first loop over constraints
                ++it_constraints;
            }
            vec = expr_str.dispatch(*it_constraints); //returns: vector; first entr: constraint equation; last n-1 entries: local sets
        }

        if(vec.front().find("NaN") != std::string::npos) {
            //not valid entry, e.g. when interating over empty set; skip
            if(k > function_symbols.size() && k < function_symbols.size() + prog.m_objective.size()) {
                throw std::invalid_argument("objective function evaluated wit NaN");
            }
            continue;
        }


        std::vector<std::string> eq_indices;
        std::stringstream ss;
        std::smatch match;

        for(int i = 1; i < vec.size(); i++) { //for each used set
            const std::string set_string = vec.at(i);
            if(set_string.rfind("Set indexoperation_", 0) == 0) { //local sets for index operations, no indices required
                ss << set_string;
            } else if(set_string.rfind("Set realset", 0) == 0) { // if set was local in ale and had to be created in gams; multi-dimensional real sets
                ss << set_string;
                std::regex_search(set_string.begin(), set_string.end(), match, std::regex(" realset.*_index")); // get identifier of set
                eq_indices.push_back(match[0].str());
            } else if(set_string.rfind("Set ", 0) == 0) { // if set was local in ale and had to be created in gams; one-dimensional sets
                ss << set_string;
                std::regex_search(set_string.begin(), set_string.end(), match, std::regex(" .*?\\{\\}")); // get identifier of set
                eq_indices.push_back(match[0].str());
            } else if(set_string.rfind("realset", 0) == 0) { // if set is pre-defined;  real sets
                eq_indices.push_back(vec.at(i) + "_index");  //add identifier of index set to list of indices of indexed gams equation
            } else {                                         // if set is pre-defined; one-dimensional index sets
                eq_indices.push_back(set_string);            //add identifier to list of indices of indexed gams equation
            }
        }


        if(k < function_symbols.size()) {
            // parse function definitions/makros
            ss << '\n'
               << vec.front() << "\n";
        } else if(k < function_symbols.size() + prog.m_objective.size()) {
            // parse objective
            ss << ";\nvariable obj_gams_internal; equation eqobj_gams_internal; eqobj_gams_internal .. obj_gams_internal =e= " << vec.front() << ";\n";
        } else {
            // parse constraints
            if(!is_forallnode((*it_constraints).get())) { //not an indexed equation, base format of equation, insert constraint
                ss << fmt::format("\nequation eqcon{}; eqcon{}.. {};\n", k + 1, k + 1, vec.front());
            } else { //base format of indexed equation, insert indices and actual constraint
                ss << fmt::format("\nequation eqcon{}; eqcon{}(", k + 1, k + 1) << fmt::format("{}", fmt::join(eq_indices, ",")) << fmt::format(").. {};\n", vec.front());
            }
        }


        //insert missing equation indices in constraint
        std::string raw_input = ss.str();
        fmt::dynamic_format_arg_store<fmt::format_context> store;
        size_t nPos = raw_input.find("{}", 0);
        while(nPos != std::string::npos) {
            store.push_back(k + 1);
            nPos = raw_input.find("{}", nPos + 1);
        }
        input << fmt::vformat(raw_input, store);
    }

    parse_gams_solve_statement(input);
}

// moved for unix compatibility
template <typename TType, unsigned IDim>
std::string set_stringer::operator()(constant_node<set<TType, IDim>>* node) {
    auto elements = expr_str->dispatch(node);
    return dispatch(elements);
}

// moved for unix compatibility
template <unsigned IDim>
std::vector<std::string> expression_stringer_unflattened::operator()(entry_node<real<IDim>>* node) {
    auto ident_res = getIdentifier(node, 1);
    auto identifier = std::get<0>(ident_res); //get identifier and dim of tensor accessed in node

    //symbol_sizer sizer;
    std::string identifier_copy = identifier;
    std::size_t pos = identifier_copy.find("realset_");
    if(pos != std::string::npos) {
        identifier_copy.erase(pos, 8);
    }
    pos = identifier_copy.find("."); // delete all attribute attachements, i.e., ".l", ".lo", ".up", "init", "prio"
    if(pos != std::string::npos) {
        identifier_copy.erase(pos, identifier_copy.size() - pos);
    }

    auto sizes = get_parameter_shape(identifier_copy, m_symbols);

    index_stringer ind_str(this, m_symbols, sizes, indexoperation_n, std::get<1>(ident_res));
    std::tuple<std::vector<std::string>, std::vector<std::string>, std::vector<std::vector<std::string>>, std::vector<std::string>> indices;
    try {
        indices = ind_str.dispatch(node);
    } catch(util::wrongDimensionException& e) {
        throw util::wrongDimensionException("Dimension access violation in tensor \"" + identifier + "\": " + e.what());
    }

    if(identifier.rfind("realset", 0) == 0) { //add additional index if entry_node is accessing real tensor; get symbol by different name (remove realset_ flag)
        std::get<0>(indices).emplace(std::get<0>(indices).begin(), identifier + "_index");
    }
    if(std::get<1>(indices).size() == 0) { //if there is not indexoperation in the entry_node: parse normally
        return { fmt::format("({}[{}])", identifier, fmt::join(std::get<0>(indices), ", ")) };
    } //otherwise parse variant with indexoperations
    indexoperation_n++;
    std::vector<std::string> map_string;
    auto it1 = std::get<1>(indices).begin();
    auto it2 = std::get<2>(indices).begin();
    for(; it1 != std::get<1>(indices).end(); ++it1, ++it2) {
        map_string.emplace_back(fmt::format("{}({})", *it1, fmt::join(*it2, ",")));
    }
    std::vector<std::string> res = { fmt::format("sum(({}), ({}[{}]))", fmt::join(map_string, ", "), identifier, fmt::join(std::get<0>(indices), ", ")) };
    res.insert(res.end(), std::get<3>(indices).begin(), std::get<3>(indices).end());
    return res;
}


} // namespace dips