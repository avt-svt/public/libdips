#Example use:
#iwyu_tool.py -j 15 -p . ../src/* ../inc/* -- -Xiwyu   --cxx17ns  -Xiwyu  --update_comments -Xiwyu --max_line_length=160 -Xiwyu --no_fwd_decls 1> includes4.txt 2> includes5.txt
#fix_includes.py  --update_comments --noreorder --nokeep_iwyu_namespace_format < ./includes4.txt


#rst:
# include-what-you-use (iwyu)
# ----------------------------
#
# Allows to run the static code analyzer `include-what-you-use (iwyu)
# <http://include-what-you-use.org>`_ as a custom target with the build system
# `CMake <http://cmake.org>`_.
#
# .. topic:: Dependencies
#
#  This module requires the following *CMake* modules:
#
#  * ``FindPythonInterp``
#
# .. topic:: Contributors
#
#  * Florian Wolters <wolters.fl@gmail.com>

#===============================================================================
# Copyright 2015 Florian Wolters
#
# Distributed under the Boost Software License, Version 1.0. (See accompanying
# file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
#===============================================================================

# ------------------------------------------------------------------------------
# Include guard for this file.
# ------------------------------------------------------------------------------

if(iwyu_included)
  return()
endif()

set(iwyu_included TRUE)

option(BUILD_IWYU
       "Run the include-what-you-use static analyzer on the source code of the project."
       TRUE)

function(iwyu_enable)
  set(iwyu_EXECUTABLE_NAME include-what-you-use)
  find_program(iwyu_EXECUTABLE ${iwyu_EXECUTABLE_NAME} )

  if(iwyu_EXECUTABLE)
    # This is not exactly the same behavior as with CMake v3.3, since here all
    # compiled targets are analyzed.
    set(iwyu_tool_EXECUTABLE_NAME iwyu_tool.py)

	find_package (Python3 COMPONENTS Interpreter)
    find_file(iwyu_tool_EXECUTABLE ${iwyu_tool_EXECUTABLE_NAME} HINTS ${CMAKE_CURRENT_SOURCE_DIR}/cmake
)

    if(Python3_Interpreter_FOUND AND iwyu_tool_EXECUTABLE)
      set(CMAKE_EXPORT_COMPILE_COMMANDS ON PARENT_SCOPE)
      message(STATUS "IWYU OK")  
      add_custom_target(iwyu
                        COMMAND "${Python3_EXECUTABLE}" "${iwyu_tool_EXECUTABLE}" -j 2 -p "${CMAKE_BINARY_DIR}" "${DIPS_src}" -- -Xiwyu   --cxx17ns  -Xiwyu  --update_comments -Xiwyu --max_line_length=160 > includes.txt
                        COMMENT "Running the ${iwyu_tool_EXECUTABLE_NAME} compilation database driver"
                        VERBATIM
                        COMMAND_EXPAND_LISTS
                        )
    else()
    	if(Python3_Interpreter_FOUND)
    	      message(STATUS
              "Unable to find the ${iwyu_tool_EXECUTABLE_NAME} script")
    	else()
    	      message(STATUS
              "Unable to find the Python interpreter")
    	endif()

    endif()
  else()
    message(STATUS "Unable to find the ${iwyu_EXECUTABLE_NAME} executable")
  endif()
endfunction()

