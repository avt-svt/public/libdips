<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://git.rwth-aachen.de/avt-svt/public/libdips">
    <img src="images/logo.svg" alt="Logo" width="256" height="256">
  </a>

  <h3 align="center">libDIPS</h3>

  <p align="center">
    Discretization-Based Semi-Infinite and bilevel Programming Solvers
    <br />
    <br />
    <a href=https://git.rwth-aachen.de/avt-svt/public/libdips/issues>Report Bug</a>
    ·
    <a href=https://git.rwth-aachen.de/avt-svt/public/libdips/issues>Request Feature</a>
    ·
    <a href=https://git.rwth-aachen.de/avt-svt/public/libdips/issues>Propose Problem Instance</a>
  </p>
</div>

<div align="center">

![C++](https://img.shields.io/badge/C%2B%2B-17-blue?style=for-the-badge&logo=cplusplus)
![CMake](https://img.shields.io/badge/cmake-3.15-%23064F8C.svg?&style=for-the-badge&logo=cmake&logoColor=white)
![Platforms](https://img.shields.io/badge/platforms-linux--64%20%7C%20win--64-lightgrey?style=for-the-badge)
[![License](https://img.shields.io/badge/LICENSE-EPL%202.0-green.svg?&style=for-the-badge)](https://opensource.org/licenses/EPL-2.0)
</div>




<!--
![Windows](https://img.shields.io/badge/windows-%230078D6.svg?&style=for-the-badge&logo=windows&logoColor=white)
![Linux](https://img.shields.io/badge/linux-%230078D6.svg?&style=for-the-badge&logo=linux&logoColor=white)
-->

## About

libDIPS – Discretization-Based Semi-Infinite and bilevel Programming Solvers is a C++ library that builds on [libALE](https://git.rwth-aachen.de/avt-svt/public/libale/) and implements multiple discretization-based solvers for hierarchical programs ([libALE](https://git.rwth-aachen.de/avt-svt/public/libale/) is C++ library, which provides a data structure for the representation and manipulation of algebraic and logical expressions and an intuitive domain-specific language for user input).
Currently, libDIPS provides solvers for (generalized) semi-infinite programs ([(G)SIPs](#program-classes)), minmax programs ([MINMAX](#program-classes)), bilevel programs ([BLPs](#program-classes)), and existence-constrained semi-infinite programs ([ESIPs](#program-classes)).

## License
The code of libDIPS is licensed under the Eclipse Public License v2.0 (EPL 2.0).
libDIPS uses several third-party libraries by various authors.
Note that some of these do not allow using libDIPS in conjunction with these libraries in commercial software.
Refer to the [LICENSE file](./LICENSE) for details.

## Obtaining libDIPS

To access the full functionality of libDIPS, including text-based parser, MPI parallelization (only in combination with MAiNGO), and all supported subsolvers, please clone the source code from [this](https://git.rwth-aachen.de/avt-svt/public/libdips) repository.
The libDIPS repository uses submodules for all dependencies.
To ensure that the submodules are correctly initialized, please switch to the correct branch before initializing the submodules.
Hence, to obtain libDIPS, you must run the following commands (on Windows, you might need to install Git Bash first; on Linux, we recommend cloning via the [SSH link](git@git.rwth-aachen.de:avt-svt/public/libdips.git)).

```
$ git clone https://git.rwth-aachen.de/avt-svt/public/libdips.git <directory>
$ cd <directory>
$ git checkout <branch>
$ git submodule init
$ git submodule update -j 1
```


To verify that the above has worked, you can check that each subfolder in the `dep` folder is non-empty.

If you have already cloned the libDIPS repository and want to update to the latest version, execute the following git commands.

```
$ git pull
$ git submodule update -j 1
```

## Generating and Compiling the Project

libDIPS uses CMake to generate the required Visual Studio project (for Windows) or Makefile (for Linux or macOS).
You can select the desired solver by changing the CMake variable `DIPS_preferred_<solver>` accordingly (see below for details).
Please ensure that the desired solver `DIPS_enable_<solver>` is enabled.
The following subsolvers are available:

- cplex
- GAMS
- gurobi
- highs
- ipopt
- maingo (with optional MPI parallelization)

Additionally, the subsolver options can be changed, e.g., for GAMS, the GAMS version desired.
Furthermore, there are several demos to choose from, c.f., the summary table in [Modeling in libDIPS](#modeling-in-libdips).

### Windows

1. Start CMake, navigate, or type the path to your libDIPS directory and select your `build` directory.
2. In Cmake, click `Configure`, then choose Visual Studio 15 2017 Win64 as the generator (or Visual Studio 15 2017 as the generator and x64 as an optional platform for the generator in the newer versions of CMake; analogous for Visual Studio 16 2019).
Select the default native compilers.
Press `Finish` and wait for the configuration to complete.
Everything worked fine if you got the `Configuring done` message at the end.
3. If desired, you can now change the values of the CMake variables explained above.
We recommend changing only those CMake variables prefixed with `DIPS_` as a starting point.
4. Press `Generate`.
You should get a message saying, `Generating done`.
5. In Visual Studio, click the `Open Project` button (or open the `DIPS.sln` file (with Visual Studio 2017 or 2019) created by CMake in the `build` directory you specified).
Make sure to set the build type to `Release` since this will result in significant performance improvements.
6. Compile libDIPS by clicking `Build` -> `Build solution`.
Depending on which CMake variables you have enabled, c.f., above, this will create executables with the names of the demos selected during the configuration step.
The executables are created in the `Release` directory within your `build` directory.
The `*.exe` expects text-based input in ALE Syntax.
7. To debug libDIPS in Visual Studio, you must first set the desired demo as the startup project.
This is done by right-clicking on the demo within Visual Studio's project explorer and setting it as the `Startup Project`.
Then, build the project as `Debug`.

### Linux and macOS

First, create a build directory in the libDIPS directory and then navigate to it.
```
$ mkdir build
$ cd build
```
Then run CMake using the `CMakeLists.txt` from the libDIPS directory, using
```
$ cmake ..
```
After the CMake command, you can change the CMake variables explained above by adding `-D<name_of_cmake_variable>=<value>`.
To compile the code, execute
```
$ make
```
With the option `-j n`, make will compile the project using n cores, e.g., execute `make -j 4` to compile using four cores.

## Executing an Example

To execute a solver demo, open a terminal window in the `build` directory where the demo executable is located.
On Windows, and in case the steps above have been followed, the demo executable is located in the `Release` or `Debug` directory (depending on the `build' configuration in Visual Studio).
The Command Prompt or Power Shell can be opened using `Shift + Right-click` in the appropriate directory.
The demo executable, e.g., the sip_bnf demo, is then executed by
```
$ sip_bnf_demo.exe
```
Note that if no additional arguments are given, the input files in the default folder are used, and the output is written into the default output folder, e.g., for the sip_bnf demo, the default input and output folders are `sip_input` and `sip_output_bnf`, respectively.
Custom input and output paths can be set using the command
```
$ sip_bnf_demo.exe <input directory> <output directory>
```
The table for the demos' overview can be found in the next section (under [Modeling in libDIPS](#modeling-in-libdips)).

## Comparing to libDIPS & Contributing New Problem Instances

On the secondary benchmark branch, we supply a version of the code that contains extensive logging functionality and an application called `benchmark` which can be used to run libDIPS on a series of problems form our `library of problem instances` and collect the results in a .json file.
To display information on how to configure the `benchmark` application to, e.g., run the implemented appropriate solvers for a user-defined list of problem instances, use the help command
```
$ benchmark.exe --help
```

We further supply an extensive problem collection.
We welcome contributions to this problem collection.
If you want to contribute a new problem instance, use the appropriate template under `library_of_test_problems/template/`.
To submit the new problem instance open a issue to [propose a new problem instance](https://git.rwth-aachen.de/avt-svt/public/libdips/issues) or  create a new merge request to the benchmark branch using the supplied description template.
When creating a merge request it will be displayed under `Description`>`Choose a template` when creating the merge request), otherwise you can find it [here](./.gitlab/merge_request_templates/problem_instance.md).

## Modeling in libDIPS

All implemented discretization-based solvers have in common that they iteratively solve subproblems.
Different [subproblems](#subproblems-of-the-solvers) are required depending on the solver used.
For example, the sip_bnf demo requires a lower-bounding problem and a lower-level problem.
These subproblems are implemented in the `lbp.txt` and `llp.txt`, respectively.
The subproblems are connected by a [definition file](#the-definition-file-deftxt) `def.txt`.

### Subproblems of the solvers

All subproblems are written in the domain-specific language ALE.
Please refer to the MAiNGO [manual](https://avt-svt.pages.rwth-aachen.de/public/maingo/writing_problem.html#modeling_ALE) for an introduction to ALE.
For a detailed description of the ALE modeling language, refer to the README.md in the git repository of [libALE](https://git.rwth-aachen.de/avt-svt/public/libale.git).
Detailed examples of the ALE syntax can also be found at `dep\libale\demo\input_files\input.txt`.

The following table shows the implemented solvers and their `demo executables` with the corresponding subproblem and settings files.
For (G)SIP solvers, the subproblem formulations are listed below the table.
For MINMAX, ESIP, and BLP  solvers, please refer to the cited literature or the demo input files located at `demo\examples\` for the subproblem formulations.

| Program class | *Solver name* and <br> `demo executable` | Original Publication | Description | Input File Names | Subproblem Link / Name in Literature | Settings File Names<sup>1</sup> |
|-|-|-|-|-|-|-|
| [SIP](#program-classes)   | *B&F*             <br> `sip_bnf_demo`          | [Blankenship & Falk 1976](https://link.springer.com/article/10.1007/BF00934096)           | lower-bounding scheme; **no** guarantee of feasible point at termination                                                                             | `def.txt` <br> `lbp.txt` <br> `llp.txt`<sup>5</sup>                                                                                                                                       | - <br> [LBD](#subproblems-for-gsip-solvers) <br> [LLP](#subproblems-for-gsip-solvers)                                                                                       | - <br> `settings_lbp.txt` <br> `settings_llp.txt` |
| [SIP](#program-classes)   | *RRHS*            <br> `sip_rrhs_demo`         | [Mitsos 2011](https://www.tandfonline.com/doi/abs/10.1080/02331934.2010.527970)           | lower-bounding scheme similar to *B&F*; upper-bounding scheme through restriction of the right-hand side; guarantee of feasible point at termination | `def.txt` <br> `lbp.txt` <br> `ubp.txt` <br> `llp.txt`<sup>5</sup>                                                                                                                        | - <br> [LBD](#subproblems-for-gsip-solvers) <br> [UBD](#subproblems-for-gsip-solvers) <br> [LLP](#subproblems-for-gsip-solvers)                                             | - <br> `settings_lbp.txt` <br> `settings_ubp.txt` <br> `settings_llp.txt`                       |
| [SIP](#program-classes)   | *Oracle*          <br> `sip_oracle_demo`       | [Tsoukalas & Rustem 2010](https://link.springer.com/article/10.1007/s11590-010-0236-4)    | binary search in the objective space through an oracle approach                                                                                      | `def.txt` <br> `lbp.txt` <br> `ora.txt` <br> `llp.txt`<sup>5</sup>                                                                                                                        | - <br> [LBD](#subproblems-for-gsip-solvers) <br> [ORA](#subproblems-for-gsip-solvers) <br> [LLP](#subproblems-for-gsip-solvers)                                             | - <br> `settings_lbp.txt` <br> `settings_ora.txt` <br> `settings_llp.txt` |
| [SIP](#program-classes)   | *Hybrid*          <br> `sip_hybrid_demo`       | [Djelassi & Mitsos 2016](https://link.springer.com/article/10.1007/s10898-016-0476-7)     | combination of *RRHS* and *Oracle* to achieve strong convergence guarantees and optimal updates of the restriction parameter                         | `def.txt` <br> `lbp.txt` <br> `ubp.txt` <br> `res.txt` <br> `llp.txt`<sup>5</sup>                                                                                                         | - <br> [LBD](#subproblems-for-gsip-solvers) <br> [UBD](#subproblems-for-gsip-solvers) <br> [RES](#subproblems-for-gsip-solvers) <br> [LLP](#subproblems-for-gsip-solvers)   | - <br> `settings_lbp.txt` <br> `settings_ubp.txt` <br> `settings_res.txt` <br> `settings_llp.txt`|
| [MINMAX](#program-classes)| *Minmax*          <br> `minmax_bnf_demo`       | [Falk & Hoffman 1977]( https://doi.org/10.1002/nav.3800240307)                            | adapted *B&F* which utilizes minmax problem structure                                                                                                | `def.txt` <br> `lbp.txt` <br> `llp.txt`                                                                                                                                                   | - <br> LBD <br> LLP                                                                                                               | - <br> `settings_lbp.txt` <br> `settings_llp.txt` |
| [GSIP](#program-classes)  | *GSIP-RRHS*       <br> `gsip_rrhs_demo`        | [Mitsos & Tsoukalas 2014](https://link.springer.com/article/10.1007/s10898-014-0146-6)    | uses similar ideas as *RRHS* but utilizes GSIP program structure                                                                                     | `def.txt` <br> `lbp.txt` <br> `ubp.txt` <br> `llp.txt` <br> `llp_aux.txt`                                                                                                                 | - <br> [GSIP-LBD](#subproblems-for-gsip-solvers) <br> [GSIP-UBD](#subproblems-for-gsip-solvers) <br> [GSIP-LLP](#subproblems-for-gsip-solvers)  <br> [GSIP-AUX](#subproblems-for-gsip-solvers) | - <br> `settings_lbp.txt` <br> `settings_ubp.txt` <br> `settings_llp.txt` <br> `settings_aux.txt` |
| [ESIP](#program-classes)  | *B&F*             <br> `esip_bnf_demo`         | [Djelassi & Mitsos 2021](https://doi.org/10.1007/s10957-021-01813-2)                      | similar to *B&F* which solves a maxmin problem for lower level                                                                                       | `def.txt` <br> `lbp.txt` <br> `mlp.txt` <br> `llp.txt`                                                                                                                                    | - <br> (LBP) <br> (MLP)-ULP<sup>4</sup><br> (LLP)                                                                                   | - <br> `settings_ulp.txt` <br> `settings_minmax_ulp.txt` <br> `settings_minmax_llp.txt`|
| [ESIP](#program-classes)  | *Hybrid*          <br> `esip_hybrid_demo`      | [Djelassi & Mitsos 2021](https://doi.org/10.1007/s10957-021-01813-2)                      | similar to *Hybrid* but solves a minmax problem for the lower level                                                                                  | `def.txt` <br> `lbp.txt` <br> `ubp.txt`<br> `res.txt` <br> `mlp.txt` <br> `llp.txt`                                                                                                       | - <br> (LBP) <br> (UBP) <br> (RES)<br> (MLP)-ULP<sup>4</sup><br> (LLP)                                                              | - <br> `settings_ulp.txt` <br> `settings_ulp.txt` <br> `settings_res.txt` <br> `settings_minmax_ulp.txt` <br> `settings_minmax_llp.txt`|
| [BLP](#program-classes)   | *BLP-Box*         <br> `blp_box_demo`          | [Mitsos et int. Barton 2008](https://link.springer.com/article/10.1007/s10898-007-9260-z) | uses probing for upper-bounding; discretization and exclusion using boxes                                                                            | `def.txt` <br> `skl.txt`<sup>2</sup><br> `box\h_max.txt`<sup>3</sup><br> `box\lbp.txt` <br> `box\ubp.txt` <br> `box\llp.txt` <br> `box\llp_aux.txt` <br> `box\llp_aux_v.txt`<sup>3</sup>  | - <br> - <br> - <br> (8) <br> (11) <br> inner problem (9) <br> (10) <br> -                                                        | `settings.txt` <br>(used for all subproblems) |
| [BLP](#program-classes)   | *BLP-noBox*       <br> `blp_no_box_demo`       | [Djelassi et int. Mitsos 2019](https://doi.org/10.1007/s10898-019-00764-3)                | uses probing for upper-bounding; discretization similar to *B&F*                                                                                     | `def.txt` <br> `skl.txt`<sup>2</sup><br> `no_box\lbp.txt` <br> `no_box\ubp.txt` <br> `no_box\llp.txt` <br> `no_box\llp_aux.txt`                                                           | - <br> - <br> BLP-LBD <br> BLP-UPD <br> BLP-LLP <br> BLP-AUX                                                                      | `settings.txt` <br>(used for all subproblems) |
| [BLP](#program-classes)   | *BLP-AdaptOracle* <br> `blp_adapt_oracle_demo` | [Jungen & Mitsos 2024](#references)                                                      | adapted *Oracle* for bilevel problems; improved oracle to generate better upper bounds                                                               | `def.txt` <br> `skl.txt`<sup>2</sup><br> `adapt_oracle\lbp.txt` <br> `adapt_oracle\ora.txt` <br> `adapt_oracle\llp.txt` <br> `adapt_oracle\ora_aux.txt`                  | - <br> - <br> (LBP) <br> (UBP) <br> (ORA) <br> (LLP) <br> (AUX)                                                                   | `settings.txt` <br>(used for all subproblems) |

>Note:<br>
<sup>1</sup> Settings file names for subsolver MAiNGO; for other subsolvers, the naming conventions might be different, e.g., for using GAMS with BARON, the settings file names are `baron.opt`, `baron.op1`, ... <br>
<sup>2</sup> The `blp_box_demo`, `blp_no_box_demo`, and `blp_adapt_oracle_demo` use a different folder hierarchy. The solver-specific subproblems are located in their respective subfolders, indicted by `box\`, `no_box\`, and `adapt_oracle\`, respectively. The `skl.txt` includes all function and variable definitions for the BLP solvers.<br>
<sup>3</sup> To obtain an upper bound on the inner-objective, `h_max.txt` is solved once. The problem formulated in `llp_aux_v.txt` is solved to check if a feasible box has been computed. In the original publication, interval analysis is used instead.<br>
<sup>4</sup> The original publication gave (MLP) as a maxmin problem. (MLP)-ULP is the upper-level problem of this maxmin problem, similar to LBD in minmax-B&F.<br>
<sup>5</sup> Solver supports multiple llps. For a basic example of `sip_bnf_demo`, see `demo\examples\sip_input_multiple_llps`. Naming scheme: `llp1.txt`, `llp2.txt`, ...<br>

#### Program Classes
- ##### <a name="SIP"></a> **SIP**
$$
\newcommand{\mpmin}[1]{\underset{#1}{\text{min}}\,}
\newcommand{\mb}[1]{\boldsymbol{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mpst}{\text{s.t.}\,}
\newcommand{\mpeq}{{}=}
\newcommand{\mple}{{}\le}
\newcommand{\hostset}[1]{\left[\mb{#1}^{lb},\mb{#1}^{ub}\right]\subsetneq\R^{n_#1}}
\newcommand{\feassetX}{\mc{X}\coloneqq\left\{\mb{x}\in\hostset{x}:\mb{v}^{iu}\left(\mb{x}\right)\mple\mb{0},\ \mb{v}^{eu}\left(\mb{x}\right)\mpeq\mb{0}\right\}}
\newcommand{\feassetY}{\mc{Y}\coloneqq\left\{\mb{y}\in\hostset{y}:\mb{v}^{il}\left(\mb{y}\right)\mple\mb{0},\ \mb{v}^{el}\left(\mb{y}\right)\mpeq\mb{0}\right\}}
\begin{equation}
    \tag{SIP}
    \begin{array}{rlrlrr}
        \mpmin{\mb{x} \in \mc{X}} & f\left(\mb{x}\right) \\
        \mpst   & \mb{g}^{u}\left(\mb{x},\mb{y}\right) \mple \mb{0}, \ \forall \ \mb{y} \in \mc{Y} \\
                & \feassetX\\
                & \feassetY
    \end{array}
\end{equation}
$$

- ##### <a name="MINMAX"></a> **MINMAX**
$$
\newcommand{\mpmin}[1]{\underset{#1}{\text{min}}\,}
\newcommand{\mpmax}[1]{\underset{#1}{\text{max}}\,}
\newcommand{\mb}[1]{\boldsymbol{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mpst}{\text{s.t.}\,}
\newcommand{\mpeq}{{}=}
\newcommand{\mple}{{}\le}
\newcommand{\hostset}[1]{\left[\mb{#1}^{lb},\mb{#1}^{ub}\right]\subsetneq\R^{n_#1}}
\newcommand{\feassetX}{\mc{X}\coloneqq\left\{\mb{x}\in\hostset{x}:\mb{v}^{iu}\left(\mb{x}\right)\mple\mb{0},\ \mb{v}^{eu}\left(\mb{x}\right)\mpeq\mb{0}\right\}}
\newcommand{\feassetY}{\mc{Y}\coloneqq\left\{\mb{y}\in\hostset{y}:\mb{v}^{il}\left(\mb{y}\right)\mple\mb{0},\ \mb{v}^{el}\left(\mb{y}\right)\mpeq\mb{0}\right\}}
\begin{equation}
    \tag{MINMAX}
    \begin{array}{rlrlrr}
        \mpmin{\mb{x} \in \mc{X}} & \mpmax{y \in \mc{Y}} f\left(\mb{x},\mb{y}\right) \\
        \mpst   & \feassetX\\
                & \feassetY
    \end{array}
\end{equation}
$$

- ##### <a name="GSIP"></a> **GSIP**
$$
\newcommand{\mpmin}[1]{\underset{#1}{\text{min}}\,}
\newcommand{\mb}[1]{\boldsymbol{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mpst}{\text{s.t.}\,}
\newcommand{\mpeq}{{}=}
\newcommand{\mple}{{}\le}
\newcommand{\hostset}[1]{\left[\mb{#1}^{lb},\mb{#1}^{ub}\right]\subsetneq\R^{n_#1}}
\newcommand{\feassetX}{\mc{X}\coloneqq\left\{\mb{x}\in\hostset{x}:\mb{v}^{iu}\left(\mb{x}\right)\mple\mb{0},\ \mb{v}^{eu}\left(\mb{x}\right)\mpeq\mb{0}\right\}}
\newcommand{\feassetYX}[1]{\mc{Y}\left(\mb{x}\right)\coloneqq\left\{\mb{#1}\in\hostset{#1}:\mb{g}^{l}\left(\mb{x},\mb{#1}\right)\mple\mb{0},\ \mb{v}^{il}\left(\mb{#1}\right)\mple\mb{0},\ \mb{v}^{el}\left(\mb{#1}\right)\mpeq\mb{0}\right\}}
\begin{equation}
  \tag{GSIP}
  \begin{array}{rlrlrr}
    \mpmin{\mb{x}\in\mc{X}}&f\left(\mb{x}\right)\\
    \mpst&\mb{g}^{u}(\mb{x},\mb{y})\mple\mb{0},\ \forall\ \mb{y}\in\mc{Y}\left(\mb{x}\right)\\
        &\feassetX\\
        &\feassetYX{y}
  \end{array}
\end{equation}
$$

- ##### <a name="ESIP"></a> **ESIP**
$$
\newcommand{\mpmin}[1]{\underset{#1}{\text{min}}\,}
\newcommand{\mb}[1]{\boldsymbol{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mpst}{\text{s.t.}\,}
\newcommand{\mpeq}{{}=}
\newcommand{\mple}{{}\le}
\newcommand{\hostset}[1]{\left[\mb{#1}^{lb},\mb{#1}^{ub}\right]\subsetneq\R^{n_#1}}
\begin{equation}
\tag{ESIP}
\begin{array}{rlrlrr}
\mpmin{\mb{x}\in\mc{X}}&f\left(\mb{x}\right)\\
    \mpst&\forall\ \mb{y}\in\mc{Y}\left[\exists\mb{z}\in\mc{Z}:\mb{g}^{u}\left(\mb{x},\mb{y},\mb{z}\right)\mple\mb{0}\right]\\
    &\mc{X}\coloneqq\left\{\mb{x}\in\hostset{x}:\mb{v}^{iu}\left(\mb{x}\right)\mple\mb{0},\ \mb{v}^{eu}\left(\mb{x}\right)\mpeq\mb{0}\right\}\\
    &\mc{Y}\coloneqq\left\{\mb{y}\in\hostset{y}:\mb{v}^{il}\left(\mb{y}\right)\mple\mb{0},\ \mb{v}^{el}\left(\mb{y}\right)\mpeq\mb{0}\right\}\\
    &\mc{Z}\coloneqq\left\{\mb{z}\in\hostset{z}:\mb{v}^{ie}\left(\mb{z}\right)\mple\mb{0},\ \mb{v}^{ee}\left(\mb{z}\right)\mpeq\mb{0}\right\}
\end{array}
\end{equation}
$$
- ##### <a name="BLP"></a> **BLP**
$$
\newcommand{\mpmin}[1]{\underset{#1}{\text{min}}\,}
\newcommand{\mparg}{\text{arg}\,}
\newcommand{\mb}[1]{\boldsymbol{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mpst}{\text{s.t.}\,}
\newcommand{\mpeq}{{}=}
\newcommand{\mple}{{}\le}
\newcommand{\hostset}[1]{\left[\mb{#1}^{lb},\mb{#1}^{ub}\right]\subsetneq\R^{n_#1}}
\newcommand{\feassetYX}[1]{\mc{Y}\left(\mb{x}\right)\coloneqq\left\{\mb{#1}\in\hostset{#1}:\mb{g}^{l}\left(\mb{x},\mb{#1}\right)\mple\mb{0},\ \mb{v}^{il}\left(\mb{#1}\right)\mple\mb{0},\ \mb{v}^{el}\left(\mb{#1}\right)\mpeq\mb{0}\right\}}
\begin{equation}
    \tag{BLP}
    \begin{array}{rl}
        \mpmin{\mb{x}\in \mc{X}, \mb{y}\in \mc{Y}} & f\left(\mb{x},\mb{y}\right) \\
        \mpst & \mb{g}^{u}\left(\mb{x},\mb{y}\right) \mple \mb{0} \\
              & \mb{y} \in \mparg \mpmin{\mb{z}\in \mc{Y}\left(\mb{x}\right)} h\left(\mb{x},\mb{z}\right) \\
              & \feassetYX{y}
    \end{array}
\end{equation}
$$

#### Subproblems for (G)SIP  solvers

- ##### <a name="lbd"></a> **LBD** - lower-bounding problem
$$
\newcommand{\mpmin}[1]{\underset{#1}{\text{min}}\,}
\newcommand{\mb}[1]{\boldsymbol{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mpst}{\text{s.t.}\,}
\newcommand{\mpeq}{{}=}
\newcommand{\mple}{{}\le}
\newcommand{\hostset}[1]{\left[\mb{#1}^{lb},\mb{#1}^{ub}\right]\subsetneq\R^{n_#1}}
\newcommand{\feassetX}{\mc{X}\coloneqq\left\{\mb{x}\in\hostset{x}:\mb{v}^{iu}\left(\mb{x}\right)\mple\mb{0},\ \mb{v}^{eu}\left(\mb{x}\right)\mpeq\mb{0}\right\}}
\begin{equation}
    \tag{LBP}
    \begin{array}{rlrlrr}
        \mpmin{\mb{x} \in \mc{X}} & f\left(\mb{x}\right)\\
        \mpst   & \mb{g}^{u}\left(\mb{x},\mb{y}^{k}\right) \mple \mb{0}, \ \forall \ \mb{y}^{k} \in \mc{Y}^{LBD}\\
                & \feassetX,
    \end{array}
\end{equation}
$$

- ##### <a name="GSIP-LBP"></a> **GSIP-LBP** - lower-bounding problem
$$
\newcommand{\mpmin}[1]{\underset{#1}{\text{min}}\,}
\newcommand{\mb}[1]{\boldsymbol{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mpst}{\text{s.t.}\,}
\newcommand{\mpeq}{{}=}
\newcommand{\mple}{{}\le}
\newcommand{\hostset}[1]{\left[\mb{#1}^{lb},\mb{#1}^{ub}\right]\subsetneq\R^{n_#1}}
\newcommand{\feassetX}{\mc{X}\coloneqq\left\{\mb{x}\in\hostset{x}:\mb{v}^{iu}\left(\mb{x}\right)\mple\mb{0},\ \mb{v}^{eu}\left(\mb{x}\right)\mpeq\mb{0}\right\}}
\begin{equation}
    \tag{GSIP-LBD}
    \begin{array}{rlrlrr}
        \mpmin{\mb{x} \in \mc{X}} & f\left(\mb{x}\right)\\
        \mpst   & \mpmin{} \left\{g_{i}^{u}\left(\mb{x},\mb{y}^{k}\right), \mpmin{j \in \left\{1 \dots n_{gu}\right\}} -g_{j}^{l}\left(\mb{x},\mb{y}^{k}\right)\right\} \mple 0, \ \forall \ i \in \{1 \dots n_{g^u}\}, \ \forall \ \mb{y}^{k} \in \mc{Y}^{LBD}\\
                & \feassetX,
    \end{array}
\end{equation}
$$

- ##### <a name="ubp"></a> **UBP** - upper-bounding problem
$$
\newcommand{\eps}{\varepsilon}
\newcommand{\mpmin}[1]{\underset{#1}{\text{min}}\,}
\newcommand{\mb}[1]{\boldsymbol{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mpst}{\text{s.t.}\,}
\newcommand{\mpeq}{{}=}
\newcommand{\mple}{{}\le}
\newcommand{\hostset}[1]{\left[\mb{#1}^{lb},\mb{#1}^{ub}\right]\subsetneq\R^{n_#1}}
\newcommand{\feassetX}{\mc{X}\coloneqq\left\{\mb{x}\in\hostset{x}:\mb{v}^{iu}\left(\mb{x}\right)\mple\mb{0},\ \mb{v}^{eu}\left(\mb{x}\right)\mpeq\mb{0}\right\}}
\begin{equation}
    \tag{UBP}
    \begin{array}{rlrlrr}
        \mpmin{\mb{x} \in \mc{X}} & f\left(\mb{x}\right)\\
        \mpst   & \mb{g}^{u}\left(\mb{x},\mb{y}^{k}\right) \mple - \eps^{r} \cdot \mb{1}, \ \forall \ \mb{y}^{k} \in \mc{Y}^{UBD}\\
                & \feassetX.
    \end{array}
\end{equation}
$$

- ##### <a name="GSIP-UBP"></a> **GSIP-UBP** - upper-bounding problem
$$
\newcommand{\eps}{\varepsilon}
\newcommand{\mpmin}[1]{\underset{#1}{\text{min}}\,}
\newcommand{\mb}[1]{\boldsymbol{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mpst}{\text{s.t.}\,}
\newcommand{\mpeq}{{}=}
\newcommand{\mple}{{}\le}
\newcommand{\hostset}[1]{\left[\mb{#1}^{lb},\mb{#1}^{ub}\right]\subsetneq\R^{n_#1}}
\newcommand{\feassetX}{\mc{X}\coloneqq\left\{\mb{x}\in\hostset{x}:\mb{v}^{iu}\left(\mb{x}\right)\mple\mb{0},\ \mb{v}^{eu}\left(\mb{x}\right)\mpeq\mb{0}\right\}}
\begin{equation}
    \tag{GSIP-UBP}
    \begin{array}{rlrlrr}
        \mpmin{\mb{x} \in \mc{X}} & f\left(\mb{x}\right)\\
        \mpst   & \mpmin{} \left\{g_{i}^{u}\left(\mb{x},\mb{y}^{k}\right), \mpmin{j \in \left\{1 \dots n_{gl}\right\}} -g_{j}^{l}\left(\mb{x},\mb{y}^{k}\right)\right\} \mple - \eps^{r}, \ \forall \ i \in \{1 \dots n_{g^u}\}, \ \forall \ \mb{y}^{k} \in \mc{Y}^{LBD}\\
                & \feassetX,
    \end{array}
\end{equation}
$$

- ##### <a name="llp"></a> **(GSIP-)LLP** - lower-level problem; note that for all SIP  solvers it holds $n_{gl} = 0$.
$$
\newcommand{\mpmax}[1]{\underset{#1}{\text{max}}\,}
\newcommand{\mb}[1]{\boldsymbol{#1}}
\newcommand{\mbb}[1]{\bar{\boldsymbol{#1}}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mpst}{\text{s.t.}\,}
\newcommand{\mpeq}{{}=}
\newcommand{\mple}{{}\le}
\newcommand{\hostset}[1]{\left[\mb{#1}^{lb},\mb{#1}^{ub}\right]\subsetneq\R^{n_#1}}
\newcommand{\feassetY}{\mc{Y}\coloneqq\left\{\mb{y}\in\hostset{y}:\mb{v}^{il}\left(\mb{y}\right)\mple\mb{0},\ \mb{v}^{el}\left(\mb{y}\right)\mpeq\mb{0}\right\}}
\begin{equation}
    \newcommand{\vect}[1]{\boldsymbol{#1}}
    \tag{(GSIP-)LLP}
    \begin{array}{rlrlrr}
        g^{u,*} \mpeq \mpmax{\mb{y} \in \mc{Y}} & \mpmax{j \in \left\{1 \dots n_{gu}\right\}} g^{u}_{j}\left(\mbb{x},\mb{y}\right) \\
        \mpst   & \mb{g}^{l}\left(\mb{x},\mb{y}\right) \leq \mb{0} \\
                & \feassetY
    \end{array}
\end{equation}
$$

- ##### <a name="aux"></a> **GSIP-AUX** - auxiliary problem
$$
\newcommand{\mpmin}[1]{\underset{#1}{\text{min}}\,}
\newcommand{\mpmax}[1]{\underset{#1}{\text{max}}\,}
\newcommand{\mb}[1]{\boldsymbol{#1}}
\newcommand{\mbb}[1]{\bar{\boldsymbol{#1}}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mpst}{\text{s.t.}\,}
\newcommand{\mpeq}{{}=}
\newcommand{\mple}{{}\le}
\newcommand{\mpge}{{}\ge}
\newcommand{\hostset}[1]{\left[\mb{#1}^{lb},\mb{#1}^{ub}\right]\subsetneq\R^{n_#1}}
\newcommand{\feassetY}{\mc{Y}\coloneqq\left\{\mb{y}\in\hostset{y}:\mb{v}^{il}\left(\mb{y}\right)\mple\mb{0},\ \mb{v}^{el}\left(\mb{y}\right)\mpeq\mb{0}\right\}}
\begin{equation}
    \newcommand{\vect}[1]{\boldsymbol{#1}}
    \tag{GSIP-AUX}
    \begin{array}{rlrlrr}
        \mpmin{\mb{y} \in \mc{Y}} & \mpmax{j \in \left\{1 \dots n_{gl}\right\}} g^{l}\left(\mbb{x},\mb{y}\right) \\
        \mpst   & \mpmax{j \in \left\{1 \dots n_{gu}\right\}} g^{u}_{j} \left(\mbb{x}, \mb{y}\right) \mpge \alpha \cdot g^{u,*} \\
                & \feassetY
    \end{array}
\end{equation}
$$

- ##### <a name="ora"></a> **ORA** - oracle approach
$$
\newcommand{\mpmin}[1]{\underset{#1}{\text{min}}\,}
\newcommand{\mb}[1]{\boldsymbol{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mpst}{\text{s.t.}\,}
\newcommand{\mpeq}{{}=}
\newcommand{\mple}{{}\le}
\newcommand{\hostset}[1]{\left[\mb{#1}^{lb},\mb{#1}^{ub}\right]\subsetneq\R^{n_#1}}
\newcommand{\feassetX}{\mc{X}\coloneqq\left\{\mb{x}\in\hostset{x}:\mb{v}^{iu}\left(\mb{x}\right)\mple\mb{0},\ \mb{v}^{eu}\left(\mb{x}\right)\mpeq\mb{0}\right\}}
\begin{equation}
    \newcommand{\vect}[1]{\boldsymbol{#1}}
    \tag{ORA}
    \begin{array}{rlrlrr}
        \mpmin{\mb{x} \in \mc{X}, \nu} & \nu \\
        \mpst   & f\left(\mb{x}\right) - f^{t} \mple \nu \\
                & \mb{g}^{u}\left(\mb{x},\mb{y}^{k}\right) \mple \nu \cdot \mb{1}, \ \forall \ \mb{y}^{k} \in \mc{Y}^{ORA}\\
                & \feassetX.
    \end{array}
\end{equation}
$$

- ##### <a name="res"></a> **RES** - restricted target
$$
\newcommand{\mpmin}[1]{\underset{#1}{\text{min}}\,}
\newcommand{\mb}[1]{\boldsymbol{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mpst}{\text{s.t.}\,}
\newcommand{\mpeq}{{}=}
\newcommand{\mple}{{}\le}
\newcommand{\hostset}[1]{\left[\mb{#1}^{lb},\mb{#1}^{ub}\right]\subsetneq\R^{n_#1}}
\newcommand{\feassetX}{\mc{X}\coloneqq\left\{\mb{x}\in\hostset{x}:\mb{v}^{iu}\left(\mb{x}\right)\mple\mb{0},\ \mb{v}^{eu}\left(\mb{x}\right)\mpeq\mb{0}\right\}}
\begin{equation}
    \newcommand{\vect}[1]{\boldsymbol{#1}}
    \tag{RES}
    \begin{array}{rlrlrr}
        \mpmin{\mb{x} \in \mc{X}, \eta} & -\eta \\
        \mpst   & f\left(\mb{x}\right) - f^{RES} \mple 0 \\
                & \mb{g}^{u}\left(\mb{x},\mb{y}^k\right) \mple -\eta \cdot \mb{1}, \ \forall \ \mb{y}^{k} \in \mc{Y}^{RES} \\
                & \feassetX,
    \end{array}
\end{equation}
$$

### The definition file `def.txt`

The definition file `def.txt` connects the different subproblems.
The definition file `def.txt` defines the following list:
- problem/procedure (definitions)
- discretized index and variable with its cardinality
- internal parameters of the respective solvers, e.g., the restriction of the right-hand side parameter in the sip_rrhs approach of [Mitsos 2011](https://www.tandfonline.com/doi/abs/10.1080/02331934.2010.527970)
- (optional) information related to the problem (with info keys such as: *"name"*, *"source"*, *"objective"*, *"x category"*, *"y category"*, etc.)

The following table gives an overview of the `def.txt` with the necessary definitions for each solver in the respective program classes.

*Note* :
- the mark **'X'** specifies that the definition **must** be defined in the following problem.
- mark **'o'** specifies that the definition is optional (not necessary).

| **Definitions**                       | **Descriptions**                      | **SIP**<br>*B&F* | **SIP**<br>*RRHS* | **SIP**<br>*Oracle* | **SIP**<br>*Hybrid* | **MINMAX**<br>*Minmax* | **GSIP**<br>*GSIP-RRHS* | **ESIP**<br>*B&F* | **ESIP**<br>*Hybrid* | **BLP**<br>*Box* | **BLP**<br>*noBox* | **BLP**<br>*AdaptOracle* |
|:-------------------------------------:|:-------------------------------------:|:----------------:|:-----------------:|:-------------------:|:-------------------:|:----------------------:|:-----------------------:|:-----------------:|:--------------------:|:----------------:|:------------------:|:------------------------:|
| ```set_disc(<disc-#>)```              | name of discretization index set      | X                | X                 | X                   | X                   | X                      | X                       | X                 | X                    | X                | X                  | X                        |
| ```set_disc_parameter_src(<disc-#>,``` <br>  ```<parameter name>)``` | \<parameter name>: discretized parameter [$\boldsymbol{y}^k$](#lbd); <br> value: lower-level parameter name [$\boldsymbol{y}$](#llp) | X                | X                 | X                   | X                   | X                      | X                       | X                 | X                    | X                | X                  | X                        |
| ```set_disc_variable_src(<disc-#>,```  <br>  ```<variable name>)```  | \<variable  name>: discretized variable  [$\boldsymbol{z}^k$]();     <br> value: lower-level variable  name [$\boldsymbol{z}$]()     | o                | o                 | o                   | o                   | o                      | o                       | o                 | o                    | X                | o                  | o                        |
| ```set_internal("eps_res")```         | [$\varepsilon^{r}$](#ubp)             |                  | X                 |                     | X                   |                        | X                       | X                 | X                    |                  |                    |                          |
| ```set_internal("ora_target")```      | [$f^{t}$](#ora)                       |                  |                   | X                   |                     |                        |                         |                   |                      |                  |                    |                          |
| ```set_internal("init_lbd")```        | initialized lower bound of [$f$](#ora)|                  |                   |o                    |                     |                        |                         |                   |                      |                  |                    |                          |
| ```set_internal("init_ubd")```        | initialized upper bound of [$f$](#ora)|                  |                   |o                    |                     |                        |                         |                   |                      |                  |                    | o                        |
| ```set_internal("res_target")```      | [$f^{RES}$](#res)                     |                  |                   |                     | X                   |                        |                         |                   | X                    |                  |                    | o                        |
| ```set_internal("alpha")```           | [$\alpha$](#aux)                      |                  |                   |                     |                     |                        | X                       |                   |                      |                  |                    | X                        |
| ```set_internal("g_u")```             | [$g^{u,*}$](#aux)                     |                  |                   |                     |                     |                        |                         |                   |                      |                  |                    |                          |
| ```set_internal("u_obj_target")```    | [$f^{t}$]()                           |                  |                   |                     |                     |                        |                         |                   |                      |                  |                    | X                        |
| ```push_internal("l_obj_star")```     | [$h^{*}$]()                           |                  |                   |                     |                     |                        |                         |                   |                      | X                |                    | X                        |
| ```set_internal("eps_l_obj")```       | [$\varepsilon_{h}$]()                 |                  |                   |                     |                     |                        |                         |                   |                      | X                |                    |                          |
| ```push_internal("eps2_l_obj")```     | [$\varepsilon_{h2}$]()                |                  |                   |                     |                     |                        |                         |                   |                      | X                |                    |                          |
| ```push_internal("box_parameter")```  | [$v$]()                               |                  |                   |                     |                     |                        |                         |                   |                      | X                |                    |                          |
| ```push_internal("u_cont_var")```     | upper level continuous variables      |                  |                   |                     |                     |                        |                         |                   |                      | X                |                    |                          |
| ```push_internal("l_obj_max")```      | [$h^{max}$]()                         |                  |                   |                     |                     |                        |                         |                   |                      | X                |                    |                          |
| ```push_internal("current_lbd")```    | current LBD                           |                  |                   |                     |                     |                        |                         |                   |                      | X                |                    |                          |
| ```set_internal("aux_target")```      | [$\bar{f}^{l}$]()                     |                  |                   |                     |                     |                        |                         |                   |                      |                  | X                  |                          |
| ```set_internal("eps_aux")```         | [$\varepsilon^{l,AUX}$]()             |                  |                   |                     |                     |                        |                         |                   |                      |                  | X                  |                          |
| ```set_internal("l_obj_val")```       | placeholder                           |                  |                   |                     |                     |                        |                         |                   |                      |                  | X                  |                          |
| ```set_internal("ubp_target")```      | placeholder                           |                  |                   |                     |                     |                        |                         |                   |                      |                  | X                  |                          |
| ```set_internal("eps_ubp")```         | [$\varepsilon^{l,UBD}$]()             |                  |                   |                     |                     |                        |                         |                   |                      |                  | X                  |                          |
| ```set_info("<info_key>")```          | used for problem metadata             | o                | o                 | o                   | o                   | o                      | o                       | o                 | o                    | o                | o                  | o                        |

The following example is taken from `demo\examples\sip_input\def.txt` .

**Lower-Bounding Procedure**
```
programdefinition("lbp"):
set_disc(1) = lbp_k_disc;
set_disc_parameter_src(1, lbp_y_disc) = y;
```
**Upper-Bounding Procedure**
```
programdefinitions("ubp"):
set_disc(1) = ubp_k_disc;
set_disc_parameter_src(1, ubp_y_disc) = y;
set_internal("eps_res") = eps_res;
```
**Oracle** - for sip_ora demo
```
programdefinitions("ora"):
set_disc(1) = ora_k_disc;
set_disc_parameter_src(1, ora_y_disc) = y;
set_internal("ora_target") = f_target;
set_internal("init_lbd") = "-11";
set_internal("init_ubd") = "0";
```
**Res** - for sip_hybrid demo
```
programdefinitions("res"):
set_disc(1) = res_k_disc;
set_disc_parameter_src(1, res_y_disc) = y;
set_internal("res_target") = f_res;
```

## What can go wrong? (Errors)
### 1) The submodules do not seem to work
Please ensure that all submodules are up-to-date.
To check whether all submodules are up-to-date, one can use the following command.
```
$ git submodule status
```
The shown **hash values** (reference) should correspond with the oned from `dep\`.
Note that the URLs of the submodules have to be correct.

If a shown hash value does not match the value in `dep\`, the respective submodule must be updated or, in some cases, reinitialized.
For additional information, please refer to the documentation of the `git submodule` using
```
$ git help submodule
```
### 2) MAiNGO does not compile, or MAiNGO terminates unexpectedly during the solution
Please refer to the MAiNGO [FAQs](https://avt-svt.pages.rwth-aachen.de/public/maingo/faq.html).


## References

Blankenship, J.W., & Falk, J.E. (1976). [Infinitely constrained optimization problems](https://link.springer.com/article/10.1007/BF00934096). *Journal of Optimization Theory and Applications*, 19, 261-281.

Djelassi, H., Glass,  M., & Mitsos, A. (2019). [Discretization-based algorithms for generalized semi-infinite and bilevel programs with coupling equality constraints](https://link.springer.com/article/10.1007/s10898-019-00764-3). *Journal of Global Optimization*, 75, 341-392.

Djelassi, H., & Mitsos, A. (2016). [A hybrid discretization algorithm with guaranteed feasibility for the global solution of semi-infinite programs](https://link.springer.com/article/10.1007/s10898-016-0476-7). *Journal of Global Optimization*, 68, 227-253.

Djelassi, H., Mitsos, A. (2021). [Global Solution of Semi-infinite Programs with Existence Constraints](https://doi.org/10.1007/s10957-021-01813-2). *Journal of Optimization Theory and Applications*, 188, 863–881.

Falk, J. E. & Hoffman, K. (1977). [A nonconvex max-min problem](https://doi.org/10.1002/nav.3800240307). *Naval Research Logistics*, 24, 441–450.

Jungen, D. & Mitsos, A. [An Improved Oracle Adaption for Bilevel Programs](http://doi.org/10.1016/B978-0-443-28824-1.50551-2).  In *34th European Symposium on Computer Aided Process Engineering /15th International Symposium on Process Systems Engineering. ESCAPE-34/PSE-24*, edited by F. Manenti & G. V. Reklaitis (Elsevier - Health Sciences Division 2024), 3301-3306.

Mitsos, A. (2011). [Global optimization of semi-infinite programs via restriction of the right-hand side](https://www.tandfonline.com/doi/abs/10.1080/02331934.2010.527970). *A Journal of Mathematical Programming and Operations Research*, 60, 1291-1308.

Mitsos, A., Lemonidis, P. & Barton, P.I. (2008). [Global solution of bilevel programs with a nonconvex inner program](https://doi.org/10.1007/s10898-007-9260-z). *Journal of Global Optimization*, 42, 475–513.

Mitsos, A., & Tsoukalas, A. (2014). [Global optimization of generalized semi-infinite programs via restriction of the right hand side](https://link.springer.com/article/10.1007/s10898-014-0146-6). *Journal of Global Optimization*, 74, 1-17.

Tsoukalas, A., & Rustem, B. (2019). [A feasible point adaptation of the Blankenship and Falk algorithm for semi-infinite programming](https://link.springer.com/article/10.1007/s11590-010-0236-4). *Optimization Letters*, 5, 705-716.


