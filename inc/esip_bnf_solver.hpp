#pragma once

#include <functional> // for function
#include <utility>    // for pair, move
#include <iosfwd>     // for istream
#include <string>     // for string, allocator, basic_string
#include <vector>     // for vector
#include <atomic>     // for atomic

#include "common.hpp"
#include "solver.hpp"         // for set_tolerance_robust_base, solve_record, solver
#include "parser_chain.hpp"   // for programdefinition_parser_chain
#include "discretization.hpp" // for discretization
#include "helper.hpp"         // for ale
#include "program.hpp"        // for program
#include "symbol_table.hpp"   // for symbol_table


namespace dips {



using namespace ale;

class esip_bnf_program_definition_parser : public programdefinition_parser_chain {
public:
    esip_bnf_program_definition_parser(std::istream&, symbol_table&);

    std::vector<discretization> get_ulp_discretizations();
    std::vector<discretization> get_mlp_discretizations();
};

// TODO: currently only a single discretization is allowed in the minmax problem
// also note that the different discretizations should be seperated.
// max_y1_y2 min_z max(g1(x,y1,z),g2(x,y2,z)) =/= max( max_y1 min_z g1(x,y1,z), max_y2 min_z g2(x,y2,z))
// with multiple discretizations you solve the later

class esip_bnf_solver {
public:
    struct program_info {
        void add_mlp_and_llp(const program& mlp, const program& llp, const discretization& ulp_disc, const discretization& mlp_disc);

        program ulp;
        std::vector<std::pair<program, program>> mlp_llp_pairs;
        std::vector<discretization> ulp_discs;
        std::vector<discretization> mlp_discs; //only one for each mlp
        std::map<std::string, std::string> lbp_info, mlp_info;
        std::map<std::string, std::vector<std::string>> lbp_info_list, mlp_info_list;
    };

    esip_bnf_solver(symbol_table& symbols, std::function<solver*(symbol_table&, std::string name)> make_subsolver, std::string solver_name = "esip_output_bnf/") :
        symbols(symbols),
        make_subsolver(std::move(make_subsolver)),
        solver_base_name(std::move(solver_name)) {};
    solve_record solve(program_info);
    bool set_option(const std::string&, double);
    bool set_option(const std::string&, const std::string&);
    bool set_option(std::string, bool);
    void set_tolerance_robust(std::string option, double value) { set_tolerance_robust_base(*this, std::move(option), value); };
    void pass_option(const std::string&, double);
    double get_option(const std::string&) const;
    void set_solver_base_name(std::string);

private:
    std::function<solver*(symbol_table&, std::string)> make_subsolver;
    symbol_table& symbols;
    std::string solver_base_name;
    std::vector<std::pair<std::string, double>> options;

    std::string settings_ulp = "settings_ulp.txt";
    std::string settings_minmax_ulp = "settings_minmax_ulp.txt";
    std::string settings_minmax_llp = "settings_minmax_llp.txt";

    double feas_tol = 1e-6;
    double abs_tol = 1e-4;
    double rel_tol = 1e-4;

    double abs_tol_ulp = 1e-1 * abs_tol;
    double rel_tol_ulp = 1e-1 * rel_tol;

    double rel_tol_minmax = 1e-1;
    double abs_tol_minmax = 5e-1 * feas_tol;
    double red_tol_minmax = 2;

    int max_iter = 1000;
    double max_time = 86400;

    bool discr_all = false;
    std::atomic<bool> aborted = false;
};



} // namespace dips
