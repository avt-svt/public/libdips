#pragma once

#include <stddef.h>   // for size_t
#include <functional> // for function
#include <string>     // for string
#include <vector>     // for vector

#include "solver.hpp"         // for solve_record, solution
#include "discretization.hpp" // for discretization
#include "symbol_table.hpp"   // for symbol_table
#include "helper.hpp"         // for ale
#include "symbol.hpp"         // for base_symbol



namespace dips {
struct discretization;



using namespace ale;

void fix_variables(const solve_record&, symbol_table&);
void fix_variables(const solution&, symbol_table&);
void fix_variables(const solve_record&, symbol_table&, std::vector<std::string>);

void fix_initial(const solve_record&, symbol_table&);
void fix_initial(const solution&, symbol_table&);
void fix_initial(const solve_record&, symbol_table&, const std::vector<std::string>&);

void set_real(const std::string&, double, symbol_table&);

void copy_variables(const solve_record&, solve_record&);
void copy_variables(const solution& source, solution& dest);

void add_renamed_symbol_to_record(const std::string& src_name, const std::string& dest_name, symbol_table& symbols, solve_record& dest);

void add_discretization_parameters(const discretization&, symbol_table&, solve_record&);

void reset_symbols(const discretization&, symbol_table&);

void discretize(const discretization&, const solve_record&, const solve_record&, symbol_table&);

void discretize(const discretization&, const solve_record&, symbol_table&);

//used to delete function calls to callbacks to locally created objects that are no longer present after the current function returns.
//since the object itself might be reused, we do want to make sure the callback vector is cleared if the scope of the function is left.
struct scoped_callback_vector_clearer {
    std::vector<std::function<void()>>& managed;
    scoped_callback_vector_clearer(std::vector<std::function<void()>>& vec) :
        managed(vec) {};
    ~scoped_callback_vector_clearer();
    scoped_callback_vector_clearer(scoped_callback_vector_clearer&) = delete;
    scoped_callback_vector_clearer& operator=(const scoped_callback_vector_clearer&) = delete;
    scoped_callback_vector_clearer(scoped_callback_vector_clearer&&) = delete;
    scoped_callback_vector_clearer& operator=(scoped_callback_vector_clearer&&) = delete;
};

struct clean_up_task_list {
    std::vector<std::function<void()>> managed;
    clean_up_task_list() = default;
    clean_up_task_list(std::function<void()>&& in) :
        managed { std::move(in) } {};
    ~clean_up_task_list();
    clean_up_task_list(clean_up_task_list&) = delete;
    clean_up_task_list& operator=(const clean_up_task_list&) = delete;
    clean_up_task_list(clean_up_task_list&&) = delete;
    clean_up_task_list& operator=(clean_up_task_list&&) = delete;
};

} // namespace dips
