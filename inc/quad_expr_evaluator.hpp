#pragma once

#include <stdexcept>
#include <vector> // for vector
#include <string>
#include <unordered_map>

#include "QuadExpr.hpp"         // for QuadExpr
#include "expression.hpp"       // for expression
#include "symbol_table.hpp"     // for symbol_table
#include "scalar_evaluator.hpp" // for ScalarVariableIdManager
#include "helper.hpp"           // for ale
#include "value.hpp"            // for boolean, real



namespace dips {
class ScalarVariableIdManager;

using namespace ale;

struct ExpressionNotQuadraticError : public std::invalid_argument {
    explicit ExpressionNotQuadraticError(const std::string& msg) :
        std::invalid_argument(msg) { }
};

// types of quadratic constraints
enum class QuadConstraintType { EQUAL,
    NOT_EQUAL,
    LESS,
    GREATER,
    LESS_EQUAL,
    GREATER_EQUAL };

// simple class containing a quadratic expression and a constraint type
// represents: expr < 0, expr = 0, ... where expr is at most quadratic
class QuadConstraint {
public:
    QuadConstraint(QuadExpr expr, QuadConstraintType type);

    [[nodiscard]] const QuadExpr& getExpr() const;
    [[nodiscard]] QuadConstraintType getType() const;

    [[nodiscard]] QuadConstraint negate() const;

private:
    QuadExpr expr;
    QuadConstraintType type;
};

// list of quadratic constraints which should all be true
using QuadConstraintList = std::vector<QuadConstraint>;

/**
     * Evaluates expressions which are of the form x^T.Q.x+b.x+c
     */
QuadExpr evaluate_quadratic(expression<real<0>>& expr, symbol_table& symbols, const ScalarVariableIdManager& id_manager);

/**
     * Evaluates quadratic constraints: expr < 0, (and >, <=, >=, !=, =),
     * and their conjunctions/disjunctions/negations
     */
QuadConstraintList evaluate_quadratic_constraint(expression<ale::boolean<0>>& expr, symbol_table& symbols, const ScalarVariableIdManager& id_manager);

} // namespace dips
