#pragma once

#include "solver.hpp"
#include "expression.hpp"
#include "symbol_table.hpp"
#include "symbol_finder.hpp"
#include "util/evaluator.hpp"
#include "util/expression_utils.hpp"
#include "symbol_printer.hpp"
#include "gams.h"

#include <algorithm>
#include <deque>
#include <exception>
#include <numeric>
#include <sstream>
#include <tuple>
#include <utility>
#include <vector>
#include <iterator>

namespace dips {

using namespace ale;

class gams_parser {
public:
    virtual ~gams_parser() = default;
    gams_parser(program& prog, symbol_table& symbols, std::string solver_type, std::string problem_class, double decimals) :
        prog(prog),
        symbols(symbols),
        solver_type(std::move(solver_type)),
        problem_class(std::move(problem_class)),
        decimals(decimals) {};
    virtual void parse_gams(std::stringstream&) = 0;
    void parse_gams_solve_statement(std::stringstream&);

protected:
    program& prog;
    symbol_table& symbols;
    std::string problem_class;
    std::string solver_type;
    double decimals;
};

struct unsupportedNodeParsingException : public std::invalid_argument {
    unsupportedNodeParsingException(const std::string& ex) :
        std::invalid_argument(ex) { }
};


} // namespace dips