#pragma once

#include <functional> // for function
#include <utility>    // for move, pair
#include <iosfwd>     // for istream
#include <string>     // for string, basic_string, allocator
#include <vector>     // for vector
#include <atomic>     // for atomic

#include "common.hpp"
#include "solver.hpp"         // for set_tolerance_robust_base, solve_record, solver
#include "parser_chain.hpp"   // for programdefinition_parser_chain
#include "discretization.hpp" // for discretization
#include "helper.hpp"         // for ale
#include "program.hpp"        // for program
#include "symbol_table.hpp"   // for symbol_table



namespace dips {



using namespace ale;

class minmax_bnf_program_definition_parser : public programdefinition_parser_chain {
public:
    minmax_bnf_program_definition_parser(std::istream&, symbol_table&);
};

class minmax_bnf_solver {
public:
    struct program_info {
        void add_llp(const program&, const discretization&);

        program lbp;
        std::vector<program> llps;
        std::vector<discretization> lbp_discs;

        std::map<std::string, std::string> lbp_info;
        std::map<std::string, std::vector<std::string>> lbp_info_list;
    };

    minmax_bnf_solver(symbol_table& symbols, std::function<solver*(symbol_table&, std::string)> make_subsolver, std::string solver_name = "minmax_output_bnf/") :
        symbols(symbols),
        make_subsolver(std::move(make_subsolver)),
        solver_name({ std::move(solver_name) }) {};
    solve_record solve(program_info);
    //minmax has solver name vector like subproblem solvers, because it is also used as a subsolver in ESIP problems
    std::vector<std::string> solver_name;
    bool set_option(const std::string&, double);
    bool set_option(const std::string&, const std::string&);
    bool set_option(const std::string&, bool);
    void set_tolerance_robust(std::string option, double value) { set_tolerance_robust_base(*this, std::move(option), value); };
    void pass_option(const std::string&, double);
    [[nodiscard]] double get_option(const std::string&) const;
    void set_solver_base_name(const std::string&);
    std::string get_solver_base_name();

private:
    std::function<solver*(symbol_table&, std::string)> make_subsolver;
    symbol_table& symbols;
    std::vector<std::pair<std::string, double>> options;

    std::string settings_lbp = "settings_lbp.txt";
    std::string settings_lbp_llp = "settings_llp.txt";


    double abs_tol = 1e-2;
    double rel_tol = 1e-2;

    double abs_tol_lbp = 1e-1 * abs_tol;
    double rel_tol_lbp = 1e-1 * rel_tol;
    double abs_tol_lbp_llp = 1e-2 * abs_tol;
    double rel_tol_lbp_llp = 1e-2 * rel_tol;

    double red_tol_lbp_llp = 1.25; // must be > 1

    int max_iter = 1000;
    double max_time = 86400;

    bool discr_all = true;
    std::atomic<bool> aborted = false;
};



} // namespace dips
