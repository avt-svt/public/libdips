#pragma once

#include <sstream> // for istream
#include <string>  // for string, basic_string
#include <vector>  // for vector

#include "programdefinition_parser.hpp" // for programdefinition_parser
#include "discretization.hpp"           // for discretization
#include "symbol_table.hpp"             // for symbol_table




namespace dips {


/**
     * Parses a list of programdefinition_parsers constructed using "specifications"
     * in any order.
     */
class programdefinition_parser_chain {
public:
    struct parser_specification {
        std::string block_name;
        std::vector<std::string> required_internals;
    };
    programdefinition_parser_chain(std::istream& is, ale::symbol_table& symbols, const std::vector<parser_specification>& specifications);

    /**
         * Constructs each programdefinition_parser and parses the input with it.
         * The resulting parses will be stored in definition_parsers.
         */
    void parse();
    bool fail();
    std::vector<discretization> get_discretizations(const std::string& block);
    std::string get_internal(const std::string& block, const std::string& internal);
    std::vector<std::string> get_internals_list(const std::string& block, const std::string& internal);
    std::string get_info(const std::string& block, const std::string& info_name);
    std::vector<std::string> get_info_list(const std::string& block, const std::string& info_name);
    std::map<std::string, std::string> get_info(const std::string& block);
    std::map<std::string, std::vector<std::string>> get_info_list(const std::string& block);

protected:
    std::vector<parser_specification> specifications;
    std::vector<programdefinition_parser> definition_parsers;
    std::string content; // stores content of "is"
    ale::symbol_table& symbols;
};

} // namespace dips
