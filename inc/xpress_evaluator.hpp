#pragma once

#include <functional>
#include <string>
#include <unordered_map>
#include <vector>

#include "functional_variable_lister.hpp"
#include "quad_expr_evaluator.hpp"

class XPRBprob;
class XPRBvar;

namespace dips {

// shorten function signature
using VariableLister = functional_variable_lister<XPRBvar>;
using addQuadConstraintFunction = std::function<void(XPRBprob&, const QuadConstraintList&, const VariableLister&, const std::string&)>;

/**
 * Evaluates Quadratic expressions as objectives but allows exp() in constraints
 * by splitting a constraint into multiple constraints.
 * It also adds new variables (without a name) to (only) the variables list.
 */
class xpress_evaluator : public quad_expr_evaluator {
public:
    // addQuadConstraintFunction: should add a quadratic constraint to the given model
    xpress_evaluator(symbol_table& symbols, VariableLister& var_lister,
      addQuadConstraintFunction add_quad_constraint, XPRBprob& model);

    QuadExpr operator()(exp_node* node) override;

private:
    addQuadConstraintFunction add_quad_constraint;
    VariableLister& var_lister;
    XPRBprob& model;
};

} // namespace dips
