#pragma once

#include <string>        // for string, basic_string, char_traits, operator+, operator==, hash, operator<<, to_string
#include <unordered_map> // for _Node_const_iterator, operator!=, _Node_iterator_base, unordered_map
#include <map>           // for map
#include <limits>        // for numeric_limits
#include <algorithm>
#include <vector>      // for vector
#include <memory>      // for allocator, unique_ptr
#include <filesystem>  // for path
#include <iostream>    // for operator<<, basic_ostream, cout, ostream
#include <exception>   // for invalid_argument
#include <type_traits> // for remove_reference<>::type
#include <utility>     // for pair, move
#include <stdexcept>   // for invalid_argument

#include "program.hpp" // for program
#include "symbol.hpp"  // for base_symbol
#include "symbol_printer.hpp"
#include "symbol_table.hpp"   // for symbol_table
#include "discretization.hpp" // for discretization


namespace dips {




enum solve_status {
    ABNORMAL = 0,
    ABORTED,
    EXCEEDED_MAX_TIME,
    EXCEEDED_MAX_ITERATIONS,
    ASSUMPTION_VIOLATION_LIKELY,
    NUMERICAL_SENSITIVE,
    UNKNOWN,
    INFEASIBLE,
    FEASIBLE,
    LOCAL,
    GLOBAL,
    UNBOUNDED
};

std::string to_string(solve_status);

using Smap = std::unordered_map<std::string, std::unique_ptr<ale::base_symbol>>;

//solution is a wrapper around std::unordered_map<std::string, std::unique_ptr<ale::base_symbol>>
//because the MSVC compiler doesn't think std::unordered_map<std::string,MoveOnly> can be noexcept move-constructed [not defined by the standard]
//while it would work with gcc as long as it is only ever moved, we wrap this here to allow use under windows
class solution : public Smap //deleting solution via pointer to Smap is not allowed!
{
public:
    solution() = default;
    solution(solution&& mE) noexcept :
        Smap(std::move(mE)) { }
    solution(const solution& other) {
        for(const auto& entry : other) {
            this->emplace(entry.first, std::unique_ptr<ale::base_symbol>(entry.second->clone()));
        }
    }

    solution& operator=(const solution& other) {
        this->clear();
        for(const auto& entry : other) {
            this->emplace(entry.first, std::unique_ptr<ale::base_symbol>(entry.second->clone()));
        }
        return *this;
    }

    solution& operator=(solution&& other) noexcept {
        if(this != &other) {
            Smap::operator=(std::move(other));
        }
        return *this;
    }
    ~solution() = default;
};

struct solve_record {
    solve_record() :
        m_status(ABNORMAL) { }
    solve_record(solve_status status) :
        m_status(status) { }

    solve_record(solve_record&& mE) {
        m_status = std::move(mE.m_status);
        m_cpu = std::move(mE.m_cpu);
        m_lbd = std::move(mE.m_lbd);
        m_ubd = std::move(mE.m_ubd);
        m_solution = solution(mE.m_solution);
    }

    //need custom copy constructor because map->unique_ptr is not copyable.
    solve_record(const solve_record& other) {
        m_status = other.m_status;
        m_cpu = other.m_cpu;
        m_lbd = other.m_lbd;
        m_ubd = other.m_ubd;
        m_solution = solution(other.m_solution);
    }

    //assignment constructor (both normal and move)
    solve_record& operator=(const solve_record& x) {
        this->m_status = x.m_status;
        this->m_cpu = x.m_cpu;
        this->m_lbd = x.m_lbd;
        this->m_ubd = x.m_ubd;
        this->m_solution.clear();
        this->m_solution = solution(x.m_solution);
        return *this;
    }

    solve_record& operator=(solve_record&& x) = default;

    ~solve_record() = default;

    solution m_solution;
    solve_status m_status;
    double m_cpu = 0;
    double m_lbd = -std::numeric_limits<double>::infinity();
    double m_ubd = std::numeric_limits<double>::infinity();
};
void dump_json(const solve_record& rec, const std::filesystem::path&);
void report(const solve_record&);

void report(const solve_record&, std::vector<std::string>);

void report(ale::symbol_table&, const std::vector<std::vector<discretization>>&);

class solver {
public:
    solver(const std::string& name = "") {
        solver_name.clear();
        solver_name.push_back(name);
    }
    virtual ~solver() = default;

    virtual solve_record solve(program) = 0;
    virtual bool set_option(std::string, double) = 0;
    virtual bool set_option(std::string, std::string) = 0;



    virtual double get_option(std::string) = 0;

    virtual void abort() {};

    void pass_option(const std::string&, double);

    std::vector<std::string> solver_name;

protected:
    std::map<std::string, double> options;
};

bool check_termination(solve_record& rec, double feas_vio_ubp, double abs_tol, double rel_tol, double feas_tol);

bool check_aborted(double max_time, solve_record& rec, const std::string& solver_base_name, bool aborted);

template <class T>
void set_tolerance_robust_base(T& sip_solver, std::string option, double value) {
    if(option.find(std::string("tol")) != std::string::npos) {
        if(value < 1e-9) {
            std::cout << option << " would exceed minimal tolerance 1e-9.\n"
                      << "Setting " + option + " = 1e-9\n";
            value = 1e-9;
        }
        if(sip_solver.set_option(option, value)) {
            return;
        } else {
            throw std::invalid_argument("set_tolerance_robust failed for for option: " + option + " = " + std::to_string(value));
        }
    }
    throw std::invalid_argument("set_tolerance_robust used for option: " + option + " = " + std::to_string(value));
}


//Use this instead of calling push and having to make sure to call pop at every eary return.
//On construction pushes a value that is popped at destruction.
//Impossible to pop twice.
struct scoped_push {
    scoped_push(std::vector<std::string>& vec, const std::string& input) :
        guarded_vector(vec) {
        guarded_vector.emplace_back(input);
    }
    scoped_push(scoped_push&) = delete;
    scoped_push& operator=(const scoped_push&) = delete;
    scoped_push(scoped_push&&) = delete;
    scoped_push& operator=(scoped_push&&) = delete;
    ~scoped_push() {
        trigger();
    }
    //if we do not want to pop scope at destruction
    void dismiss();

    // pops scope and deactivates the trigger
    void trigger();

    bool enabled = true;
    std::vector<std::string>& guarded_vector;
};
} // namespace dips
