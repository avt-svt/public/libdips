/**********************************************************************************
 * Copyright (c) 2019 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * @file ipoptProblem.h
 *
 * @brief File containing declaration of problem class used by Ipopt.
 *        Based on ample file by Carl Laird, Andreas Waechter     IBM    2005-08-09
 *
 **********************************************************************************/

#pragma once

#include <cassert>
#include <cmath>
#include <memory>
#include <vector>

// Needed according to Ipopt Dll readme:
#define HAVE_CONFIG_H
#include "util/evaluator.hpp"

#include "IpTNLP.hpp"
#include "common.hpp"
#include "fadbad_evaluator.hpp"
#include "program.hpp"
#include "symbol_finder.hpp"
#include "symbol_table.hpp"

namespace dips {

template <typename VarType>
std::tuple<std::vector<VarType>, std::vector<double>, std::vector<double>, std::vector<double>> unzip_variable_info(const std::vector<std::tuple<VarType, double, double, double>>& v) {
    std::vector<VarType> vars;
    std::vector<double> lb;
    std::vector<double> ub;
    std::vector<double> init;
    for(const auto& t : v) {
        vars.push_back(std::get<0>(t));
        lb.push_back(std::get<1>(t));
        ub.push_back(std::get<2>(t));
        init.push_back(std::get<3>(t));
    }
    return std::make_tuple(vars, lb, ub, init);
}

/**
 * @class IpoptProblem
 * @brief Class for representing problems to be solved by IpOpt, providing an interface to the problem definition in
 * problem.h used by MC++
 *
 * This class is a specialization of the problem definition class within the Ipopt C++ API.
 * Derivatives are obtained through algorithmic differentiation utilizing FADBAD.
 *
 */
template <typename VarType>
class IpoptProblem : public Ipopt::TNLP {
public:
    IpoptProblem(dips::program& prog, ale::symbol_table& symbols, bool is_qcqp) :
        _prog { prog }, _symbols { symbols }, _qcqp(is_qcqp) {
        // find all variables that are used in the program statement
        auto used_variables = find_variables(prog, symbols);
        _id_manager.set_ids(used_variables, symbols);

        auto variable_info = _id_manager.map_over_variable_symbols<std::tuple<VarType, double, double, double>>(
          [this](double lb, double ub, double init, bool integral, const std::string& name,
            const std::string& comment, const std::vector<size_t>& index) {
              init = std::isnan(init) ? (lb + ub) / 2 : init;
              return std::tuple<VarType, double, double, double>(VarType(), init, lb, ub);
          },
          symbols);

        std::tie(_variables, _xStart, _xL, _xU) = unzip_variable_info(variable_info);

        // calculate the number of evaluated constraints
        // note: this is neccessary because it does not match _prog.m_constraints.size() because for example
        // "forall i in {1, 2, 3}: f(x) = g(i)" evaluates 3 constraints

        for(auto& constr : _prog.m_constraints) {
            auto constraint = evaluate_fadbad_constraint(constr, _symbols, _id_manager, _variables);
            _nineq += constraint.ineq.size();
            _neq += constraint.eq.size();
        }



        // set initial solution
        _solution_f = std::numeric_limits<double>::max();
        _solutionX.resize(get_n_vars());
    }

    /**
     * @brief returns the number of variables
     */
    Ipopt::Index get_n_vars() { return _variables.size(); }

    /**
     * @brief Function called by Ipopt to get basic information on the problem
     *
     * @param[out] n is the number of optimization variables
     * @param[out] m is the total number of constraints (both equality and inequality)
     * @param[out] nnz_jac_g is the number of non-zero elements of the Jacobian of the constraints (assumed to be dense)
     * @param[out] nnz_h_lag is the number of non-zero elements of the Hessian of the Lagrangian (not used since BigMC
     * currently only relies on BFGS)
     * @param[out] Index_style Information on indexing of arrays (using C-style indexing, i.e., starting with 0)
     */
    bool get_nlp_info(Ipopt::Index& n, Ipopt::Index& m, Ipopt::Index& nnz_jac_g, Ipopt::Index& nnz_h_lag,
      IndexStyleEnum& Index_style) {
        // # variables
        n = get_n_vars();

        // Total # of constraints (Ipopt does not differentiate between ineq and eq)
        m = _nineq + _neq;

        // # non zeros in Jacobian
        nnz_jac_g = m * n; // TODO maybe adapt this, it just says that any Jac entry can be nz here

        // # non zeros in Hessian of Lagrangian
        nnz_h_lag = static_cast<int>(0.5 * (n * n + n)); // TODO maybe adapt this, it just says that any Hes entry can be nz here

        //compute number of nonzeros by simple evaluation
        if(_qcqp) {
            _hessian_structure = get_current_hessian_structure(_xStart.data(), m, n);
            const std::vector<bool>& hessian_entry_non_zero = _hessian_structure;
            nnz_h_lag = std::count(hessian_entry_non_zero.begin(), hessian_entry_non_zero.end(), true);
        }


        // Use the C style indexing (0-based)
        Index_style = TNLP::C_STYLE;

        // std::cout << "get_nlp_info end" << std::endl;
        return true;
    }

    /**
     * @brief Function called by Ipopt to get information on variables bounds
     * @param[in] n is the number of optimization variables
     * @param[in] m is the total number of constraints (both equality and inequality)
     * @param[out] x_l is a pointer to an array containing the lower bounds on the optimization variables
     * @param[out] x_u is a pointer to an array containing the upper bounds on the optimization variables
     * @param[out] g_l is a pointer to an array containing the lower bounds on the constraints (zero for equalities, and
     * -2e19 for inequalities)
     * @param[out] g_u is a pointer to an array containing the upper bounds on the constraints (all zero)
     */
    bool get_bounds_info(Ipopt::Index n, Ipopt::Number* x_l, Ipopt::Number* x_u, Ipopt::Index m, Ipopt::Number* g_l,
      Ipopt::Number* g_u) final {
        // Variable bounds
        std::copy_n(_xL.begin(), n, x_l);
        std::copy_n(_xU.begin(), n, x_u);

        // Constraints:
        // Ipopt interprets any number greater than nlp_upper_bound_inf as
        // infinity. The default value of nlp_upper_bound_inf and nlp_lower_bound_inf
        // is 1e19 and can be changed through ipopt options.

        // Inequalities
        std::vector<ConstraintContainer<VarType>> constraints;
        for(auto& constr : _prog.m_constraints) {
            constraints.push_back(evaluate_fadbad_constraint(constr, _symbols, _id_manager, _variables));
        }

        size_t constr_index = 0;
        for(auto& constr : constraints) {
            for(auto& ineq : constr.ineq) {
                g_l[constr_index] = -2e19;
                g_u[constr_index] = 0;
                constr_index += 1;
            }
        }
        for(auto& constr : constraints) {
            for(auto& eq : constr.eq) {
                g_l[constr_index] = 0;
                g_u[constr_index] = 0;
                constr_index += 1;
            }
        }


        return true;
    }

    /**
     * @brief Function called by Ipopt to query the starting point for local search
     *
     * @param[in] n is the number of optimization variables
     * @param[in] m is the total number of constraints (both equality and inequality)
     * @param[in] init_x indicates that a starting point for x is required
     * @param[out] x is a pointer to an array containing the initial point
     * @param[in] init_z not used in MAiNGO implementation
     * @param[in] z_L not used in MAiNGO implementation
     * @param[in] z_U not used in MAiNGO implementation
     * @param[in] init_lambda not used in MAiNGO implementation
     * @param[in] lambda not used in MAiNGO implementation
     */
    bool get_starting_point(Ipopt::Index n, bool init_x, Ipopt::Number* x, bool init_z, Ipopt::Number* z_L,
      Ipopt::Number* z_U, Ipopt::Index m, bool init_lambda, Ipopt::Number* lambda) final {
        // Make sure Ipopt is only asking for an initial point, not multipliers
        assert(init_x == true);
        assert(init_z == false);
        assert(init_lambda == false);

        std::copy_n(_xStart.begin(), n, x);

        return true;
    }

    /**
     * @brief Function called by Ipopt to evaluate the objective function
     *
     * @param[in] n is the number of optimization variables
     * @param[in] x is a pointer to an array containing the point at which the objective is to be evaluated
     * @param[in] new_x indicates whether the current x is different from the previous one handed to one of the
     * evaluation functions
     * @param[out] obj_value is the value of the objective function at the current point
     */
    bool eval_f(Ipopt::Index n, const Ipopt::Number* x, bool new_x, Ipopt::Number& obj_value) final {
        std::copy_n(x, n, _variables.begin());

        obj_value = evaluate_fadbad_expr(_prog.m_objective.front(), _symbols, _id_manager, _variables).x().x();

        return true;
    }

    /**
     * @brief Function called by Ipopt to evaluate the gradient of the objective function
     *
     * @param[in] n is the number of optimization variables
     * @param[in] x is a pointer to an array containing the point at which the objective is to be evaluated
     * @param[in] new_x indicates whether the current x is different from the previous one handed to one of the
     * evaluation functions
     * @param[out] grad_f is a pointer to an array containing the gradient of the objective function at the current
     * point
     */
    bool eval_grad_f(Ipopt::Index n, const Ipopt::Number* x, bool new_x, Ipopt::Number* grad_f) final {
        for(Ipopt::Index i = 0; i < n; ++i) {
            _variables[i] = x[i];
            _variables[i].x().diff(i, n);
        }
        VarType f = evaluate_fadbad_expr(_prog.m_objective.front(), _symbols, _id_manager, _variables);

        for(Ipopt::Index i = 0; i < n; ++i) {
            grad_f[i] = f.x().d(i);
        }

        return true;
    }

    /**
     * @brief Function called by Ipopt to evaluate the constraints
     *
     * @param[in] n is the number of optimization variables
     * @param[in] x is a pointer to an array containing the point at which the objective is to be evaluated
     * @param[in] new_x indicates whether the current x is different from the previous one handed to one of the
     * evaluation functions
     * @param[in] m is the total number of constraints (both equality and inequality)
     * @param[out] g is a pointer to an array containing the values of the constraints at the current point
     */
    bool eval_g(Ipopt::Index n, const Ipopt::Number* x, bool new_x, Ipopt::Index m, Ipopt::Number* g) final {
        std::copy_n(x, n, _variables.begin());

        std::vector<ConstraintContainer<VarType>> constraints;
        for(auto& constr : _prog.m_constraints) {
            constraints.push_back(evaluate_fadbad_constraint(constr, _symbols, _id_manager, _variables));
        }

        size_t constr_index = 0;
        for(auto& constr : constraints) {
            for(auto& ineq : constr.ineq) {
                g[constr_index] = ineq.x().x();
                constr_index += 1;
            }
        }
        for(auto& constr : constraints) {
            for(auto& eq : constr.eq) {
                g[constr_index] = eq.x().x();
                constr_index += 1;
            }
        }
        return true;
    }

    /**
     * @brief Function called by Ipopt to evaluate the constraints
     *
     * @param[in] n is the number of optimization variables
     * @param[in] x is a pointer to an array containing the point at which the objective is to be evaluated
     * @param[in] new_x indicates whether the current x is different from the previous one handed to one of the
     * evaluation functions
     * @param[in] m is the total number of constraints (both equality and inequality)
     * @param[in] nele_jac is not documented in MAiNGO
     * @param[out] iRow is a pointer to an array containing the row indices according to the sparsity pattern (see
     * https://www.coin-or.org/Ipopt/documentation/node23.html).
     * @param[out] jCol is a pointer to an array containing the row indices according to the sparsity pattern (see
     * https://www.coin-or.org/Ipopt/documentation/node23.html).
     * @param[in,out] values is a pointer to an array containing the jacobian of the constraints at the current point.
     * If the function is called with values==NULL, only information on the structure of the Jacobian is required.
     */
    bool eval_jac_g(Ipopt::Index n, const Ipopt::Number* x, bool new_x, Ipopt::Index m, Ipopt::Index nele_jac,
      Ipopt::Index* iRow, Ipopt::Index* jCol, Ipopt::Number* values) final {
        if(values == NULL) {
            // No Sparsity.
            Ipopt::Index k = 0;
            for(Ipopt::Index i = 0; i < m; ++i) {
                for(Ipopt::Index j = 0; j < n; ++j) {
                    iRow[k] = i;
                    jCol[k] = j;
                    ++k;
                }
            }
        } else {
            for(Ipopt::Index j = 0; j < n; ++j) {
                _variables[j] = x[j];
                _variables[j].x().diff(j, n);
            }

            std::vector<ConstraintContainer<VarType>> constraints;
            for(auto& constr : _prog.m_constraints) {
                constraints.push_back(evaluate_fadbad_constraint(constr, _symbols, _id_manager, _variables));
            }

            size_t value_index = 0;
            for(auto& constr : constraints) {
                for(auto& ineq : constr.ineq) {
                    for(int i = 0; i < n; ++i) {
                        values[value_index] = ineq.x().d(i);
                        value_index += 1;
                    }
                }
            }
            for(auto& constr : constraints) {
                for(auto& eq : constr.eq) {
                    for(int i = 0; i < n; ++i) {
                        values[value_index] = eq.x().d(i);
                        value_index += 1;
                    }
                }
            }
        }

        return true;
    }



    /***
     *  @brief returs a vector for each entry of the hessian of the Lagrangian if the entry was non zero in any of the constraints or the objective
     * 
     */
    std::vector<bool> get_current_hessian_structure(const Ipopt::Number* x, Ipopt::Index m, Ipopt::Index n) {
        Ipopt::Index nnz_h_lag = static_cast<int>(0.5 * (n * n + n));
        // request full hessian
        std::vector<Ipopt::Index> sparse_index;
        sparse_index.reserve(nnz_h_lag);
        std::vector<Ipopt::Index> sparse_row_index;
        sparse_row_index.reserve(nnz_h_lag);
        std::vector<Ipopt::Index> sparse_col_index;
        sparse_col_index.reserve(nnz_h_lag);
        Ipopt::Index k = 0;
        for(Ipopt::Index j = 0; j < n; ++j) {
            for(Ipopt::Index i = 0; i <= j; ++i) {
                sparse_row_index.push_back(j);
                sparse_col_index.push_back(i);
                sparse_index.push_back(k);
                ++k;
            }
        }
        // calculate hessian of objective and constraints
        auto [hess_f, hess_gs] = eval_h_seperated(x, sparse_index, sparse_row_index, sparse_col_index, nnz_h_lag, m, n);
        std::vector<bool> hessian_entry_non_zero(nnz_h_lag);
        for(int i = 0; i < nnz_h_lag; i++) {
            hessian_entry_non_zero[i] = std::fabs(hess_f[i]) > 1e-8;
            for(const auto& hess_g : hess_gs) {
                hessian_entry_non_zero[i] = hessian_entry_non_zero[i] || std::fabs(hess_g[i]) > 1e-8;
            }
        }
        return hessian_entry_non_zero;
    }

    /* *
     *  @brief Function to evaluate the Hessian of objective function and each constraint seperately.
     *  This information can be used if all functions are quadratic, since then only the multipliers change.
     *  @returns  hessians of objective then vector of hessian of constraints
     *
     *   For  arguments see eval_h_intern
     * */
    std::pair<std::vector<double>, std::vector<std::vector<double>>> eval_h_seperated(const Ipopt::Number* x, std::vector<Ipopt::Index> sparse_index, std::vector<Ipopt::Index> sparse_row_index, std::vector<Ipopt::Index> sparse_col_index, Ipopt::Index nele_hess, Ipopt::Index m, Ipopt::Index n) {
        std::vector<std::vector<double>> hessian_gs(m);
        for(auto& hess_g : hessian_gs) {
            hess_g = std::vector(nele_hess, 0.0);
        }
        std::vector<double> hessian_f(nele_hess);
        for(unsigned iVar = 0; iVar < n; iVar++) {
            _variables[iVar] = x[iVar];
            _variables[iVar].diff(iVar, n);
            _variables[iVar].x().diff(iVar, n);
        }
        VarType f = evaluate_fadbad_expr(_prog.m_objective.front(), _symbols, _id_manager, _variables);

        for(Ipopt::Index k : sparse_index) {
            Ipopt::Index i = sparse_row_index[k];
            Ipopt::Index j = sparse_col_index[k];
            hessian_f[k] = f.d(i).d(j);
        }
        // std::cout << values[j + n*i] << " ";
        std::vector<ConstraintContainer<VarType>> constraints;
        for(auto& constr : _prog.m_constraints) {
            constraints.push_back(evaluate_fadbad_constraint(constr, _symbols, _id_manager, _variables));
        }
        size_t constr_index = 0;
        for(auto& constr : constraints) {
            for(auto& ineq : constr.ineq) {
                for(Ipopt::Index k : sparse_index) {
                    Ipopt::Index l = sparse_row_index[k];
                    Ipopt::Index j = sparse_col_index[k];
                    hessian_gs[constr_index][k] = ineq.d(l).d(j);
                }
                constr_index += 1;
            }
        }
        for(auto& constr : constraints) {
            for(auto& eq : constr.eq) {
                for(Ipopt::Index k : sparse_index) {
                    Ipopt::Index l = sparse_row_index[k];
                    Ipopt::Index j = sparse_col_index[k];
                    hessian_gs[constr_index][k] = eq.d(l).d(j);
                }
                constr_index += 1;
            }
        }
        return { hessian_f, hessian_gs };
    }

    /***
     *  @brief Function called to actually evaluate the Hessian
     *  @param[in] sparse_col_index the column belonging to the non-zero entries (length nele_hess)
     *  @param[in] sparse_row_index the row belonging to the non-zero entries (length nele_hess)
     *  @param[in] sparse_index is the index into sparse_row_index and sparse_col_index (length nele_hess)
     *  @param[out] iRow if values is NULL, this is set to the row indices of the values vector (length nele_hess)
     *  @param[out] iCol if values is NULL, this is set to the column indices of the values vector (length nele_hess)
     *  For other  arguments see eval_h
     */
    bool eval_h_intern(Ipopt::Index n, const Ipopt::Number* x, bool new_x, Ipopt::Number obj_factor, Ipopt::Index m,
      const Ipopt::Number* lambda, bool new_lambda, Ipopt::Index nele_hess, Ipopt::Index* iRow, Ipopt::Index* jCol, std::vector<Ipopt::Index> sparse_index, std::vector<Ipopt::Index> sparse_row_index, std::vector<Ipopt::Index> sparse_col_index, Ipopt::Number* values) {

        assert(sparse_index.size() == nele_hess);

        if(values == NULL) {

            for(Ipopt::Index k : sparse_index) {
                iRow[k] = sparse_row_index[k];
                jCol[k] = sparse_col_index[k];
            }
        } else {

            try {
                std::vector<double> hessian_f;
                std::vector<std::vector<double>> hessian_gs;

                //in qcqp case, we can use saved values of last iteration
                if(_qcqp && !this->_fix_hessian_matrices.first.empty()) {
                    std::tie(hessian_f, hessian_gs) = _fix_hessian_matrices;
                } else {
                    std::tie(hessian_f, hessian_gs) = eval_h_seperated(x, sparse_index, sparse_row_index, sparse_col_index, nele_hess, m, n);
                }

                Ipopt::Index index = 0;
                for(Ipopt::Index k : sparse_index) {
                    Ipopt::Index iVar = sparse_row_index[k];
                    Ipopt::Index jVar = sparse_col_index[k];
                    double hess_f_value = hessian_f[k];
                    double hess_g_value = 0.0;
                    for(size_t i = 0; i < m; i++) {
                        hess_g_value += lambda[i] * hessian_gs[i][k];
                    }

                    values[k] = obj_factor * hess_f_value + hess_g_value;
                    index++;
                }

                if(index >= nele_hess + 1) {
                    throw std::range_error("Wrong number of non-zero hessian elements. \n");
                }
            }

            catch(std::exception& e) {
                std::cout << "    Warning: Evaluation of second derivatives resulted in an exception. \n "
                             "Reason: "
                          << e.what() << std::endl;
            }
        }

        return true;
    }
    /**
     * @brief Function called by Ipopt to evaluate the Hessian 
     *
     * @param[in] n the number of variables x in the problem
     * @param[in] x first call: NULL; later calls: the values for the primal variables x at which the Hessian is to be evaluated
     * @param[in] new_x false if any evaluation method (eval_*) was previously called with the same values in x, true otherwise; see also TNLP::eval_f
     * @param[in] obj_factor factor σf in front of the objective term in the Hessian
     * @param[in] m the number of constraints g(x) in the problem
     * @param[in] lambda the values for the constraint multipliers λ at which the Hessian is to be evaluated
     * @param[in] new_lambda false if any evaluation method was previously called with the same values in lambda, true otherwise
     * @param[in] nele_hess the number of nonzero elements in the Hessian
     * @param[in] iRow first call: array of length nele_hess to store the row indices of entries in the Hessian; later calls: NULL
     * @param[in] first call: array of length nele_hess to store the column indices of entries in the Hessian; later calls: NULL
     * @param[in] values first call: NULL; later calls: array of length nele_hess to store the values of the entries in the Hessian
     */
    bool eval_h(Ipopt::Index n, const Ipopt::Number* x, bool new_x, Ipopt::Number obj_factor, Ipopt::Index m,
      const Ipopt::Number* lambda, bool new_lambda, Ipopt::Index nele_hess, Ipopt::Index* iRow,
      Ipopt::Index* jCol, Ipopt::Number* values) final {

        std::vector<Ipopt::Index> sparse_index;
        sparse_index.reserve(nele_hess);
        std::vector<Ipopt::Index> sparse_row_index;
        sparse_row_index.reserve(nele_hess);
        std::vector<Ipopt::Index> sparse_col_index;
        sparse_col_index.reserve(nele_hess);
        if(!_qcqp) {
            //request full hessian
            Ipopt::Index k = 0;
            for(Ipopt::Index j = 0; j < n; ++j) {
                for(Ipopt::Index i = 0; i <= j; ++i) {
                    sparse_row_index.push_back(j);
                    sparse_col_index.push_back(i);
                    sparse_index.push_back(k);
                    ++k;
                }
            }
            eval_h_intern(n, x, new_x, obj_factor, m, lambda, new_lambda, nele_hess, iRow, jCol, sparse_index, sparse_row_index, sparse_col_index, values);

        } else {

            std::vector<bool> is_non_zero = _hessian_structure;
            //request full hessian
            Ipopt::Index k = 0;
            Ipopt::Index o = 0;
            for(Ipopt::Index j = 0; j < n; ++j) {
                for(Ipopt::Index i = 0; i <= j; ++i) {
                    if(is_non_zero[o++]) {
                        sparse_row_index.push_back(j);
                        sparse_col_index.push_back(i);
                        sparse_index.push_back(k);
                        ++k;
                    }
                }
            }
            eval_h_intern(n, x, new_x, obj_factor, m, lambda, new_lambda, nele_hess, iRow, jCol, sparse_index, sparse_row_index, sparse_col_index, values);
        }
        return true;
    }

    /**
     * @brief Function called by Ipopt to communicate the result of the local search
     *
     * @param[in] status Return code of Ipopt (not used since feasibility is checked in ubp.cpp and local optimality is
     * not as important in this case).
     * @param[in] n is the number of optimization variables
     * @param[in] x is a pointer to an array containing the solution point of the local search
     * @param[in] obj_value is the objective function value at the solution point
     * @param[in] z_L not used
     * @param[in] z_U not used
     * @param[in] m not used
     * @param[in] g not used
     * @param[in] lambda not used
     * @param[in] ip_data not used
     * @param[in] ip_cq not used
     */
    void finalize_solution(Ipopt::SolverReturn status, Ipopt::Index n, const Ipopt::Number* x, const Ipopt::Number* z_L,
      const Ipopt::Number* z_U, Ipopt::Index m, const Ipopt::Number* g,
      const Ipopt::Number* lambda, Ipopt::Number obj_value, const Ipopt::IpoptData* ip_data,
      Ipopt::IpoptCalculatedQuantities* ip_cq) final {
        std::copy_n(x, n, _solutionX.begin());
        _solution_f = obj_value;
    }

    /**
     * @brief Function called from the upper bounding wrapper to specify the variable bounds and starting point
     *
     * @param[in] xL is a vector containing the lower bounds on the optimization variables
     * @param[in] xU is a vector containing the upper bounds on the optimization variables
     * @param[in] xStart is a vector containing the starting point to be used in local search
     */
    void set_bounds_and_starting_point(const std::vector<double>& xL, const std::vector<double>& xU,
      const std::vector<double>& xStart) {
        _xL = xL;
        _xU = xU;
        _xStart = xStart;
    }

    std::tuple<double, const ScalarVariableIdManager&, const std::vector<double>&> get_solution() {
        return { _solution_f, _id_manager, _solutionX };
    }

protected:
    /**
     * @name Internal IPOPT Problem variables
     */
    /**@{*/
    // ale related
    dips::program& _prog;        /*!< reference to ale problem */
    ale::symbol_table& _symbols; /*!< reference to ale symbol table */

    // ipopt related
    Ipopt::Index _nineq = 0;           /*!< number of inequalities */
    Ipopt::Index _neq = 0;             /*!< number of equalities */
    double _solution_f = 0;            /*!< solution value */
    std::vector<double> _xL {};        /*!< vector holding lower bounds */
    std::vector<double> _xU {};        /*!< vector holding upper bounds */
    std::vector<double> _xStart {};    /*!< vector holding the initial point */
    std::vector<double> _solutionX {}; /*!< vector holding the solution point */

    bool _qcqp;                                                                                        /*!< flag to enable optimizations when it is known that the problem functions all have constant hessian */
    std::pair<std::vector<double>, std::vector<std::vector<double>>> _fix_hessian_matrices { {}, {} }; /*<! only used in the qcqp case, saves the hessians from the starting point*/
    std::vector<bool> _hessian_structure {};                                                           /*<! only used in the qcqp case, saves the hessian structure from the starting point*/
    std::vector<VarType> _variables {};                                                                /*!< variable vector required for algorithmic differentiation */
    ScalarVariableIdManager _id_manager;
    std::unordered_map<std::string, size_t>
      _variable_base_ids; /*!< index map required for algorithmic differentiation */
    /**@}*/
};

} // namespace dips
