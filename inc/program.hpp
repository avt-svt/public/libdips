#pragma once

#include "expression.hpp"
#include "discretization.hpp"

#include <list>

namespace dips {



struct program {
    std::list<ale::expression<ale::real<0>>> m_objective;
    std::list<ale::expression<ale::boolean<0>>> m_constraints;
    std::list<ale::expression<ale::boolean<0>>> m_relaxations;
    std::list<ale::expression<ale::boolean<0>>> m_squashes;
    std::list<ale::expression<ale::real<0>>> m_outputs;
};



} // namespace dips
