#pragma once

#include <atomic> // for atomic
#include <string> // for string, allocator

#include "solver.hpp"       // for solve_record, solver
#include "symbol_table.hpp" // for symbol_table
#include "helper.hpp"       // for ale
#include "program.hpp"      // for program



namespace dips {


using namespace ale;

class gurobi_solver : public solver {
public:
    explicit gurobi_solver(symbol_table&, std::string name = "gurobi_solver");

    // return a new heap allocated gurobi_solver
    static solver* make_solver(symbol_table& symbols, std::string gurobi_solver_name);

    // try to solve the model and return a solve_record of the solution
    solve_record solve(program prog) final;

    // aborts (all) optimizations currently in progress
    void abort() final;

    // set option of name 'option' ('abs_tol', 'rel_tol', 'num_threads', 'lp_alg', 'int_tol', 'settings') to value
    // returns false if the option name  is not valid or the value is outside the accepted range
    // otherwise it returns true
    bool set_option(std::string option, double value) final;
    bool set_option(std::string option, std::string value) final;
    double get_option(std::string option) final;

private:
    symbol_table& symbols;
    std::string solver_name;

    std::atomic<bool> abort_optimization = false;

    std::string settings_file = "settings.txt";
    double abs_tol = 1e-6;
    double rel_tol = 1e-4;
    int num_threads = 1;
    int lp_alg = -1;
    double int_tol = 1e-5;
    int non_convex = -1;
    double time_limit = 1e100;
    int solution_limit = 2000000000;
    bool int_focus = true;
};

} // namespace dips
