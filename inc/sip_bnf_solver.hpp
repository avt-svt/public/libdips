#pragma once

#include <functional> // for function
#include <utility>    // for move, pair
#include <iosfwd>     // for istream
#include <string>     // for string, allocator, basic_string
#include <vector>     // for vector
#include <atomic>     // for atomic

#include "common.hpp"
#include "solver.hpp"         // for set_tolerance_robust_base, solve_record, solver
#include "parser_chain.hpp"   // for programdefinition_parser_chain
#include "discretization.hpp" // for discretization
#include "helper.hpp"         // for ale
#include "program.hpp"        // for program
#include "symbol_table.hpp"   // for symbol_table



namespace dips {



using namespace ale;

class sip_bnf_program_definition_parser : public programdefinition_parser_chain {
public:
    sip_bnf_program_definition_parser(std::istream&, symbol_table&);
};

class sip_bnf_solver {
    // algorithm statement based on
    // Blankenship, J. W.; Falk, James E. (1976):
    // Infinitely constrained optimization problems.
    // In J. Optim. Theory. Appl. 19 (2), pp. 261-281.
    // DOI: 10.1007/BF00934096.
    // with extensions of the lower bounding procedure
    // of algorithm 3.1 in Djelassi, H.:
    // Discretization-Based Algorithms for the Global
    // Solution of Hierarchical Programs.
    // Dissertation, RWTH Aachen University (2020)
    // Extensions:
    //  - support multiple llps
    //  - discretization of all or only single llp
    //    possible
    //  - the termination critereon is checked more
    //    often / at different place(s)
    //  - iterations are differently defined
public:
    struct program_info {
        void add_llp(const program&, const discretization&);

        program lbp;
        std::vector<program> llps;
        std::vector<discretization> lbp_discs;

        std::map<std::string, std::string> lbp_info;
        std::map<std::string, std::vector<std::string>> lbp_info_list;
    };

    sip_bnf_solver(symbol_table& symbols, std::function<solver*(symbol_table&, std::string)> make_subsolver, std::string solver_name = "sip_output_bnf/") :
        symbols(symbols),
        make_subsolver(std::move(make_subsolver)),
        solver_base_name(std::move(solver_name)) {};
    solve_record solve(program_info);
    bool set_option(const std::string&, double);
    bool set_option(const std::string&, const std::string&);
    bool set_option(const std::string&, bool);
    void set_tolerance_robust(std::string option, double value) { set_tolerance_robust_base(*this, std::move(option), value); };
    void pass_option(const std::string&, double);
    [[nodiscard]] double get_option(const std::string&) const;
    void set_solver_base_name(std::string);
    std::string get_solver_base_name();

private:
    std::function<solver*(symbol_table&, std::string)> make_subsolver;
    symbol_table& symbols;
    std::string solver_base_name;
    std::vector<std::pair<std::string, double>> options;

    std::string settings_lbp = "settings_lbp.txt";
    std::string settings_lbp_llp = "settings_llp.txt";


    double feas_tol = 1e-2; // must be >= 0

    double abs_tol_lbp = 1e-1 * feas_tol;
    double rel_tol_lbp = 1e-1 * feas_tol;
    double abs_tol_lbp_llp = 1e-2 * feas_tol;
    double rel_tol_lbp_llp = 1e-2 * feas_tol;

    double red_tol_lbp_llp = 1.25; // must be > 1

    int max_iter = 1000;
    double max_time = 86400;

    bool discr_all = false;
    std::atomic<bool> aborted = false;
};



} // namespace dips
