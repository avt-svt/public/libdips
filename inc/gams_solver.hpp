#pragma once

#include "solver.hpp"
#include "expression.hpp"
#include "symbol_table.hpp"
#include "symbol_finder.hpp"
#include "util/evaluator.hpp"
#include "util/expression_utils.hpp"
#include "symbol_printer.hpp"
#include "gams.h"

#include <algorithm>
#include <deque>
#include <exception>
#include <numeric>
#include <sstream>
#include <tuple>
#include <vector>



namespace dips {



using namespace ale;
using namespace ale::util;

class gams_solver : public solver {
public:
    gams_solver(symbol_table& symbols, const std::string& gams_solver_name = "gams/") :
        symbols(symbols),
        solver(gams_solver_name) {};
    static solver* make_solver(symbol_table& symbols, const std::string& gams_solver_name = "gams/") {
        return new gams_solver(symbols, gams_solver_name);
    }
    solve_record solve(program prog) final;
    bool set_option(std::string, double) final;
    bool set_option(std::string, std::string) final;
    double get_option(std::string) final;
    static void print_GAMSExceptionExecution(gams::GAMSExceptionExecution& ex);
    static bool check_gams_solver_status(int);
    static void copy_gams_model_status(int model_status, solve_record& rec);

private:
    symbol_table& symbols;
    double abs_tol = 0;            // default value in GAMS: 0
    double rel_tol = 1.0e-04;      // default value in GAMS: 1.0e-04
    double max_time = 10000000000; // default value in GAMS: 10000000000
    int opt_file = 1;
    int decimals = 8;     // max value 8
    int gams_threads = 1; // default value in GAMS: 1;  -n = reserve n cores; 0 = use all available cores; n = use n cores
    std::string problem_class = "minlp";
    std::string solver_type = "baron";
    bool flattening = false;
};

template <unsigned IDim>
std::string entry_name(const std::string& base, size_t* indexes, char delimiter) {
    return base + delimiter + ale::helper::serialize_indexes<IDim>(indexes, delimiter);
}

namespace helper {

    struct validshape_visitor {

        template <typename TType>
        bool operator()(value_symbol<TType>* sym) {
            return std::visit(*this, sym->get_value_variant());
        }

        template <typename TType>
        bool operator()(TType* /*sym*/) {
            return true;
        }

        bool operator()(parameter_symbol<real<0>>* /*sym*/) {
            return true;
        }

        template <unsigned IDim>
        bool operator()(parameter_symbol<real<IDim>>* sym) {
            for(int i = 0; i < IDim; i++) {
                if(sym->m_value.shape(i) == 0) {
                    return false;
                }
            }
            return true;
        }
    };

} // namespace helper

inline bool has_validshape(base_symbol* sym) {
    return call_visitor(helper::validshape_visitor(), sym);
}

class gams_symbol_reader {
public:
    gams_symbol_reader(gams::GAMSJob& job, bool flattened) :
        m_job(job), flattened(flattened) {};

    // base_symbol dispatch
    void dispatch(base_symbol* sym) {
        std::visit(*this, sym->get_base_variant());
    }

    template <typename TType>
    void operator()(value_symbol<TType>* sym) {
        std::visit(*this, sym->get_value_variant());
    }

    // symbol visits
    template <typename TType>
    void operator()(parameter_symbol<TType>* sym) {
        throw std::invalid_argument("cannot read non-real symbol from GAMS. Symbol with name \"" + sym->m_name + "\"");
    }

    template <unsigned IDim>
    void operator()(parameter_symbol<real<IDim>>* sym) {
        size_t indexes[IDim];
        for(int i = 0; i < IDim; ++i) {
            indexes[i] = 0;
        }
        if(flattened) {
            while(indexes[0] < sym->m_value.shape(0)) {
                try {
                    double gams_var_level = m_job.outDB().getVariable(entry_name<IDim>(sym->m_name, indexes, '_')).findRecord().level();
                    double gams_var_upper = m_job.outDB().getVariable(entry_name<IDim>(sym->m_name, indexes, '_')).findRecord().upper();
                    double gams_var_lower = m_job.outDB().getVariable(entry_name<IDim>(sym->m_name, indexes, '_')).findRecord().lower();
                    if(gams_var_level < gams_var_lower || gams_var_level > gams_var_upper) {
                        sym->m_value[indexes] = (gams_var_upper - gams_var_lower) / 2 + gams_var_lower;
                        std::cout << "warning: level of \"" << sym->m_name << "[" << ale::helper::serialize_indexes<IDim>(indexes, ',')
                                  << "]\" retrieved from GAMS is not within bounds: violated lb <= level <= ub: "
                                  << gams_var_lower << " <= " << gams_var_level << " <= " << gams_var_upper
                                  << ". Possible reason: variable does not exist in problem. Setting level to (ub-lb)/2 + lb = "
                                  << sym->m_value[indexes] << '\n';
                    } else {
                        sym->m_value[indexes] = gams_var_level;
                    }
                } catch(const gams::GAMSException& ex) {
                    sym->m_value[indexes] = 0;
                    std::cout << "warning (GAMS interface): could not retrieve level of \""
                              << sym->m_name << "[" << ale::helper::serialize_indexes<IDim>(indexes, ',')
                              << "]\". setting level to 0.\n";
                }
                for(int i = IDim - 1; i >= 0; --i) {
                    if(++indexes[i] < sym->m_value.shape(i)) {
                        break;
                    } else if(i != 0) {
                        indexes[i] = 0;
                    }
                }
            }
        } else { // unflattened
            auto records = m_job.outDB().getVariable(sym->m_name);
            auto it = records.begin();
            while(indexes[0] < sym->m_value.shape(0)) {
                try {
                    double gams_var_level = (*it).level();
                    double gams_var_upper = (*it).upper();
                    double gams_var_lower = (*it).lower();
                    if(gams_var_level < gams_var_lower || gams_var_level > gams_var_upper) {
                        sym->m_value[indexes] = (gams_var_upper - gams_var_lower) / 2 + gams_var_lower;
                        std::cout << "warning: level of \"" << sym->m_name << "[" << ale::helper::serialize_indexes<IDim>(indexes, ',')
                                  << "]\" retrieved from GAMS is not within bounds: violated lb <= level <= ub: "
                                  << gams_var_lower << " <= " << gams_var_level << " <= " << gams_var_upper
                                  << ". Possible reason: variable does not exist in problem. Setting level to (ub-lb)/2 + lb = "
                                  << sym->m_value[indexes] << '\n';
                    } else {
                        sym->m_value[indexes] = gams_var_level;
                    }
                    ++it;
                } catch(const gams::GAMSException& ex) {
                    sym->m_value[indexes] = 0;
                    std::cout << "warning (GAMS interface): could not retrieve level of \""
                              << sym->m_name << "[" << ale::helper::serialize_indexes<IDim>(indexes, ',')
                              << "]\". setting level to 0.\n";
                }
                for(int i = IDim - 1; i >= 0; --i) {
                    if(++indexes[i] < sym->m_value.shape(i)) {
                        break;
                    } else if(i != 0) {
                        indexes[i] = 0;
                    }
                }
            }
        }
    }

    void operator()(parameter_symbol<real<0>>* sym) {
        double gams_var_level = m_job.outDB().getVariable(sym->m_name).findRecord().level();
        double gams_var_upper = m_job.outDB().getVariable(sym->m_name).findRecord().upper();
        double gams_var_lower = m_job.outDB().getVariable(sym->m_name).findRecord().lower();
        if(gams_var_level < gams_var_lower || gams_var_level > gams_var_upper) {
            sym->m_value = (gams_var_upper - gams_var_lower) / 2 + gams_var_lower;
            std::cout << "warning: level of \"" << sym->m_name
                      << "\" retrieved from GAMS is not within bounds: violated lb <= level <= ub: "
                      << gams_var_lower << " <= " << gams_var_level << " <= " << gams_var_upper
                      << ". Possible reason: variable does not exist in problem. Setting level to (ub-lb)/2 + lb = "
                      << sym->m_value << '\n';
        } else {
            sym->m_value = gams_var_level;
        }
    }

    template <typename TType>
    void operator()(variable_symbol<TType>* /*sym*/) {
        std::string type = typeid(TType).name();
        throw std::invalid_argument("cannot read to variable symbol of TType \"" + type + "\"");
    }

    template <typename TType>
    void operator()(function_symbol<TType>* sym) {
        std::string type = typeid(TType).name();
        throw std::invalid_argument("cannot read function symbol of TType \"" + type + "\" and name \"" + sym->m_name + "\"");
    }

private:
    gams::GAMSJob& m_job;
    bool flattened;
};

} // namespace dips
