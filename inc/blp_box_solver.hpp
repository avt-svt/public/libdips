#pragma once

#include <atomic>     // for atomic
#include <functional> // for function
#include <utility>    // for move, pair
#include <iosfwd>     // for istream
#include <string>     // for string, allocator, basic_string
#include <vector>     // for vector

#include "common.hpp"
#include "solver.hpp"         // for set_tolerance_robust_base, solve_record, solver
#include "parser_chain.hpp"   // for programdefinition_parser_chain
#include "discretization.hpp" // for discretization
#include "helper.hpp"         // for ale
#include "program.hpp"        // for program
#include "symbol_table.hpp"   // for symbol_table

namespace dips {



using namespace ale;

class blp_box_program_definition_parser : public programdefinition_parser_chain {
public:
    blp_box_program_definition_parser(std::istream&, symbol_table&);
};

class blp_box_solver {
    // algorithm statement based on algorithm 1
    // in Mitsos, Alexander; Lemonidis, Panayiotis; Barton, Paul I. (2008):
    // Global solution of bilevel programs with a nonconvex inner program.
    // In: J. Global Optim. 42 (4), S. 475�513.
    // DOI: 10.1007/s10898-007-9260-z.
    // Extensions:
    //  - TODO: extend to multiple followers.
    //  - TODO: currently we always add the binary variables for the logigal constrained.
    //          - additionally the boxes are oversized if they coincide with the boundary of the upper level
    //            variable in order to fix the logical error introduced by always adding the binary variables
    //  - TODO: allow multiple upper level variables
    //  - TODO: decide how non-existing coupling equality constraints should be handeld. Possible solutions
    //          - add dummy constraint which is always true, i.e., p = -1
    //          - add a flag in the def.txt
    //          - if llp_aux_v.txt cannot be found, add flag and skip that part of the algorithm

public:
    struct program_info {
        void add_llp(program, discretization);

        program lbp;
        std::vector<std::string> lbp_variable;
        std::vector<std::string> box_parameter;
        std::vector<std::string> h_max_names;
        std::vector<double> h_max_values;

        program ubp;
        std::string ubp_lbd;
        std::string ubp_eps_h;

        std::vector<program> aux;
        std::vector<std::string> aux_hstar;
        std::vector<std::string> aux_eps_h2;

        std::vector<program> aux_v;

        std::vector<program> llps;
        std::vector<program> h_max;
        std::vector<discretization> lbp_discs;
        std::map<std::string, std::string> lbp_info, ubp_info, aux_info;
        std::map<std::string, std::vector<std::string>> lbp_info_list, ubp_info_list, aux_info_list;
    };

    blp_box_solver(symbol_table& symbols, std::function<solver*(symbol_table&, std::string)> make_subsolver, std::string solver_name = "blp_output_box/") :
        symbols(symbols),
        make_subsolver(make_subsolver),
        solver_base_name(std::move(solver_name)) {};
    solve_record solve(program_info);
    bool set_option(std::string, double);
    bool set_option(std::string, std::string);
    bool set_option(std::string, bool);
    void set_tolerance_robust(std::string option, double value) { set_tolerance_robust_base(*this, option, value); };
    void pass_option(std::string, double);
    [[nodiscard]] double get_option(std::string) const;
    void set_solver_base_name(std::string);

    static void box_generator(std::string&, std::string&, double, const discretization&, const solve_record&, symbol_table&);

private:
    std::function<solver*(symbol_table&, std::string)> make_subsolver;
    std::vector<std::function<void()>> abort_subsolver_callbacks;
    symbol_table& symbols;
    std::string solver_base_name;
    std::vector<std::pair<std::string, double>> options;

    std::string settings_lbp = "settings_lbp.txt";
    std::string settings_ubp = "settings_ubp.txt";
    std::string settings_lbp_llp = "settings_llp.txt";
    std::string settings_lbp_llp_aux = "settings_llp_aux.txt";
    std::string settings_lbp_llp_aux_v = "settings_llp_aux_v.txt";

    double abs_tol = 1e-3;
    double rel_tol = 1e-2;
    double feas_tol = 1e-4;


    double abs_tol_lbp_llp = 0.5e-2 * abs_tol; // must be smaller than 1/2*abs_tol, abs_tol_ubp_eps_h
    const double rel_tol_lbp_llp = 1e-9;       //we have strict requirements on the abs_tol


    double abs_tol_ubp_eps_h = 1e-2 * abs_tol;  // must be > 0
    double abs_tol_aux_eps_h2 = 1e-2 * abs_tol; // must be > 0, but bigger than abs_tol_lbp_llp

    double abs_tol_aux = 1e-2 * abs_tol;
    double rel_tol_aux = 1e-2 * rel_tol;

    double abs_tol_aux_v = 1e-2 * abs_tol;
    double rel_tol_aux_v = 1e-2 * rel_tol;

    int max_iter = 1000;
    double max_time = 86400;
    bool discr_all = false; // TODO: this is currently not supported because only one llp is allowed.


    double init_d_box = 1; // must be element (0, 1]
    double d_box = init_d_box;
    double red_d = 0.5;      // must be element < 1
    double min_d_box = 1e-6; // if d_box is reduced to a value < min_d_box the algorithm aborts

    std::atomic<bool> aborted = false;
};



} // namespace dips
