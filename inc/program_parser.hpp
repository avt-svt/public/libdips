#pragma once

#include <iosfwd> // for istream

#include "parser.hpp"       // for parser, parser::had_error
#include "program.hpp"      // for program
#include "symbol_table.hpp" // for symbol_table


namespace dips {



class program_parser : private ale::parser {
public:
    program_parser(std::istream&, ale::symbol_table&);

    using ale::parser::parser;

    program parse();

    using ale::parser::clear;
    using ale::parser::fail;
    using ale::parser::forbid_expressions;
    using ale::parser::forbid_keywords;
    using ale::parser::had_error;
    using ale::parser::print_errors;

private:
    void parse_definitions();
    void parse_objective(program&);
    void parse_constraints(program&);
    void parse_relaxations(program&);
    void parse_squashes(program&);
    void parse_outputs(program&);
    void parse_programdefinitions(program&);
    void recover_block();
};



} // namespace dips
