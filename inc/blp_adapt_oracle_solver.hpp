#pragma once

#include <atomic>     // for atomic
#include <functional> // for function
#include <utility>    // for move, pair
#include <iosfwd>     // for istream
#include <string>     // for string, allocator, basic_string
#include <vector>     // for vector

#include "common.hpp"
#include "solver.hpp"         // for set_tolerance_robust_base, solve_record, solver
#include "parser_chain.hpp"   // for programdefinition_parser_chain
#include "discretization.hpp" // for discretization
#include "helper.hpp"         // for ale
#include "program.hpp"        // for program
#include "symbol_table.hpp"   // for symbol_table

namespace dips {



using namespace ale;

class blp_adapt_oracle_program_definition_parser : public programdefinition_parser_chain {
public:
    blp_adapt_oracle_program_definition_parser(std::istream&, symbol_table&);
};

class blp_adapt_oracle_solver {

public:
    struct program_info {
        void add_llp(program, program, discretization);

        program lbp;
        std::vector<std::string> ulp_variables;

        program ora;
        std::string obj_target_name;
        double init_ubd = std::numeric_limits<double>::infinity();
        double init_lbd = -std::numeric_limits<double>::infinity();

        std::vector<program> llps;

        std::vector<program> ora_auxs;
        std::vector<std::string> ora_aux_hstar;
        std::vector<std::string> ora_aux_alpha;

        std::vector<discretization> ora_discs;
        std::map<std::string, std::string> lbp_info, ora_info, ora_aux_info;
        std::map<std::string, std::vector<std::string>> lbp_info_list, ora_info_list, ora_aux_info_list;
    };

    blp_adapt_oracle_solver(symbol_table& symbols, std::function<solver*(symbol_table&, std::string)> make_subsolver, std::string solver_name = "blp_output_adapt_oracle/") :
        symbols(symbols),
        make_subsolver(make_subsolver),
        solver_base_name(std::move(solver_name)) {};
    solve_record solve(program_info);
    bool set_option(std::string, double);
    bool set_option(std::string, std::string);
    bool set_option(std::string, bool);
    void set_tolerance_robust(std::string option, double value) { set_tolerance_robust_base(*this, option, value); };
    void pass_option(std::string, double);
    [[nodiscard]] double get_option(std::string) const;
    void set_solver_base_name(std::string);

private:
    std::function<solver*(symbol_table&, std::string)> make_subsolver;
    symbol_table& symbols;
    std::string solver_base_name;
    std::vector<std::pair<std::string, double>> options;

    std::string settings_lbp = "settings_lbp.txt";
    std::string settings_lbp_llp = "settings_llp.txt";
    std::string settings_ora = "settings_ora.txt";
    std::string settings_ora_llp = "settings_llp.txt";
    std::string settings_llp_ora_aux = "settings_aux.txt";

    double obj_target = std::numeric_limits<double>::quiet_NaN();

    double feas_tol = 0; // must be >= 0
    double abs_tol = 1e-2;
    double rel_tol = 1e-2;

    double abs_tol_lbp = 1e-1 * abs_tol;
    double rel_tol_lbp = 1e-1 * rel_tol;
    double abs_tol_lbp_llp = 1e-2 * abs_tol;
    double rel_tol_lbp_llp = 1e-2 * rel_tol;

    double abs_tol_ora = 1e-1 * abs_tol;
    double rel_tol_ora = 1e-1 * rel_tol;
    double abs_tol_ora_llp = 1e-2 * abs_tol;
    double rel_tol_ora_llp = 1e-2 * rel_tol;
    double abs_tol_ora_eps_h = abs_tol_ora_llp; // must be > 0

    double abs_tol_llp_ora_aux = 1e-2 * abs_tol;
    double rel_tol_llp_ora_aux = 1e-2 * rel_tol;

    double init_alpha = 1 - abs_tol_ora_eps_h; // inital alpha parameter; must be < 1
    double alpha = init_alpha;                 // alpha parameter
    double red_alpha = 1.5;                    // reduction parameter of alpha; must be > 1
    double min_alpha = 1e-9;                   // if alpha is reduced to a value such that alpha*gu_max < min_alpha the algorithm aborts

    int max_iter = 1000;
    double max_time = 86400;

    bool discr_all = false;
    std::atomic<bool> aborted = false;
};



} // namespace dips
