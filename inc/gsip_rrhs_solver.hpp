#pragma once

#include <functional> // for function
#include <utility>    // for move, pair
#include <iosfwd>     // for istream
#include <map>        // for map
#include <string>     // for string, allocator, basic_string
#include <vector>     // for vector
#include <atomic>     // for atomic

#include "common.hpp"
#include "solver.hpp"         // for set_tolerance_robust_base, solve_record, solver
#include "parser_chain.hpp"   // for programdefinition_parser_chain
#include "discretization.hpp" // for discretization
#include "helper.hpp"         // for ale
#include "program.hpp"        // for program
#include "symbol_table.hpp"   // for symbol_table



namespace dips {



using namespace ale;

class gsip_rrhs_program_definition_parser : public programdefinition_parser_chain {
public:
    gsip_rrhs_program_definition_parser(std::istream&, symbol_table&);
};

class gsip_rrhs_solver {
    // algorithm statement based on algorithm 1
    // in Mitsos, A., Tsoukalas, A.:
    // Global optimization of generalized semi-
    // infinite programs via restriction of the
    // right hand side.
    // J.Global Optim. (2015).
    // https ://doi.org/10.1007/s10898-014-0146-6
    // Extensions:
    //  - discretization of all or only single llp
    //    possible
    //  - added feasibility tolerance
    //  - added tolerance update
    // Simplifications:
    //  - one eps^L forall constraings g_j^L
public:
    struct program_info {
        void add_llp(const program&, const program&, const discretization&, const discretization&);

        program lbp;
        program ubp;
        std::vector<program> llps;
        std::vector<program> auxs;
        std::vector<discretization> lbp_discs;
        std::vector<discretization> ubp_discs;
        std::string ubp_restrict_u;
        std::string ubp_restrict_l;
        std::string aux_alpha;
        std::string aux_g_u;

        std::map<std::string, std::string> lbp_info, ubp_info, aux_info;
        std::map<std::string, std::vector<std::string>> lbp_info_list, ubp_info_list, aux_info_list;
    };

    gsip_rrhs_solver(symbol_table& symbols, std::function<solver*(symbol_table&, std::string)> make_subsolver, std::string solver_name = "gsip_output_rrhs/") :
        symbols(symbols),
        make_subsolver(std::move(make_subsolver)),
        solver_base_name(std::move(solver_name)) {};
    solve_record solve(program_info);
    bool set_option(const std::string&, double);
    bool set_option(const std::string&, const std::string&);
    bool set_option(const std::string&, bool);
    void set_tolerance_robust(std::string option, double value) { set_tolerance_robust_base(*this, std::move(option), value); };
    void pass_option(const std::string&, double);
    [[nodiscard]] double get_option(const std::string&) const;
    void set_solver_base_name(std::string);
    std::string get_solver_base_name();

private:
    std::function<solver*(symbol_table&, std::string)> make_subsolver;
    symbol_table& symbols;
    std::string solver_base_name;
    std::vector<std::pair<std::string, double>> options;

    std::string settings_lbp = "settings_lbp.txt";
    std::string settings_ubp = "settings_ubp.txt";
    std::string settings_lbp_llp = "settings_llp.txt";
    std::string settings_ubp_llp = "settings_llp.txt";
    std::string settings_lbp_aux = "settings_aux.txt";


    double feas_tol = 0; // must be >= 0
    double abs_tol = 1e-2;
    double rel_tol = 1e-2;

    double abs_tol_lbp = 1e-1 * abs_tol;
    double rel_tol_lbp = 1e-1 * rel_tol;
    double abs_tol_lbp_llp = 1e-2 * abs_tol;
    double rel_tol_lbp_llp = 1e-2 * rel_tol;

    double abs_tol_lbp_aux = 1e-3 * abs_tol;
    double rel_tol_lbp_aux = 1e-3 * rel_tol;

    double abs_tol_ubp = 1e-1 * abs_tol;
    double rel_tol_ubp = 1e-1 * rel_tol;
    double abs_tol_ubp_llp = 1e-2 * abs_tol;
    double rel_tol_ubp_llp = 1e-2 * rel_tol;

    double red_tol_lbp_llp = 1.25; // must be > 1
    double red_tol_ubp_llp = 1.25; // must be > 1

    double init_res_u = 1;         // inital restriction parameter eps_u; must be > 0
    double eps_res_u = init_res_u; // restricion parameter eps_u
    double red_res_u = 2;          // reduction parameter of eps_res_u; must be > 1
    double min_eps_res_u = 1e-9;   // if eps_res_u is reduced to a value < min_eps_res_u the algorithm aborts

    double init_res_l = 1;         // inital restriction parameter eps_u; must be > 0
    double eps_res_l = init_res_l; // restricion parameter eps_u
    double red_res_l = 2;          // reduction parameter of eps_res_u; must be > 1
    double min_eps_res_l = 1e-9;   // if eps_res_l is reduced to a value < min_eps_res_l the algorithm aborts

    double init_alpha = 0.25;  // inital alpha parameter; must be < 1
    double alpha = init_alpha; // alpha parameter
    double red_alpha = 1.2;    // reduction parameter of alpha; must be > 1
    double min_alpha = 1e-9;   // if alpha is reduced to a value such that alpha*gu_max < min_alpha the algorithm aborts

    double tol_llp_slater = 1e-5; // tolerance when to consider x as an llp Slater point.
                                  // x is a Slater point if g^L(x, y*) < - tol_llp_slater.
                                  // tol_llp_slater must   be >= 0
                                  // tol_llp_slater should be >= deltaIneq of the subsolver

    int max_iter = 1000;
    double max_time = 86400;

    bool discr_all = false;
    double ubp_guard = -1;         // ubp is only computed if feas_vio of lbp is less than ubp_guard; to turn of set ubp_guard = -1
    bool init_res_dynamic = false; // if ubp_guard is on: dynamically overwrite eps_res_intit with feas_vio_lbp when the ubp is first computed
    std::atomic<bool> aborted = false;

protected:
    bool check_slater(const solve_record& lbp_rec, program& llp_prog, const solve_record& llp_rec);
};



} // namespace dips
