#pragma once

#include <atomic>     // for atomic
#include <functional> // for function
#include <utility>    // for move, pair
#include <iosfwd>     // for istream
#include <string>     // for string, allocator, basic_string
#include <vector>     // for vector

#include "common.hpp"
#include "solver.hpp"         // for set_tolerance_robust_base, solve_record, solver
#include "parser_chain.hpp"   // for programdefinition_parser_chain
#include "discretization.hpp" // for discretization
#include "helper.hpp"         // for ale
#include "program.hpp"        // for program
#include "symbol_table.hpp"   // for symbol_table


namespace dips {



using namespace ale;

class blp_no_box_program_definition_parser : public programdefinition_parser_chain {
public:
    blp_no_box_program_definition_parser(std::istream&, symbol_table&);
};

class blp_no_box_solver {
public:
    struct program_info {
        program lbp;
        program llp;
        program aux;
        program ubp;
        discretization disc;

        std::string aux_target;
        std::string aux_eps;
        std::string l_obj_val;
        std::string ubp_target;
        std::string ubp_eps;
        std::string current_lbd;
        std::vector<std::string> ul_variables;
        std::map<std::string, std::string> lbp_info, ubp_info, aux_info;
        std::map<std::string, std::vector<std::string>> lbp_info_list, ubp_info_list, aux_info_list;
    };
    blp_no_box_solver(symbol_table& symbols, std::function<solver*(symbol_table&, std::string)> make_subsolver, std::string solver_name = "blp_output_no_box/") :
        symbols(symbols),
        make_subsolver(std::move(make_subsolver)),
        solver_base_name(std::move(solver_name)) {};
    solve_record solve(program_info);
    bool set_option(const std::string&, double);
    bool set_option(const std::string&, const std::string&);
    void set_tolerance_robust(std::string option, double value) { set_tolerance_robust_base(*this, std::move(option), value); };
    void pass_option(const std::string&, double);
    [[nodiscard]] double get_option(const std::string&) const;
    void set_solver_base_name(std::string);
    std::string get_solver_base_name();

private:
    std::function<solver*(symbol_table&, std::string)> make_subsolver;
    std::vector<std::function<void()>> abort_subsolver_callbacks;
    symbol_table& symbols;
    std::string solver_base_name;
    std::vector<std::pair<std::string, double>> options;
    std::string settings_lbp = "settings_lbp.txt";
    std::string settings_ubp = "settings_ubp.txt";
    std::string settings_llp = "settings_llp.txt";
    std::string settings_aux = "settings_aux.txt";
    double abs_tol = 1e-3;
    double rel_tol = 1e-2;
    double feas_tol = 1e-4;

    double abs_tol_aux = 1e-2 * abs_tol;
    double rel_tol_aux = 1e-2 * rel_tol;

    int max_iter = 1000;
    double max_time = 86400;

    std::atomic<bool> aborted = false;
};



} // namespace dips
