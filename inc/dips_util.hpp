/**********************************************************************************
* Copyright (c) 2019 Process Systems Engineering (AVT.SVT), RWTH Aachen University
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* http://www.eclipse.org/legal/epl-2.0.
*
* SPDX-License-Identifier: EPL-2.0
*
**********************************************************************************/

#pragma once

#include "symbol_finder.hpp"                       // for find_variables
#include "util/var_bounds_consistency_checker.hpp" // for variable_bounds_consistency_checker

#include <string>
#include <cstring>
#include <filesystem>
#include <iostream>
#include <set>
#include <sstream>
#include <algorithm>
#include <fstream>

#include <cmath>  // for fabs
#include <cctype> // for isspace

namespace dips::util {



std::filesystem::path getInputPath(int argc, char* argv[], std::filesystem::path default_path) {
    if(argc > 3) {
        throw std::invalid_argument("Too many arguments given.");
    }
    if(argc == 1 || std::strlen(argv[1]) == 0) {
        std::cout << "Using default input path: " << default_path << std::endl;
        return default_path;
    }
    // hotfix: always adding seperator behind output path
    std::string p_str = argv[1];
    p_str += std::filesystem::path::preferred_separator;
    std::filesystem::path input_path = std::filesystem::path(p_str);
    std::cout << "Using user defined input path: " << input_path.generic_string() << std::endl;
    return input_path;
}

std::filesystem::path getOutputPath(int argc, char* argv[], std::filesystem::path default_path) {
    if(argc > 3) {
        throw std::invalid_argument("Too may arguments given.");
    }
    if(argc < 3) {
        std::cout << "Using default output path: " << default_path << std::endl;
        return default_path;
    }
    // hotfix: always adding seperator behind output path
    std::string p_str = argv[2];
    p_str += std::filesystem::path::preferred_separator;
    std::filesystem::path output_path = std::filesystem::path(p_str);
    std::cout << "Using user defined output path: " << output_path.generic_string() << std::endl;
    return output_path;
}



// Function to convert the contents of a comma delimited file to a set of unique strings, trimming newlines
std::set<std::string> read_comma_delimited(const std::filesystem::path& filePath,
  const std::vector<char>& charsToTrim = { '"', '\'' }) {
    std::set<std::string> uniqueStrings;
    std::ifstream file(filePath);

    // Check if the file opened successfully
    if(!file.is_open()) {
        std::cerr << "Error opening file: " << filePath << std::endl;
        return uniqueStrings; // Return an empty set
    }

    std::stringstream ss;
    ss << file.rdbuf(); // Read the file content into the stringstream

    while(ss.good()) {
        std::string substr;
        std::getline(ss, substr, ','); // Read until a comma

        // Trim whitespace and additional specified characters
        substr.erase(std::remove_if(substr.begin(), substr.end(), [&](unsigned char c) {
            return std::isspace(c) || std::find(charsToTrim.begin(), charsToTrim.end(), c) != charsToTrim.end();
        }),
          substr.end());

        if(!substr.empty()) {
            uniqueStrings.insert(substr);
        }
    }

    return uniqueStrings;
}


bool check_objective_value(solve_record rec, double set_ubd, double abs_tol, double rel_tol) {
    std::cout << "\nComparing user-set (" << set_ubd << ") and computed (UBD = " << rec.m_ubd << ", LBD = " << rec.m_lbd << ") objective value:\n";
    if(!(rec.m_status == INFEASIBLE || rec.m_status == GLOBAL)) {
        std::cout << "  Warning: solver status = " << dips::to_string(rec.m_status) << '\n';
    }

    if(rec.m_status == INFEASIBLE || rec.m_ubd >= std::numeric_limits<double>::infinity() || set_ubd >= std::numeric_limits<double>::infinity()) {
        if(rec.m_status == INFEASIBLE && rec.m_ubd >= std::numeric_limits<double>::infinity() && set_ubd >= std::numeric_limits<double>::infinity()) {
            std::cout << "  User-set and computed solution both reported problem to be infeasible.\n";
            return true;
        }
        std::cout << "  Warning: User-set and computed optimal objective value differ.\n";
        return false;
    } else if(std::fabs(set_ubd - rec.m_ubd) <= abs_tol) {
        std::cout << "  User-set and computed optimal objective value within absolute tolerance = " << abs_tol << ".\n";
        if(set_ubd < rec.m_lbd) {
            std::cout << "  Warning: User-set optimal objective value < computed LBD.\n";
        }
        return true;
    } else if((std::fmax(rec.m_ubd, set_ubd) < std::numeric_limits<double>::infinity() && std::fmax(rec.m_ubd, set_ubd) <= std::fmin(rec.m_ubd, set_ubd) + rel_tol * std::fabs(std::fmax(rec.m_ubd, set_ubd)))
              && (std::fmax(rec.m_lbd, set_ubd) < std::numeric_limits<double>::infinity() && std::fmax(rec.m_lbd, set_ubd) <= std::fmin(rec.m_lbd, set_ubd) + rel_tol * std::fabs(std::fmax(rec.m_lbd, set_ubd)))) {
        std::cout << "  User-set and computed optimal objective value within relative tolerance = " << rel_tol << ".\n";
        if(set_ubd < rec.m_lbd) {
            std::cout << "  Warning: User-set optimal objective value < computed LBD.\n";
        }
        return true;
    }

    std::cout << "  Warning: User-set and computed optimal objective value differ.\n";
    return false;
}

bool check_variable_bounds(symbol_table& symbols, std::vector<std::vector<program>> all_progs) {
    std::set<std::string> vars;
    for(std::vector<program> proglist : all_progs) {
        for(program prog : proglist) {
            std::set<std::string> var_prog = find_variables(prog, symbols);
            vars.insert(var_prog.begin(), var_prog.end());
        }
    }

    std::vector<std::string> inconsistent_bounds;
    for(auto sym : vars) {
        auto var_bounds_consistency_checker = ale::util::variable_bounds_consistency_checker(symbols);
        auto inc_bound = var_bounds_consistency_checker.dispatch(symbols.resolve(sym));
        inconsistent_bounds.insert(inconsistent_bounds.end(), inc_bound.begin(), inc_bound.end());
    }
    if(!inconsistent_bounds.empty()) {
        std::cout << "\nWarning: Encounterend lower-bound > upper bound for the following variables:\n";
        for(auto& element : inconsistent_bounds) {
            std::cout << "  " << element << '\n';
        }
        std::cout << '\n';
    }
    return inconsistent_bounds.empty();
}

} // namespace dips::util
