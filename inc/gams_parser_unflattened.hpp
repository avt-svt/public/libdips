#pragma once

#include "solver.hpp"
#include "expression.hpp"
#include "symbol_table.hpp"
#include "symbol_finder.hpp"
#include "util/evaluator.hpp"
#include "util/expression_utils.hpp"
#include "symbol_printer.hpp"
#include "gams_solver.hpp"
#include "gams_parser.hpp"
#include "util/visitor_utils.hpp"

#include <algorithm>
#include <deque>
#include <exception>
#include <fmt/args.h>
#include <fmt/core.h>
#include <fmt/ranges.h>
#include <numeric>
#include <regex>
#include <sstream>
#include <tuple>
#include <vector>

namespace dips {



using namespace ale;
using namespace ale::util;

class gams_parser_unflattened : protected gams_parser {
public:
    virtual ~gams_parser_unflattened() = default;
    gams_parser_unflattened(program& prog, symbol_table& symbols, std::string& solver_type, std::string& problem_class, double decimals) :
        gams_parser(prog, symbols, solver_type, problem_class, decimals) {};
    void parse_gams(std::stringstream& input) final;

private:
};


inline std::vector<std::string> getResultVector(std::vector<std::string> child_result, std::vector<std::string> local_sets, std::string result_string) { //helper method for concatenate: local sets of evaluated child, own added local sets, result string
    child_result[0] = result_string;
    child_result.insert(child_result.end(), local_sets.begin(), local_sets.end());
    return child_result;
}

inline std::vector<std::string> getResultVector(std::vector<std::string> child_result, std::string result_string) { //helper method for concatenate: local sets of evaluated child and result string
    child_result[0] = result_string;
    return child_result;
}

inline std::vector<std::string> getResultVector(std::vector<std::vector<std::string>> child_results, std::string result_string) { //helper method for concatenate: local sets of evaluated children and result string
    std::vector<std::string> result = { result_string };
    for(auto it = child_results.begin(); it != child_results.end(); ++it) {
        result.insert(result.end(), (*it).begin() + 1, (*it).end());
    }
    return result;
}

inline std::string localset_string(int localset_n) {
    return fmt::format("_{}", localset_n);
}

namespace helper {

    struct typecheck_forallnode_visitor {

        template <typename TType>
        bool operator()(TType* /*node*/) {
            return false;
        }

        template <typename TType>
        bool operator()(forall_node<TType>* /*node*/) {
            return true;
        }
    };


} // namespace helper

template <typename TType>
bool is_forallnode(value_node<TType>* node) {
    return call_visitor(helper::typecheck_forallnode_visitor(), node);
}

class expression_stringer_unflattened;

/*
    Given a set node, returns name of set for parameters and string of format "Type {} /SetValues/" for constant nodes (whereas
    in the the blank the name of the set can be inserted.
    */
class set_stringer {
public:
    set_stringer(expression_stringer_unflattened* expr_str, symbol_table& symbols) :
        expr_str(expr_str), m_symbols(symbols) {};

    template <typename TType, unsigned IDim>
    std::string operator()(parameter_node<set<TType, IDim>>* node) {
        return node->name;
    }

    template <typename TType, unsigned IDim>
    std::string operator()(constant_node<set<TType, IDim>>* node);

    template <typename TType>
    std::string operator()(indicator_set_node<TType>* node) {
        auto elements = evaluate_expression(node, m_symbols);
        return dispatch(elements);
    }

    template <typename TType, unsigned IDim>
    std::string operator()(entry_node<set<TType, IDim>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: entry_node<set<TType>,IDim>");
    }

    template <typename TType, unsigned IDim>
    std::string getTensorEntries(tensor<TType, IDim> tensor, int tensor_no, const std::string& entry_delimiter) {
        for(int i = 0; i < IDim; ++i) {
            if(tensor.shape(i) == 0) {
                return "";
            }
        }
        size_t indexes[IDim];
        for(int i = 0; i < IDim; ++i) {
            indexes[i] = 0;
        }
        std::stringstream ss;
        while(indexes[0] < tensor.shape(0)) {
            ss << tensor_no << "." << ale::helper::serialize_indexes<IDim>(indexes, '.') << entry_delimiter << tensor[indexes];
            for(int i = IDim - 1; i >= 0; --i) {
                if(++indexes[i] < tensor.shape(i)) {
                    break;
                } else if(i != 0) {
                    indexes[i] = 0;
                }
            }
            if(indexes[0] < tensor.shape(0)) {
                ss << ",";
            }
        }
        return ss.str();
    }

    template <unsigned IDim>
    std::string dispatch(std::list<tensor<double, IDim>> arg) {
        std::list<std::string> element_strings, indices_strings;
        for(auto it = arg.begin(); it != arg.end(); ++it) {
            element_strings.push_back(getTensorEntries(*it, std::distance(arg.begin(), it) + 1, " ="));
            indices_strings.push_back(std::to_string(std::distance(arg.begin(), it) + 1));
        }
        std::vector<std::string> super_sets(IDim + 1);
        std::fill(super_sets.begin(), super_sets.end(), "*");
        return "Set {}(*) " + fmt::format("/{}/; ", fmt::join(indices_strings, ", ")) + "Parameter {}" + fmt::format("({}) ", fmt::join(super_sets, ",")) + fmt::format("/{}/", fmt::join(element_strings, ", "));
    }

    template <unsigned IDim>
    std::string dispatch(std::list<tensor<int, IDim>> /*arg*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: index tensor set");
    }

    template <unsigned IDim>
    std::string dispatch(std::list<tensor<bool, IDim>> /*arg*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: boolean tensor set");
    }

    static std::string dispatch(std::list<int> arg) {
        // only indexes encountered -> no definition of additional parameters necessary.
        std::list<std::string> indices_strings;
        for(auto it = arg.begin(); it != arg.end(); ++it) {
            indices_strings.push_back(std::to_string(*it));
        }
        return "Set {}(*) " + fmt::format("/{}/; ", fmt::join(indices_strings, ", "));
    }

    template <typename TType>
    std::string dispatch(std::list<TType> arg) {
        std::list<std::string> element_strings, indices_strings;
        for(auto it = arg.begin(); it != arg.end(); ++it) {
            std::string ind = std::to_string(std::distance(arg.begin(), it) + 1);
            element_strings.push_back(fmt::format("{}={}", ind, *it));
            indices_strings.push_back(ind);
        }
        return "Set {}(*) " + fmt::format("/{}/; ", fmt::join(indices_strings, ", ")) + "Parameter {}(*) " + fmt::format("/{}/", fmt::join(element_strings, ", "));
    }

private:
    expression_stringer_unflattened* expr_str;
    symbol_table& m_symbols;
};

class expression_stringer_unflattened {
public:
    expression_stringer_unflattened(symbol_table& symbols) :
        m_symbols(symbols) {};

    // expression dispatch
    template <typename TReturn, typename TType>
    TReturn dispatch(expression<TType>& /*expr*/) {
        std::string type = typeid(TType).name();
        throw unsupportedNodeParsingException("expression dispatch not implemented for GAMS for expression type" + type);
    }

    std::vector<std::string> dispatch(expression<real<0>>& expr) {
        return dispatch(expr.get());
    }

    std::vector<std::string> dispatch(expression<ale::boolean<0>>& expr) {
        return dispatch(expr.get());
    }

    // value_node dispatch
    template <typename TReturn, typename TType>
    TReturn dispatch(value_node<TType>* /*node*/) {
        std::string type = typeid(TType).name();
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: value_node<TType> with TType " + type);
    }

    template <typename TType>
    typename set<TType, 0>::basic_type dispatch(value_node<set<TType, 0>>* node) {
        return evaluate_expression(node, m_symbols);
    }

    template <typename TType>
    typename set<TType, 0>::basic_type dispatch(value_symbol<set<TType, 0>>* node) {
        return evaluate_expression(node, m_symbols);
    }

    template <unsigned IDim>
    std::vector<std::string> dispatch(value_node<real<IDim>>* node) {
        return std::visit(*this, node->get_variant());
    }

    std::vector<std::string> dispatch(value_node<real<0>>* node) {
        return std::visit(*this, node->get_variant());
    }

    std::vector<std::string> dispatch(value_node<ale::index<0>>* node) {
        return std::visit(*this, node->get_variant());
    }

    std::vector<std::string> dispatch(value_node<ale::boolean<0>>* node) {
        return std::vector(std::visit(*this, node->get_variant()));
    }

    // terminal visits
    template <typename TType>
    std::vector<std::string> operator()(constant_node<TType>* /*node*/) {
        std::string type = typeid(TType).name();
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: constant_node<TType> with TType " + type);
    }

    std::vector<std::string> operator()(constant_node<ale::index<0>>* node) {
        return { std::to_string(node->value) };
    }

    std::vector<std::string> operator()(constant_node<real<0>>* node) {
        return { std::to_string(node->value) };
    }

    std::vector<std::string> operator()(constant_node<ale::boolean<0>>* node) {
        return std::vector<std::string>(1, std::to_string(node->value));
    }

    template <typename TType>
    std::vector<std::string> operator()(parameter_node<TType>* node) {
        std::string type = typeid(TType).name();
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: parameter_node<TType> with TType " + type + " and name '" + node->name + "'");
    }

    std::vector<std::string> operator()(parameter_node<ale::index<0>>* node) {
        auto sym = m_symbols.resolve<ale::index<0>>(node->name);
        auto par_sym = dynamic_cast<parameter_symbol<ale::index<0>>*>(sym);
        if(par_sym == nullptr) {
            return { node->name };
        }
        return { par_sym->m_name };
    }

    std::vector<std::string> operator()(parameter_node<real<0>>* node) {
        std::string identifier = node->name;
        auto sym = m_symbols.resolve<real<0>>(identifier);
        auto par_sym = dynamic_cast<parameter_symbol<real<0>>*>(sym);
        if(par_sym != nullptr) { //check if identifier as alternate name
            identifier = par_sym->m_name;
        }
        if(identifier.rfind("realset", 0) == 0) {
            return { fmt::format("{}[{}]", identifier, identifier + "_index") };
        }
        return { identifier };
    }

    template <unsigned IDim>
    std::vector<std::string> operator()(parameter_node<real<IDim>>* node) {
        auto sym = m_symbols.resolve<real<0>>(node->name);
        auto par_sym = dynamic_cast<parameter_symbol<real<0>>*>(sym);
        if(par_sym == nullptr) { //check if identifier as alternate name
            return { node->name };
        }
        return { par_sym->m_name };
    }

    std::vector<std::string> operator()(parameter_node<ale::boolean<0>>* node) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: parameter_node<boolean<0>> with name '" + node->name + "'");
    }



    std::vector<std::string> operator()(index_minus_node* /*node*/) {
        return {};
    }

    std::vector<std::string> operator()(index_addition_node* /*node*/) {
        return {};
    }

    std::vector<std::string> operator()(index_multiplication_node* /*node*/) {
        return {};
    }

    template <unsigned IDim>
    std::tuple<std::string, int> getIdentifier(entry_node<real<IDim>>* node, int depth) {
        auto child_node = dynamic_cast<entry_node<real<IDim + 1>>*>(node->template get_child<0>());
        if(child_node == nullptr) {
            return { dispatch(node->template get_child<0>()).front(), depth };
        }
        return getIdentifier(child_node, depth + 1);
    }

    std::tuple<std::string, int> getIdentifier(entry_node<real<LIBALE_MAX_DIM - 1>>* node, int depth) {
        return { dispatch(node->template get_child<0>()).front(), depth };
    }

    template <unsigned IDim>
    std::vector<std::string> operator()(entry_node<real<IDim>>* node);

    template <unsigned IDim>
    std::vector<std::string> operator()(entry_node<ale::index<IDim>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: entry_node<ale::index<IDim>>");
    }

    std::vector<std::string> operator()(function_node<real<0>>* node) {
        std::list<std::string> args;
        auto duplicate_list = node->children;
        for(auto it = duplicate_list.begin(); it != duplicate_list.end(); ++it) {
            args.emplace_back(dispatch(it->release()).front());
        }
        if(args.empty()) return { node->name };
        return { fmt::format("{}({})", node->name, fmt::join(args, ", ")) };
    }

    template <unsigned IDim>
    std::vector<std::string> operator()(vector_node<real<IDim>>* node) {
        return dispatch(node->template get_child<0>());
    }

    template <typename TType>
    std::vector<std::string> operator()(function_node<TType>* node) {
        std::string type = typeid(TType).name();
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: function_node<TType> called '" + node->name + "' with TType " + type);
    }

    template <unsigned IDim>
    std::vector<std::string> operator()(tensor_node<real<IDim>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: tensor_node<real<IDim>>");
    }

    template <unsigned IDim>
    std::vector<std::string> operator()(index_shift_node<real<IDim>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: index_shift_node<real<IDim>>");
    }

    template <typename TType>
    std::vector<std::string> operator()(attribute_node<TType>* node) {
        std::string type = typeid(TType).name();
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: symbol " + node->variable_name + " has unsupported type \"" + type + "\" in attribute call.");
    }

    template <unsigned IDim>
    std::vector<std::string> operator()(attribute_node<real<IDim>>* node) {
        variable_symbol<real<IDim>>* sym = cast_variable_symbol<real<IDim>>(m_symbols.resolve(node->variable_name));
        if(!sym) {
            throw std::invalid_argument("symbol " + node->variable_name + " has unexpected type in attribute call.");
        }
        auto par_sym = dynamic_cast<parameter_symbol<real<IDim>>*>(sym);
        std::string name;
        if(par_sym == nullptr) { //check if identifier as alternate name
            name = node->variable_name;
        } else {
            name = par_sym->m_name;
        }

        switch(node->attribute) {
            case variable_attribute_type::INIT:
                return { name + ".l" };
            case variable_attribute_type::LB:
                return { name + ".lo" };
            case variable_attribute_type::UB:
                return { name + ".up" };
            case variable_attribute_type::PRIO:
                if(sym->integral()) {
                    return { name + ".prior" };
                } else {
                    throw unsupportedNodeParsingException("prio attribute call on symbol " + node->variable_name + " for non integral non scalars not supported");
                }
            default:
                throw std::invalid_argument("symbol " + node->variable_name + " has unexpected attribute.");
        }
    }

    // non-terminal real node visits
    std::vector<std::string> operator()(minus_node* node) {
        auto child_result = dispatch(node->get_child<0>());
        auto result = fmt::format("(-{})", child_result.front());
        return getResultVector(child_result, result);
    }

    std::vector<std::string> operator()(round_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: round_node");
    }

    std::vector<std::string> operator()(index_to_real_node* node) {
        if(dynamic_cast<constant_node<ale::index<0>>*>(node->get_child<0>()) == nullptr) {
            auto child_result = dispatch(node->get_child<0>());
            return { fmt::format("{}{}[{}]", child_result.front(), "_asreal", child_result.front()) };
        }
        return dispatch(node->get_child<0>());
    }

    std::vector<std::string> operator()(ale::real_to_index_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: real_to_index_node");
    }

    std::vector<std::string> operator()(inverse_node* node) {
        auto child_result = dispatch(node->get_child<0>());
        auto result = fmt::format("(1/{})", child_result.front());
        return getResultVector(child_result, result);
    }

    std::vector<std::string> operator()(addition_node* node) {
        std::vector<std::vector<std::string>> children_results;
        std::string result_string = "(";
        for(auto it = node->children.begin(); it != node->children.end(); ++it) {
            if(it != node->children.begin()) {
                result_string += "+";
            }
            auto child_result = dispatch(it->get());
            result_string += child_result.front();
            children_results.emplace_back(child_result);
        }
        result_string += ")";
        return getResultVector(children_results, result_string);
    }

    std::vector<std::string> operator()(multiplication_node* node) {
        std::vector<std::vector<std::string>> children_results;
        std::string result_string = "(";
        for(auto it = node->children.begin(); it != node->children.end(); ++it) {
            if(it != node->children.begin()) {
                result_string += "*";
            }
            auto child_result = dispatch(it->get());
            result_string += child_result.front();
            children_results.emplace_back(child_result);
        }
        result_string += ")";
        return getResultVector(children_results, result_string);
    }

    std::vector<std::string> operator()(exponentiation_node* node) {
        bool real_exponent = true;
        for(auto it = node->children.begin(); it != node->children.end(); ++it) {
            if(it != node->children.begin()) {
                if(is_tree_constant(it->get(), m_symbols)) {
                    if((int)evaluate_expression(it->get(), m_symbols) == (double)evaluate_expression(it->get(), m_symbols)) {
                        real_exponent = false;
                    }
                }
            }
        }
        if(real_exponent) {
            std::vector<std::vector<std::string>> children_results;
            std::string result_string = "(";
            for(auto it = node->children.begin(); it != node->children.end(); ++it) {
                if(it != node->children.begin()) {
                    result_string += "**";
                }
                std::vector<std::string> child_result = dispatch(it->get());
                children_results.emplace_back(child_result);
                result_string += "(" + child_result.front() + ")";
            }
            result_string += ")";
            return getResultVector(children_results, result_string);
        } else {
            int nchild = node->children.size();
            std::string result_string;
            std::vector<std::vector<std::string>> children_results;
            for(int i = 0; i < (nchild - 1); ++i) {
                result_string += "power(";
            }
            for(auto it = node->children.begin(); it != node->children.end(); ++it) {
                if((it != node->children.end()) && (it != node->children.begin())) {
                    result_string += ",";
                }
                std::vector<std::string> child_result = dispatch(it->get());
                result_string += "(" + child_result.front() + ")";
                if(it != node->children.begin()) {
                    result_string += ")";
                }
                children_results.emplace_back(child_result);
            }
            return getResultVector(children_results, result_string);
        }
    }

    std::vector<std::string> operator()(exp_node* node) {
        std::vector<std::string> child_result = dispatch(node->get_child<0>());
        return getResultVector(child_result, "(exp(" + child_result.front() + "))");
    }

    std::vector<std::string> operator()(xexpy_node* node) {
        std::vector<std::vector<std::string>> children_results = { dispatch(node->get_child<0>()), dispatch(node->get_child<1>()) };
        return getResultVector(children_results, "((" + children_results[0].front() + ")*exp(" + children_results[1].front() + "))");
    }

    std::vector<std::string> operator()(xexpax_node* node) {
        std::vector<std::vector<std::string>> children_results = { dispatch(node->get_child<0>()), dispatch(node->get_child<1>()) };
        return getResultVector(children_results, "((" + children_results[0].front() + ")*exp((" + children_results[0].front() + ")*(" + children_results[1].front() + ")))");
    }

    std::vector<std::string> operator()(log_node* node) {
        std::vector<std::string> child_result = dispatch(node->get_child<0>());
        return getResultVector(child_result, "(log(" + child_result.front() + "))");
    }

    std::vector<std::string> operator()(xlogx_node* node) {
        std::vector<std::string> child_result = dispatch(node->get_child<0>());
        return getResultVector(child_result, "(" + child_result.front() + "*log(" + child_result.front() + "))");
    }

    std::vector<std::string> operator()(xlog_sum_node* node) {

        if(!(node->children.size() % 2 == 0)) {
            throw std::invalid_argument("called xlog_sum with odd number of arguments");
        }
        if(node->children.size() < 2) {
            throw std::invalid_argument("called xlog_sum with less than 2 arguments");
        }
        std::vector<std::string> vars;
        std::vector<std::string> coeff;
        std::vector<std::vector<std::string>> children_results;
        for(auto it = node->children.begin(); it != node->children.end(); ++it) {
            if(is_tree_constant(it->get(), m_symbols)) {
                if(!(evaluate_expression(it->get(), m_symbols) > 0)) {
                    throw std::invalid_argument("called xlog_sum with non-positive argument");
                }
            }
            std::vector<std::string> child_result = dispatch(it->get());
            children_results.emplace_back(child_result);
            if(distance(node->children.begin(), it) < (int)(node->children.size() / 2)) {
                vars.emplace_back(child_result.front());
            } else {
                coeff.emplace_back(child_result.front());
            }
        }
        std::string partial;
        for(int i = 0; i < (int)(node->children.size() / 2); ++i) {
            if(i != 0) {
                partial += "+";
            }
            partial += coeff[i] + "*" + vars[i];
        }
        return getResultVector(children_results, "(" + vars[0] + "*log(" + partial + "))");
    }

    std::vector<std::string> operator()(sqrt_node* node) {
        std::vector<std::string> child_result = dispatch(node->get_child<0>());
        return getResultVector(child_result, "(sqrt(" + child_result.front() + "))");
    }

    std::vector<std::string> operator()(sin_node* node) {
        std::vector<std::string> child_result = dispatch(node->get_child<0>());
        return getResultVector(child_result, "(sin(" + child_result.front() + "))");
    }

    std::vector<std::string> operator()(sinh_node* node) {
        std::vector<std::string> child_result = dispatch(node->get_child<0>());
        return getResultVector(child_result, "(sinh(" + child_result.front() + "))");
    }

    std::vector<std::string> operator()(asin_node* node) {
        std::vector<std::string> child_result = dispatch(node->get_child<0>());
        return getResultVector(child_result, "(arcsin(" + child_result.front() + "))");
    }

    std::vector<std::string> operator()(asinh_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: asinh");
    }

    std::vector<std::string> operator()(cos_node* node) {
        std::vector<std::string> child_result = dispatch(node->get_child<0>());
        return getResultVector(child_result, "(cos(" + child_result.front() + "))");
    }

    std::vector<std::string> operator()(cosh_node* node) {
        std::vector<std::string> child_result = dispatch(node->get_child<0>());
        return getResultVector(child_result, "(cosh(" + child_result.front() + "))");
    }

    std::vector<std::string> operator()(coth_node* node) {
        std::vector<std::string> child_result = dispatch(node->get_child<0>());
        return getResultVector(child_result, "(cosh(" + child_result.front() + ")/" + "sinh(" + child_result.front() + "))");
    }

    std::vector<std::string> operator()(acoth_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: acoth");
    }

    std::vector<std::string> operator()(acos_node* node) {
        std::vector<std::string> child_result = dispatch(node->get_child<0>());
        return getResultVector(child_result, "(arccos(" + child_result.front() + "))");
    }

    std::vector<std::string> operator()(acosh_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: acosh");
    }

    std::vector<std::string> operator()(tan_node* node) {
        std::vector<std::string> child_result = dispatch(node->get_child<0>());
        return getResultVector(child_result, "(tan(" + child_result.front() + "))");
    }

    std::vector<std::string> operator()(tanh_node* node) {
        std::vector<std::string> child_result = dispatch(node->get_child<0>());
        return getResultVector(child_result, "(tanh(" + child_result.front() + "))");
    }

    std::vector<std::string> operator()(atan_node* node) {
        std::vector<std::string> child_result = dispatch(node->get_child<0>());
        return getResultVector(child_result, "(arctan(" + child_result.front() + "))");
    }

    std::vector<std::string> operator()(atanh_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: atanh");
    }

    std::vector<std::string> operator()(arh_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: arh");
    }

    std::vector<std::string> operator()(lmtd_node* node) {
        std::vector<std::string> dT1 = dispatch(node->get_child<0>());
        std::vector<std::string> dT2 = dispatch(node->get_child<1>());
        return getResultVector({ dT1, dT2 }, "((" + dT1.front() + "-" + dT2.front() + ")/log(" + dT1.front() + "/" + dT2.front() + "))");
    }

    std::vector<std::string> operator()(rlmtd_node* node) {
        std::vector<std::string> dT1 = dispatch(node->get_child<0>());
        std::vector<std::string> dT2 = dispatch(node->get_child<1>());
        return getResultVector({ dT1, dT2 }, "(log(" + dT1.front() + "/" + dT2.front() + ")/(" + dT1.front() + "-" + dT2.front() + "))");
    }

    std::vector<std::string> operator()(cost_turton_node* node) {
        std::vector<std::string> x = dispatch(node->get_child<0>());
        std::vector<std::string> p1 = dispatch(node->get_child<1>());
        std::vector<std::string> p2 = dispatch(node->get_child<2>());
        std::vector<std::string> p3 = dispatch(node->get_child<3>());
        std::cout << "Warning cost_turton: Base of real power (**) ist not allowed to be negative"
                  << "\n";
        return getResultVector({ x, p1, p2, p3 }, "((" + std::to_string(10.) + "**" + p1.front() + "+" + p2.front() + "*log(" + x.front() + ")/log(" + std::to_string(10.) + ")+" + p3.front() + "*power(log(" + x.front() + ")/log(" + std::to_string(10.) + "),2)))");
    }

    std::vector<std::string> operator()(abs_node* node) {
        std::vector<std::string> child_result = dispatch(node->get_child<0>());
        return getResultVector(child_result, "(abs(" + child_result.front() + "))");
    }

    std::vector<std::string> operator()(pos_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: pos_node");
    }

    std::vector<std::string> operator()(neg_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: neg_node");
    }

    std::vector<std::string> operator()(lb_func_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: lb_func_node");
    }

    std::vector<std::string> operator()(ub_func_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: ub_func_node");
    }

    std::vector<std::string> operator()(bounding_func_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: bounding_func_node");
    }

    std::vector<std::string> operator()(ale::squash_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: squash_node");
    }

    std::vector<std::string> operator()(xabsx_node* node) {
        std::vector<std::string> child_result = dispatch(node->get_child<0>());
        return getResultVector(child_result, "((" + child_result.front() + ")*(abs(" + child_result.front() + ")))");
    }

    template <typename TType>
    std::vector<std::string> operator()(set_max_node<TType>* node) {
        auto value = evaluate_expression(node, m_symbols);
        return { std::to_string(value) };
    }

    template <typename TType>
    std::vector<std::string> operator()(set_min_node<TType>* node) {
        auto value = evaluate_expression(node, m_symbols);
        return { std::to_string(value) };
    }

    std::vector<std::string> operator()(min_node* node) {
        if(node->children.empty()) {
            throw std::invalid_argument("called min_node with no children");
        }
        std::string result_string = "(";
        if(node->children.size() > 1) {
            result_string += "min(";
        }
        std::vector<std::vector<std::string>> children_results;
        for(auto it = node->children.begin(); it != node->children.end(); ++it) {
            if(it != node->children.begin()) {
                result_string += ",";
            }
            std::vector<std::string> child_result = dispatch(it->get());
            result_string += child_result.front();
            children_results.emplace_back(child_result);
        }
        if(node->children.size() > 1) {
            result_string += ")";
        }
        result_string += ")";
        return getResultVector(children_results, result_string);
    }

    std::vector<std::string> operator()(max_node* node) {
        if(node->children.empty()) {
            throw std::invalid_argument("called max_node with no children");
        }
        std::string result_string = "(";
        if(node->children.size() > 1) {
            result_string += "max(";
        }
        std::vector<std::vector<std::string>> children_results;
        for(auto it = node->children.begin(); it != node->children.end(); ++it) {
            if(it != node->children.begin()) {
                result_string += ",";
            }
            std::vector<std::string> child_result = dispatch(it->get());
            result_string += child_result.front();
            children_results.emplace_back(child_result);
        }
        if(node->children.size() > 1) {
            result_string += ")";
        }
        result_string += ")";
        return getResultVector(children_results, result_string);
    }

    std::vector<std::string> operator()(mid_node* node) {
        std::vector<std::string> arg1 = dispatch(node->get_child<0>());
        std::vector<std::string> arg2 = dispatch(node->get_child<1>());
        std::vector<std::string> arg3 = dispatch(node->get_child<2>());
        return getResultVector({ arg1, arg2, arg3 }, "(min(max(" + arg1.front() + "," + arg2.front() + "),min(max(" + arg2.front() + "," + arg3.front() + "),max(" + arg3.front() + "," + arg1.front() + "))))");
    }

    std::vector<std::string> operator()(sum_node<ale::index<0>>* node) {
        auto elements = dispatch(node->template get_child<0>());
        if(elements.empty()) {
            return { "0" };
        }
        m_symbols.push_scope();
        std::string set_string = std::visit(set_stringer(this, m_symbols), (node->template get_child<0>())->get_variant());
        std::string index_string = set_string;
        if(set_string.rfind("Set {}(*) /", 0) == 0) {
            index_string = node->name + "{}" + localset_string(localset_n);
            set_string = fmt::format(set_string, index_string, index_string + "_asreal");
            m_symbols.define(node->name, new parameter_symbol<ale::index<0>>(index_string, ale::index<0>::basic_type()));
            auto elements = dispatch(node->template get_child<0>());
            m_symbols.define(index_string, new parameter_symbol<set<ale::index<0>, 0>>(index_string, elements));
            localset_n++;
        } else {
            m_symbols.define(node->name, new parameter_symbol<ale::index<0>>(set_string, ale::index<0>::basic_type()));
        }
        std::vector<std::string> result = dispatch(node->template get_child<1>());
        m_symbols.pop_scope();
        result[0] = fmt::format("sum({},{})", index_string, result[0]);
        result.emplace(result.end(), set_string);
        return result;
    }

    template <unsigned IDim>
    std::vector<std::string> operator()(sum_node<ale::index<IDim>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: sum_node<ale::index<IDim>>");
    }

    template <unsigned IDim>
    std::vector<std::string> operator()(sum_node<real<IDim>>* node) {
        auto elements = dispatch(node->template get_child<0>());
        if(elements.empty()) {
            return { "0" };
        }
        m_symbols.push_scope();
        std::string set_string = std::visit(set_stringer(this, m_symbols), (node->template get_child<0>())->get_variant());
        std::string index_string = set_string;
        if(set_string.rfind("Set {}(*) /", 0) == 0) {
            index_string = fmt::format("realset_{}_index", node->name + "{}" + localset_string(localset_n));
            set_string = fmt::format(set_string, index_string, fmt::format("realset_{}", node->name + "{}"));
            m_symbols.define(node->name, new parameter_symbol<real<0>>(index_string, real<0>::basic_type()));
            localset_n++;
        } else {
            set_string = "realset_" + set_string;
            index_string = fmt::format("realset_{}_index", set_string);
            m_symbols.define(node->name, new parameter_symbol<real<0>>(set_string, real<0>::basic_type()));
        }
        std::vector<std::string> result = dispatch(node->template get_child<1>());
        m_symbols.pop_scope();
        result[0] = fmt::format("sum({},{})", index_string, result[0]);
        result.emplace(result.end(), set_string);
        return result;
    }

    std::vector<std::string> operator()(product_node<ale::index<0>>* node) {
        auto elements = dispatch(node->template get_child<0>());
        if(elements.empty()) {
            return { "1" };
        }
        m_symbols.push_scope();
        std::string set_string = std::visit(set_stringer(this, m_symbols), (node->template get_child<0>())->get_variant());
        std::string index_string = set_string;
        if(set_string.rfind("Set {}(*) /", 0) == 0) {
            index_string = node->name + "{}" + localset_string(localset_n);
            set_string = fmt::format(set_string, index_string, index_string + "_asreal");
            m_symbols.define(node->name, new parameter_symbol<ale::index<0>>(index_string, ale::index<0>::basic_type()));
            auto elements = dispatch(node->template get_child<0>());
            m_symbols.define(index_string, new parameter_symbol<set<ale::index<0>, 0>>(index_string, elements));
            localset_n++;
        } else {
            m_symbols.define(node->name, new parameter_symbol<ale::index<0>>(set_string, ale::index<0>::basic_type()));
        }
        std::vector<std::string> result = dispatch(node->template get_child<1>());
        m_symbols.pop_scope();
        result[0] = fmt::format("prod({},{})", index_string, result[0]);
        result.emplace(result.end(), set_string);
        return result;
    }

    template <unsigned IDim>
    std::vector<std::string> operator()(product_node<ale::index<IDim>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: product_node<ale::index<IDim>>");
    }

    template <unsigned IDim>
    std::vector<std::string> operator()(product_node<real<IDim>>* node) {
        auto elements = dispatch(node->template get_child<0>());
        if(elements.empty()) {
            return { "1" };
        }
        m_symbols.push_scope();
        std::string set_string = std::visit(set_stringer(this, m_symbols), (node->template get_child<0>())->get_variant());
        std::string index_string = set_string;
        if(set_string.rfind("Set {}(*) /", 0) == 0) {
            index_string = fmt::format("realset_{}_index", node->name + "{}" + localset_string(localset_n));
            set_string = fmt::format(set_string, index_string, fmt::format("realset_{}", node->name + "{}"));
            m_symbols.define(node->name, new parameter_symbol<real<0>>(index_string, real<0>::basic_type()));
            localset_n++;
        } else {
            set_string = "realset_" + set_string;
            index_string = fmt::format("realset_{}_index", set_string);
            m_symbols.define(node->name, new parameter_symbol<real<0>>(set_string, real<0>::basic_type()));
        }
        std::vector<std::string> result = dispatch(node->template get_child<1>());
        m_symbols.pop_scope();
        result[0] = fmt::format("prod({},{})", index_string, result[0]);
        result.emplace(result.end(), set_string);
        return result;
    }

    std::vector<std::string> operator()(norm2_node* node) {
        std::vector<std::string> arg1 = dispatch(node->get_child<0>());
        std::vector<std::string> arg2 = dispatch(node->get_child<1>());
        return getResultVector({ arg1, arg2 }, "(sqrt(power(" + arg1.front() + ",2)+power(" + arg2.front() + ",2)))");
    }

    std::vector<std::string> operator()(sum_div_node* node) {
        if(node->children.size() % 2 == 0) {
            throw std::invalid_argument("called sum_div with even number of arguments");
        }
        if(node->children.size() < 3) {
            throw std::invalid_argument("called sum_div with less than 3 arguments");
        }
        std::vector<std::string> vars;
        std::vector<std::string> coeff;
        std::vector<std::vector<std::string>> children_results;
        for(auto it = node->children.begin(); it != node->children.end(); ++it) {
            if(is_tree_constant(it->get(), m_symbols)) {
                if(!(evaluate_expression(it->get(), m_symbols) > 0)) {
                    throw std::invalid_argument("called sum_div with non-positive argument");
                }
            }
            std::vector<std::string> child_result = dispatch(it->get());
            children_results.emplace_back(child_result);
            if(distance(node->children.begin(), it) < (int)(node->children.size() / 2)) {
                vars.emplace_back(child_result.front());
            } else {
                coeff.emplace_back(child_result.front());
            }
        }
        std::string partial = coeff[1] + "*" + vars[0];
        for(int i = 1; i < (int)(node->children.size() / 2); ++i) {
            partial += "+" + coeff[i + 1] + "*" + vars[i];
        }
        return getResultVector(children_results, "((" + coeff[0] + "*" + vars[0] + ")/(" + partial + "))");
    }

    std::vector<std::string> operator()(covar_matern_1_node* node) {
        std::vector<std::string> x = dispatch(node->get_child<0>());
        return getResultVector(x, "(exp(-sqrt(" + x.front() + ")))");
    }

    std::vector<std::string> operator()(covar_matern_3_node* node) {
        std::vector<std::string> x = dispatch(node->get_child<0>());
        std::string tmp = "(sqrt(3)*sqrt(" + x.front() + "))";
        return getResultVector(x, "(exp(-" + tmp + ")+" + tmp + "*exp(-" + tmp + "))");
    }

    std::vector<std::string> operator()(covar_matern_5_node* node) {
        std::vector<std::string> x = dispatch(node->get_child<0>());
        std::string tmp = "(sqrt(5)*sqrt(" + x.front() + "))";
        return getResultVector(x, "(exp(-" + tmp + ")+" + tmp + "*exp(-" + tmp + ")+" + std::to_string(5.) + "/" + std::to_string(3.) + "*" + x.front() + "*exp(-" + tmp + "))");
    }

    std::vector<std::string> operator()(covar_sqrexp_node* node) {
        std::vector<std::string> x = dispatch(node->get_child<0>());
        return getResultVector(x, "(exp(-" + std::to_string(0.5) + "*" + x.front() + "))");
    }

    std::vector<std::string> operator()(af_lcb_node* node) {
        std::vector<std::string> mu = dispatch(node->get_child<0>());
        std::vector<std::string> sigma = dispatch(node->get_child<1>());
        std::vector<std::string> kappa = dispatch(node->get_child<2>());
        return getResultVector({ mu, sigma, kappa }, "(" + mu.front() + "-" + kappa.front() + "*" + sigma.front() + ")");
    }

    std::vector<std::string> operator()(af_ei_node* node) {
        std::vector<std::string> mu = dispatch(node->get_child<0>());
        std::vector<std::string> sigma = dispatch(node->get_child<1>());
        std::vector<std::string> fmin = dispatch(node->get_child<2>());
        if(is_tree_constant(node->get_child<1>(), m_symbols)) {
            if(evaluate_expression(node->get_child<1>(), m_symbols) == 0) {
                return getResultVector({ mu, sigma, fmin }, "(max(" + fmin.front() + "-" + mu.front() + "," + std::to_string(0.) + "))");
            }
        }
        std::string x = "(" + mu.front() + "-" + fmin.front() + ")";
        std::string gcd = "(erf(" + std::to_string(1.) + "/sqrt(2)*(-" + x + "/" + sigma.front() + "))/" + std::to_string(2.) + "+" + std::to_string(0.5) + ")";
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: erf");
        std::string gpd = "(" + std::to_string(1.) + "/(sqrt(2*" + std::to_string(M_PI) + "))*exp(-power(-" + x + "/" + sigma.front() + ",2)/" + std::to_string(2.) + "))";
        return getResultVector({ mu, sigma, fmin }, "((-" + x + ")*" + gcd + "+" + sigma.front() + "*" + gpd + ")");
    }

    std::vector<std::string> operator()(af_pi_node* node) {
        std::vector<std::string> mu = dispatch(node->get_child<0>());
        std::vector<std::string> sigma = dispatch(node->get_child<1>());
        std::vector<std::string> fmin = dispatch(node->get_child<2>());

        double mu_eval = evaluate_expression(node->get_child<0>(), m_symbols);
        double sigma_eval = evaluate_expression(node->get_child<1>(), m_symbols);
        double fmin_eval = evaluate_expression(node->get_child<2>(), m_symbols);

        if(sigma_eval == 0 && fmin_eval <= mu_eval) {
            return getResultVector({ mu, sigma, fmin }, "(0)");
        }
        if(sigma_eval == 0 && fmin_eval > mu_eval) {
            return getResultVector({ mu, sigma, fmin }, "(1)");
        }
        std::string x = "(" + mu.front() + "-" + fmin.front() + ")";
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: erf");
        return getResultVector({ mu, sigma, fmin }, "(erf(" + std::to_string(1.) + "/sqrt(2)*(-" + x + "/" + sigma.front() + "))/" + std::to_string(2.) + "+" + std::to_string(0.5) + ")");
    }

    std::vector<std::string> operator()(gpdf_node* node) {
        std::vector<std::string> x = dispatch(node->get_child<0>());
        return getResultVector(x, "(" + std::to_string(1.) + "/(sqrt(2*" + std::to_string(M_PI) + "))*exp(-power(" + x.front() + ",2)/" + std::to_string(2.) + "))");
    }

    std::vector<std::string> operator()(regnormal_node* node) {
        std::vector<std::string> x = dispatch(node->get_child<0>());
        std::vector<std::string> a = dispatch(node->get_child<1>());
        std::vector<std::string> b = dispatch(node->get_child<2>());
        return getResultVector({ x, a, b }, "(" + x.front() + "/sqrt(" + a.front() + "+" + b.front() + "*power(" + x.front() + ",2)))");
    }

    std::vector<std::string> operator()(erf_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: erf");
    }

    std::vector<std::string> operator()(erfc_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: erfc");
    }

    std::vector<std::string> operator()(nrtl_dtau_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: nrtl_dtau");
    }

    std::vector<std::string> operator()(nrtl_dgtau_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: nrtl_dgtau");
    }

    std::vector<std::string> operator()(nrtl_gdtau_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: nrtl_gdtau");
    }

    std::vector<std::string> operator()(antoine_psat_node* node) {
        std::vector<std::string> t = dispatch(node->get_child<0>());
        std::vector<std::string> p1 = dispatch(node->get_child<1>());
        std::vector<std::string> p2 = dispatch(node->get_child<2>());
        std::vector<std::string> p3 = dispatch(node->get_child<3>());
        std::cout << "Warning antoine_psat: Base of real power (**) ist not allowed to be negative"
                  << "\n";
        return getResultVector({ t, p1, p2, p3 }, "((" + std::to_string(10.) + "**" + p1.front() + "-" + p2.front() + "/(" + p3.front() + "+" + t.front() + ")))");
    }

    std::vector<std::string> operator()(antoine_tsat_node* node) {
        std::vector<std::string> t = dispatch(node->get_child<0>());
        std::vector<std::string> p1 = dispatch(node->get_child<1>());
        std::vector<std::string> p2 = dispatch(node->get_child<2>());
        std::vector<std::string> p3 = dispatch(node->get_child<3>());
        return getResultVector({ t, p1, p2, p3 }, "(" + p2.front() + "/(" + p1.front() + "-log(" + t.front() + ")/log(" + std::to_string(10.) + "))-" + p3.front() + ")");
    }

    std::vector<std::string> operator()(ext_antoine_psat_node* node) {
        std::vector<std::string> t = dispatch(node->get_child<0>());
        std::vector<std::string> p1 = dispatch(node->get_child<1>());
        std::vector<std::string> p2 = dispatch(node->get_child<2>());
        std::vector<std::string> p3 = dispatch(node->get_child<3>());
        std::vector<std::string> p4 = dispatch(node->get_child<4>());
        std::vector<std::string> p5 = dispatch(node->get_child<5>());
        std::vector<std::string> p6 = dispatch(node->get_child<6>());
        std::vector<std::string> p7 = dispatch(node->get_child<7>());
        std::cout << "Warning ext_antoine_psat: Base of real power (**) ist not allowed to be negative"
                  << "\n";
        return getResultVector({ t, p1, p2, p3, p4, p5, p6, p7 }, "(exp(" + p1.front() + "+" + p2.front() + "/(" + t.front() + "+" + p3.front() + ")+" + t.front() + "*" + p4.front() + "+" + p5.front() + "*log(" + t.front() + ")+" + p6.front() + "*(" + t.front() + "**" + p7.front() + ")))");
    }

    std::vector<std::string> operator()(wagner_psat_node* node) {
        std::vector<std::string> t = dispatch(node->get_child<0>());
        std::vector<std::string> p1 = dispatch(node->get_child<1>());
        std::vector<std::string> p2 = dispatch(node->get_child<2>());
        std::vector<std::string> p3 = dispatch(node->get_child<3>());
        std::vector<std::string> p4 = dispatch(node->get_child<4>());
        std::vector<std::string> p5 = dispatch(node->get_child<5>());
        std::vector<std::string> p6 = dispatch(node->get_child<6>());
        std::string Tr = "(" + t.front() + "/" + p5.front() + ")";
        std::cout << "Warning wagner_psat: Base of real power (**) ist not allowed to be negative"
                  << "\n";
        return getResultVector({ t, p1, p2, p3, p4, p5, p6 }, "(" + p6.front() + "*exp((" + p1.front() + "*(1-" + Tr + ")+" + p2.front() + "*power(1-" + Tr + "," + std::to_string(1.5) + ")+" + p3.front() + "*((1-" + Tr + ")**" + std::to_string(2.5) + ")+" + p4.front() + "*power(1-" + Tr + ",5))/" + Tr + "))");
    }

    std::vector<std::string> operator()(ik_cape_psat_node* node) {
        std::vector<std::string> t = dispatch(node->get_child<0>());
        std::vector<std::string> p1 = dispatch(node->get_child<1>());
        std::vector<std::string> p2 = dispatch(node->get_child<2>());
        std::vector<std::string> p3 = dispatch(node->get_child<3>());
        std::vector<std::string> p4 = dispatch(node->get_child<4>());
        std::vector<std::string> p5 = dispatch(node->get_child<5>());
        std::vector<std::string> p6 = dispatch(node->get_child<6>());
        std::vector<std::string> p7 = dispatch(node->get_child<7>());
        std::vector<std::string> p8 = dispatch(node->get_child<8>());
        std::vector<std::string> p9 = dispatch(node->get_child<9>());
        std::vector<std::string> p10 = dispatch(node->get_child<10>());
        return getResultVector({ t, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10 }, "(exp(" + p1.front() + "+" + p2.front() + "*" + t.front() + "+" + p3.front() + "*power(" + t.front() + ",2)+" + p4.front() + "*power(" + t.front() + ",3)+" + p5.front() + "*power(" + t.front() + ",4)+" + p6.front() + "*power(" + t.front() + ",5)+" + p7.front() + "*power(" + t.front() + ",6)+" + p8.front() + "*power(" + t.front() + ",7)+" + p9.front() + "*power(" + t.front() + ",8)+" + p10.front() + "*power(" + t.front() + ",9)))");
    }

    std::vector<std::string> operator()(aspen_hig_node* node) {
        std::vector<std::string> t = dispatch(node->get_child<0>());
        std::vector<std::string> t0 = dispatch(node->get_child<1>());
        std::vector<std::string> p1 = dispatch(node->get_child<2>());
        std::vector<std::string> p2 = dispatch(node->get_child<3>());
        std::vector<std::string> p3 = dispatch(node->get_child<4>());
        std::vector<std::string> p4 = dispatch(node->get_child<5>());
        std::vector<std::string> p5 = dispatch(node->get_child<6>());
        std::vector<std::string> p6 = dispatch(node->get_child<7>());
        return getResultVector({ t, t0, p1, p2, p3, p4, p5, p6 }, "(" + p1.front() + "*(" + t.front() + "-" + t0.front() + ")+" + p2.front() + "/2*(power(" + t.front() + ",2)-power(" + t0.front() + ",2))+" + p3.front() + "/3*(power(" + t.front() + ",3)" + "-power(" + t0.front() + ",3))+" + p4.front() + "/4*(power(" + t.front() + ",4)-power(" + t0.front() + ",4))+" + p5.front() + "/5*(power(" + t.front() + ",5)" + "-power(" + t0.front() + ",5))+" + p6.front() + "/6*(power(" + t.front() + ",6)-power(" + t0.front() + ",6)))");
    }

    std::vector<std::string> operator()(nasa9_hig_node* node) {
        std::vector<std::string> t = dispatch(node->get_child<0>());
        std::vector<std::string> t0 = dispatch(node->get_child<1>());
        std::vector<std::string> p1 = dispatch(node->get_child<2>());
        std::vector<std::string> p2 = dispatch(node->get_child<3>());
        std::vector<std::string> p3 = dispatch(node->get_child<4>());
        std::vector<std::string> p4 = dispatch(node->get_child<5>());
        std::vector<std::string> p5 = dispatch(node->get_child<6>());
        std::vector<std::string> p6 = dispatch(node->get_child<7>());
        std::vector<std::string> p7 = dispatch(node->get_child<8>());
        return getResultVector({ t, t0, p1, p2, p3, p4, p5, p6, p7 }, "(-" + p1.front() + "*(1/" + t.front() + "-1/" + t0.front() + ")+" + p2.front() + "*log(" + t.front() + "/" + t0.front() + ")+" + p3.front() + "*(" + t.front() + "-" + t0.front() + ")+" + p4.front() + "/2*(power(" + t.front() + ",2)" + "-power(" + t0.front() + ",2))+" + p5.front() + "/3*(power(" + t.front() + ",3)-power(" + t0.front() + ",3))+" + p6.front() + "/4*(power(" + t.front() + ",4)" + "-power(" + t0.front() + ",4))+" + p7.front() + "/5*(power(" + t.front() + ",5)-power(" + t0.front() + ",5)))");
    }

    std::vector<std::string> operator()(dippr106_dhvap_node* node) {
        std::vector<std::string> t = dispatch(node->get_child<0>());
        std::vector<std::string> tc = dispatch(node->get_child<1>());
        std::vector<std::string> p2 = dispatch(node->get_child<2>());
        std::vector<std::string> p3 = dispatch(node->get_child<3>());
        std::vector<std::string> p4 = dispatch(node->get_child<4>());
        std::vector<std::string> p5 = dispatch(node->get_child<5>());
        std::vector<std::string> p6 = dispatch(node->get_child<6>());
        std::string tr = "(" + t.front() + "/" + tc.front() + ")";

        if(is_tree_constant(node->get_child<0>(), m_symbols) && is_tree_constant(node->get_child<1>(), m_symbols)) {
            if((evaluate_expression(node->get_child<0>(), m_symbols) / evaluate_expression(node->get_child<1>(), m_symbols)) < 1) {
                std::cout << "Warning dippr106_dhvap: Base of real power (**) ist not allowed to be negative"
                          << "\n";
                return getResultVector({
                                         t,
                                         tc,
                                         p2,
                                         p3,
                                         p4,
                                         p5,
                                         p6,
                                       },
                  "(" + p2.front() + "*((1-" + tr + ")**" + p3.front() + "+" + p4.front() + "*" + tr + "+" + p5.front() + "*power(" + tr + ",2)+" + p6.front() + "*power(" + tr + ",3)))");
            } else {
                return getResultVector({ t, tc, p2, p3, p4, p5, p6 }, std::to_string(0.));
            }
        } else {
            std::cout << "Warning dippr106_dhvap: Temperature is not a constant. Check T < T_crit for all possible values of T manually!"
                      << "\n";
            std::cout << "Warning dippr106_dhvap: Base of real power (**) ist not allowed to be negative"
                      << "\n";
            return getResultVector({
                                     t,
                                     tc,
                                     p2,
                                     p3,
                                     p4,
                                     p5,
                                     p6,
                                   },
              "(" + p2.front() + "*((1-" + tr + ")**" + p3.front() + "+" + p4.front() + "*" + tr + "+" + p5.front() + "*power(" + tr + ",2)+" + p6.front() + "*power(" + tr + ",3)))");
        }
    }

    std::vector<std::string> operator()(dippr107_hig_node* node) {
        std::vector<std::string> t = dispatch(node->get_child<0>());
        std::vector<std::string> t0 = dispatch(node->get_child<1>());
        std::vector<std::string> p1 = dispatch(node->get_child<2>());
        std::vector<std::string> p2 = dispatch(node->get_child<3>());
        std::vector<std::string> p3 = dispatch(node->get_child<4>());
        std::vector<std::string> p4 = dispatch(node->get_child<5>());
        std::vector<std::string> p5 = dispatch(node->get_child<6>());
        return getResultVector({ t, t0, p1, p2, p3, p4, p5 }, "(" + p1.front() + "*(" + t.front() + "-" + t0.front() + ")+" + p2.front() + "*" + p3.front() + "*(1/tanh(" + p3.front() + "/" + t.front() + ")-1/tanh(" + p3.front() + "/" + t0.front() + "))" + "-" + p4.front() + "*" + p5.front() + "*(tanh(" + p5.front() + "/" + t.front() + ")-tanh(" + p5.front() + "/" + t0.front() + ")))");
    }

    std::vector<std::string> operator()(dippr127_hig_node* node) {
        std::vector<std::string> t = dispatch(node->get_child<0>());
        std::vector<std::string> t0 = dispatch(node->get_child<1>());
        std::vector<std::string> p1 = dispatch(node->get_child<2>());
        std::vector<std::string> p2 = dispatch(node->get_child<3>());
        std::vector<std::string> p3 = dispatch(node->get_child<4>());
        std::vector<std::string> p4 = dispatch(node->get_child<5>());
        std::vector<std::string> p5 = dispatch(node->get_child<6>());
        std::vector<std::string> p6 = dispatch(node->get_child<7>());
        std::vector<std::string> p7 = dispatch(node->get_child<8>());
        return getResultVector({ t, t0, p1, p2, p3, p4, p5, p6, p7 }, "(" + p1.front() + "*(" + t.front() + "-" + t0.front() + ")+" + p2.front() + "*" + p3.front() + "*(1/(exp(" + p3.front() + "/" + t.front() + ")-1)-1/(exp(" + p3.front() + "/" + t0.front() + ")-1))+" + p4.front() + "*" + p5.front() + "*(1/(exp(" + p5.front() + "/" + t.front() + ")-1)-1/(exp(" + p5.front() + "/" + t0.front() + ")-1))+" + p6.front() + "*" + p7.front() + "*(1/(exp(" + p7.front() + "/" + t.front() + ")-1)-1/(exp(" + p7.front() + "/" + t0.front() + ")-1)))");
    }

    std::vector<std::string> operator()(watson_dhvap_node* node) {
        std::vector<std::string> t = dispatch(node->get_child<0>());
        std::vector<std::string> tc = dispatch(node->get_child<1>());
        std::vector<std::string> a = dispatch(node->get_child<2>());
        std::vector<std::string> b = dispatch(node->get_child<3>());
        std::vector<std::string> t1 = dispatch(node->get_child<4>());
        std::vector<std::string> dHT1 = dispatch(node->get_child<5>());
        std::string tr = "(1-" + t.front() + "/" + tc.front() + ")";
        if(is_tree_constant(node->get_child<0>(), m_symbols) && is_tree_constant(node->get_child<0>(), m_symbols)) {
            if((1 - evaluate_expression(node->get_child<0>(), m_symbols) / evaluate_expression(node->get_child<1>(), m_symbols)) > 0) {
                std::cout << "Warning watson_dhvap: Base of real power (**) ist not allowed to be negative"
                          << "\n";
                return getResultVector({ t, tc, a, b, t1, dHT1 }, "(" + dHT1.front() + "*((" + tr.front() + "/(1-" + t1.front() + "/" + tc.front() + "))**" + a.front() + "+" + b.front() + "*" + tr.front() + "))");
            } else {
                return getResultVector({ t, tc, a, b, t1, dHT1 }, std::to_string(0.));
            }
        } else {
            std::cout << "Warning watson_dhvap: Temperature is not a constant. Check T < T_crit for all possible values of T manually!"
                      << "\n";
            std::cout << "Warning watson_dhvap: Base of real power (**) ist not allowed to be negative"
                      << "\n";
            return getResultVector({ t, tc, a, b, t1, dHT1 }, "(" + dHT1.front() + "*((" + tr.front() + "/(1-" + t1.front() + "/" + tc.front() + "))**" + a.front() + "+" + b.front() + "*" + tr.front() + "))");
        }
    }

    std::vector<std::string> operator()(schroeder_ethanol_p_node* node) {
        std::vector<std::string> t = dispatch(node->get_child<0>());
        const std::string t_c_K = std::to_string(514.71);
        const std::string n_Tsat_1 = std::to_string(-8.94161);
        const std::string n_Tsat_2 = std::to_string(1.61761);
        const std::string n_Tsat_3 = std::to_string(-51.1428);
        const std::string n_Tsat_4 = std::to_string(53.1360);
        const std::string k_Tsat_1 = std::to_string(1.0);
        const std::string k_Tsat_2 = std::to_string(1.5);
        const std::string k_Tsat_3 = std::to_string(3.4);
        const std::string k_Tsat_4 = std::to_string(3.7);
        const std::string p_c = std::to_string(62.68);
        std::cout << "Warning schroeder_ethanol_p: Base of real power (**) ist not allowed to be negative"
                  << "\n";
        return getResultVector(t, "(" + p_c + "*(exp(" + t_c_K + "/" + t.front() + "*(" + n_Tsat_1 + "*((1-" + t.front() + "/" + t_c_K + ")**" + k_Tsat_1 + ")+" + n_Tsat_2 + "*((1-" + t.front() + "/" + t_c_K + ")**" + k_Tsat_2 + ")+(" + n_Tsat_3 + ")*((1-" + t.front() + "/" + t_c_K + ")**" + k_Tsat_3 + ")+" + n_Tsat_4 + "*((1-" + t.front() + "/" + t_c_K + ")**" + k_Tsat_4 + ")))))");
    }

    std::vector<std::string> operator()(schroeder_ethanol_rholiq_node* node) {
        std::vector<std::string> t = dispatch(node->get_child<0>());
        const std::string t_c_K = std::to_string(514.71);
        const std::string n_liq_1 = std::to_string(9.00921);
        const std::string n_liq_2 = std::to_string(-23.1668);
        const std::string n_liq_3 = std::to_string(30.9092);
        const std::string n_liq_4 = std::to_string(-16.5459);
        const std::string n_liq_5 = std::to_string(3.64294);
        const std::string k_liq_1 = std::to_string(0.5);
        const std::string k_liq_2 = std::to_string(0.8);
        const std::string k_liq_3 = std::to_string(1.1);
        const std::string k_liq_4 = std::to_string(1.5);
        const std::string k_liq_5 = std::to_string(3.3);
        const std::string rho_c = std::to_string(273.195);
        std::cout << "Warning schroeder_ehanol_rholiq: Base of real power (**) ist not allowed to be negative"
                  << "\n";
        return getResultVector(t, "(" + rho_c + "*(1+" + n_liq_1 + "*((1-" + t.front() + "/" + t_c_K + ")**" + k_liq_1 + ")+(" + n_liq_2 + ")*((1-" + t.front() + "/" + t_c_K + ")**" + k_liq_2 + ")+" + n_liq_3 + "*((1-" + t.front() + "/" + t_c_K + ")**" + k_liq_3 + ")+(" + n_liq_4 + ")*((1-" + t.front() + "/" + t_c_K + ")**" + k_liq_4 + ")+" + n_liq_5 + "*((1-" + t.front() + "/" + t_c_K + ")**" + k_liq_5 + ")))");
    }

    std::vector<std::string> operator()(schroeder_ethanol_rhovap_node* node) {
        std::vector<std::string> t = dispatch(node->get_child<0>());
        const std::string t_c_K = std::to_string(514.71);
        const std::string n_vap_1 = std::to_string(-1.75362);
        const std::string n_vap_2 = std::to_string(-10.5323);
        const std::string n_vap_3 = std::to_string(-37.6407);
        const std::string n_vap_4 = std::to_string(-129.762);
        const std::string k_vap_1 = std::to_string(0.21);
        const std::string k_vap_2 = std::to_string(1.1);
        const std::string k_vap_3 = std::to_string(3.4);
        const std::string k_vap_4 = std::to_string(10);
        const std::string rho_c = std::to_string(273.195);
        std::cout << "Warning schroeder_ethanol_rhovap: Base of real power (**) ist not allowed to be negative"
                  << "\n";
        return getResultVector(t, "(" + rho_c + "*(exp(" + n_vap_1 + "*((1-" + t.front() + "/" + t_c_K + ")**" + k_vap_1 + ")+(" + n_vap_2 + ")*((1-" + t.front() + "/" + t_c_K + ")**" + k_vap_2 + ")+(" + n_vap_3 + ")*((1-" + t.front() + "/" + t_c_K + ")**" + k_vap_3 + ")+(" + n_vap_4 + ")*((1-" + t.front() + "/" + t_c_K + ")**" + k_vap_4 + "))))");
    }

    std::vector<std::string> operator()(nrtl_tau_node* node) {
        std::vector<std::string> t = dispatch(node->get_child<0>());
        std::vector<std::string> a = dispatch(node->get_child<1>());
        std::vector<std::string> b = dispatch(node->get_child<2>());
        std::vector<std::string> e = dispatch(node->get_child<3>());
        std::vector<std::string> f = dispatch(node->get_child<4>());
        return getResultVector({ t, a, b, e, f }, "(" + a.front() + "+(" + b.front() + "/" + t.front() + ")+" + e.front() + "*log(" + t.front() + ")+" + f.front() + "*" + t.front() + ")");
    }

    std::vector<std::string> operator()(nrtl_g_node* node) {
        std::vector<std::string> t = dispatch(node->get_child<0>());
        std::vector<std::string> a = dispatch(node->get_child<1>());
        std::vector<std::string> b = dispatch(node->get_child<2>());
        std::vector<std::string> e = dispatch(node->get_child<3>());
        std::vector<std::string> f = dispatch(node->get_child<4>());
        std::vector<std::string> alpha = dispatch(node->get_child<5>());
        //the parameters are a,b,e,f and alpha, for further info, see ASPEN help
        return getResultVector({ t, a, b, e, f, alpha }, "(exp(-(" + alpha.front() + ")*((" + a.front() + ")+(" + b.front() + ")/(" + t.front() + ")+(" + e.front() + ")*log(" + t.front() + ")+(" + f.front() + ")*(" + t.front() + "))))");
    }

    std::vector<std::string> operator()(nrtl_gtau_node* node) {
        std::vector<std::string> t = dispatch(node->get_child<0>());
        std::vector<std::string> a = dispatch(node->get_child<1>());
        std::vector<std::string> b = dispatch(node->get_child<2>());
        std::vector<std::string> e = dispatch(node->get_child<3>());
        std::vector<std::string> f = dispatch(node->get_child<4>());
        std::vector<std::string> alpha = dispatch(node->get_child<5>());
        //the parameters are a,b,e,f and alpha, for further info, see ASPEN help
        return getResultVector({ t, a, b, e, f, alpha }, "(exp(-" + alpha.front() + "*(" + a.front() + "+" + b.front() + "/" + t.front() + "+" + e.front() + "*log(" + t.front() + ")+" + f.front() + "*" + t.front() + "))*(" + a.front() + "+" + b.front() + "/" + t.front() + "+" + e.front() + "*log(" + t.front() + ")+" + f.front() + "*" + t.front() + "))");
    }

    // non-terminal boolean node visits
    std::vector<std::string> operator()(entry_node<ale::boolean<0>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: entry_node<boolean<0>>");
    }

    std::vector<std::string> operator()(negation_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: negation_node");
    }

    std::vector<std::string> operator()(equal_node<real<0>>* node) {
        std::vector<std::vector<std::string>> children_results = { dispatch(node->get_child<0>()), dispatch(node->get_child<1>()) };
        return getResultVector(children_results, children_results.at(0).front() + " =e= " + children_results.at(1).front());
    }

    std::vector<std::string> operator()(less_node<real<0>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: less_node<real<0>>");
    }

    std::vector<std::string> operator()(less_equal_node<real<0>>* node) {
        std::vector<std::vector<std::string>> children_results = { dispatch(node->get_child<0>()), dispatch(node->get_child<1>()) };
        return getResultVector(children_results, children_results.at(0).front() + " =l= " + children_results.at(1).front());
    }

    std::vector<std::string> operator()(greater_node<real<0>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: greater_node<real<0>>");
    }

    std::vector<std::string> operator()(greater_equal_node<real<0>>* node) {
        std::vector<std::vector<std::string>> children_results = { dispatch(node->get_child<0>()), dispatch(node->get_child<1>()) };
        return getResultVector(children_results, children_results.at(0).front() + " =g= " + children_results.at(1).front());
    }

    std::vector<std::string> operator()(equal_node<ale::index<0>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: equal_node<ale::index<0>>");
    }

    std::vector<std::string> operator()(less_node<ale::index<0>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: less_node<ale::index<0>>");
    }

    std::vector<std::string> operator()(less_equal_node<ale::index<0>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: less_equal_node<ale::index<0>>");
    }

    std::vector<std::string> operator()(greater_node<ale::index<0>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: greater_node<ale::index<0>>");
    }

    std::vector<std::string> operator()(greater_equal_node<ale::index<0>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: greater_equal_node<ale::index<0>>");
    }

    std::vector<std::string> operator()(disjunction_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: disjunction_node");
    }

    std::vector<std::string> operator()(conjunction_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: conjunction_node");
    }

    template <typename TType>
    std::vector<std::string> operator()(element_node<TType>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: element_node");
    }

    std::vector<std::string> operator()(vector_node<ale::boolean<0>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: vector_node<boolean<0>>");
    }

    std::vector<std::string> operator()(function_node<ale::boolean<0>>* node) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: function_node<boolean<0>> with name '" + node->name + "'");
    }

    std::vector<std::string> operator()(tensor_node<ale::boolean<0>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: tensor_node<boolean<0>>");
    }

    std::vector<std::string> operator()(index_shift_node<ale::boolean<0>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: index_shift_node<boolean<0>>");
    }

    std::vector<std::string> operator()(forall_node<ale::index<0>>* node) {
        auto elements = dispatch(node->template get_child<0>());
        if(elements.empty()) {
            return { "NaN" };
        }
        m_symbols.push_scope();
        std::string set_string = std::visit(set_stringer(this, m_symbols), (node->template get_child<0>())->get_variant());
        if(set_string.rfind("Set {}(*) /", 0) == 0) {
            set_string = fmt::format(set_string, node->name + "{}", node->name + "{}" + "_asreal");
            m_symbols.define(node->name, new parameter_symbol<ale::index<0>>(node->name + "{}", ale::index<0>::basic_type()));
            auto elements = dispatch(node->template get_child<0>());
            m_symbols.define(node->name + "{}", new parameter_symbol<set<ale::index<0>, 0>>(node->name + "{}", elements));
        } else {
            m_symbols.define(node->name, new parameter_symbol<ale::index<0>>(set_string, ale::index<0>::basic_type()));
        }
        std::vector<std::string> result = dispatch(node->template get_child<1>());
        m_symbols.pop_scope();
        if(result.front() == "NaN") {
            return result;
        }
        result.emplace(result.end(), set_string);
        return result;
    }

    template <unsigned IDim>
    std::vector<std::string> operator()(forall_node<ale::index<IDim>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: forall_node<ale::index<IDim>>");
    }

    template <unsigned IDim>
    std::vector<std::string> operator()(forall_node<real<IDim>>* node) {
        auto elements = dispatch(node->template get_child<0>());
        if(elements.empty()) {
            return { "NaN" };
        }
        m_symbols.push_scope();
        std::string set_string = std::visit(set_stringer(this, m_symbols), (node->template get_child<0>())->get_variant());
        if(set_string.rfind("Set {}(*) /", 0) == 0) {
            set_string = fmt::format(set_string, fmt::format("realset_{}_index", node->name + "{}", localset_string(localset_n)), fmt::format("realset_{}", node->name + "{}"));
            m_symbols.define(node->name, new parameter_symbol<real<0>>(fmt::format("realset_{}", node->name + "{}"), real<0>::basic_type()));
            m_symbols.define(node->name + "{}", new parameter_symbol<set<real<IDim>, 0>>(node->name + "{}", elements));
        } else {
            set_string = "realset_" + set_string;
            m_symbols.define(node->name, new parameter_symbol<real<0>>(set_string, real<0>::basic_type()));
        }
        std::vector<std::string> result = dispatch(node->template get_child<1>());
        m_symbols.pop_scope();
        if(result.front() == "NaN") {
            return result;
        }
        result.emplace(result.end(), set_string);
        return result;
    }

private:
    symbol_table& m_symbols;
    int indexoperation_n { 0 };
    int localset_n { 0 };
};

class symbol_stringer_unflattened {
public:
    symbol_stringer_unflattened(std::stringstream& output, symbol_table& symbols) :
        m_output(output), m_symbols(symbols) {};

    // base_symbol dispatch
    void dispatch(base_symbol* sym) {
        std::visit(*this, sym->get_base_variant());
    }

    template <typename TType>
    void operator()(value_symbol<TType>* sym) {
        std::visit(*this, sym->get_value_variant());
    }

    // symbol visits
    template <typename TType>
    void operator()(parameter_symbol<TType>* sym) {
        // skipping other parameters
    }

    void operator()(parameter_symbol<ale::index<0>>* sym) {
        m_output << "$macro " << sym->m_name << " '" << sym->m_value << "'\n";
        m_output << "Parameter " << sym->m_name << "_asreal(*) / " << sym->m_value << " = " << sym->m_value << " /;\n";
    }

    void operator()(parameter_symbol<set<ale::index<0>, 0>>* sym) {
        expression_stringer_unflattened expr_str(m_symbols);
        set_stringer set_str(&expr_str, m_symbols);
        m_output << fmt::format(set_str.dispatch(sym->m_value), sym->m_name, sym->m_name + "_asreal") << ";\n";
    }

    template <unsigned IDim_inner>
    void operator()(parameter_symbol<set<real<IDim_inner>, 0>>* sym) {
        expression_stringer_unflattened expr_str(m_symbols);
        set_stringer set_str(&expr_str, m_symbols);
        m_output << fmt::format(set_str.dispatch(sym->m_value), fmt::format("realset_{}_index", sym->m_name), "realset_" + sym->m_name) << ";\n";
    }

    template <unsigned IDim_inner>
    void operator()(parameter_symbol<set<ale::index<IDim_inner>, 0>>* sym) {
        throw unsupportedNodeParsingException("Indextensorsets not implemented for GAMS. Indextensorset with name '" + sym->m_name + "'");
    }

    template <typename TType, unsigned IDim>
    void operator()(parameter_symbol<set<TType, IDim>>* sym) {
        throw unsupportedNodeParsingException("Tensors of set types not implemented for GAMS. Tensors of set types with name '" + sym->m_name + "'");
    }

    template <unsigned IDim>
    void operator()(parameter_symbol<real<IDim>>* sym) {
        for(int i = 0; i < IDim; ++i) {
            if(sym->m_value.shape(i) == 0) {
                return;
            }
        }
        m_output << "parameter " << sym->m_name << "\n  /\n";
        size_t indexes[IDim];
        for(int i = 0; i < IDim; ++i) {
            indexes[i] = 0;
        }
        while(indexes[0] < sym->m_value.shape(0)) {
            m_output << "  " << ale::helper::serialize_indexes<IDim>(indexes, '.') << " = " << std::to_string(sym->m_value[indexes]);
            for(int i = IDim - 1; i >= 0; --i) {
                if(++indexes[i] < sym->m_value.shape(i)) {
                    break;
                } else if(i != 0) {
                    indexes[i] = 0;
                }
            }
            if(indexes[0] < sym->m_value.shape(0)) {
                m_output << ",\n";
            }
        }
        m_output << "\n  /;\n";
    }

    void operator()(parameter_symbol<real<0>>* sym) {
        m_output << "parameter " << sym->m_name << "\n  /\n  " << std::to_string(sym->m_value) << "\n  /;\n";
    }

    template <typename TType>
    void operator()(variable_symbol<TType>* sym) {
        throw std::invalid_argument("cannot derive GAMS input from non-real variable with name '" + sym->m_name + "'");
    }

    template <unsigned IDim>
    void operator()(variable_symbol<real<IDim>>* sym) {
        for(int i = 0; i < IDim; ++i) {
            if(sym->shape(i) == 0) {
                return;
            }
        }
        size_t indexes[IDim];
        for(int i = 0; i < IDim; ++i) {
            indexes[i] = 0;
        }
        if(sym->integral()) {
            m_output << "integer ";
        }
        std::vector<std::string> super_sets(IDim);
        std::fill(super_sets.begin(), super_sets.end(), "*");
        m_output << fmt::format("variable {}({}) \n  /\n", sym->m_name, fmt::join(super_sets, ","));
        while(indexes[0] < sym->shape(0)) {
            std::string entry = ale::helper::serialize_indexes<IDim>(indexes, '.');
            if(sym->lower()[indexes] > -std::numeric_limits<double>::infinity()) {
                m_output << "  " << entry << ".lo = " << sym->lower()[indexes];
            }
            if(sym->upper()[indexes] < std::numeric_limits<double>::infinity()) {
                m_output << ",\n"
                         << "  " << entry << ".up = " << sym->upper()[indexes];
            }
            if(!std::isnan(sym->init()[indexes])) {
                m_output << ",\n"
                         << "  " << entry << ".L = " << sym->init()[indexes];
            }
            if(!std::isnan(sym->prio()[indexes])) {
                if(!sym->integral()) {
                    throw std::invalid_argument("Can not set prio for non integral variable (named '" + sym->m_name + "')");
                }
                m_output << ",\n"
                         << "  " << entry << ".prior = " << sym->prio()[indexes];
            }
            for(int i = IDim - 1; i >= 0; --i) {
                if(++indexes[i] < sym->shape(i)) {
                    break;
                } else if(i != 0) {
                    indexes[i] = 0;
                }
            }
            if(indexes[0] < sym->shape(0)) {
                m_output << ",\n";
            }
        }
        m_output << "\n  /;\n";
    }

    void operator()(variable_symbol<real<0>>* sym) {
        if(sym->integral()) {
            m_output << "integer variable " << sym->m_name << ";\n";
        } else {
            m_output << "variable " << sym->m_name << ";\n";
        }
        if(sym->lower() > -std::numeric_limits<double>::infinity()) {
            m_output << "  " << sym->m_name << ".lo = " << sym->lower() << ";\n";
        }
        if(sym->upper() < std::numeric_limits<double>::infinity()) {
            m_output << "  " << sym->m_name << ".up = " << sym->upper() << ";\n";
        }
        if(!std::isnan(sym->init())) {
            m_output << "  " << sym->m_name << ".L = " << sym->init() << ";\n";
        }
        if(!std::isnan(sym->prio())) {
            if(!sym->integral()) {
                throw std::invalid_argument("Can not set prio for non integral variable (named '" + sym->m_name + "')");
            }
            m_output << "  " << sym->m_name << ".prior = " << sym->prio() << ";\n";
        }
    }


    template <typename TType>
    void operator()(function_symbol<TType>* sym) {
        throw unsupportedNodeParsingException("cannot string function. name is '" + sym->m_name + "'. use function_stringer instead.");
    }


private:
    std::stringstream& m_output;
    symbol_table& m_symbols;
};

class function_stringer {
public:
    function_stringer(symbol_table& symbols) :
        m_symbols(symbols) {};

    // base_symbol dispatch
    std::vector<std::string> dispatch(base_symbol* sym) {
        //returns: vector; first entr: function macro; last n-1 entries: local sets
        return std::visit(*this, sym->get_base_variant());
    }

    template <unsigned IDim>
    void define_argument(const std::string& name, std::vector<size_t> shape) {
        if constexpr(IDim != 0) {
            if(shape.size() < IDim) {
                define_argument<IDim - 1>(name, shape);
            } else {
                size_t value_shape[IDim];
                auto it = shape.begin();
                for(int i = 0; i < IDim; i++) {
                    value_shape[i] = *it;
                    it++;
                }
                m_symbols.define(name, new parameter_symbol<real<IDim>>(name, value_shape));
            }
        }
    }

    std::vector<std::string> operator()(function_symbol<real<0>>* sym) {
        m_symbols.push_scope();
        auto shape = (sym->arg_shapes).begin();
        auto arg_name = (sym->arg_names).begin();
        for(auto it = (sym->arg_dims).begin(); it != (sym->arg_dims).end(); ++it) {
            if(*it == 0) {
                m_symbols.define((*arg_name), new parameter_symbol<real<0>>((*arg_name), real<0>::basic_type()));
            } else {
                define_argument<LIBALE_MAX_DIM>(*arg_name, *shape);
            }
            arg_name++;
            shape++;
        }
        expression_stringer_unflattened expr_str(m_symbols);
        std::vector<std::string> result = expr_str.dispatch(sym->expr);

        if(result.front().find("NaN") != std::string::npos) {
            // encountered function with empty set. setting as 0
            std::cout << "encountered function definition \"" << sym->m_name << "\" with empty set. setting macro as 0.\n";
            result.clear();
            result.push_back("0");
        }

        if(!(sym->arg_names).empty()) {
            result[0].insert(0, "(" + fmt::format("{}", fmt::join(sym->arg_names, ",")) + ") ");
        }
        result[0].insert(0, "$macro " + sym->m_name);
        m_symbols.pop_scope();

        return result;
    }

    template <typename TType>
    std::vector<std::string> operator()(function_symbol<TType>* sym) {
        throw unsupportedNodeParsingException("function_symbol for not real / multidimensional types not implemented for GAMS. Most function may be rewritten as onedimensional functions. Function name is '" + sym->m_name + "'. Or try the GAMSCPP flattened interface.");
    }

    template <typename TType>
    std::vector<std::string> operator()(value_symbol<TType>* /*sym*/) {
        throw unsupportedNodeParsingException("function stringer may only be used with functions. encountered value_symbol");
    }

private:
    symbol_table& m_symbols;
};

// return for an index operation node: tuple containing the string identifing the map/resset for index operation nodes as well as the for the map relevant indices in a vector
class index_operation_stringer {

    /*
		returns tuple containing:
		1: index operation identifier (e.g. ip2tj for i+2*j)
		2: vector containing all the indices (e.g. i,'2',j for i+2*j)
		3: vector containing lists of all elements of the parameters (e.g. if i is iterator for I={1,2} and j for J = {3,4}, the vector contains {{1,2}, {2}, {3,4}} for i+2*j)
		4: vector containing all the operations of the elements (e.g. {p,t} for 
		*/

public:
    index_operation_stringer(expression_stringer_unflattened* expr_str, symbol_table& symbols) :
        expr_str(expr_str), symbols(symbols) {};

    template <typename TType>
    std::tuple<std::string, std::vector<std::string>, std::vector<std::list<int>>, std::vector<std::string>> dispatch(value_node<TType>* node) {
        return std::visit(*this, node->get_variant());
    }

    std::tuple<std::string, std::vector<std::string>, std::vector<std::list<int>>, std::vector<std::string>> operator()(index_addition_node* node) {
        std::vector<std::string> string_indices;
        std::vector<std::string> list_indices;
        std::vector<std::list<int>> elements;
        std::vector<std::string> operators;
        for(auto it = node->children.begin(); it != node->children.end(); ++it) {
            auto child_res = dispatch(it->get());
            string_indices.emplace_back(std::get<0>(child_res));
            list_indices.insert(list_indices.end(), std::get<1>(child_res).begin(), std::get<1>(child_res).end());
            elements.insert(elements.end(), std::get<2>(child_res).begin(), std::get<2>(child_res).end());
            operators.insert(operators.end(), std::get<3>(child_res).begin(), std::get<3>(child_res).end());
            if(it != --(node->children.end())) {
                operators.emplace_back("p");
            }
        }
        return { fmt::format("{}", fmt::join(string_indices, "p")), list_indices, elements, operators };
    }

    std::tuple<std::string, std::vector<std::string>, std::vector<std::list<int>>, std::vector<std::string>> operator()(index_minus_node* node) {
        // TODO: fix bug: current implementation violates operator precedence
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: index_minus_node");
        //auto child_res = dispatch(node->template get_child<0>());
        //std::get<3>(child_res).emplace_back("m");
        //return { "m" + std::get<0>(child_res), std::get<1>(child_res), std::get<2>(child_res), std::get<3>(child_res) };
    }

    std::tuple<std::string, std::vector<std::string>, std::vector<std::list<int>>, std::vector<std::string>> operator()(index_multiplication_node* node) {
        // TODO: fix bug: current implementation violates operator precedence
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: index_multiplication_node");
        //std::vector<std::string> string_indices;
        //std::vector<std::string> list_indices;
        //std::vector<std::list<int>> elements;
        //std::vector<std::string> operators;
        //for (auto it = node->children.begin(); it != node->children.end(); ++it) {
        //	auto child_res = dispatch(it->get());
        //	string_indices.emplace_back(std::get<0>(child_res));
        //	list_indices.insert(list_indices.end(), std::get<1>(child_res).begin(), std::get<1>(child_res).end());
        //	elements.insert(elements.end(), std::get<2>(child_res).begin(), std::get<2>(child_res).end());
        //	operators.insert(operators.end(), std::get<3>(child_res).begin(), std::get<3>(child_res).end());
        //	if (it != --(node->children.end())) {
        //		operators.emplace_back("t");
        //	}
        //}
        //return { fmt::format("{}", fmt::join(string_indices, "t")), list_indices, elements, operators };
    }

    std::tuple<std::string, std::vector<std::string>, std::vector<std::list<int>>, std::vector<std::string>> operator()(parameter_node<ale::index<0>>* node) {
        auto ind = node->name;
        auto set_ident = (expr_str->dispatch(node)).front();
        {
            auto value_sym = symbols.resolve<ale::index<0>>(set_ident);
            if(value_sym != nullptr) {
                parameter_symbol<ale::index<0>>* sym = dynamic_cast<parameter_symbol<ale::index<0>>*>(value_sym);
                return { sym->m_name, { sym->m_name }, { { sym->m_value } }, {} };
            }
        }
        auto value_sym = symbols.resolve<set<ale::index<0>, 0>>(set_ident);
        parameter_symbol<set<ale::index<0>, 0>>* sym = dynamic_cast<parameter_symbol<set<ale::index<0>, 0>>*>(value_sym);
        return { sym->m_name, { sym->m_name }, { { sym->m_value } }, {} };
    }

    template <typename TType>
    std::tuple<std::string, std::vector<std::string>, std::vector<std::list<int>>, std::vector<std::string>> operator()(constant_node<TType>* node) {
        auto ind = (expr_str->dispatch(node)).front();
        return { ind, { fmt::format("'{}'", ind) }, { { node->value } }, {} };
    }

    template <typename TType>
    std::tuple<std::string, std::vector<std::string>, std::vector<std::list<int>>, std::vector<std::string>> operator()(TType* node) {
        std::string type = typeid(TType).name();
        throw unsupportedNodeParsingException("following type is not implemented in gams: " + type);
    }

private:
    expression_stringer_unflattened* expr_str;
    symbol_table& symbols;
};

class symbol_value_getter {

public:
    std::list<int> dispatch(base_symbol* sym) {
        return std::visit(*this, sym->get_base_variant());
    }

    template <typename TType>
    std::list<int> operator()(value_symbol<TType>* sym) {
        return std::visit(*this, sym->get_value_variant());
    }

    std::list<int> operator()(parameter_symbol<ale::index<0>>* sym) {
        return { sym->m_value };
    }

    std::list<int> operator()(parameter_symbol<set<ale::index<0>, 0>>* sym) {
        return sym->m_value;
    }

    template <typename TType>
    std::list<int> operator()(TType* /*sym*/) {
        std::string type = typeid(TType).name();
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: " + type);
    }
};

class index_stringer {
public:
    index_stringer(expression_stringer_unflattened* expr_str, symbol_table& symbols, std::vector<size_t> sizes, int indexoperation_n, int parameter_dim) :
        expr_str(expr_str), symbols(symbols), sizes(sizes), index_op_str(expr_str, symbols), indexoperation_n(indexoperation_n), parameter_dim(parameter_dim) {};

    /*
		returns four vectors: 
			1: all the indices of the passed entry_node
			2: indices for map
			3: maps for mapping index operations 
			4: local sets needed for index operations
		the last three vectors are empty if no index operations in entry node
		*/

    template <typename TType>
    std::tuple<std::vector<std::string>, std::vector<std::string>, std::vector<std::vector<std::string>>, std::vector<std::string>> dispatch(value_node<TType>* node) {
        return std::visit(*this, node->get_variant());
    }

    std::tuple<std::vector<std::string>, std::vector<std::string>, std::vector<std::vector<std::string>>, std::vector<std::string>> operator()(index_addition_node* node) {
        std::tuple<std::string, std::vector<std::string>, std::vector<std::list<int>>, std::vector<std::string>> res = index_op_str.dispatch(node);
        std::get<1>(res).emplace_back(fmt::format("indexoperation_{}_res_{}", std::get<0>(res), indexoperation_n));
        std::string indexop_sets = build_indexop_sets(std::get<0>(res), std::get<2>(res), std::get<3>(res));
        return { { fmt::format("indexoperation_{}_res_{}", std::get<0>(res), indexoperation_n) }, { fmt::format("indexoperation_{}_map_{}", std::get<0>(res), indexoperation_n) }, { std::get<1>(res) }, { indexop_sets } };
    }

    std::tuple<std::vector<std::string>, std::vector<std::string>, std::vector<std::vector<std::string>>, std::vector<std::string>> operator()(index_minus_node* node) {
        // TODO: fix bug: current implementation violates operator precedence
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: index_minus_node");
        //std::tuple<std::string, std::vector<std::string>, std::vector<std::list<int>>, std::vector<std::string>> res = index_op_str.dispatch(node);
        //std::get<1>(res).emplace_back(fmt::format("indexoperation_{}_res_{}", std::get<0>(res), indexoperation_n));
        //std::string indexop_sets = build_indexop_sets(std::get<0>(res), std::get<2>(res), std::get<3>(res));
        //return { { fmt::format("indexoperation_{}_res_{}", std::get<0>(res), indexoperation_n) },{ fmt::format("indexoperation_{}_map_{}", std::get<0>(res), indexoperation_n) },{ std::get<1>(res) },{ indexop_sets } };
    }

    std::tuple<std::vector<std::string>, std::vector<std::string>, std::vector<std::vector<std::string>>, std::vector<std::string>> operator()(index_multiplication_node* node) {
        // TODO: fix bug: current implementation violates operator precedence
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: index_multiplication_node");
        //std::tuple<std::string, std::vector<std::string>, std::vector<std::list<int>>, std::vector<std::string>> res = index_op_str.dispatch(node);
        //std::get<1>(res).emplace_back(fmt::format("indexoperation_{}_res_{}", std::get<0>(res), indexoperation_n));
        //std::string indexop_sets = build_indexop_sets(std::get<0>(res), std::get<2>(res), std::get<3>(res));
        //return { { fmt::format("indexoperation_{}_res_{}", std::get<0>(res), indexoperation_n) },{ fmt::format("indexoperation_{}_map_{}", std::get<0>(res), indexoperation_n) },{ std::get<1>(res) },{ indexop_sets } };
    }

    template <unsigned IDim>
    std::tuple<std::vector<std::string>, std::vector<std::string>, std::vector<std::vector<std::string>>, std::vector<std::string>> operator()(entry_node<real<IDim>>* node) {
        dim = IDim;
        auto index_res = dispatch(node->template get_child<1>()); //index needs to be dispatched first, because next entry_node will change dim again
        auto source_res = dispatch(node->template get_child<0>());
        std::get<0>(source_res).insert(std::get<0>(source_res).end(), std::get<0>(index_res).begin(), std::get<0>(index_res).end());
        std::get<1>(source_res).insert(std::get<1>(source_res).end(), std::get<1>(index_res).begin(), std::get<1>(index_res).end());
        std::get<2>(source_res).insert(std::get<2>(source_res).end(), std::get<2>(index_res).begin(), std::get<2>(index_res).end());
        std::get<3>(source_res).insert(std::get<3>(source_res).end(), std::get<3>(index_res).begin(), std::get<3>(index_res).end());
        return source_res;
    }

    std::tuple<std::vector<std::string>, std::vector<std::string>, std::vector<std::vector<std::string>>, std::vector<std::string>> operator()(parameter_node<ale::index<0>>* node) {
        auto ind = (expr_str->dispatch(node)).front();
        std::list<int> elements = sym_val.dispatch(symbols.resolve(ind));
        index_check(elements);
        return { { ind }, {}, {}, {} };
    }

    template <typename TType>
    std::tuple<std::vector<std::string>, std::vector<std::string>, std::vector<std::vector<std::string>>, std::vector<std::string>> operator()(constant_node<TType>* node) {
        auto ind = (expr_str->dispatch(node)).front();
        index_check({ std::stoi(ind) });
        return { { fmt::format("'{}'", ind) }, {}, {}, {} };
    }

    template <typename TType>
    std::tuple<std::vector<std::string>, std::vector<std::string>, std::vector<std::vector<std::string>>, std::vector<std::string>> operator()(TType* node) {
        return { {}, {}, {}, {} };
    }

    bool index_check(std::list<int> elements) {
        // checks if all indices in the list "elements" are feasible accesses.
        // first gets max and min indices, i.e., min_value and max_value
        // then checks if min_value and max_value are within the shape of the parameter
        // E.g., the tensor x[2,8] has the "sizes" = (2,8)
        // "dim" is the current access dimension from the back
        // for "dim" = 0 we are accessing the last entry of x, i.e., x[:,accessing_here]
        // "parameter_dim" is the dimension of the tensor, here parameter_dim of x is 2

        size_t min_value = *std::min_element(elements.begin(), elements.end());
        size_t max_value = *std::max_element(elements.begin(), elements.end());
        if(min_value <= 0 || max_value > sizes.at(parameter_dim - dim - 1)) {
            std::string index_vio;
            if(min_value <= 0) {
                index_vio = std::to_string(min_value);
                if(max_value > sizes.at(parameter_dim - dim - 1)) {
                    index_vio += " and index " + std::to_string(max_value);
                }
            } else {
                index_vio = std::to_string(max_value);
            }
            std::ostringstream sizes_str;
            if(!sizes.empty()) {
                std::copy(sizes.begin(), sizes.end() - 1, std::ostream_iterator<size_t>(sizes_str, ", "));
                sizes_str << sizes.back();
            }
            throw util::wrongDimensionException("index " + index_vio
                                                + " is out of bounds at access dimension " + std::to_string(parameter_dim - dim)
                                                + ". tensor dimension is {" + sizes_str.str() + "}. ");
        }
        return true;
    }

    //build the local sets needed for index operations (res set and map)
    std::string build_indexop_sets(std::string identifier, std::vector<std::list<int>> element_lists, std::vector<std::string> operator_list) {
        std::tuple<std::list<std::string>, std::vector<std::string>> indexop_elements = build_indexop_elements({}, element_lists, 0, operator_list);
        std::get<0>(indexop_elements).sort();
        std::get<0>(indexop_elements).unique();
        std::string res_set = fmt::format("Set indexoperation_{}_res_{} /{}/; ", identifier, indexoperation_n, fmt::join(std::get<0>(indexop_elements), ","));
        std::string map_set = fmt::format("Set indexoperation_{}_map_{} /{}/;\n", identifier, indexoperation_n, fmt::join(std::get<1>(indexop_elements), ","));
        return res_set + map_set;
    }

    int calculate_index(std::list<int> values, std::vector<std::string> operators) {
        int res = *(values.rbegin());
        int i = operators.size() - 1;
        for(auto it = ++values.rbegin(); it != values.rend(); ++it) {
            if(operators.at(i) == "p") {
                res += *it;
            } else if(operators.at(i) == "t") {
                throw unsupportedNodeParsingException("conversion to GAMS input not implemented: index_multiplication_node");
                res *= *it;
            } else if(operators.at(i) == "m") {
                throw unsupportedNodeParsingException("conversion to GAMS input not implemented: index_minus_node");
                res *= -1;
                it--;
            }
            i--;
        }
        return res;
    }

    std::tuple<std::list<std::string>, std::vector<std::string>> build_indexop_elements(std::list<int> current_values, std::vector<std::list<int>> element_lists, int vector_index, std::vector<std::string> operator_list) {
        if(vector_index == element_lists.size()) {
            int res = calculate_index(current_values, operator_list);
            index_check({ res });
            return { { std::to_string(res) }, { fmt::format("{}.{}", fmt::join(current_values, "."), std::to_string(res)) } };
        }
        std::tuple<std::list<std::string>, std::vector<std::string>> res;
        for(auto it = element_lists.at(vector_index).begin(); it != element_lists.at(vector_index).end(); ++it) {
            std::list<int> temp_current_values = current_values;
            temp_current_values.emplace_back(*it);
            std::tuple<std::list<std::string>, std::vector<std::string>> temp_res = build_indexop_elements(temp_current_values, element_lists, vector_index + 1, operator_list);
            std::get<0>(res).insert(std::get<0>(res).end(), std::get<0>(temp_res).begin(), std::get<0>(temp_res).end());
            std::get<1>(res).insert(std::get<1>(res).end(), std::get<1>(temp_res).begin(), std::get<1>(temp_res).end());
        }
        return res;
    }

private:
    symbol_table& symbols;
    expression_stringer_unflattened* expr_str;
    std::vector<size_t> sizes {};
    size_t dim {};
    index_operation_stringer index_op_str;
    symbol_value_getter sym_val;
    int indexoperation_n;
    int parameter_dim;
};



} // namespace dips
