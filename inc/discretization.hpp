#pragma once

#include <string>
#include <vector>
#include <utility>

namespace dips {



struct discretization {
    std::string indexes;
    std::vector<std::pair<std::string, std::string>> parameters;
    std::vector<std::pair<std::string, std::string>> variables;
    //std::string restriction;
    //std::string target;
};



} // namespace dips