#pragma once

#include <atomic>  // for atomic
#include <mutex>   // for mutex
#include <utility> // for move
#include <string>  // for string, allocator

#include "solver.hpp"         // for solver, solve_record
#include "symbol_table.hpp"   // for symbol_table
#include "ilcplex/ilocplex.h" // for cplex
#include "helper.hpp"         // for ale
#include "program.hpp"        // for program

class IloEnv;



namespace dips {




using namespace ale;

class cplex_solver : public solver {
public:
    cplex_solver(symbol_table& symbols, const std::string& cplex_solver_name = "cplex/") :
        symbols(symbols),
        solver(cplex_solver_name) {};
    static solver* make_solver(symbol_table& symbols, std::string cplex_solver_name) {
        return new cplex_solver(symbols, std::move(cplex_solver_name));
    }
    solve_record solve(program prog) final;
    void abort() override;
    bool set_option(std::string, double) final;
    bool set_option(std::string, std::string) final;
    double get_option(std::string) override;

private:
    symbol_table& symbols;
    std::mutex solve_mutex;

    std::atomic<bool> aborted = false;

    class abort_callback : public IloCplex::MIPCallbackI {
    public:
        abort_callback(IloEnv&, std::atomic<bool>&);

    private:
        void main();
        [[nodiscard]] IloCplex::CallbackI* duplicateCallback() const;

        std::atomic<bool>& aborted;
    };
    IloCplex::Callback make_abort_callback(IloEnv&, std::atomic<bool>&);

    std::string settings_file = "settings"; // loads 'settings.prm' by default
    double abs_tol = 1e-6;
    double rel_tol = 1e-4;
    int num_threads = 1;
    double int_tol = 1e-5; //CPLEX_default
    int lp_alg = 0;
};



} // namespace dips
