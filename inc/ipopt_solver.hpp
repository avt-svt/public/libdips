#pragma once

#include "solver.hpp"
#include "symbol_table.hpp"

namespace dips {

using namespace ale;

/**
 * @class IpoptSolver
 * @brief Specialization of the abstract solver class for Ipopt
 *
 * This class is to be used with the Ipopt_problem class, which does the actual interfacing between
 * Ipopt C++ API and Libdips.
 */
class ipopt_solver : public solver {
public:
    /**
     * @brief Standard constructor
     */
    ipopt_solver(symbol_table&, const std::string& name = "ipopt_solver");

    /**
     * @brief Factory method
     */
    static solver* make_solver(symbol_table& symbols, std::string ipopt_solver_name) {
        return new ipopt_solver(symbols, std::move(ipopt_solver_name));
    }

    /**
     * @brief Call to Ipopt for solving
     */
    solve_record solve(program prog) final;

    /**
     * @brief Set numerical Ipopt options
     */
    bool set_option(std::string option, double value) final;

    /**
     * @brief Set textual Ipopt options
     */
    bool set_option(std::string option, std::string value) final;

    /**
     * @brief Return numerical Ipopt options
     */
    double get_option(std::string option) final;

private:
    symbol_table& symbols;       /*!< Reference to symbol table */
    double tol;                  /*!< Solver option: tolerance (see Ipopt doc) */
    double scaling_factor;       /*!< Solver option: objective function scaling factor (see Ipopt doc) */
    int max_iter;                /*!< Solver option: maximum iterations (see Ipopt doc) */
    std::string mu_strategy;     /*!< Solver option (see Ipopt doc) */
    std::string output_file;     /*!< Solver option: output file (see Ipopt doc) */
    std::string derivative_test; /*!< Solver option: Test derivative (see Ipopt doc) */
    std::vector<double> old_solution;
    bool old_solution_set = false;
    bool convex_qcqp = false; /*!< Solver option: uses the fact that equality constraints are linear and hessians are constant for a convex qcqp */
};

} // namespace dips
