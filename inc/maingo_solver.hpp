#pragma once

#include <stddef.h>      // for size_t
#include <utility>       // for move
#include <exception>     // for invalid_argument
#include <string>        // for string, allocator, operator==, hash
#include <unordered_map> // for unordered_map
#include <variant>       // for visit, variant
#include <vector>        // for vector
#include <stdexcept>     // for invalid_argument

#include "solver.hpp"       // for solver, solve_record
#include "symbol_table.hpp" // for symbol_table
#include "MAiNGO.h"
#include "helper.hpp"  // for ale
#include "symbol.hpp"  // for parameter_symbol, base_symbol, function_symbol, value_symbol, variable_symbol
#include "value.hpp"   // for real
#include "program.hpp" // for program



namespace dips {




using namespace ale;

class maingo_solver : public solver {
public:
    maingo_solver(symbol_table& symbols, std::string maingo_solver_name = "maingo/") :
        symbols(symbols),
        solver(std::move(maingo_solver_name)) {};
    static solver* make_solver(symbol_table& symbols, std::string maingo_solver_name) {
        return new maingo_solver(symbols, std::move(maingo_solver_name));
    }
    solve_record solve(program prog) final;
    bool set_option(std::string, double) final;
    bool set_option(std::string, std::string) final;
    double get_option(std::string) final;

private:
    symbol_table& symbols;
    std::string settings_file = "settings.txt";
    double abs_tol = 1e-6;
    double rel_tol = 1e-4;
    double max_time = 86400.0;
};

class maingo_symbol_reader {
public:
    maingo_symbol_reader(
      const std::vector<double>& variables,
      const std::unordered_map<std::string, int>& positions) :
        m_variables(variables),
        m_positions(positions) {};

    // base_symbol dispatch
    void dispatch(base_symbol* sym) {
        std::visit(*this, sym->get_base_variant());
    }

    template <typename TType>
    void operator()(value_symbol<TType>* sym) {
        std::visit(*this, sym->get_value_variant());
    }

    // symbol visits
    template <typename TType>
    void operator()(parameter_symbol<TType>* /*sym*/) {
        throw std::invalid_argument("cannot read non-real symbol from MAiNGO");
    }

    template <unsigned IDim>
    void operator()(parameter_symbol<real<IDim>>* sym) {
        size_t indexes[IDim];
        for(int i = 0; i < IDim; ++i) {
            indexes[i] = 0;
        }
        int offset = 0;
        while(indexes[0] < sym->m_value.shape(0)) {
            sym->m_value[indexes] = m_variables[m_positions.at(sym->m_name) + offset];
            offset++;
            for(int i = IDim - 1; i >= 0; --i) {
                if(++indexes[i] < sym->m_value.shape(i)) {
                    break;
                } else if(i != 0) {
                    indexes[i] = 0;
                }
            }
        }
    }

    void operator()(parameter_symbol<real<0>>* sym) {
        sym->m_value = m_variables[m_positions.at(sym->m_name)];
    }

    template <typename TType>
    void operator()(variable_symbol<TType>* /*sym*/) {
        throw std::invalid_argument("cannot read to variable symbol");
    }

    template <typename TType>
    void operator()(function_symbol<TType>* /*sym*/) {
        throw std::invalid_argument("cannot read function symbol");
    }

private:
    const std::vector<double>& m_variables;
    const std::unordered_map<std::string, int>& m_positions;
};



} // namespace dips
