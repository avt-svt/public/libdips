#pragma once

#include <nlohmann/json.hpp> // for basic_json<>::object_t, json, basic_json<>::value_type, NLOHMANN_JSON_SERIALIZE_ENUM
#include <stddef.h>          // for size_t
#include <stdint.h>          // for uint8_t
#include <fstream>
#include <memory>        // for unique_ptr
#include <algorithm>     // for copy, max
#include <array>         // for array
#include <iostream>      // for operator<<, ofstream, basic_ostream, basic_ostream<>::__ostream_type, cout, ostream
#include <exception>     // for out_of_range, invalid_argument
#include <string>        // for operator<<, char_traits, string
#include <unordered_map> // for operator==, _Node_const_iterator, _Node_iterator_base, unordered_map<>::const_iterator
#include <utility>       // for pair
#include <variant>       // for visit, variant
#include <vector>        // for vector
#include <stdexcept>     // for out_of_range, invalid_argument

#include "solver.hpp" // for solve_status, solve_record, solution, ABNORMAL, ABORTED, FEASIBLE, GLOBAL, INFEASIBLE, LOCAL, UNBOUNDED, UNKNOWN
#include "symbol.hpp" // for parameter_symbol, base_symbol, cast_parameter_symbol, parameter_symbol<>::basic_type, value_symbol
#include "tensor.hpp" // for tensor, tensor_ref
#include "value.hpp"  // for real, index, tensor_type<>::basic_type

inline void vector_to_file(const ale::parameter_symbol<ale::real<1>>& sym, const std::string& file) {

    std::ofstream output;
    output.open(file);
    for(int i = 0; i < sym.m_value.shape(0); ++i) {
        output << sym.m_value[i] << '\n';
    }
    output.close();
}
inline void vector_to_file(const std::string& name, const dips::solve_record& rec, const std::string& file) {
    auto it = rec.m_solution.find(name);
    if(it == rec.m_solution.end()) {
        std::cout << "ERROR: unable to find " << name << " in feasible solve record\n";
        return;
    }
    ale::parameter_symbol<ale::real<1>>* sym = ale::cast_parameter_symbol<ale::real<1>>(it->second.get());
    if(sym == nullptr) {
        std::cout << "ERROR: symbol " << name << " has unexpected type\n";
        return;
    }
    //vector_to_file(*sym,file);
    std::ofstream output;
    output.open(file);
    for(int i = 0; i < sym->m_value.shape(0); ++i) {
        output << sym->m_value[i] << '\n';
    }
    output.close();
}

//serilizing parameter_symbols
namespace ale {
template <typename T, unsigned N>
struct nested_vector_struct {
    using vec = std::vector<typename nested_vector_struct<T, N - 1>::vec>;
};
template <typename T>
struct nested_vector_struct<T, 0> {
    using vec = T;
};

template <typename T, unsigned N>
using nested_vector = typename nested_vector_struct<T, N>::vec;

template <typename TType, unsigned IDim>
auto tensor_as_nested_vector(tensor_ref<TType, IDim> ten) {

    if constexpr(IDim > 1) {

        using vector_element_type = nested_vector<TType, IDim - 1>;
        std::vector<vector_element_type> data;
        for(size_t i = 0; i < ten.shape(0); ++i) {
            //ten[i] is tensor_ref<TType,IDim-1>
            data.push_back(tensor_as_nested_vector<TType, IDim - 1>(ten[i]));
        }
        return data;
    } else {
        std::vector<TType> data;
        for(size_t i = 0; i < ten.shape(0); ++i) {
            data.push_back(ten[i]);
        }
        return data;
    }
}

template <typename TType, unsigned IDim>
auto tensor_as_nested_vector(tensor<TType, IDim> ten) {
    return tensor_as_nested_vector(ten.ref());
}
//given e.g T=base_real and N=2 will require std::vector<std::vector<double>> as first argument
template <class T, unsigned N>
tensor<typename T::basic_type, N> tensor_from_nested_vector(const nested_vector<typename T::basic_type, N>& vec, const std::array<size_t, N>& shape) {
    tensor<typename T::basic_type, N> t(shape);
    std::array<size_t, N - 1> new_shape;

    if(N > 1) {
        std::copy(shape.begin() + 1, shape.end(), new_shape.begin());
    }
    for(int i = 0; i < t.shape(0); i++) {
        if constexpr(N > 1) {
            t[i].assign(tensor_from_nested_vector<T, N - 1>(vec.at(i), new_shape));
        }
        if constexpr(N == 1) {
            t[i] = (vec.at(i)); //TODO: check if this was a cause for invalid reads
        }
    }
    return t;
}
struct parameter_symbol_serializer //(parcloner is used to copy values to solve record -> only contains parameters
{

    nlohmann::json dispatch(base_symbol* sym) {
        return std::visit(*this, sym->get_base_variant());
    }
    template <typename TType>
    nlohmann::json operator()(value_symbol<TType>* sym) {
        return std::visit(*this, sym->get_value_variant());
    }

    nlohmann::json operator()(ale::parameter_symbol<real<0>>* s) {
        nlohmann::json j;
        j["name"] = s->m_name;
        j["shape"] = std::vector<unsigned>();
        j["type"] = "real";
        j["value"] = s->m_value;
        return j;
    };
    template <unsigned IDIM>
    nlohmann::json operator()(ale::parameter_symbol<real<IDIM>>* s) {
        nlohmann::json j;
        j["name"] = s->m_name;
        std::array<size_t, IDIM> shape = s->m_value.shape();
        j["shape"] = shape;
        j["type"] = "real";
        j["value"] = tensor_as_nested_vector(s->m_value);
        return j;
    };
    nlohmann::json operator()(ale::parameter_symbol<index<0>>* s) {
        nlohmann::json j;
        j["name"] = s->m_name;
        j["shape"] = std::vector<unsigned>();
        j["type"] = "index";
        j["value"] = s->m_value;
        return j;
    };
    template <unsigned IDIM>
    nlohmann::json operator()(ale::parameter_symbol<index<IDIM>>* s) {
        nlohmann::json j;
        j["name"] = s->m_name;
        std::array<size_t, IDIM> shape = s->m_value.shape();
        j["shape"] = shape;
        j["type"] = "index";
        j["value"] = tensor_as_nested_vector(s->m_value);
        return j;
    };
    template <class T>
    nlohmann::json operator()(T /*s*/) {
        throw std::invalid_argument("Conversion to JSON only possible for parameter_symbols.");
    }
};

//define serialization of base_symbol (only valid for parameter symbol)
void to_json(nlohmann::json& j, ale::base_symbol* const& s);

//define serialization of base_symbol unique_ptr
void to_json(nlohmann::json& j, const std::unique_ptr<ale::base_symbol>& s);
void from_json(const nlohmann::json& j, std::unique_ptr<ale::base_symbol>& s);

} //namespace ale

namespace dips {
// map solve_status values to JSON as strings
NLOHMANN_JSON_SERIALIZE_ENUM(dips::solve_status, {
                                                   { ABNORMAL, "ABNORMAL" },
                                                   { ABORTED, "ABORTED" },
                                                   { UNKNOWN, "UNKNOWN" },
                                                   { INFEASIBLE, "INFEASIBLE" },
                                                   { FEASIBLE, "FEASIBLE" },
                                                   { LOCAL, "LOCAL" },
                                                   { GLOBAL, "GLOBAL" },
                                                   { UNBOUNDED, "UNBOUNDED" },
                                                 })

void to_json(nlohmann::json& j, const dips::solve_record& s);
void from_json(const nlohmann::json& j, dips::solve_record& s);

} //namespace dips
