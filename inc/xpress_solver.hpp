#pragma once

#include <atomic> // for atomic
#include <string> // for string, allocator

#include "solver.hpp" // for solve_record, solver
#include "symbol_table.hpp"
#include "helper.hpp" // for ale

namespace ale {
struct symbol_table;
} // namespace ale

namespace dips {
struct program;

using namespace ale;

class xpress_solver : public solver {
public:
    explicit xpress_solver(symbol_table&, std::string name = "./xpress/");

    // return a new heap allocated xpress_solver
    static solver* make_solver(symbol_table& symbols, std::string xpress_solver_name);

    // try to solve the model and return a solve_record of the solution
    solve_record solve(program prog) final;

    // aborts (all) optimizations currently in progress
    void abort() override;

    // set option of name 'option' ('abs_tol', 'rel_tol', 'num_threads', 'lp_alg', 'int_tol', 'settings') to value
    // returns false if the option name  is not valid or the value is outside the accepted range
    // otherwise it returns true
    bool set_option(std::string option, double value) final;
    bool set_option(std::string option, std::string value) final;

    double get_option(std::string option) final;

private:
    symbol_table& symbols;
    std::string solver_name;

    std::atomic<bool> abort_optimization = false;

    std::string settings_file = "settings.txt";
    double abs_tol = 1e-6;
    double rel_tol = 1e-4;
    int num_threads = 1;
    int lp_alg = -1;
    double int_tol = 1e-5;
    int non_convex = -1;
    double time_limit = 1e100;
    int solution_limit = 2000000000;
    bool int_focus = true;
};

} // namespace dips
