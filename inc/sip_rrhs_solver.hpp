#pragma once

#include <atomic>     // for atomic
#include <functional> // for function
#include <utility>    // for move, pair
#include <iosfwd>     // for istream
#include <string>     // for string, allocator, basic_string
#include <vector>     // for vector

#include "common.hpp"
#include "solver.hpp"         // for set_tolerance_robust_base, solve_record, solver
#include "parser_chain.hpp"   // for programdefinition_parser_chain
#include "discretization.hpp" // for discretization
#include "helper.hpp"         // for ale
#include "program.hpp"        // for program
#include "symbol_table.hpp"   // for symbol_table



namespace dips {



using namespace ale;

class sip_rrhs_program_definition_parser : public programdefinition_parser_chain {
public:
    sip_rrhs_program_definition_parser(std::istream&, symbol_table&);
};

class sip_rrhs_solver {
    // algorithm statement based on algorithm
    // 3.1 in Djelassi, H.:
    // Discretization-Based Algorithms for the Global
    // Solution of Hierarchical Programs.
    // Dissertation, RWTH Aachen University (2020)
    // which itself is a revised version of the
    // original algorithm proposed by
    // Mitsos, Alexander (2011):
    // Global optimization of semi-infinite programs
    // via restriction of the right-hand side.
    // In Optim. 60 (10-11), pp. 1291-1308.
    // DOI: 10.1080/02331934.2010.527970.
    // Extensions:
    //  - support multiple llps
    //  - discretization of all or only single llp
    //    possible
    //  - added feasibility tolerance to be able to
    //    compare to sip_bnf_solver
    //  - added min_eps_res to terminate if eps_res
    //    < min_eps_res, which likely corresponds to
    //    a assumption violatino (no LLP-Slater
    //    point exists)
    //  - the termination critereon is checked more
    //    often
    //  - iterations are differently defined

public:
    struct program_info {
        void add_llp(const program&, const discretization&, const discretization&);

        program lbp;
        program ubp;
        std::vector<program> llps;
        std::vector<discretization> lbp_discs;
        std::vector<discretization> ubp_discs;
        std::string ubp_restrict;

        std::map<std::string, std::string> lbp_info, ubp_info;
        std::map<std::string, std::vector<std::string>> lbp_info_list, ubp_info_list;
    };

    sip_rrhs_solver(symbol_table& symbols, std::function<solver*(symbol_table&, std::string)> make_subsolver, std::string solver_name = "sip_output_rrhs/") :
        symbols(symbols),
        make_subsolver(std::move(make_subsolver)),
        solver_base_name(std::move(solver_name)) {};
    solve_record solve(program_info);
    bool set_option(const std::string&, double);
    bool set_option(const std::string&, const std::string&);
    bool set_option(const std::string&, bool);
    void set_tolerance_robust(std::string option, double value) { set_tolerance_robust_base(*this, std::move(option), value); };
    void pass_option(const std::string&, double);
    [[nodiscard]] double get_option(const std::string&) const;
    void set_solver_base_name(std::string);
    std::string get_solver_base_name();

    void abort() {
        aborted = true;
        for(const auto& cb : abort_subsolver_callbacks) {
            cb();
        }
    };

private:
    std::function<solver*(symbol_table&, std::string)> make_subsolver;
    std::vector<std::function<void()>> abort_subsolver_callbacks;
    //add make_aborting subsolver?
    symbol_table& symbols;
    std::string solver_base_name;
    std::vector<std::pair<std::string, double>> options;

    std::string settings_lbp = "settings_lbp.txt";
    std::string settings_ubp = "settings_ubp.txt";
    std::string settings_lbp_llp = "settings_llp.txt";
    std::string settings_ubp_llp = "settings_llp.txt";


    double feas_tol = 0; // must be >= 0
    double abs_tol = 1e-2;
    double rel_tol = 1e-2;

    double abs_tol_lbp = 1e-1 * abs_tol;
    double rel_tol_lbp = 1e-1 * rel_tol;
    double abs_tol_lbp_llp = 1e-2 * abs_tol;
    double rel_tol_lbp_llp = 1e-2 * rel_tol;

    double abs_tol_ubp = 1e-1 * abs_tol;
    double rel_tol_ubp = 1e-1 * rel_tol;
    double abs_tol_ubp_llp = 1e-2 * abs_tol;
    double rel_tol_ubp_llp = 1e-2 * rel_tol;

    double red_tol_lbp_llp = 1.25; // must be > 1
    double red_tol_ubp_llp = 1.25; // must be > 1

    double init_res = 0.1;     // inital restriction parameter; must be > 0
    double eps_res = init_res; // restricion parameter
    double red_res = 10;       // reduction parameter of eps_res; must be > 1
    double min_eps_res = 1e-9; // if eps_res is reduced to a value < min_eps_res the algorithm aborts

    int max_iter = 1000;
    double max_time = 86400;

    bool discr_all = false;
    double ubp_guard = -1;         // ubp is only computed if feas_vio of lbp is less than ubp_guard; to turn of set ubp_guard = -1
    bool init_res_dynamic = false; // if ubp_guard is on: dynamically overwrite eps_res_intit with feas_vio_lbp when the ubp is first computed
    std::atomic<bool> aborted = false;
};

} // namespace dips
