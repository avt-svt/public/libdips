#pragma once

#include "solver.hpp"
#include "expression.hpp"
#include "symbol_table.hpp"
#include "symbol_finder.hpp"
#include "util/evaluator.hpp"
#include "util/visitor_utils.hpp"
#include "gams_solver.hpp"
#include "gams_parser.hpp"

//#include <iostream>
#include <deque>
#include <algorithm>
#include <exception>
#include <sstream>
#include <vector>
#include <type_traits>
#include <stack>
#include <cmath>

namespace dips {



using namespace ale;
using namespace ale::util;

class gams_parser_flattened : protected gams_parser {
public:
    virtual ~gams_parser_flattened() = default;
    gams_parser_flattened(program& prog, symbol_table& symbols, std::string& solver_type, std::string& problem_class, double decimals) :
        gams_parser(prog, symbols, solver_type, problem_class, decimals) {};
    void parse_gams(std::stringstream& input) final;

private:
    bool max_min_abs_ref = true;
};


class expression_stringer_flattened {
public:
    expression_stringer_flattened(symbol_table& symbols) :
        m_symbols(symbols) {};

    // expression dispatch
    template <typename TReturn, typename TType>
    TReturn dispatch(expression<TType>& /*expr*/) {
        throw unsupportedNodeParsingException("expression dispatch not implemented for GAMS");
    }

    std::string dispatch(expression<real<0>>& expr) {
        return dispatch(expr.get());
    }

    std::vector<std::string> dispatch(expression<ale::boolean<0>>& expr) {
        return dispatch(expr.get());
    }

    // value_node dispatcht
    template <typename TReturn, typename TType>
    TReturn dispatch(value_node<TType>* /*node*/) {
        std::string type = typeid(TType).name();
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: value_node<" + type + ">");
    }

    template <typename TType>
    typename set<TType, 0>::basic_type dispatch(value_node<set<TType, 0>>* node) {
        return evaluate_expression(node, m_symbols);
    }

    template <unsigned IDim>
    std::string dispatch(value_node<real<IDim>>* node) {
        return std::visit(*this, node->get_variant());
    }

    std::string dispatch(value_node<real<0>>* node) {
        return std::visit(*this, node->get_variant());
    }

    int dispatch(value_node<index<0>>* node) {
        return evaluate_expression(node, m_symbols);
    }

    std::vector<std::string> dispatch(value_node<ale::boolean<0>>* node) {
        return std::vector(std::visit(*this, node->get_variant()));
    }

    // terminal visits
    template <typename TType>
    std::string operator()(constant_node<TType>* /*node*/) {
        std::string type = typeid(TType).name();
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: constant_node<" + type + ">");
    }

    static std::string getValue(double value) {
        std::ostringstream out;
        out.precision(16);
        out << '(' << std::fixed << value << ')';
        return std::move(out).str();
    }

    std::string getValue(tensor<double, 1> value) {
        int ind = index_stack.top();
        index_stack.pop();
        return getValue(value[ind]);
    }

    template <unsigned IDim>
    std::string getValue(tensor<double, IDim> value) {
        int ind = index_stack.top();
        index_stack.pop();
        return getValue(value[ind]);
    }

    template <unsigned IDim>
    std::string getValue(tensor_ref<double, 1> value) {
        int ind = index_stack.top();
        index_stack.pop();
        return getValue(value[ind]);
    }

    template <unsigned IDim>
    std::string getValue(tensor_ref<double, IDim> value) {
        int ind = index_stack.top();
        index_stack.pop();
        return getValue(value[ind]);
    }

    std::string getValue(tensor_cref<double, 1> value) {
        int ind = index_stack.top();
        index_stack.pop();
        return getValue(value[ind]);
    }

    template <unsigned IDim>
    std::string getValue(tensor_cref<double, IDim> value) {
        int ind = index_stack.top();
        index_stack.pop();
        return getValue(value[ind]);
    }

    template <unsigned IDim>
    std::string operator()(constant_node<real<IDim>>* node) {
        return getValue(node->value);
    }

    std::string operator()(constant_node<real<0>>* node) {
        return getValue(node->value);
    }

    std::vector<std::string> operator()(constant_node<ale::boolean<0>>* node) {
        return std::vector<std::string>(1, getValue(node->value));
    }

    template <typename TType>
    std::string operator()(parameter_node<TType>* /*node*/) {
        std::string type = typeid(TType).name();
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: parameter_node<" + type + ">");
    }

    template <unsigned IDim>
    std::string operator()(parameter_node<real<IDim>>* node) {
        auto sym = m_symbols.resolve<real<IDim>>(node->name);
        if(!sym) {
            throw std::invalid_argument("symbol \"" + node->name + "\" is ill-defined");
        }
        auto par_sym = dynamic_cast<parameter_symbol<real<IDim>>*>(sym);
        // if parameter names should be used, uncomment the following code
        // then also the corresponding find and parse parameters in
        // the gams_parser_flattened::parse_gams
        if(par_sym != nullptr) {
            return getValue(par_sym->m_value);
        }
        std::string index_string = "";
        for(int i = 0; i < IDim; i++) {
            index_string += "_" + std::to_string(index_stack.top() + 1);
            index_stack.pop();
        }
        return node->name + index_string;
    }

    std::vector<std::string> operator()(parameter_node<ale::boolean<0>>* node) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: parameter_node<boolean<0>> with name \"" + node->name + "\"");
    }

    template <typename TType>
    std::vector<std::string> operator()(attribute_node<TType>* node) {
        std::string type = typeid(TType).name();
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: symbol " + node->variable_name + " has unsupported type \"" + type + "\" in attribute call.");
    }

    template <unsigned IDim>
    std::string operator()(attribute_node<real<IDim>>* node) {
        variable_symbol<real<IDim>>* sym = cast_variable_symbol<real<IDim>>(m_symbols.resolve(node->variable_name));
        if(!sym) {
            throw std::invalid_argument("symbol " + node->variable_name + " has unexpected type in attribute call.");
        }
        switch(node->attribute) {
            case variable_attribute_type::INIT:
                return getValue(sym->init());
            case variable_attribute_type::LB:
                return getValue(sym->lower());
            case variable_attribute_type::UB:
                return getValue(sym->upper());
            case variable_attribute_type::PRIO:
                return getValue(sym->prio());
            default:
                throw std::invalid_argument("symbol " + node->variable_name + " has unexpected attribute.");
        }
    }

    // non-terminal visits
    template <typename TType>
    std::string operator()(TType* /*node*/) {
        std::string type = typeid(TType).name();
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: " + type);
    }

    template <unsigned IDim>
    std::string getBaseIdentifier(entry_node<real<IDim>>* node) {
        auto child_node = dynamic_cast<entry_node<real<IDim + 1>>*>(node->template get_child<0>());
        if(child_node == nullptr) {
            return expression_to_string(node->template get_child<0>());
        }
        return getBaseIdentifier(child_node);
    }

    std::string getBaseIdentifier(entry_node<real<LIBALE_MAX_DIM - 1>>* node) {
        return expression_to_string(node->template get_child<0>());
    }

    template <unsigned IDim>
    std::string operator()(entry_node<real<IDim>>* node) {
        int access_index = dispatch(node->template get_child<1>());
        size_t stack_size = index_stack.size();
        index_stack.push(access_index - 1);
        auto expr_sizes = get_expression_shape(node->template get_child<0>(), m_symbols);
        // expr_sizes is the size of the symbol at the access
        // e.g. we have real[2,3,10] z and
        // want to access z[1,100,1]. expr_sizes is then (3,10)
        // want to access z[1,1,100]. expr_sizes is then (10)
        // we want to access the entry corresponding to expr_sizes[0]

        if(1 > access_index || access_index > expr_sizes[0]) {
            std::string name = getBaseIdentifier(node);
            std::string err_msg = "Dimension access violation in tensor \"" + name + "\": index " + std::to_string(access_index) + " is out of bounds";
            size_t access_dim;
            std::vector<size_t> para_sizes;
            std::ostringstream sizes_str;
            try {
                para_sizes = get_parameter_shape(name, m_symbols);
                access_dim = para_sizes.size() - (expr_sizes.size() - 1);
                if(!para_sizes.empty()) {
                    std::copy(para_sizes.begin(), para_sizes.end() - 1, std::ostream_iterator<size_t>(sizes_str, ", "));
                    sizes_str << para_sizes.back();
                }
                err_msg += " at access dimension " + std::to_string(access_dim) + ". tensor dimension is {" + sizes_str.str() + "}.";
            } catch(std::invalid_argument& e) {
                sizes_str << expr_sizes[0];
                err_msg += ". tensor dimension at access is {" + sizes_str.str() + "}.";
            }
            throw std::invalid_argument(err_msg);
        }

        std::string res = dispatch(node->template get_child<0>());
        while(index_stack.size() > stack_size) index_stack.pop();
        return res;
    }

    template <unsigned IDim>
    std::string operator()(vector_node<real<IDim>>* node) {
        return dispatch(node->template get_child<0>());
    }

    std::string operator()(function_node<real<0>>* node) {
        return evaluate_function(*this, node, m_symbols);
    }

    template <unsigned IDim>
    std::string operator()(function_node<real<IDim>>* node) {
        return evaluate_function(*this, node, m_symbols);
    }


    template <unsigned IDim>
    std::string operator()(tensor_node<real<IDim>>* node) {
        size_t ind = index_stack.top();
        index_stack.pop();
        auto child = (node->children).begin();
        std::advance(child, ind);
        return dispatch(child->get());
    }

    template <unsigned IDim>
    std::string operator()(index_shift_node<real<IDim>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: index_shift_node<real<" + std::to_string(IDim) + ">>");
    }

    // non-terminal real node visits
    std::string operator()(minus_node* node) {
        return "(-(" + dispatch(node->get_child<0>()) + "))";
    }

    std::string operator()(round_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: round_node");
    }

    std::string operator()(index_to_real_node* node) {
        int value = dispatch(node->get_child<0>());
        return getValue((double)value);
    }

    std::string operator()(inverse_node* node) {
        return "(1/(" + dispatch(node->get_child<0>()) + "))";
    }

    std::string operator()(addition_node* node) {
        std::string result = "(";
        for(auto it = node->children.begin(); it != node->children.end(); ++it) {
            if(it != node->children.begin()) {
                result += "+";
            }
            result += dispatch(it->get());
        }
        result += ")";
        return result;
    }

    std::string operator()(multiplication_node* node) {
        std::string result = "(";
        for(auto it = node->children.begin(); it != node->children.end(); ++it) {
            if(it != node->children.begin()) {
                result += "*";
            }
            result += dispatch(it->get());
        }
        result += ")";
        return result;
    }

    std::string operator()(exponentiation_node* node) {
        bool real_exponent = true;
        for(auto it = node->children.begin(); it != node->children.end(); ++it) {
            if(it != node->children.begin()) {
                if(is_tree_constant(it->get(), m_symbols)) {
                    if((int)evaluate_expression(it->get(), m_symbols) == (double)evaluate_expression(it->get(), m_symbols)) {
                        real_exponent = false;
                    }
                }
            }
        }
        if(real_exponent) {
            std::string result = "(";
            for(auto it = node->children.begin(); it != node->children.end(); ++it) {
                if(it != node->children.begin()) {
                    result += "**";
                }
                result += dispatch(it->get());
            }
            result += ")";
            return result;
        } else {
            int nchild = node->children.size();
            std::string result;
            for(int i = 0; i < (nchild - 1); ++i) {
                result += "power(";
            }
            for(auto it = node->children.begin(); it != node->children.end(); ++it) {
                if((it != node->children.end()) && (it != node->children.begin())) {
                    result += ",";
                }
                result += '(' + dispatch(it->get()) + ')';
                if(it != node->children.begin()) {
                    result += ")";
                }
            }
            return result;
        }
    }

    std::string operator()(exp_node* node) {
        return "exp(" + dispatch(node->get_child<0>()) + ")";
    }

    std::string operator()(xexpy_node* node) {
        return "((" + dispatch(node->get_child<0>()) + ")*exp(" + dispatch(node->get_child<1>()) + "))";
    }

    std::string operator()(xexpax_node* node) {
        return "((" + dispatch(node->get_child<0>()) + ")*exp((" + dispatch(node->get_child<0>()) + ")*(" + dispatch(node->get_child<1>()) + ")))";
    }

    std::string operator()(log_node* node) {
        return "log(" + dispatch(node->get_child<0>()) + ")";
    }

    std::string operator()(xlogx_node* node) {
        return "(" + dispatch(node->get_child<0>()) + "*log(" + dispatch(node->get_child<0>()) + "))";
    }

    std::string operator()(xlog_sum_node* node) {
        if(!(node->children.size() % 2 == 0)) {
            throw std::invalid_argument("called xlog_sum with odd number of arguments");
        }
        if(node->children.size() < 2) {
            throw std::invalid_argument("called xlog_sum with less than 2 arguments");
        }
        std::vector<std::string> vars;
        std::vector<std::string> coeff;
        for(auto it = node->children.begin(); it != node->children.end(); ++it) {
            if(is_tree_constant(it->get(), m_symbols)) {
                if(!(evaluate_expression(it->get(), m_symbols) > 0)) {
                    throw std::invalid_argument("called xlog_sum with non-positive argument");
                }
            }
            if(distance(node->children.begin(), it) < (int)(node->children.size() / 2)) {
                vars.emplace_back(dispatch(it->get()));
            } else {
                coeff.emplace_back(dispatch(it->get()));
            }
        }
        std::string partial;
        for(int i = 0; i < (int)(node->children.size() / 2); ++i) {
            if(i != 0) {
                partial += "+";
            }
            partial += coeff[i] + "*" + vars[i];
        }
        return "(" + vars[0] + "*log(" + partial + "))";
    }

    std::string operator()(sqrt_node* node) {
        return "sqrt(" + dispatch(node->get_child<0>()) + ")";
    }

    std::string operator()(sin_node* node) {
        return "sin(" + dispatch(node->get_child<0>()) + ")";
    }

    std::string operator()(sinh_node* node) {
        return "sinh(" + dispatch(node->get_child<0>()) + ")";
    }

    std::string operator()(asin_node* node) {
        return "arcsin(" + dispatch(node->get_child<0>()) + ")";
    }

    std::string operator()(asinh_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: asinh");
    }

    std::string operator()(cos_node* node) {
        return "cos(" + dispatch(node->get_child<0>()) + ")";
    }

    std::string operator()(cosh_node* node) {
        return "cosh(" + dispatch(node->get_child<0>()) + ")";
    }

    std::string operator()(coth_node* node) {
        return "(cosh(" + dispatch(node->get_child<0>()) + ")/" + "sinh(" + dispatch(node->get_child<0>()) + "))";
    }

    std::string operator()(acoth_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: acoth");
    }

    std::string operator()(acos_node* node) {
        return "arccos(" + dispatch(node->get_child<0>()) + ")";
    }

    std::string operator()(acosh_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: acosh");
    }

    std::string operator()(tan_node* node) {
        return "tan(" + dispatch(node->get_child<0>()) + ")";
    }

    std::string operator()(tanh_node* node) {
        return "tanh(" + dispatch(node->get_child<0>()) + ")";
    }

    std::string operator()(atan_node* node) {
        return "arctan(" + dispatch(node->get_child<0>()) + ")";
    }

    std::string operator()(atanh_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: atanh");
    }

    std::string operator()(arh_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: arh");
    }

    std::string operator()(lmtd_node* node) {
        std::string dT1 = dispatch(node->get_child<0>());
        std::string dT2 = dispatch(node->get_child<1>());
        return "((" + dT1 + "-" + dT2 + ")/log(" + dT1 + "/" + dT2 + "))";
    }

    std::string operator()(rlmtd_node* node) {
        std::string dT1 = dispatch(node->get_child<0>());
        std::string dT2 = dispatch(node->get_child<1>());
        return "(log(" + dT1 + "/" + dT2 + ")/(" + dT1 + "-" + dT2 + "))";
    }

    std::string operator()(cost_turton_node* node) {
        std::string x = dispatch(node->get_child<0>());
        std::string p1 = dispatch(node->get_child<1>());
        std::string p2 = dispatch(node->get_child<2>());
        std::string p3 = dispatch(node->get_child<3>());
        std::cout << "Warning cost_turton: Base of real power (**) ist not allowed to be negative"
                  << "\n";
        return "((" + std::to_string(10.) + "**" + p1 + "+" + p2 + "*log(" + x + ")/log(" + std::to_string(10.) + ")+" + p3 + "*power(log(" + x + ")/log(" + std::to_string(10.) + ")," + "2" + ")))";
    }

    std::string operator()(abs_node* node) {
        return "abs(" + dispatch(node->get_child<0>()) + ")";
    }

    std::string operator()(pos_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: pos_node");
    }

    std::string operator()(neg_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: neg_node");
    }

    std::string operator()(lb_func_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: lb_func_node");
    }

    std::string operator()(ub_func_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: ub_func_node");
    }

    std::string operator()(bounding_func_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: bounding_func_node");
    }

    std::string operator()(ale::squash_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: squash_node");
    }

    std::string operator()(xabsx_node* node) {
        return "((" + dispatch(node->get_child<0>()) + ")*(abs(" + dispatch(node->get_child<0>()) + ")))";
    }

    template <typename TType>
    std::string operator()(set_min_node<TType>* node) {
        auto elements = dispatch(node->template get_child<0>());
        if(elements.empty()) {
            throw std::invalid_argument("called set_min with empty set");
        }

        if(nested_max_min) {
            // nested, e.g., min(a, min(b,c)); iterate from back
            auto it = elements.rbegin();
            m_symbols.push_scope();
            m_symbols.define(node->name, new parameter_symbol<TType>(node->name, *it));
            std::string result = dispatch(node->template get_child<1>());
            ++it;
            for(; it != elements.rend(); ++it) {
                m_symbols.define(node->name, new parameter_symbol<TType>(node->name, *it));
#ifdef DIPS_use_gams_macro_min_max_reformulation
                result = "min(" + dispatch(node->template get_child<1>()) + "," + result + ")";
#else
                result = "(((" + dispatch(node->template get_child<1>()) + ") + (" + result + "))/2 - abs((" + dispatch(node->template get_child<1>()) + ") - (" + result + "))/2)";
#endif
            }
            m_symbols.pop_scope();
            return result;
        } else {
            // not nested, e.g., min(a,b,c); iterate from front
            auto it = elements.begin();
            m_symbols.push_scope();
            m_symbols.define(node->name, new parameter_symbol<TType>(node->name, *it));
            std::string result = dispatch(node->template get_child<1>());
            ++it;
            for(; it != elements.end(); ++it) {
                m_symbols.define(node->name, new parameter_symbol<TType>(node->name, *it));
                result += "," + dispatch(node->template get_child<1>());
            }
            m_symbols.pop_scope();
            return "min(" + result + ")";
        }
    }

    template <typename TType>
    std::string operator()(set_max_node<TType>* node) {
        auto elements = dispatch(node->template get_child<0>());
        if(elements.empty()) {
            throw std::invalid_argument("called set_max with empty set");
        }

        if(nested_max_min) {
            // nested, e.g., max(a, max(b,c)); iterate from back
            auto it = elements.rbegin();
            m_symbols.push_scope();
            m_symbols.define(node->name, new parameter_symbol<TType>(node->name, *it));
            std::string result = dispatch(node->template get_child<1>());
            ++it;
            for(; it != elements.rend(); ++it) {
                m_symbols.define(node->name, new parameter_symbol<TType>(node->name, *it));
#ifdef DIPS_use_gams_macro_min_max_reformulation
                result = "max(" + dispatch(node->template get_child<1>()) + "," + result + ")";
#else
                result = "(((" + dispatch(node->template get_child<1>()) + ") + (" + result + "))/2 + abs((" + dispatch(node->template get_child<1>()) + ") - (" + result + "))/2)";
#endif
            }
            m_symbols.pop_scope();
            return result;
        } else {
            // not nested, e.g., max(a,b,c); iterate from front
            auto it = elements.begin();
            m_symbols.push_scope();
            m_symbols.define(node->name, new parameter_symbol<TType>(node->name, *it));
            std::string result = dispatch(node->template get_child<1>());
            ++it;
            for(; it != elements.end(); ++it) {
                m_symbols.define(node->name, new parameter_symbol<TType>(node->name, *it));
                result += "," + dispatch(node->template get_child<1>());
            }
            m_symbols.pop_scope();
            return "max(" + result + ")";
        }
    }

    std::string operator()(min_node* node) {
        if(node->children.empty()) {
            throw std::invalid_argument("called min_node with no children");
        }

        if(nested_max_min) {
            // nested, e.g., min(a, min(b,c)); iterate from back
            auto it = node->children.rbegin();
            std::string result = "(" + dispatch(it->get()) + ")";
            it++;
            for(; it != node->children.rend(); ++it) {
#ifdef DIPS_use_gams_macro_min_max_reformulation
                result = "min(" + dispatch(it->get()) + "," + result + ")";
#else
                result = "(((" + dispatch(it->get()) + ") + (" + result + "))/2 - abs((" + dispatch(it->get()) + ") - (" + result + "))/2)";
#endif
            }
            return result;
        } else {
            // not nested, e.g., min(a,b,c); iterate from front
            auto it = node->children.begin();
            std::string result = "(" + dispatch(it->get()) + ")";
            it++;
            for(; it != node->children.end(); ++it) {
                result += "," + dispatch(it->get());
            }
            return "min(" + result + ")";
        }
    }

    std::string operator()(max_node* node) {
        if(node->children.empty()) {
            throw std::invalid_argument("called max_node with no children");
        }

        if(nested_max_min) {
            // nested, e.g., max(a, max(b,c)); iterate from back
            auto it = node->children.rbegin();
            std::string result = "(" + dispatch(it->get()) + ")";
            it++;
            for(; it != node->children.rend(); ++it) {
#ifdef DIPS_use_gams_macro_min_max_reformulation
                result = "max(" + dispatch(it->get()) + "," + result + ")";
#else
                result = "(((" + dispatch(it->get()) + ") + (" + result + "))/2 + abs((" + dispatch(it->get()) + ") - (" + result + "))/2)";
#endif
            }
            return result;
        } else {
            // not nested, e.g., max(a,b,c); iterate from front
            auto it = node->children.begin();
            std::string result = "(" + dispatch(it->get()) + ")";
            it++;
            for(; it != node->children.end(); ++it) {
                result += "," + dispatch(it->get());
            }
            return "max(" + result + ")";
        }
    }

    std::string operator()(mid_node* node) {
        std::string arg1 = dispatch(node->get_child<0>());
        std::string arg2 = dispatch(node->get_child<1>());
        std::string arg3 = dispatch(node->get_child<2>());
        return "min(max(" + arg1 + "," + arg2 + "),min(max(" + arg2 + "," + arg3 + "),max(" + arg3 + "," + arg1 + ")))";
    }

    template <typename TType>
    std::string operator()(sum_node<TType>* node) {
        auto elements = dispatch(node->template get_child<0>());
        std::string result = "(";
        if(elements.begin() == elements.end()) {
            std::cout << "called sum with emtpy set (by convention equals 0)\n";
            result += "0";
        } else {
            m_symbols.push_scope();
            for(auto it = elements.begin(); it != elements.end(); ++it) {
                if(it != elements.begin()) {
                    result += "+";
                }
                m_symbols.define(node->name, new parameter_symbol<TType>(node->name, *it));
                result += dispatch(node->template get_child<1>());
            }
            m_symbols.pop_scope();
        }
        result += ")";
        return result;
    }

    template <typename TType>
    std::string operator()(product_node<TType>* node) {
        auto elements = dispatch(node->template get_child<0>());
        std::string result = "(";
        if(elements.begin() == elements.end()) {
            std::cout << "called product with emtpy set (by convention equals 1)\n";
            result += "1";
        } else {
            m_symbols.push_scope();
            for(auto it = elements.begin(); it != elements.end(); ++it) {
                if(it != elements.begin()) {
                    result += "*";
                }
                m_symbols.define(node->name, new parameter_symbol<TType>(node->name, *it));
                result += dispatch(node->template get_child<1>());
            }
            m_symbols.pop_scope();
        }
        result += ")";
        return result;
    }

    std::string operator()(norm2_node* node) {
        return "sqrt(power(" + dispatch(node->get_child<0>()) + ",2)+power(" + dispatch(node->get_child<1>()) + ",2))";
    }

    std::string operator()(sum_div_node* node) {
        if(node->children.size() % 2 == 0) {
            throw std::invalid_argument("called sum_div with even number of arguments");
        }
        if(node->children.size() < 3) {
            throw std::invalid_argument("called sum_div with less than 3 arguments");
        }
        std::vector<std::string> vars;
        std::vector<std::string> coeff;
        for(auto it = node->children.begin(); it != node->children.end(); ++it) {
            if(is_tree_constant(it->get(), m_symbols)) {
                if(!(evaluate_expression(it->get(), m_symbols) > 0)) {
                    throw std::invalid_argument("called sum_div with non-positive argument");
                }
            }
            if(distance(node->children.begin(), it) < (int)(node->children.size() / 2)) {
                vars.emplace_back(dispatch(it->get()));
            } else {
                coeff.emplace_back(dispatch(it->get()));
            }
        }
        std::string partial = coeff[1] + "*" + vars[0];
        for(int i = 1; i < (int)(node->children.size() / 2); ++i) {
            partial += "+" + coeff[i + 1] + "*" + vars[i];
        }
        return "((" + coeff[0] + "*" + vars[0] + ")/(" + partial + "))";
    }

    std::string operator()(covar_matern_1_node* node) {
        std::string x = dispatch(node->get_child<0>());
        return "exp(-sqrt(" + x + "))";
    }

    std::string operator()(covar_matern_3_node* node) {
        std::string x = dispatch(node->get_child<0>());
        std::string tmp = "(sqrt(3)*sqrt(" + x + "))";
        return "(exp(-" + tmp + ")+" + tmp + "*exp(-" + tmp + "))";
    }

    std::string operator()(covar_matern_5_node* node) {
        std::string x = dispatch(node->get_child<0>());
        std::string tmp = "(sqrt(5)*sqrt(" + x + "))";
        return "(exp(-" + tmp + ")+" + tmp + "*exp(-" + tmp + ")+" + std::to_string(5.) + "/" + std::to_string(3.) + "*" + x + "*exp(-" + tmp + "))";
    }

    std::string operator()(covar_sqrexp_node* node) {
        std::string x = dispatch(node->get_child<0>());
        return "exp(-" + std::to_string(0.5) + "*" + x + ")";
    }

    std::string operator()(af_lcb_node* node) {
        std::string mu = dispatch(node->get_child<0>());
        std::string sigma = dispatch(node->get_child<1>());
        std::string kappa = dispatch(node->get_child<2>());
        return "(" + mu + "-" + kappa + "*" + sigma + ")";
    }

    std::string operator()(af_ei_node* node) {
        std::string mu = dispatch(node->get_child<0>());
        std::string sigma = dispatch(node->get_child<1>());
        std::string fmin = dispatch(node->get_child<2>());
        if(is_tree_constant(node->get_child<1>(), m_symbols)) {
            if(evaluate_expression(node->get_child<1>(), m_symbols) == 0) {
                return "max(" + fmin + "-" + mu + "," + std::to_string(0.) + ")";
            }
        }
        std::string x = "(" + mu + "-" + fmin + ")";
        std::string gcd = "(erf(" + std::to_string(1.) + "/sqrt(2)*(-" + x + "/" + sigma + "))/" + std::to_string(2.) + "+" + std::to_string(0.5) + ")";
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: erf");
        std::string gpd = "(" + std::to_string(1.) + "/(sqrt(2*" + std::to_string(M_PI) + "))*exp(-power(-" + x + "/" + sigma + ",2)/" + std::to_string(2.) + "))";
        return "((-" + x + ")*" + gcd + "+" + sigma + "*" + gpd + ")";
    }

    std::string operator()(af_pi_node* node) {
        std::string mu = dispatch(node->get_child<0>());
        std::string sigma = dispatch(node->get_child<1>());
        std::string fmin = dispatch(node->get_child<2>());

        double mu_eval = evaluate_expression(node->get_child<0>(), m_symbols);
        double sigma_eval = evaluate_expression(node->get_child<1>(), m_symbols);
        double fmin_eval = evaluate_expression(node->get_child<2>(), m_symbols);

        if(sigma_eval == 0 && fmin_eval <= mu_eval) {
            return "(0)";
        }
        if(sigma_eval == 0 && fmin_eval > mu_eval) {
            return "(1)";
        }
        std::string x = "(" + mu + "-" + fmin + ")";
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: erf");
        return "(erf(" + std::to_string(1.) + "/sqrt(2)*(-" + x + "/" + sigma + "))/" + std::to_string(2.) + "+" + std::to_string(0.5) + ")";
    }

    std::string operator()(gpdf_node* node) {
        std::string x = dispatch(node->get_child<0>());
        return "(" + std::to_string(1.) + "/(sqrt(2*" + std::to_string(M_PI) + "))*exp(-power(" + x + ",2)/" + std::to_string(2.) + "))";
    }

    std::string operator()(regnormal_node* node) {
        std::string x = dispatch(node->get_child<0>());
        std::string a = dispatch(node->get_child<1>());
        std::string b = dispatch(node->get_child<2>());
        return "(" + x + "/sqrt(" + a + "+" + b + "*power(" + x + ",2)))";
    }

    std::string operator()(erf_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: erf");
    }

    std::string operator()(erfc_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: erfc");
    }

    std::string operator()(nrtl_dtau_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: nrtl_dtau");
    }

    std::string operator()(nrtl_dgtau_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: nrtl_dgtau");
    }

    std::string operator()(nrtl_gdtau_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: nrtl_gdtau");
    }

    std::string operator()(antoine_psat_node* node) {
        std::string t = dispatch(node->get_child<0>());
        std::string p1 = dispatch(node->get_child<1>());
        std::string p2 = dispatch(node->get_child<2>());
        std::string p3 = dispatch(node->get_child<3>());
        std::cout << "Warning antoine_psat: Base of real power (**) ist not allowed to be negative"
                  << "\n";
        return "((" + std::to_string(10.) + "**" + p1 + "-" + p2 + "/(" + p3 + "+" + t + ")))";
    }

    std::string operator()(antoine_tsat_node* node) {
        std::string t = dispatch(node->get_child<0>());
        std::string p1 = dispatch(node->get_child<1>());
        std::string p2 = dispatch(node->get_child<2>());
        std::string p3 = dispatch(node->get_child<3>());
        return "(" + p2 + "/(" + p1 + "-log(" + t + ")/log(" + std::to_string(10.) + "))-" + p3 + ")";
    }

    std::string operator()(ext_antoine_psat_node* node) {
        std::string t = dispatch(node->get_child<0>());
        std::string p1 = dispatch(node->get_child<1>());
        std::string p2 = dispatch(node->get_child<2>());
        std::string p3 = dispatch(node->get_child<3>());
        std::string p4 = dispatch(node->get_child<4>());
        std::string p5 = dispatch(node->get_child<5>());
        std::string p6 = dispatch(node->get_child<6>());
        std::string p7 = dispatch(node->get_child<7>());
        std::cout << "Warning ext_antoine_psat: Base of real power (**) ist not allowed to be negative"
                  << "\n";
        return "(exp(" + p1 + "+" + p2 + "/(" + t + "+" + p3 + ")+" + t + "*" + p4 + "+" + p5 + "*log(" + t + ")+" + p6 + "*(" + t + "**" + p7 + ")))";
    }

    std::string operator()(wagner_psat_node* node) {
        std::string t = dispatch(node->get_child<0>());
        std::string p1 = dispatch(node->get_child<1>());
        std::string p2 = dispatch(node->get_child<2>());
        std::string p3 = dispatch(node->get_child<3>());
        std::string p4 = dispatch(node->get_child<4>());
        std::string p5 = dispatch(node->get_child<5>());
        std::string p6 = dispatch(node->get_child<6>());
        std::string Tr = "(" + t + "/" + p5 + ")";
        std::cout << "Warning wagner_psat: Base of real power (**) ist not allowed to be negative"
                  << "\n";
        return "(" + p6 + "*exp((" + p1 + "*(1-" + Tr + ")+" + p2 + "*power(1-" + Tr + "," + std::to_string(1.5) + ")+" + p3 + "*((1-" + Tr + ")**" + std::to_string(2.5) + ")+"
               + p4 + "*power(1-" + Tr + ",5))/" + Tr + "))";
    }

    std::string operator()(ik_cape_psat_node* node) {
        std::string t = dispatch(node->get_child<0>());
        std::string p1 = dispatch(node->get_child<1>());
        std::string p2 = dispatch(node->get_child<2>());
        std::string p3 = dispatch(node->get_child<3>());
        std::string p4 = dispatch(node->get_child<4>());
        std::string p5 = dispatch(node->get_child<5>());
        std::string p6 = dispatch(node->get_child<6>());
        std::string p7 = dispatch(node->get_child<7>());
        std::string p8 = dispatch(node->get_child<8>());
        std::string p9 = dispatch(node->get_child<9>());
        std::string p10 = dispatch(node->get_child<10>());
        return "(exp(" + p1 + "+" + p2 + "*" + t + "+" + p3 + "*power(" + t + ",2)+" + p4 + "*power(" + t + ",3)+" + p5 + "*power(" + t + ",4)+"
               + p6 + "*power(" + t + ",5)+" + p7 + "*power(" + t + ",6)+" + p8 + "*power(" + t + ",7)+" + p9 + "*power(" + t + ",8)+"
               + p10 + "*power(" + t + ",9)))";
    }

    std::string operator()(aspen_hig_node* node) {
        std::string t = dispatch(node->get_child<0>());
        std::string t0 = dispatch(node->get_child<1>());
        std::string p1 = dispatch(node->get_child<2>());
        std::string p2 = dispatch(node->get_child<3>());
        std::string p3 = dispatch(node->get_child<4>());
        std::string p4 = dispatch(node->get_child<5>());
        std::string p5 = dispatch(node->get_child<6>());
        std::string p6 = dispatch(node->get_child<7>());
        return "(" + p1 + "*(" + t + "-" + t0 + ")+" + p2 + "/2*(power(" + t + ",2)-power(" + t0 + ",2))+" + p3 + "/3*(power(" + t + ",3)" + "-power(" + t0 + ",3))+" + p4 + "/4*(power(" + t + ",4)-power(" + t0 + ",4))+" + p5 + "/5*(power(" + t + ",5)" + "-power(" + t0 + ",5))+" + p6 + "/6*(power(" + t + ",6)-power(" + t0 + ",6)))";
    }

    std::string operator()(nasa9_hig_node* node) {
        std::string t = dispatch(node->get_child<0>());
        std::string t0 = dispatch(node->get_child<1>());
        std::string p1 = dispatch(node->get_child<2>());
        std::string p2 = dispatch(node->get_child<3>());
        std::string p3 = dispatch(node->get_child<4>());
        std::string p4 = dispatch(node->get_child<5>());
        std::string p5 = dispatch(node->get_child<6>());
        std::string p6 = dispatch(node->get_child<7>());
        std::string p7 = dispatch(node->get_child<8>());
        return "(-" + p1 + "*(1/" + t + "-1/" + t0 + ")+" + p2 + "*log(" + t + "/" + t0 + ")+" + p3 + "*(" + t + "-" + t0 + ")+" + p4 + "/2*(power(" + t + ",2)" + "-power(" + t0 + ",2))+" + p5 + "/3*(power(" + t + ",3)-power(" + t0 + ",3))+" + p6 + "/4*(power(" + t + ",4)" + "-power(" + t0 + ",4))+" + p7 + "/5*(power(" + t + ",5)-power(" + t0 + ",5)))";
    }

    std::string operator()(dippr106_dhvap_node* node) {
        std::string t = dispatch(node->get_child<0>());
        std::string tc = dispatch(node->get_child<1>());
        std::string p2 = dispatch(node->get_child<2>());
        std::string p3 = dispatch(node->get_child<3>());
        std::string p4 = dispatch(node->get_child<4>());
        std::string p5 = dispatch(node->get_child<5>());
        std::string p6 = dispatch(node->get_child<6>());
        std::string tr = "(" + t + "/" + tc + ")";

        if(is_tree_constant(node->get_child<0>(), m_symbols) && is_tree_constant(node->get_child<1>(), m_symbols)) {
            if((evaluate_expression(node->get_child<0>(), m_symbols) / evaluate_expression(node->get_child<1>(), m_symbols)) < 1) {
                std::cout << "Warning dippr106_dhvap: Base of real power (**) ist not allowed to be negative"
                          << "\n";
                return "(" + p2 + "*((1-" + tr + ")**" + p3 + "+" + p4 + "*" + tr + "+" + p5 + "*power(" + tr + ",2)+" + p6 + "*power(" + tr + ",3)))";
            } else {
                return std::to_string(0.);
            }
        } else {
            std::cout << "Warning dippr106_dhvap: Temperature is not a constant. Check T < T_crit for all possible values of T manually!"
                      << "\n";
            std::cout << "Warning dippr106_dhvap: Base of real power (**) ist not allowed to be negative"
                      << "\n";
            return "(" + p2 + "*((1-" + tr + ")**" + p3 + "+" + p4 + "*" + tr + "+" + p5 + "*power(" + tr + ",2)+" + p6 + "*power(" + tr + ",3)))";
        }
    }

    std::string operator()(dippr107_hig_node* node) {
        std::string t = dispatch(node->get_child<0>());
        std::string t0 = dispatch(node->get_child<1>());
        std::string p1 = dispatch(node->get_child<2>());
        std::string p2 = dispatch(node->get_child<3>());
        std::string p3 = dispatch(node->get_child<4>());
        std::string p4 = dispatch(node->get_child<5>());
        std::string p5 = dispatch(node->get_child<6>());
        return "(" + p1 + "*(" + t + "-" + t0 + ")+" + p2 + "*" + p3 + "*(1/tanh(" + p3 + "/" + t + ")-1/tanh(" + p3 + "/" + t0 + "))" + "-" + p4 + "*" + p5 + "*(tanh(" + p5 + "/" + t + ")-tanh(" + p5 + "/" + t0 + ")))";
    }

    std::string operator()(dippr127_hig_node* node) {
        std::string t = dispatch(node->get_child<0>());
        std::string t0 = dispatch(node->get_child<1>());
        std::string p1 = dispatch(node->get_child<2>());
        std::string p2 = dispatch(node->get_child<3>());
        std::string p3 = dispatch(node->get_child<4>());
        std::string p4 = dispatch(node->get_child<5>());
        std::string p5 = dispatch(node->get_child<6>());
        std::string p6 = dispatch(node->get_child<7>());
        std::string p7 = dispatch(node->get_child<8>());
        return "(" + p1 + "*(" + t + "-" + t0 + ")+" + p2 + "*" + p3 + "*(1/(exp(" + p3 + "/" + t + ")-1)-1/(exp(" + p3 + "/" + t0 + ")-1))+"
               + p4 + "*" + p5 + "*(1/(exp(" + p5 + "/" + t + ")-1)-1/(exp(" + p5 + "/" + t0 + ")-1))+"
               + p6 + "*" + p7 + "*(1/(exp(" + p7 + "/" + t + ")-1)-1/(exp(" + p7 + "/" + t0 + ")-1)))";
    }

    std::string operator()(watson_dhvap_node* node) {
        std::string t = dispatch(node->get_child<0>());
        std::string tc = dispatch(node->get_child<1>());
        std::string a = dispatch(node->get_child<2>());
        std::string b = dispatch(node->get_child<3>());
        std::string t1 = dispatch(node->get_child<4>());
        std::string dHT1 = dispatch(node->get_child<5>());
        std::string tr = "(1-" + t + "/" + tc + ")";
        if(is_tree_constant(node->get_child<0>(), m_symbols) && is_tree_constant(node->get_child<1>(), m_symbols)) {
            if((1 - evaluate_expression(node->get_child<0>(), m_symbols) / evaluate_expression(node->get_child<1>(), m_symbols)) > 0) {
                std::cout << "Warning watson_dhvap: Base of real power (**) ist not allowed to be negative"
                          << "\n";
                return "(" + dHT1 + "*((" + tr + "/(1-" + t1 + "/" + tc + "))**" + a + "+" + b + "*" + tr + "))";
            } else {
                return std::to_string(0.);
            }
        } else {
            std::cout << "Warning watson_dhvap: Temperature is not a constant. Check T < T_crit for all possible values of T manually!"
                      << "\n";
            std::cout << "Warning watson_dhvap: Base of real power (**) ist not allowed to be negative"
                      << "\n";
            return "(" + dHT1 + "*((" + tr + "/(1-" + t1 + "/" + tc + "))**" + a + "+" + b + "*" + tr + "))";
        }
    }

    std::string operator()(schroeder_ethanol_p_node* node) {
        std::string t = dispatch(node->get_child<0>());
        const std::string t_c_K = std::to_string(514.71);
        const std::string n_Tsat_1 = std::to_string(-8.94161);
        const std::string n_Tsat_2 = std::to_string(1.61761);
        const std::string n_Tsat_3 = std::to_string(-51.1428);
        const std::string n_Tsat_4 = std::to_string(53.1360);
        const std::string k_Tsat_1 = std::to_string(1.0);
        const std::string k_Tsat_2 = std::to_string(1.5);
        const std::string k_Tsat_3 = std::to_string(3.4);
        const std::string k_Tsat_4 = std::to_string(3.7);
        const std::string p_c = std::to_string(62.68);
        std::cout << "Warning schroeder_ethanol_p: Base of real power (**) ist not allowed to be negative"
                  << "\n";
        return "(" + p_c + "*(exp(" + t_c_K + "/" + t + "*(" + n_Tsat_1 + "*((1-" + t + "/" + t_c_K + ")**" + k_Tsat_1 + ")+" + n_Tsat_2 + "*((1-" + t + "/" + t_c_K + ")**" + k_Tsat_2 + ")+("
               + n_Tsat_3 + ")*((1-" + t + "/" + t_c_K + ")**" + k_Tsat_3 + ")+" + n_Tsat_4 + "*((1-" + t + "/" + t_c_K + ")**" + k_Tsat_4 + ")))))";
    }

    std::string operator()(schroeder_ethanol_rholiq_node* node) {
        std::string t = dispatch(node->get_child<0>());
        const std::string t_c_K = std::to_string(514.71);
        const std::string n_liq_1 = std::to_string(9.00921);
        const std::string n_liq_2 = std::to_string(-23.1668);
        const std::string n_liq_3 = std::to_string(30.9092);
        const std::string n_liq_4 = std::to_string(-16.5459);
        const std::string n_liq_5 = std::to_string(3.64294);
        const std::string k_liq_1 = std::to_string(0.5);
        const std::string k_liq_2 = std::to_string(0.8);
        const std::string k_liq_3 = std::to_string(1.1);
        const std::string k_liq_4 = std::to_string(1.5);
        const std::string k_liq_5 = std::to_string(3.3);
        const std::string rho_c = std::to_string(273.195);
        std::cout << "Warning schroeder_ehanol_rholiq: Base of real power (**) ist not allowed to be negative"
                  << "\n";
        return "(" + rho_c + "*(1+" + n_liq_1 + "*((1-" + t + "/" + t_c_K + ")**" + k_liq_1 + ")+(" + n_liq_2 + ")*((1-" + t + "/" + t_c_K + ")**" + k_liq_2 + ")+"
               + n_liq_3 + "*((1-" + t + "/" + t_c_K + ")**" + k_liq_3 + ")+(" + n_liq_4 + ")*((1-" + t + "/" + t_c_K + ")**" + k_liq_4 + ")+"
               + n_liq_5 + "*((1-" + t + "/" + t_c_K + ")**" + k_liq_5 + ")))";
    }

    std::string operator()(schroeder_ethanol_rhovap_node* node) {
        std::string t = dispatch(node->get_child<0>());
        const std::string t_c_K = std::to_string(514.71);
        const std::string n_vap_1 = std::to_string(-1.75362);
        const std::string n_vap_2 = std::to_string(-10.5323);
        const std::string n_vap_3 = std::to_string(-37.6407);
        const std::string n_vap_4 = std::to_string(-129.762);
        const std::string k_vap_1 = std::to_string(0.21);
        const std::string k_vap_2 = std::to_string(1.1);
        const std::string k_vap_3 = std::to_string(3.4);
        const std::string k_vap_4 = std::to_string(10);
        const std::string rho_c = std::to_string(273.195);
        std::cout << "Warning schroeder_ethanol_rhovap: Base of real power (**) ist not allowed to be negative"
                  << "\n";
        return "(" + rho_c + "*(exp(" + n_vap_1 + "*((1-" + t + "/" + t_c_K + ")**" + k_vap_1 + ")+(" + n_vap_2 + ")*((1-" + t + "/" + t_c_K + ")**" + k_vap_2 + ")+("
               + n_vap_3 + ")*((1-" + t + "/" + t_c_K + ")**" + k_vap_3 + ")+(" + n_vap_4 + ")*((1-" + t + "/" + t_c_K + ")**" + k_vap_4 + "))))";
    }

    std::string operator()(nrtl_tau_node* node) {
        std::string t = dispatch(node->get_child<0>());
        std::string a = dispatch(node->get_child<1>());
        std::string b = dispatch(node->get_child<2>());
        std::string e = dispatch(node->get_child<3>());
        std::string f = dispatch(node->get_child<4>());
        return "(" + a + "+(" + b + "/" + t + ")+" + e + "*log(" + t + ")+" + f + "*" + t + ")";
    }

    std::string operator()(nrtl_g_node* node) {
        std::string t = dispatch(node->get_child<0>());
        std::string a = dispatch(node->get_child<1>());
        std::string b = dispatch(node->get_child<2>());
        std::string e = dispatch(node->get_child<3>());
        std::string f = dispatch(node->get_child<4>());
        std::string alpha = dispatch(node->get_child<5>());
        //the parameters are a,b,e,f and alpha, for further info, see ASPEN help
        return "(exp(-(" + alpha + ")*((" + a + ")+(" + b + ")/(" + t + ")+(" + e + ")*log(" + t + ")+(" + f + ")*(" + t + "))))";
    }

    std::string operator()(nrtl_gtau_node* node) {
        std::string t = dispatch(node->get_child<0>());
        std::string a = dispatch(node->get_child<1>());
        std::string b = dispatch(node->get_child<2>());
        std::string e = dispatch(node->get_child<3>());
        std::string f = dispatch(node->get_child<4>());
        std::string alpha = dispatch(node->get_child<5>());
        //the parameters are a,b,e,f and alpha, for further info, see ASPEN help
        return "(exp(-" + alpha + "*(" + a + "+" + b + "/" + t + "+" + e + "*log(" + t + ")+" + f + "*" + t + "))*(" + a + "+" + b + "/" + t + "+" + e + "*log(" + t + ")+" + f + "*" + t + "))";
    }

    // non-terminal boolean node visits
    std::vector<std::string> operator()(entry_node<ale::boolean<0>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: entry_node<boolean<0>>");
    }

    std::vector<std::string> operator()(negation_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: negation_node");
    }

    std::vector<std::string> operator()(equal_node<real<0>>* node) {
        return std::vector<std::string>(1, dispatch(node->get_child<0>()) + " =e= " + dispatch(node->get_child<1>()));
    }

    std::vector<std::string> operator()(less_node<real<0>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: less_node<real<0>>");
    }

    std::vector<std::string> operator()(less_equal_node<real<0>>* node) {
        return std::vector<std::string>(1, dispatch(node->get_child<0>()) + " =l= " + dispatch(node->get_child<1>()));
    }

    std::vector<std::string> operator()(greater_node<real<0>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: greater_node<real<0>>");
    }

    std::vector<std::string> operator()(greater_equal_node<real<0>>* node) {
        return std::vector<std::string>(1, dispatch(node->get_child<0>()) + " =g= " + dispatch(node->get_child<1>()));
    }

    std::vector<std::string> operator()(equal_node<index<0>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: equal_node<index<0>>");
    }

    std::vector<std::string> operator()(less_node<index<0>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: less_node<index<0>>");
    }

    std::vector<std::string> operator()(less_equal_node<index<0>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: less_equal_node<index<0>>");
    }

    std::vector<std::string> operator()(greater_node<index<0>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: greater_node<index<0>>");
    }

    std::vector<std::string> operator()(greater_equal_node<index<0>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: greater_equal_node<index<0>>");
    }

    std::vector<std::string> operator()(disjunction_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: disjunction_node");
    }

    std::vector<std::string> operator()(conjunction_node* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: conjunction_node");
    }

    template <typename TType>
    std::vector<std::string> operator()(element_node<TType>* /*node*/) {
        std::string type = typeid(TType).name();
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: element_node<" + type + ">");
    }

    std::vector<std::string> operator()(vector_node<ale::boolean<0>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: vector_node<boolean<0>>");
    }

    std::vector<std::string> operator()(function_node<ale::boolean<0>>* node) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: function_node<boolean<0>> with name \"" + node->name + "\"");
    }

    std::vector<std::string> operator()(tensor_node<ale::boolean<0>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: tensor_node<boolean<0>>");
    }

    std::vector<std::string> operator()(index_shift_node<ale::boolean<0>>* /*node*/) {
        throw unsupportedNodeParsingException("conversion to GAMS input not implemented: index_shift_node<boolean<0>>");
    }

    template <typename TType>
    std::vector<std::string> operator()(forall_node<TType>* node) {
        auto elements = dispatch(node->template get_child<0>());
        std::vector<std::string> result;
        m_symbols.push_scope();
        for(auto it = elements.begin(); it != elements.end(); ++it) {
            m_symbols.define(node->name, new parameter_symbol<TType>(node->name, *it));
            auto temp = dispatch(node->template get_child<1>());
            for(auto it = temp.begin(); it != temp.end(); ++it) {
                result.push_back(*it);
            }
        }
        m_symbols.pop_scope();
        return result;
    }

    bool get_nested_min_max() {
        return nested_max_min;
    }
    void set_nested_min_max(bool value) {
        nested_max_min = value;
    }

private:
    symbol_table& m_symbols;
    std::stack<size_t> index_stack {};
    bool nested_max_min = true;
};

class symbol_stringer_flattened {
public:
    symbol_stringer_flattened(std::stringstream& output, symbol_table& symbols) :
        m_output(output), m_symbols(symbols) {};


    static std::string getValue(double value) {
        std::ostringstream out;
        out.precision(16);
        out << '(' << std::fixed << value << ')';
        std::cout << out.str();
        return std::move(out).str();
    }

    // base_symbol dispatch
    void dispatch(base_symbol* sym) {
        std::visit(*this, sym->get_base_variant());
    }

    template <typename TType>
    void operator()(value_symbol<TType>* sym) {
        std::visit(*this, sym->get_value_variant());
    }

    // symbol visits
    template <typename TType>
    void operator()(parameter_symbol<TType>* sym) {
        // skipping all but real parameters
    }

    template <unsigned IDim>
    void operator()(parameter_symbol<real<IDim>>* sym) {
        for(int i = 0; i < IDim; ++i) {
            if(sym->m_value.shape(i) == 0) {
                return;
            }
        }
        m_output << "parameter\n";
        size_t indexes[IDim];
        for(int i = 0; i < IDim; ++i) {
            indexes[i] = 0;
        }
        while(indexes[0] < sym->m_value.shape(0)) {
            m_output << entry_name<IDim>(sym->m_name, indexes, '_');
            m_output << " /" << getValue(sym->m_value[indexes]) << "/\n";
            for(int i = IDim - 1; i >= 0; --i) {
                if(++indexes[i] < sym->m_value.shape(i)) {
                    break;
                } else if(i != 0) {
                    indexes[i] = 0;
                }
            }
        }
        m_output << ";\n";
    }

    void operator()(parameter_symbol<real<0>>* sym) {
        m_output << "parameter " << sym->m_name << " /" << getValue(sym->m_value) << "/;\n";
    }


    template <unsigned IDim>
    void operator()(variable_symbol<real<IDim>>* sym) {
        for(int i = 0; i < IDim; ++i) {
            if(sym->shape(i) == 0) {
                return;
            }
        }
        size_t indexes[IDim];
        for(int i = 0; i < IDim; ++i) {
            indexes[i] = 0;
        }
        while(indexes[0] < sym->shape(0)) {
            if(sym->integral()) {
                m_output << "integer variable ";
            } else {
                m_output << "variable ";
            }
            m_output << entry_name<IDim>(sym->m_name, indexes, '_') << ";\n";
            if(sym->lower()[indexes] > -std::numeric_limits<double>::infinity()) {
                m_output << entry_name<IDim>(sym->m_name, indexes, '_') << ".lo = " << sym->lower()[indexes] << ";\n";
            }
            if(sym->upper()[indexes] < std::numeric_limits<double>::infinity()) {
                m_output << entry_name<IDim>(sym->m_name, indexes, '_') << ".up = " << sym->upper()[indexes] << ";\n";
            }
            if(!std::isnan(sym->init()[indexes])) {
                m_output << entry_name<IDim>(sym->m_name, indexes, '_') << ".L = " << sym->init()[indexes] << ";\n";
            }
            if(!std::isnan(sym->prio()[indexes])) {
                if(!sym->integral()) {
                    throw std::invalid_argument("Can not set prio for non integral variable (named \"" + sym->m_name + "\")");
                }
                m_output << entry_name<IDim>(sym->m_name, indexes, '_') << ".prior = " << sym->prio()[indexes];
            }
            for(int i = IDim - 1; i >= 0; --i) {
                if(++indexes[i] < sym->shape(i)) {
                    break;
                } else if(i != 0) {
                    indexes[i] = 0;
                }
            }
        }
    }

    void operator()(variable_symbol<real<0>>* sym) {
        if(sym->integral()) {
            m_output << "integer variable " << sym->m_name << ";\n";
        } else {
            m_output << "variable " << sym->m_name << ";\n";
        }
        if(sym->lower() > -std::numeric_limits<double>::infinity()) {
            m_output << sym->m_name << ".lo = " << sym->lower() << ";\n";
        }
        if(sym->upper() < std::numeric_limits<double>::infinity()) {
            m_output << sym->m_name << ".up = " << sym->upper() << ";\n";
        }
        if(!std::isnan(sym->init())) {
            m_output << sym->m_name << ".L = " << sym->init() << ";\n";
        }
        if(!std::isnan(sym->prio())) {
            if(!sym->integral()) {
                throw std::invalid_argument("Can not set prio for non integral variable (named \"" + sym->m_name + "\")");
            }
            m_output << sym->m_name << ".prior = " << sym->prio() << ";\n";
        }
    }

    template <typename TType>
    void operator()(function_symbol<TType>* sym) {
        //Skip functions since they are evaluated instantly
    }

private:
    std::stringstream& m_output;
    symbol_table& m_symbols;
};


} // namespace dips
