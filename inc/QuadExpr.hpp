#pragma once

#include <functional> // for function
#include <map>        // for map
#include <vector>     // for vector
#include <ostream>    // for ostream
#include <string>     // for string
#include <utility>    // for pair

/// Overview: The goal of this module is to enable memory efficient assembly of sparse quadratic and linear expressions
/// that are propagated through an MCPP DAG.
///           An linear expression is for example (c+a0x0+a1x1...+anxn). Often only a few coefficients ai are not zero.
///           The class LinExpr respresents a linear expression by only saving c and values and indices of nonzero
///           coefficients ai.
///
///           Multiplying two linear expressions yields a quadratic expression.
///           Quadratic expressions can be represented by
///           (1): (c0+ax)*(c1+bx)=x.T*(a.T*b)*x + (c1*a+c2*b)*x + c0*c1 =x.T*Q*x+d*x+e
///           Avoiding a dense representation of the matrix Q saves memory in most cases. This is archieved with the
///           class SparseMatrix.
///
///           The struct QuadExpr represents a full quadratic expression by combining an LinExpr part (representing d
///           and e) and a SparseMatrix reprenting Q. Higher orders (e.g. cubic ) are not supported and result in an
///           exception.

namespace dips {

struct OperationResultNotQuadratic : public std::invalid_argument {
    explicit OperationResultNotQuadratic(const std::string& msg) :
        std::invalid_argument(msg) { }
};

/**
 * @struct LinExpr
 * @brief A simple sparse vector class with an additional constant field. Represents c+a0*x0+a1*x1... where nonzero
 * a0...an  and c are saved. This struct is used to avoid the need of propagating the IloExpr object resulting in HUGE
 * RAM usage.
 *
 * The nonzero values and indices are stored in ascending index order.
 * Elementwise binary arithetic operations  are supported, which skip elements that are zero in both operands.
 * Elements can be efficently be appended.
 */
class LinExpr {
public:
    /**
     * @brief Default constructor, LinExpr only contains a constant term
     */
    explicit LinExpr(double scalar = 0.0);

    /** @brief Returns true if expression only contains a constant and no linear term*/
    [[nodiscard]] bool is_scalar() const;

    /** @brief Getter for the constant term of the linear expression*/
    double& constant();

    /** @brief Getter for the constant term of the linear expression*/
    [[nodiscard]] double constant() const;

    /**
     * @brief Getter for elments in the sparse vector, corresponding to the linear part of the expression
     *
     * @param[in] index is the index of the value requested. Function returns a_index.
     */
    [[nodiscard]] double get_value(unsigned index) const;

    /**
     * @brief Setter for elments in the sparse vector, corresponding to the linear part of the expression
     *
     * @param[in] index is the index of the value to change.
     * @param[in] value is the value that should be saved
     */
    void set_value(unsigned index, double value);

    /** @brief Getter for the ids of nonzero entries*/
    [[nodiscard]] const std::vector<unsigned>& get_non_zero_ids() const;

    /** @brief Getter for the ids of nonzero entries*/
    [[nodiscard]] const std::vector<double>& get_non_zero_values() const;

    /** @brief  Function to print content of sparse linear expression*/
    [[nodiscard]] std::string print() const;

    /**
     * @brief Helper function: apply a function f(unsigned id,double az,double bz) to all coefficients of a and b that
     * are related to an index that occurs in either a or b.
     *
     * For example if we want to calculate ci=ai+bi, we must be careful, because a and b are saved as sparse vectors,
     * i.e. the first entry in a.values does not correspond to a_0, but to a_{a._indices[0]}. Assume that we want have
     * a=(0):1,(3):4 and b=(3):1,(4):2, then this f will be called with f(0,1,0), f(3,4,1) and f(4,0,2), skipping over
     * indices that are neither in a or b. This function is an efficient alternative to writing both sparse vectors to
     * full vectors apply a function that acts elementswise on both vectors and create a sparse vector from that.
     */
    static void for_all_coeffs(const std::function<void(unsigned, double, double)>& f, const LinExpr& a,
      const LinExpr& b);

    static void for_all_coeffs_plus(LinExpr& out, const LinExpr& a, const LinExpr& b);
    static void for_all_coeffs_minus(LinExpr& out, const LinExpr& a, const LinExpr& b);

    void clear();

private:
    std::vector<double> _values {}; /*!< The linear coefficients saved in the vector*/

    std::vector<unsigned> _ids {}; /*!< Sorted list of all indices indentifing nonzero entries*/
    double _constant = 0;          /*!< The constant term in linear (more exactly affine) expression*/

    friend LinExpr scale(const LinExpr& LHS, double scale);
    friend std::ostream& operator<<(std::ostream& os, const LinExpr& dt);
};

/** @brief  Function to print content of LinExpr*/
std::ostream& operator<<(std::ostream& os, const LinExpr& dt);

/** @brief Helper function:do an elementwise binary operation on LHS and RHS and save result in returned variable*/
LinExpr calculate_binary_operation_element_wise(const LinExpr& LHS, const LinExpr& RHS,
  const std::function<double(double, double)>& op);

/** @brief Operator+ for LinExpr: Elementwise addition */
LinExpr operator+(const LinExpr& LHS, const LinExpr& RHS);

/** @brief Operator- for LinExpr: Elementwise subtraction */
LinExpr operator-(const LinExpr& LHS, const LinExpr& RHS);

/** @brief Operator* for LinExpr: Elementwise  multiplication*/
LinExpr operator*(const LinExpr& LHS, const LinExpr& RHS);

/** @brief Operator/ for LinExpr: Elementwise  division */
LinExpr operator/(const LinExpr& LHS, const LinExpr& RHS);

/** @brief scale LinExpression by scalar*/
LinExpr scale(const LinExpr& LHS, double scale);

/** @brief Operator* for LinExpr and scalar double : Elementwise  multiplication*/
LinExpr operator*(const LinExpr& LHS, double scalar);

/** @brief Operator* for LinExpr and scalar double : Elementwise  multiplication*/
LinExpr operator*(double scalar, const LinExpr& RHS);

/** @brief Operator/ for LinExpr and scalar double : Elementwise  division*/
LinExpr operator/(const LinExpr& LHS, double divisor);

/**
* @struct  PureQuadExpr
* @brief A simple representation of a quadratic term. In essence, we save the vectors of value, i, j to represent the expression sum_k(value[k]*x[i[k]]*x[j[k]]).
*
* Elementwise operations are supported; If the quadratic term is instead saved in matrix from x^T*Q*x,
* we obtain have a smaller representation (terms with same variables are combined), but doing this during the
* evaluation turns out to be computationally inefficent.
*/
class PureQuadExpr {
public:
    /** @brief adds a term to sum(a[i]*xj[i]*xk[i]) */
    void add_term(unsigned index1, unsigned index2, double value);

    /** @brief Getter for the ids of nonzero entries*/
    [[nodiscard]] const std::vector<std::pair<unsigned, unsigned>>& get_non_zero_ids() const;

    /** @brief Getter for the values of nonzero entries*/
    [[nodiscard]] const std::vector<double>& get_non_zero_values() const;

    /** @brief  Function to print content of sparse linear expression*/
    [[nodiscard]] std::string print() const;

    /** @brief Deletes the contents*/
    void clear();

    PureQuadExpr operator+=(const PureQuadExpr& RHS);
    PureQuadExpr operator-=(const PureQuadExpr& RHS);

private:
    std::vector<double> _values {}; /*!< The linear coefficients saved in the vector*/

    std::vector<std::pair<unsigned, unsigned>> _ids {}; /*!< Non-sorted list of all indices indentifing nonzero entries*/

    friend PureQuadExpr scale(PureQuadExpr LHS, double scalar);
    friend PureQuadExpr operator+(PureQuadExpr LHS, const PureQuadExpr& RHS);
    friend PureQuadExpr operator-(PureQuadExpr LHS, const PureQuadExpr& RHS);
    friend std::ostream& operator<<(std::ostream& os, const PureQuadExpr& dt);
};

/** @brief  Function to print content of PureQuadExpr*/
std::ostream& operator<<(std::ostream& os, const PureQuadExpr& dt);


/** @brief Operator+ for PureQuadExpr: Elementwise addition */
PureQuadExpr operator+(PureQuadExpr LHS, const PureQuadExpr& RHS);

/** @brief Operator- for PureQuadExpr: Elementwise subtraction */
PureQuadExpr operator-(PureQuadExpr LHS, const PureQuadExpr& RHS);


/** @brief scale PureQuadExpression by scalar*/
PureQuadExpr scale(PureQuadExpr LHS, double scalar);

/** @brief Operator* for PureQuadExpr and scalar double : Elementwise  multiplication*/
PureQuadExpr operator*(PureQuadExpr LHS, double scalar);

/** @brief Operator* for PureQuadExpr and scalar double : Elementwise  multiplication*/
PureQuadExpr operator*(double scalar, PureQuadExpr RHS);




/**
 * @struct  SparseMatrix
 * @brief A simple  Sparse Matrix using directory of keys format.
 *
 * Elementwise operations (except division) are supported;
 */
class SparseMatrix {
public:
    /**
     * @brief Getter for element of given row and column
     * @param[in] row is the row of the element requested
     * @param[in] col is the column of the element requested
     */
    [[nodiscard]] double get_element(unsigned row, unsigned col) const;

    /**
     * @brief Setter for element of given row and column. Already set values are overridden.
     * @param[in] row is the row of the element to be changed
     * @param[in] col is the column of the element to be changed
     * @param[in] value is the value the element is to be changed to
     */
    void setElement(unsigned row, unsigned col, double value);

    /** @brief Getter for a whole row of the matrix as a LinExpr object*/
    [[nodiscard]] LinExpr getRow(unsigned row) const;

    /** @brief Gets the number of rows saved in the matrix. There is at least one row with row id smaller than the
     * number of rows
     */
    [[nodiscard]] unsigned get_number_of_rows() const;

    /** @brief Append a row to the bottom of the sparse matrix */
    void append_row(const LinExpr& row);

    /**
     *  @brief Append a row to the matrix with a given row index, but leave empty rows between the previous last row and
     * the newly added row.
     *
     *  Insetion is not allowed, i.e., can only append to the matrix not insert.
     */
    void append_row(const LinExpr& row, unsigned rowNumber);

    /** @brief  Function to print content of sparse matrix */
    [[nodiscard]] std::string print() const;

    void clear();

private:
    std::map<std::pair<unsigned, unsigned>, double>
      _matrix {}; /*!< sorted map saving the entries of the matrix, sorted first after rows, then after columns */
    friend SparseMatrix scale(SparseMatrix LHS, double scale);
    friend SparseMatrix add(SparseMatrix LHS, double scalar);
    friend std::ostream& operator<<(std::ostream& os, const SparseMatrix& dt);
};

/** @brief  Function to print content of sparse matrix */
std::ostream& operator<<(std::ostream& os, const SparseMatrix& dt);

/** @brief Operator+ for SparseMatrix: Elementwise addition */
SparseMatrix operator+(const SparseMatrix& LHS, const SparseMatrix& RHS);

/** @brief Operator- for SparseMatrix: Elementwise  subtraction */
SparseMatrix operator-(const SparseMatrix& LHS, const SparseMatrix& RHS);

/** @brief Operator* for SparseMatrix: Elementwise  multiplication */
SparseMatrix operator*(const SparseMatrix& LHS, const SparseMatrix& RHS);

/** @brief Operator+ for SparseMatrix and scalar: Elementwise addition */
SparseMatrix add(SparseMatrix LHS, double scalar);

/** @brief Operator* for SparseMatrix and scalar: Elementwise multiplication */
SparseMatrix scale(SparseMatrix LHS, double scale);

/** @brief Operator+ for SparseMatrix and scalar: Elementwise addition */
SparseMatrix operator+(const SparseMatrix& LHS, double scalar);

/** @brief Operator+ for SparseMatrix and scalar: Elementwise addition */
SparseMatrix operator+(double scalar, const SparseMatrix& RHS);

/** @brief Operator- for SparseMatrix and scalar: Elementwise subtraction */
SparseMatrix operator-(const SparseMatrix& LHS, double scalar);

/** @brief Operator- for SparseMatrix: Elementwise negation */
SparseMatrix operator-(const SparseMatrix& LHS);

/** @brief Operator- for SparseMatrix and scalar: Elementwise subtraction */
SparseMatrix operator-(double scalar, const SparseMatrix& RHS);

/** @brief Operator* for SparseMatrix and scalar: Elementwise multiplication */
SparseMatrix operator*(const SparseMatrix& LHS, double scalar);

/** @brief Operator* for SparseMatrix and scalar: Elementwise multiplication */
SparseMatrix operator*(double scalar, const SparseMatrix& RHS);

/** @brief Order of expression */
enum class Order {
    SCALAR,   // only a constant
    LINEAR,   // a constant and linear terms
    QUADRATIC // any expression with quadratic terms
};

/**
 * @class  QuadExpr
 * @brief General quadratic expression with a sparse matrix for the quadratic part and a sparse linear expression for
 * the linear part (including a constant term)
 *
 * Elementwise operations (except division) are supported;
 */
class QuadExpr {
public:
    QuadExpr();
    explicit QuadExpr(const LinExpr& linExpr);
    explicit QuadExpr(double scalar);

    /** @brief Returns true if expression contains quadratic terms */
    [[nodiscard]] bool is_quadratic() const;

    /** @brief Returns true if expression contains linear terms but no quadratic terms */
    [[nodiscard]] bool is_linear() const;

    /** @brief Returns true if expression contains only constants */
    [[nodiscard]] bool is_scalar() const;

    /** @brief Returns linear part of expression */
    [[nodiscard]] const LinExpr& getLinearPart() const;

    /** @brief Returns quadratic part of expression */
    [[nodiscard]] const PureQuadExpr& getQuadraticPart() const;

    /** @brief Returns constant term of expression */
    [[nodiscard]] double getConstant() const;

    /** @brief raises the QuadExpr to a power */
    void pow(const QuadExpr& other);

    QuadExpr& operator+=(const QuadExpr& RHS);
    QuadExpr& operator-=(const QuadExpr& RHS);
    QuadExpr& operator*=(const QuadExpr& RHS);
    QuadExpr& operator=(double scalar);
    QuadExpr& operator*=(double scalar);
    QuadExpr& operator/=(double scalar);

private:
    PureQuadExpr quadraticPart {};
    LinExpr linearPart {};

    Order order;

    friend QuadExpr operator+(const QuadExpr&, const QuadExpr&);
    friend QuadExpr operator+(const QuadExpr&);
    friend QuadExpr operator-(const QuadExpr&, const QuadExpr&);
    friend QuadExpr operator-(const QuadExpr&);
    friend QuadExpr operator*(const QuadExpr&, const QuadExpr&);
    friend QuadExpr operator*(const QuadExpr&, double);
    friend QuadExpr operator*(double, const QuadExpr&);
    friend QuadExpr operator/(const QuadExpr& LHS, double scalar);
    friend QuadExpr operator/(double scalar, const QuadExpr& RHS);
    friend QuadExpr operator/(const QuadExpr& LHS, const QuadExpr& RHS);
};

QuadExpr operator+(const QuadExpr& RHS);
QuadExpr operator+(const QuadExpr& LHS, const QuadExpr& RHS);
QuadExpr operator-(const QuadExpr& LHS, const QuadExpr& RHS);
QuadExpr operator-(const QuadExpr& RHS);
QuadExpr operator*(const QuadExpr& LHS, const QuadExpr& RHS);
QuadExpr operator*(const QuadExpr& LHS, double RHS);
QuadExpr operator*(double LHS, const QuadExpr& RHS);
QuadExpr operator/(const QuadExpr& LHS, double scalar);
QuadExpr operator/(double scalar, const QuadExpr& RHS);
QuadExpr operator/(const QuadExpr& LHS, const QuadExpr& RHS);

} // namespace dips
