#pragma once

#include <exception> // for invalid_argument
#include <string>    // for string
#include <variant>   // for visit, variant
#include <stdexcept> // for invalid_argument

#include "symbol_table.hpp" // for symbol_table
#include "solver.hpp"       // for solve_record
#include "helper.hpp"       // for ale
#include "symbol.hpp"       // for base_symbol, parameter_symbol, value_symbol, variable_symbol, function_symbol, value_symbol<>::variant
#include "value.hpp"        // for real



namespace dips {


using namespace ale;

void add_variable_to_solve_record(const std::string& var_name, symbol_table& symbols, solve_record& rec);


} // namespace dips
