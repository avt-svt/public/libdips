#pragma once

#include <stddef.h> // for size_t
#include <variant>  // for variant
#include <unordered_map>
#include <vector> // for vector

#include "scalar_evaluator.hpp"    // for ScalarVariableIdManager
#include "quad_expr_evaluator.hpp" // for QuadConstraintList
#include "QuadExpr.hpp"            // for QuadExpr
#include "value.hpp"               // for boolean, real
#include "expression.hpp"          // for expression
#include "symbol_table.hpp"        // for symbol_table



namespace dips {


/**
     * Represents y = e^x
     */
struct GurobiExponentialConstraint {
    size_t variable_id_x;
    size_t variable_id_y;
};

using GurobiConstraintList = std::vector<std::variant<GurobiExponentialConstraint>>;

/**
     * Evaluates expressions which are of the form x^T.Q.x+b.x+c but also allows exp
     */
QuadExpr evaluate_gurobi(expression<real<0>>& expr, symbol_table& symbols, ScalarVariableIdManager& id_manager, GurobiConstraintList& grb_constraints, std::vector<QuadConstraintList>& quad_constraint_lists);

/**
     * Evaluates constraints in which the expressions can be evaluated using evaluate_gurobi
     */
QuadConstraintList evaluate_gurobi_constraint(expression<ale::boolean<0>>& expr, symbol_table& symbols, ScalarVariableIdManager& id_manager, GurobiConstraintList& grb_constraints, std::vector<QuadConstraintList>& quad_constraint_lists);

} // namespace dips
