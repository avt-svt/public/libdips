#pragma once

#include <vector>
#include <array>
#include <unordered_map>
#include <exception>

#include "util/expression_utils.hpp"
#include "symbol_table.hpp"
#include "solver.hpp"

namespace dips {

using namespace ale;

struct ScalarInvalidEntryNodeError : public std::runtime_error {
    explicit ScalarInvalidEntryNodeError(size_t nesting_level) :
        std::runtime_error(""), nesting_level(nesting_level) { }

    size_t nesting_level;
};

template <typename VarType>
using get_value_func = std::function<double(VarType)>;

template <typename VarType>
using variable_adder_func = std::function<VarType(double, double, double, bool, const std::string&, const std::string&, const std::vector<size_t>&)>;

namespace helper {

    template <typename VarType>
    class set_record_visitor {
    public:
        set_record_visitor(const get_value_func<VarType>& get_value, const std::vector<VarType>& variable_list) :
            get_value(get_value), variable_list(variable_list) { }

        void operator()(base_symbol* /*sym*/) {
            throw std::invalid_argument("unexpected symbol visit");
        }

        template <unsigned IDim>
        void operator()(parameter_symbol<real<IDim>>* sym) {
            if constexpr(IDim == 0) {
                sym->m_value = get_value(variable_list.at(base_id));
            } else {
                // check that the tensor is not empty
                if(tensor_num_elements(sym->m_value.shape()) == 0) {
                    return;
                }

                // calculate id of first variable
                size_t offset = base_id - tensor_num_elements(sym->m_value.shape()) + 1;

                auto index = last_tensor_index(sym->m_value.shape());
                size_t linear_index = 0;
                do {
                    sym->m_value[index.data()] = get_value(variable_list.at(offset + linear_index));
                    linear_index++;
                } while(decrease_tensor_index(sym->m_value.shape(), index));
            }
        }

        void set_base_id(size_t new_base_id) {
            base_id = new_base_id;
        }

    private:
        const get_value_func<VarType>& get_value;
        const std::vector<VarType>& variable_list;
        size_t base_id = 0;
    };

    template <typename VarType>
    class add_variable_visitor {
    public:
        add_variable_visitor(const variable_adder_func<VarType>& add_variable, std::vector<VarType>& result) :
            add_variable(add_variable), result(result) { }

        void operator()(base_symbol* /*sym*/) {
            throw std::invalid_argument("only variables should be dispatched");
        }

        template <typename TType>
        void operator()(variable_symbol<TType>* sym) {
            if constexpr(get_node_dimension<TType> == 0) {
                result.at(base_id) = add_variable(sym->lower(), sym->upper(), sym->init(), sym->integral(), sym->m_name, sym->comment(), {});
            } else {
                // check that the tensor is not empty
                if(tensor_num_elements(sym->shape()) == 0) {
                    return;
                }

                // calculate id of first variable
                size_t offset = base_id - tensor_num_elements(sym->shape()) + 1;

                // elements are stored in decreasing order of their indexes
                // see scalar_evaluator/get_variable_id
                auto index = last_tensor_index(sym->shape());
                size_t linear_index = 0;
                do {
                    double lb = sym->lower()[index.data()];
                    double ub = sym->upper()[index.data()];
                    double init = sym->init()[index.data()];

                    result.at(offset + linear_index) = add_variable(lb, ub, init, sym->integral(), sym->m_name, sym->comment(), index);

                    linear_index++;
                } while(decrease_tensor_index(sym->shape(), index));
            }
        }

        void set_base_id(size_t new_base_id) {
            base_id = new_base_id;
        }

    private:
        const variable_adder_func<VarType>& add_variable;
        std::vector<VarType>& result;
        size_t base_id = 0;
    };

} // namespace helper

/**
 * Class to manage IDs of all used variables. Each element in tensor variables is assigned a seperate ID
 */
class ScalarVariableIdManager {
public:
    /**
     * Initialize all ids of the variables in 'used_variables'.
     */
    void set_ids(const std::set<std::string>& used_variables, symbol_table& symbols) {
        for(const auto& sym_name : used_variables) {
            auto shape = get_parameter_shape(sym_name, symbols);
            size_t base_id = tensor_num_elements(shape) - 1 + next_free_id;

            variable_base_ids[sym_name] = base_id;
            next_free_id = base_id + 1;
        }
    }

    /**
     * Get ID of scalar variable or assign one if it is not found
     */
    size_t get_or_assign_id(const std::string& name) {
        // check if variable was already assigned an id
        if(variable_base_ids.count(name) == 0) {
            variable_base_ids[name] = next_free_id;
            next_free_id += 1;
        }

        return get_id(name);
    }

    /**
     * Get ID of scalar variable
     */
    size_t get_id(const std::string& name) const {
        // check if variable was already assigned an id
        if(variable_base_ids.count(name) == 0) {
            throw std::invalid_argument("variable has no associated id");
        }

        return variable_base_ids.at(name);
    }

    /**
     * Get ID of tensor-variable element at index or assign ID to full tensor if it is not found
     * Note: it is not checked wether the index is valid
     */
    template <size_t IDim>
    size_t get_or_assign_id(const std::string& name, const std::array<size_t, IDim>& shape, const std::vector<size_t>& index) {
        // check if variable was already assigned a base id
        if(variable_base_ids.count(name) == 0) {
            // calculate the number of elements of the tensor
            size_t base_id = tensor_num_elements(shape) - 1 + next_free_id;

            // update map and next_free_id
            variable_base_ids[name] = base_id;
            next_free_id = base_id + 1;
        }

        return get_id(name, shape, index);
    }

    /**
     * Get ID of tensor-variable element at index
     * Note: it is not checked wether the index is valid
     */
    template <size_t IDim>
    size_t get_id(const std::string& name, const std::array<size_t, IDim>& shape, const std::vector<size_t>& index) const {
        // check if variable was already assigned a base id
        if(variable_base_ids.count(name) == 0) {
            throw std::invalid_argument("variable has no associated id");
        }

        return variable_base_ids.at(name) - flatten_index(shape, index);
    }

    /**
     * Set the solve_record by mapping over variable_list.
     * 
     * get_value: given variable_list[id] it should return the value of the optimized variable of the id
     * variable_list: some list of values from which given variable_list[id] the optimized value can be calculated
     * rec: the record the values are set
     */
    template <typename VarType>
    void set_record(const get_value_func<VarType>& get_value, solve_record& rec, const std::vector<VarType>& variable_list) const {
        helper::set_record_visitor<VarType> visitor(get_value, variable_list);
        for(const auto& [var_name, base_id] : variable_base_ids) {
            // check that var_name is not a helper variable
            if(rec.m_solution.count(var_name) > 0) {
                visitor.set_base_id(base_id);
                call_visitor(visitor, rec.m_solution[var_name].get());
            }
        }
    }

    /**
     * Calls add_variable on each variable-element, and returns a vector of the returned values in the order given by variable_base_ids
     * 
     * add_variable: should create scalar variable with signature (lb, ub, init, integral, name, comment, index) -> VarType
     */
    template <typename VarType>
    std::vector<VarType> map_over_variable_symbols(const variable_adder_func<VarType>& add_variable, symbol_table& symbols) {
        std::vector<VarType> result;
        result.resize(next_free_id); // next_free_id is also the number of already used variables

        helper::add_variable_visitor<VarType> visitor(add_variable, result);
        for(const auto& [var_name, base_id] : variable_base_ids) {
            auto* sym = symbols.resolve(var_name);
            if(sym == nullptr) {
                throw std::invalid_argument("symbol could not be resolved");
            }

            visitor.set_base_id(base_id);
            call_visitor(visitor, sym);
        }

        return result;
    }

private:
    template <size_t IDim>
    static size_t flatten_index(const std::array<size_t, IDim>& shape, const std::vector<size_t>& index) {
        auto subsizes = subsizes_of_shape(shape);

        size_t flattened_index = 0;
        for(size_t i = 0; i < IDim; ++i) {
            flattened_index += subsizes.at(i) * index.at(i);
        }

        return flattened_index;
    }

    std::unordered_map<std::string, size_t> variable_base_ids; // map of variable name to its biggest id
    size_t next_free_id = 0;                                   // points to the next available id in variable_base_ids
};

/**
 * This class abstracts visitors which only evaluate scalar expressions and the
 * internal solver only uses scalar variables. 
 */
class scalar_evaluator {
public:
    scalar_evaluator(symbol_table& symbols, const ScalarVariableIdManager& id_manager) :
        symbols(symbols), id_manager(id_manager) { }

    /**
     * Verify that the cached index is appropriate for the given shape.
     */
    template <size_t IDim>
    void assert_valid_index_for_shape(const std::array<size_t, IDim>& shape) {
        if(IDim != indexes.size()) {
            throw std::invalid_argument("(Internal) Not a valid index for the given shape");
        }

        // check that index is in bounds of shape
        for(size_t i = 0; i < IDim; ++i) {
            if(indexes.at(i) >= shape.at(i)) {
                // throw indicating at which dimension the invalid index was encountered
                // this exception will be catched in the entry_node
                throw ScalarInvalidEntryNodeError(i);
            }
        }
    }


    /**
     * Verify no index is cached. (ie it is valid for shape = {})
     */
    void assert_empty_indexes() const {
        if(!indexes.empty()) {
            throw std::invalid_argument("(Internal) Expected no indexes");
        }
    }

    symbol_table& symbols;
    std::vector<size_t> indexes {}; // cache of indexes from entry_nodes
    const ScalarVariableIdManager& id_manager;
};

} // namespace dips
