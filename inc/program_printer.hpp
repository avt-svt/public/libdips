#pragma once

#include <string> // for string

#include "program.hpp" // for program
#include "helper.hpp"  // for ale

namespace dips {


using namespace ale;

std::string program_to_string(program& prog);
void print(program& prog);


} // namespace dips
