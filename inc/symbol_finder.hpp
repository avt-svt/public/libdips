#pragma once

#include <set>    // for set
#include <string> // for string

#include "expression.hpp"   // for expression
#include "symbol_table.hpp" // for symbol_table
#include "program.hpp"      // for program
#include "helper.hpp"       // for ale
#include "node.hpp"         // for value_node_variant


namespace dips {


using namespace ale;

std::set<std::string> find_variables(value_node_variant expr, symbol_table& symbols, bool ignore_attribute_node = true);

template <typename TType>
std::set<std::string> find_variables(expression<TType>& expr, symbol_table& symbols, bool ignore_attribute_node = true) {
    return find_variables(expr.get(), symbols, ignore_attribute_node);
}

std::set<std::string> find_variables(program& prog, symbol_table& symbols, bool ignore_attribute_node = true);

std::set<std::string> find_parameters(value_node_variant expr, symbol_table& symbols);

template <typename TType>
std::set<std::string> find_parameters(expression<TType>& expr, symbol_table& symbols) {
    return find_parameters(expr.get(), symbols);
}

std::set<std::string> find_parameters(program& prog, symbol_table& symbols);

std::set<std::string> find_functions(value_node_variant expr, symbol_table& symbols);

template <typename TType>
std::set<std::string> find_functions(expression<TType>& expr, symbol_table& symbols) {
    return find_functions(expr.get(), symbols);
}

std::set<std::string> find_functions(program& prog, symbol_table& symbols);

bool has_prio_variable(value_node_variant expr, symbol_table& symbols);

template <typename TType>
bool has_prio_variable(expression<TType>& expr, symbol_table& symbols) {
    return has_prio_variable(expr.get(), symbols);
}

bool has_prio_variable(program& prog, symbol_table& symbols);



} // namespace dips
