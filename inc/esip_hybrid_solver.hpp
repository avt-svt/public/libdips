#pragma once

#include <functional> // for function
#include <utility>    // for pair, move
#include <iosfwd>     // for istream
#include <string>     // for string, allocator, basic_string
#include <vector>     // for vector
#include <atomic>     // for atomic

#include "common.hpp"
#include "solver.hpp"         // for set_tolerance_robust_base, solve_record, solver
#include "parser_chain.hpp"   // for programdefinition_parser_chain
#include "discretization.hpp" // for discretization
#include "helper.hpp"         // for ale
#include "program.hpp"        // for program
#include "symbol_table.hpp"   // for symbol_table


namespace dips {


using namespace ale;

class esip_hybrid_program_definition_parser : public programdefinition_parser_chain {
public:
    esip_hybrid_program_definition_parser(std::istream&, symbol_table&);

    std::vector<discretization> get_ubp_discretizations();
    std::vector<discretization> get_lbp_discretizations();
    std::vector<discretization> get_res_discretizations();
    std::vector<discretization> get_mlp_discretizations();
};
class esip_hybrid_solver {
public:
    struct program_info {
        void add_mlp_and_llp(const program& mlp, const program& llp, const discretization& lbp_disc, const discretization& ubp_disc, const discretization& res_disc, const discretization& mlp_disc);

        program lbp;
        program ubp;
        std::string ubp_restrict;
        program res;
        std::string res_target;
        std::vector<std::pair<program, program>> mlp_llp_pairs;
        std::vector<discretization> lbp_discs;
        std::vector<discretization> ubp_discs;
        std::vector<discretization> res_discs;
        std::vector<discretization> mlp_discs; //only one for each mlp
        std::map<std::string, std::string> lbp_info, ubp_info, mlp_info, res_info;
        std::map<std::string, std::vector<std::string>> lbp_info_list, ubp_info_list, mlp_info_list, res_info_list;
    };
    esip_hybrid_solver(symbol_table& symbols, std::function<solver*(symbol_table&, std::string)> make_subsolver, std::string solver_name = "esip_hybrid_output/") :
        symbols(symbols),
        make_subsolver(std::move(make_subsolver)),
        solver_base_name(std::move(solver_name)) {};
    solve_record solve(program_info prog);
    bool set_option(const std::string&, double);
    bool set_option(const std::string&, const std::string&);
    double get_option(const std::string&) const;
    void set_tolerance_robust(std::string option, double value) { set_tolerance_robust_base(*this, std::move(option), value); }

private:
    std::function<solver*(symbol_table&, std::string)> make_subsolver;
    std::vector<std::pair<std::string, double>> options;
    std::string solver_base_name;
    symbol_table& symbols;
    std::string settings_lbp = "settings_lbp.txt";
    std::string settings_lbp_minmax_ulp = "settings_minmax_ulp.txt";
    std::string settings_lbp_minmax_llp = "settings_minmax_llp.txt";
    std::string settings_ubp = "settings_ubp.txt";
    std::string settings_ubp_minmax_ulp = "settings_minmax_ulp.txt";
    std::string settings_ubp_minmax_llp = "settings_minmax_llp.txt";
    std::string settings_res = "settings_res.txt";
    std::string settings_res_minmax_ulp = "settings_minmax_ulp.txt";
    std::string settings_res_minmax_llp = "settings_minmax_llp.txt";
    double feas_tol = 0; // must be >= 0
    double abs_tol = 1e-3;
    double rel_tol = 1e-3;

    double abs_tol_lbp = 1e-1 * abs_tol;
    double rel_tol_lbp = rel_tol;
    double abs_tol_lbp_minmax = 1e-2 * abs_tol;
    double rel_tol_lbp_minmax = rel_tol_lbp;
    double abs_tol_lbp_minmax_ulp = 1e-3 * abs_tol;
    double rel_tol_lbp_minmax_ulp = rel_tol_lbp;
    double abs_tol_lbp_minmax_llp = 1e-3 * abs_tol;
    double rel_tol_lbp_minmax_llp = rel_tol_lbp;

    double abs_tol_ubp = 1e-1 * abs_tol;
    double rel_tol_ubp = 1e-1 * rel_tol;
    double abs_tol_ubp_minmax = 1e-2 * abs_tol;
    double rel_tol_ubp_minmax = rel_tol_ubp;
    double abs_tol_ubp_minmax_ulp = 1e-3 * abs_tol;
    double rel_tol_ubp_minmax_ulp = rel_tol_ubp;
    double abs_tol_ubp_minmax_llp = 1e-3 * abs_tol;
    double rel_tol_ubp_minmax_llp = rel_tol_ubp;

    double abs_tol_res = 1e-2 * abs_tol;
    double rel_tol_res = 1e-2 * rel_tol;
    double abs_tol_res_minmax = 1e-2 * abs_tol;
    double rel_tol_res_minmax = rel_tol_res;
    double abs_tol_res_minmax_ulp = 1e-3 * abs_tol;
    double rel_tol_res_minmax_ulp = rel_tol_res;
    double abs_tol_res_minmax_llp = 1e-3 * abs_tol;
    double rel_tol_res_minmax_llp = rel_tol_res;

    double red_tol_lbp_minmax = 1.25; // must be > 1
    double red_tol_ubp_minmax = 1.25; // must be > 1
    double red_tol_res_minmax = 1.25; // must be > 1



    double init_res = 1;       // inital restriction parameter; must be > 0
    double eps_res = init_res; // restricion parameter
    double red_res = 2;        // reduction parameter of eps_res; must be > 1

    int max_iter = 1000;
    double max_time = 86400;

    bool discr_all = false;
    std::atomic<bool> aborted = false;
};



} // namespace dips
