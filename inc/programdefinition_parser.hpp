#pragma once

#include <map>    // for map
#include <iosfwd> // for istream
#include <string> // for string, basic_string
#include <vector> // for vector

#include "parser.hpp" // for parser
#include "program.hpp"
#include "discretization.hpp" // for discretization
#include "symbol_table.hpp"   // for symbol_table


namespace dips {

/**
     * Parses a programdefinitions block of the following form:
     * 
     * 
     * programdefinitions("<block_name>"):
     * (INSTR \n)+
     * 
     * 
     * with INSTR being one of:
     * 
     * set_disc(<disc_name>) = <disc_index_set_name>;
     * set_disc_variable_src(<disc_name>, <disc>) = <disc_var>;
     * set_disc_parameter_src(<disc_name>, <disc>) = <disc_var>;
     * set_internal("<internal_name>") = <variable_binding>;
     * set_info(<info_name>) = <value>;
     * 
     * 
     * For each value given in "required_internals" a set_internal is expected.
     */
class programdefinition_parser : public ale::parser {
public:
    programdefinition_parser(std::istream&, ale::symbol_table&, const std::string& block_name, const std::vector<std::string>& required_internals);

    bool parse(); //return true if block name was encountered

    //returns the discretizations. NOT in order of occurance, but sorted in used discretization name!
    [[nodiscard]] std::vector<discretization> get_discretizations() const;
    [[nodiscard]] std::map<std::string, discretization> get_discretization_map() const;
    [[nodiscard]] std::map<std::string, std::string> get_internals() const;
    [[nodiscard]] std::map<std::string, std::vector<std::string>> get_internals_list() const;
    [[nodiscard]] std::map<std::string, std::string> get_info() const;
    [[nodiscard]] std::map<std::string, std::vector<std::string>> get_info_list() const;
    [[nodiscard]] std::string get_block_name() const;

    bool fail() { return ale::parser::fail() || !required_internals_read(); };

private:
    void parse_programdefinition();
    [[nodiscard]] bool required_internals_read() const;
    void recover_block();

    std::string block_name;                                         // search for block programdefinitions("<block_name>"):
    std::vector<std::string> required_internals;                    // internal parameters that must be set
    std::map<std::string, std::string> internals;                   // internal parameters
    std::map<std::string, std::vector<std::string>> internals_list; // list of internal parameters
    std::map<std::string, std::string> info;                        // optional additional info, e.g., author
    std::map<std::string, std::vector<std::string>> info_list;      // optional additional info, e.g., alias
    std::map<std::string, discretization> discretization_mapping;   // maps name of discretization used in input file to the discretizations
};

} // namespace dips
