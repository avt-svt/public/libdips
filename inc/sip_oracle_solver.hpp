#pragma once

#include <functional> // for function
#include <utility>    // for move, pair
#include <iosfwd>     // for istream
#include <limits>     // for numeric_limits
#include <map>        // for map
#include <string>     // for string, allocator, basic_string
#include <vector>     // for vector
#include <atomic>     // for atomic

#include "common.hpp"
#include "solver.hpp"         // for set_tolerance_robust_base, solve_record, solver
#include "parser_chain.hpp"   // for programdefinition_parser_chain
#include "discretization.hpp" // for discretization
#include "helper.hpp"         // for ale
#include "program.hpp"        // for program
#include "symbol_table.hpp"   // for symbol_table



namespace dips {



using namespace ale;

class sip_oracle_program_definition_parser : public programdefinition_parser_chain {
public:
    sip_oracle_program_definition_parser(std::istream&, symbol_table&);

    std::vector<discretization> get_lbp_discretizations();
    std::vector<discretization> get_ora_discretizations();
    std::map<std::string, std::string> get_ora_internals();
};

class sip_oracle_solver {
    // algorithm statement based on algorithm 3
    // of Tsoukalas, Angelos; Rustem, Ber� (2011):
    // A feasible point adaptation of the Blankenship and
    // Falk algorithm for semi-infinite programming.
    // In Optim. Lett. 5 (4), pp. 705-716.
    // DOI: 10.1007/s11590-010-0236-4.
    // Extensions:
    //  - support multiple llps
    //  - discretization of all or only single llp
    //    possible
    //  - added feasibility tolerance to be able to
    //    compare to sip_bnf_solver
    //  - solve lbp and corresponding llps once if initial
    //    discretization set is empty or no LBD is provided
    //    by the user
    //  - solving auxilliary problem utoptian_ubp if no UBD
    //    is provided by the user to compute a UBD (which is
    //    not necessarily feasible). If the UBD is not
    //    attainable, the SIP is infeasible.
    //  - avoid looping of algorithm if ora is infeasible:
    //    lbp is computed if ora is infeasible to provide
    //    hints on whether the ora problem is wrongly
    //    implemented
    //  - added tolerance update
    //  - the termination critereon is checked more
    //    often
    //  - iterations are differently defined

public:
    struct program_info {
        void add_llp(const program&, const discretization&, const discretization&);

        program lbp;
        program ora;
        double init_ubd = std::numeric_limits<double>::infinity();
        double init_lbd = -std::numeric_limits<double>::infinity();
        std::string ora_target;
        std::vector<program> llps;
        std::vector<discretization> lbp_discs;
        std::vector<discretization> ora_discs;

        std::map<std::string, std::string> lbp_info, ora_info;
        std::map<std::string, std::vector<std::string>> lbp_info_list, ora_info_list;
    };
    sip_oracle_solver(symbol_table& symbols, std::function<solver*(symbol_table&, std::string)> make_subsolver, std::string solver_name = "sip_output_ora/") :
        symbols(symbols),
        make_subsolver(std::move(make_subsolver)),
        solver_base_name(std::move(solver_name)) {};
    solve_record solve(program_info);
    bool set_option(const std::string&, double);
    bool set_option(const std::string&, const std::string&);
    bool set_option(const std::string&, bool);
    void set_tolerance_robust(std::string option, double value) { set_tolerance_robust_base(*this, std::move(option), value); };
    void pass_option(const std::string&, double);
    [[nodiscard]] double get_option(const std::string&) const;
    void set_solver_base_name(std::string);
    std::string get_solver_base_name();

private:
    std::function<solver*(symbol_table&, std::string)> make_subsolver;
    symbol_table& symbols;
    std::string solver_base_name;
    std::vector<std::pair<std::string, double>> options;

    std::string settings_lbp = "settings_lbp.txt";
    std::string settings_ora = "settings_ora.txt";
    std::string settings_lbp_llp = "settings_llp.txt";
    std::string settings_ora_llp = "settings_llp.txt";


    double feas_tol = 0; // must be >= 0
    double abs_tol = 1e-2;
    double rel_tol = 1e-2;

    double abs_tol_lbp = 1e-1 * abs_tol;
    double rel_tol_lbp = 1e-1 * rel_tol;
    double abs_tol_lbp_llp = 1e-2 * abs_tol;
    double rel_tol_lbp_llp = 1e-2 * rel_tol;

    double abs_tol_ora = 1e-1 * abs_tol;
    double rel_tol_ora = 1e-1 * rel_tol;
    double abs_tol_ora_llp = 1e-2 * abs_tol;
    double rel_tol_ora_llp = 1e-2 * rel_tol;

    double red_tol_ora = 1.25;     // must be > 1
    double red_tol_lbp_llp = 1.25; // must be > 1
    double red_tol_ora_llp = 1.25; // must be > 1

    int max_iter = 1000;
    double max_time = 86400;

    bool discr_all = false;
    std::atomic<bool> aborted = false;
};



} // namespace dips
