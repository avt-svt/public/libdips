/**********************************************************************************
 * Copyright (c) 2019 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

#pragma once

#include <string>
#include <unordered_map>
#include <vector>
#include <cmath> // for sin, cos, etc in case of double

#include "util/expression_utils.hpp"
#include "util/visitor_utils.hpp"

#include "expression.hpp"
#include "symbol_table.hpp"
#include "scalar_evaluator.hpp"

namespace dips {


using namespace ale;

/**
 * @struct ConstraintContainer
 * @brief Containter for constraint evaluation
 */
template <typename Var>
struct ConstraintContainer {
    std::vector<Var> eq;   /*!< Equality constraints*/
    std::vector<Var> ineq; /*!< Inequality constraints*/
};

template <typename Var>
Var evaluate_fadbad_expr(expression<real<0>>& expr, symbol_table& symbols,
  const ScalarVariableIdManager& id_manager,
  const std::vector<Var>& variables);

template <typename Var>
ConstraintContainer<Var> evaluate_fadbad_constraint(expression<ale::boolean<0>>& expr, symbol_table& symbols,
  const ScalarVariableIdManager& id_manager,
  const std::vector<Var>& variables);

} // namespace dips


//
// implementation
//

namespace dips {

namespace helper {

    using namespace ale;

    template <typename Var>
    class fadbad_expr_evaluator : private scalar_evaluator {
    public:
        fadbad_expr_evaluator(symbol_table& symbols, const ScalarVariableIdManager& id_manager, const std::vector<Var>& variables) :
            scalar_evaluator(symbols, id_manager), variables(variables) { }

        /**
     * Default visit for any node assuming all indexes are already specified
     *
     * Say we have a symbol of type
     * set{index}[2, 3] x := ...
     * then the parameter_node "x" can only be evaluated if indexes is of size 2.
     * This would be the case for the expression "x[1, 2]"
     */
        template <typename TType>
        owning_ref<tensor_type<atom_type<TType>, 0>> operator()(value_node<TType>* node) {
            constexpr auto IDim = get_node_dimension<TType>;

            auto res = get_subtree_value(node, symbols);
            if(!res) {
                throw std::invalid_argument("could not evaluate expression: " + expression_to_string(node));
            }

            if constexpr(IDim > 0) {
                assert_valid_index_for_shape(res->shape());

                auto out = *res[indexes.data()];
                indexes = {};
                return out;
            } else {
                assert_empty_indexes();
                return *res;
            }
        }

        /**
     * Default visit for real value_nodes
     * This visit is overridden by any of the following implementations
     * It tries to evaluate constant real expressions in case no other implementation is given
     */
        template <unsigned IDim>
        Var operator()(value_node<real<IDim>>* node) {
            auto res = get_subtree_value(node, symbols);
            if(!res) {
                throw std::invalid_argument("could not evaluate expression: " + expression_to_string(node));
            }

            if constexpr(IDim > 0) {
                assert_valid_index_for_shape(res->shape());

                Var out = (*res)[indexes.data()];
                indexes = {};
                return out;
            } else {
                assert_empty_indexes();
                return *res;
            }
        }

        /**
     * Default visit for boolean valued nodes
     * It tries to evaluate the expression using the evaluator and throws if that fails
     * This visit is overridden by any of the following implementations
     */
        template <unsigned IDim>
        ConstraintContainer<Var> operator()(value_node<ale::boolean<IDim>>* node) {
            auto res = get_subtree_value(node, symbols);
            if(!res) {
                throw std::invalid_argument("could not evaluate expression: " + expression_to_string(node));
            }

            bool result = true;
            if constexpr(IDim > 0) {
                assert_valid_index_for_shape(res->shape());

                result = *res[indexes.data()];
                indexes = {};
            } else {
                assert_empty_indexes();
                result = *res;
            }

            if(result) {
                return { { 0 }, {} }; // 0=0
            }
            return { { 1 }, {} }; // 1=0
        }

        //
        // terminal nodes
        //

        template <unsigned IDim>
        Var operator()(constant_node<real<IDim>>* node) {
            if constexpr(IDim == 0) {
                assert_empty_indexes();
                return node->value;
            } else {
                assert_valid_index_for_shape(node->value.shape());

                Var out = node->value[indexes.data()];
                indexes = {};
                return out;
            }
        }

        // parameter node related visits
        template <unsigned IDim>
        Var operator()(parameter_node<real<IDim>>* node) {
            auto* sym = symbols.resolve<real<IDim>>(node->name);
            if(sym == nullptr) {
                if(!symbols.resolve(node->name)) {
                    throw std::invalid_argument("symbol " + node->name + " not found");
                }
                throw std::invalid_argument("symbol " + node->name + " has unexpected type. Expected a real valued parameter of dimension " + std::to_string(IDim) + " but got " + symbol_to_string(sym));
            }
            return call_visitor(*this, sym);
        }

        template <unsigned IDim>
        Var operator()(parameter_symbol<real<IDim>>* sym) {
            if constexpr(IDim == 0) {
                assert_empty_indexes();
                return sym->m_value;
            } else {
                assert_valid_index_for_shape(sym->m_value.shape());

                Var out = sym->m_value[indexes.data()];
                indexes = {};
                return out;
            }
        }

        template <unsigned IDim>
        Var operator()(variable_symbol<real<IDim>>* sym) {
            if constexpr(IDim == 0) {
                assert_empty_indexes();
                return variables.at(id_manager.get_id(sym->m_name));
            } else {
                assert_valid_index_for_shape(sym->shape());

                Var out = variables.at(id_manager.get_id(sym->m_name, sym->shape(), indexes));
                indexes = {};
                return out;
            }
        }

        template <unsigned IDim>
        Var operator()(expression_symbol<real<IDim>>* /*sym*/) {
            throw std::invalid_argument("visit of expression_symbol should not occur");
        }

        template <unsigned IDim>
        Var operator()(function_symbol<real<IDim>>* /*sym*/) {
            throw std::invalid_argument("visit of function_symbol should not occur");
        }

        //
        // non-terminal real node visits
        //

        Var operator()(minus_node* node) { return -evaluate_child(*this, node); }

        Var operator()(addition_node* node) {
            auto values = evaluate_children(*this, node);

            Var sum(0.0);
            for(const auto& x : values) {
                sum += x;
            }

            return sum;
        }

        Var operator()(inverse_node* node) {
            return 1 / evaluate_child(*this, node);
        }

        Var operator()(exponentiation_node* node) {
            using std::pow;
            Var result = 1;
            auto values = evaluate_children(*this, node);

            for(auto it = values.rbegin(); it != values.rend(); ++it) {
                result = pow(*it, result);
            }
            return result;
        }

        Var operator()(multiplication_node* node) {
            auto values = evaluate_children(*this, node);

            Var prod(1.0);
            for(const auto& x : values) {
                prod *= x;
            }

            return prod;
        }

        template <typename TType>
        Var operator()(sum_node<TType>* node) {
            auto values = evaluate_children_iterated(*this, node, symbols);

            Var sum(0.0);
            for(const auto& x : values) {
                sum += x;
            }

            return sum;
        }

        template <typename TType>
        Var operator()(product_node<TType>* node) {
            auto values = evaluate_children_iterated(*this, node, symbols);

            Var prod(1.0);
            for(const auto& x : values) {
                prod *= x;
            }

            return prod;
        }
        Var operator()(min_node* node) {
            if(node->children.empty()) {
                throw std::runtime_error("  Error: fadbad_evaluator --  Called min without arguments");
            }
            auto values = evaluate_children(*this, node);
            Var result = *values.begin();
            for(auto value : values) {
                if(result > value) {
                    result = value;
                }
            }
            return result;
        }


        Var operator()(max_node* node) {
            if(node->children.empty()) {
                throw std::runtime_error("  Error: fadbad_evaluator --  Called min without arguments");
            }
            auto values = evaluate_children(*this, node);
            Var result = *values.begin();
            for(auto value : values) {
                if(result < value) {
                    result = value;
                }
            }
            return result;
        }


        template <typename TType>
        Var operator()(set_min_node<TType>* node) {
            auto values = evaluate_children_iterated(*this, node, symbols);
            Var result = *values.begin();
            for(auto value : values) {
                if(result > value) {
                    result = value;
                }
            }
            return result;
        }


        template <typename TType>
        Var operator()(set_max_node<TType>* node) {
            auto values = evaluate_children_iterated(*this, node, symbols);
            Var result = *values.begin();
            for(auto value : values) {
                if(result < value) {
                    result = value;
                }
            }
            return result;
        }

        Var operator()(exp_node* node) {
            using std::exp;
            return exp(evaluate_child(*this, node));
        }


        Var operator()(log_node* node) {
            using std::log;
            return log(evaluate_child(*this, node));
        }


        Var operator()(sqrt_node* node) {
            using std::sqrt;
            return sqrt(evaluate_child(*this, node));
        }


        Var operator()(sin_node* node) {
            using std::sin;
            return sin(evaluate_child(*this, node));
        }


        Var operator()(asin_node* node) {
            using std::asin;
            return asin(evaluate_child(*this, node));
        }


        Var operator()(cos_node* node) {
            using std::cos;
            return cos(evaluate_child(*this, node));
        }


        Var operator()(acos_node* node) {
            using std::acos;
            return acos(evaluate_child(*this, node));
        }


        Var operator()(tan_node* node) {
            using std::tan;
            return tan(evaluate_child(*this, node));
        }


        Var operator()(atan_node* node) {
            using std::atan;
            return atan(evaluate_child(*this, node));
        }

        //
        // non-terminal tensor node
        //

        template <unsigned IDim>
        Var operator()(function_node<real<IDim>>* node) {
            return evaluate_function(*this, node, symbols);
        }

        template <unsigned IDim>
        Var operator()(tensor_node<real<IDim>>* node) {
            // get first index
            size_t current_index = indexes.front();
            indexes.erase(indexes.begin());

            if(node->children.size() <= current_index) {
                throw std::invalid_argument("index " + std::to_string(current_index) + " is out of bounds for tensor " + expression_to_string(node));
            }

            // get child at current_index
            auto child_ptr = node->children.begin();
            std::advance(child_ptr, current_index);

            return call_visitor(*this, *child_ptr);
        }

        template <unsigned IDim>
        Var operator()(vector_node<real<IDim>>* /*node*/) {
            throw std::invalid_argument("visit of vector_node should not occur");
        }

        template <unsigned IDim>
        Var operator()(entry_node<real<IDim>>* node) {
            // store indexes in reversed order before evaluating tensor child
            auto indexes_copy = indexes;
            indexes = {};
            auto index = call_visitor(*this, node->template get_child<1>());
            indexes = indexes_copy;

            indexes.insert(indexes.begin(), index - 1);

            try {
                // evaluate correct entry of tensor
                return call_visitor(*this, node->template get_child<0>());
            } catch(const ScalarInvalidEntryNodeError& e) {
                // handle out of bounds errors
                if(e.nesting_level > 0) {
                    throw ScalarInvalidEntryNodeError(e.nesting_level - 1);
                }
                throw std::invalid_argument("index " + std::to_string(index) + " is out of bounds for tensor " + expression_to_string(node->template get_child<0>()));
            }
        }

        //
        // non-terminal boolean node visits
        //

        ConstraintContainer<Var> operator()(equal_node<real<0>>* node) {
            auto [lhs, rhs] = evaluate_children_tuple(*this, node);
            return { { lhs - rhs }, {} };
        }
        ConstraintContainer<Var> operator()(less_equal_node<real<0>>* node) {
            auto [lhs, rhs] = evaluate_children_tuple(*this, node);
            return { {}, { lhs - rhs } };
        }
        ConstraintContainer<Var> operator()(greater_equal_node<real<0>>* node) {
            auto [lhs, rhs] = evaluate_children_tuple(*this, node);
            return { {}, { rhs - lhs } };
        }

        template <typename TType>
        ConstraintContainer<Var> operator()(forall_node<TType>* node) {
            auto values = evaluate_children_iterated(*this, node, symbols);

            ConstraintContainer<Var> result;
            for(const auto& x : values) {
                result.eq.insert(result.eq.end(), x.eq.begin(), x.eq.end());
                result.ineq.insert(result.ineq.end(), x.ineq.begin(), x.ineq.end());
            }

            return result;
        }

        ConstraintContainer<Var> operator()(conjunction_node* node) {
            ConstraintContainer<Var> result;
            auto values = evaluate_children(*this, node);
            for(const auto& v : values) {
                result.eq.insert(result.eq.end(), v.eq.begin(), v.eq.end());
                result.ineq.insert(result.ineq.end(), v.ineq.begin(), v.ineq.end());
            }
            return result;
        }

    private:
        const std::vector<Var>& variables;
    };

} // namespace helper

template <typename Var>
Var evaluate_fadbad_expr(expression<real<0>>& expr, symbol_table& symbols,
  const ScalarVariableIdManager& id_manager,
  const std::vector<Var>& variables) {
    helper::fadbad_expr_evaluator<Var> eval(symbols, id_manager, variables);
    return call_visitor(eval, expr);
}

template <typename Var>
ConstraintContainer<Var> evaluate_fadbad_constraint(expression<ale::boolean<0>>& expr, symbol_table& symbols,
  const ScalarVariableIdManager& id_manager,
  const std::vector<Var>& variables) {
    helper::fadbad_expr_evaluator<Var> eval(symbols, id_manager, variables);
    return call_visitor(eval, expr);
}

} // namespace dips
