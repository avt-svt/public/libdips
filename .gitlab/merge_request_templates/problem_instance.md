## Summary

Give a short summary of the submitted problem instance, most importantly which of the supported problem types it belongs to.

## Literature

Please give a reference to the relevant publication.

# Known solution

Is a (approximate) value for the optimal objective known?

## Relevant logs and/or screenshots

(Paste any relevant logs - use code blocks (```) to format console output.)


## Checklist

- [ ]  used the template in /library_of_test_problems/templates/ if applicable
- [ ]  put the files under /library_of_test_problems/<PROBLEM_CLASS>/<FirstAuthorName>_<Year>-<ProblemLabel>
- [ ]  filled `set_info("name") = "";` in def.txt using the format <FirstAuthorName>_<Year>-<ProblemLabel>
- [ ]  filled `set_info("objective") = "";`
- [ ]  filled `set_info("x category") = "";`
- [ ]  filled `set_info("y category") = "";`
- [ ]  add bib entry under /test_problems_literature/test_problems_literature.bib
- [ ]  ran the problem once to ensure there is no syntax error

/label ~problem_instance
/cc @libdips-group
