definitions:
binary[9, 9, 9] x;

objective:
x[1][1][1];

constraints:
forall i in {1 .. 9} : 
    forall j in {1 .. 9} : 
        sum(k in {1 .. 9} : x[i][j][k]) = 1; # each cell can only contain one number

forall i in {1 .. 9} : 
    forall k in {1 .. 9} : 
        sum(j in {1 .. 9} : x[i][j][k]) = 1; # each column can only contain each number once

forall j in {1 .. 9} : 
    forall k in {1 .. 9} : 
        sum(i in {1 .. 9} : x[i][j][k]) = 1; # each row can only contain each number once
        
forall k in {1 .. 9} : 
    forall a in {0 .. 2} : 
        forall b in {0 .. 2} : 
            sum(i in {1 .. 3} : sum(j in {1 .. 3} : x[3*a + i][3*b + j][k])) = 1; # each grid-cell can only contain each number once
