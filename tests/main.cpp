#include <array>
#include <fstream>
#include <memory>
#include <sstream>

#ifdef DIPS_enable_cplex
#include "cplex_solver.hpp"
#endif
#ifdef DIPS_enable_gams
#include "gams_solver.hpp"
#endif
#ifdef DIPS_enable_gurobi
#include "gurobi_solver.hpp"
#endif
#ifdef DIPS_enable_highs
#include "highs_solver.hpp"
#endif
#ifdef DIPS_enable_ipopt
#include "ipopt_solver.hpp"
#endif
#ifdef DIPS_enable_maingo
#include "maingo_solver.hpp"
#endif
#ifdef DIPS_enable_xpress
#include "xpress_solver.hpp"
#endif

#include "solver.hpp"
#include "program_parser.hpp"
#include "symbol.hpp"

using sudoku_matrix = std::array<std::array<int, 9>, 9>;


bool check_value(double actual, double expected,double eps = 0.001) { return std::abs(actual - expected) < eps; }

bool check_sudoku(const sudoku_matrix& sudoku) {
    std::array<int, 9> number_count{};

    // check rows
    for (int i = 0; i < 9; i++) {
        number_count.fill(0);
        for (int j = 0; j < 9; j++) { number_count.at(sudoku.at(j).at(i) - 1)++; }

        for (int j = 0; j < 9; j++) {
            if (number_count.at(j) != 1) {
                return false;
            }
        }
    }

    // check columns
    for (int i = 0; i < 9; i++) {
        number_count.fill(0);
        for (int j = 0; j < 9; j++) { number_count.at(sudoku.at(i).at(j) - 1)++; }

        for (int j = 0; j < 9; j++) {
            if (number_count.at(j) != 1) {
                return false;
            }
        }
    }

    // check grids
    for (int x = 0; x < 3; x++) {
        for (int y = 0; y < 3; y++) {
            number_count.fill(0);
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) { number_count.at(sudoku.at(3 * y + j).at(3 * x + i) - 1)++; }
            }

            for (int j = 0; j < 9; j++) {
                if (number_count.at(j) != 1) {
                    return false;
                }
            }
        }
    }

    // print solution
    for (int x = 0; x < 9; x++) {
        if (x % 3 == 0) {
            std::cout << " |-----------------------|\n";
        }
        for (int y = 0; y < 9; y++) {
            if (y % 3 == 0) {
                std::cout << " | ";
            }
            else {
                std::cout << " ";
            }
            std::cout << sudoku.at(y).at(x);
        }
        std::cout << " |" << std::endl;
    }
    std::cout << " |-----------------------|\n";
    return true;
}

// append content of file test_data/problems/(name) to stringstream
bool readFileIntoBuffer(std::stringstream* b, const std::string& name) {
    std::ifstream file("test_data/problems/" + name);
    if (!file.is_open()) {
        std::cout << "Could not open file test_data/problems/" + name << std::endl;
        return false;
    }

    std::cout << "\n===================================================================\n"
              << "opening problem: " << name << "\n"
              << "===================================================================\n";
    *b << file.rdbuf();
    file.close();

    return true;
}

// output content of buffer and parse it (using par), then solve it using solver and print the solve_record
dips::solve_record solve_program(const std::stringstream& buffer, dips::program_parser* par,
                                 std::unique_ptr<dips::solver> solver) {
    // output status
    std::cout << "[parsing test:]\n";
    std::cout << buffer.str() << std::endl;
    std::cout << std::endl;

    // parse / solve program
    std::cout << "[solving program:]\n";
    dips::program test_program = par->parse();
    dips::solve_record rec = solver->solve(test_program);

    // print solution record
    std::cout << "\n\n[final status:]\n";
    dips::report(rec);
    std::cout << std::endl;

    return rec;
}

template <typename solver_type>
bool simple_test(const std::string& output_path = "") {
    ale::symbol_table symbols;
    std::stringstream buffer;
    dips::program_parser par(buffer, symbols);
    
    std::unique_ptr<dips::solver> solver = std::make_unique<solver_type>(symbols, output_path);
    solver->set_option("non_convex", 2);

    if (!readFileIntoBuffer(&buffer, "simple_problem.txt")) {
        return false;
    }

    dips::solve_record rec = solve_program(buffer, &par, std::move(solver));

    double x = ale::cast_parameter_symbol<ale::real<0>>(rec.m_solution["x"].get())->m_value;
    double y = ale::cast_parameter_symbol<ale::real<0>>(rec.m_solution["y"].get())->m_value;
    if (!(rec.m_status == dips::GLOBAL))        throw std::runtime_error("failed simple_test: rec.m_status == dips::GLOBAL");
    if (!(check_value(rec.m_lbd, -1.41421)))    throw std::runtime_error("failed simple_test: check_value(rec.m_lbd, -1.41421)");
    if (!(check_value(rec.m_ubd, -1.41421)))    throw std::runtime_error("failed simple_test: check_value(rec.m_ubd, -1.41421)");
    if (!(check_value(x, -0.707107)))           throw std::runtime_error("failed simple_test: check_value(x, -0.707107)");
    if (!(check_value(y, -0.707107)))           throw std::runtime_error("failed simple_test: check_value(y, -0.707107)");

    return true;
}

template <typename solver_type>
bool simple_infeasible_test(const std::string& output_path = "") {
    ale::symbol_table symbols;
    std::stringstream buffer;
    dips::program_parser par(buffer, symbols);

    std::unique_ptr<dips::solver> solver = std::make_unique<solver_type>(symbols, output_path);
    solver->set_option("non_convex", 2);

    if (!readFileIntoBuffer(&buffer, "infeasible_simple_problem.txt")) {
        return false;
    }


    dips::solve_record rec = solve_program(buffer, &par, std::move(solver));

    if (!(rec.m_status == dips::INFEASIBLE))                        throw std::runtime_error("failed simple_infeasible_test: rec.m_status == dips::INFEASIBLE");
    if (!(rec.m_lbd == std::numeric_limits<double>::infinity()))    throw std::runtime_error("failed simple_infeasible_test: rec.m_lbd == std::numeric_limits<double>::infinity()");
    if (!(rec.m_ubd == std::numeric_limits<double>::infinity()))    throw std::runtime_error("failed simple_infeasible_test: rec.m_ubd == std::numeric_limits<double>::infinity()");

    return true;
}

template <typename solver_type>
bool simple_unbounded_test(const std::string& output_path = "") {
    ale::symbol_table symbols;
    std::stringstream buffer;
    dips::program_parser par(buffer, symbols);

    std::unique_ptr<dips::solver> solver = std::make_unique<solver_type>(symbols, output_path);
    solver->set_option("non_convex", 2);

    if (!readFileIntoBuffer(&buffer, "unbounded_simple_problem.txt")) {
        return false;
    }

    dips::solve_record rec = solve_program(buffer, &par, std::move(solver));

    if(!(rec.m_status == dips::UNBOUNDED))                          throw std::runtime_error("failed simple_unbounded_test: rec.m_status == dips::UNBOUNDED");
    if(!(rec.m_lbd == -std::numeric_limits<double>::infinity()))    throw std::runtime_error("failed simple_unbounded_test: rec.m_lbd == -std::numeric_limits<double>::infinity()");;
    if(!(rec.m_ubd == -std::numeric_limits<double>::infinity()))    throw std::runtime_error("failed simple_unbounded_test: rec.m_ubd == -std::numeric_limits<double>::infinity()");;

    return true;
}

template <typename solver_type>
bool qcp_test(const std::string& output_path = "") {
    ale::symbol_table symbols;
    std::stringstream buffer;
    dips::program_parser par(buffer, symbols);

    if (!readFileIntoBuffer(&buffer, "qcp_problem.txt")) {
        return false;
    }

    std::unique_ptr<dips::solver> solver = std::make_unique<solver_type>(symbols, output_path);
    solver->set_option("non_convex", 2);
    solver->set_option("problem_class", "convex_qcqp");//only for Ipopt
    dips::solve_record rec = solve_program(buffer, &par, std::move(solver));

    double x = ale::cast_parameter_symbol<ale::real<0>>(rec.m_solution["x"].get())->m_value;
    double y = ale::cast_parameter_symbol<ale::real<0>>(rec.m_solution["y"].get())->m_value;
    double z = ale::cast_parameter_symbol<ale::real<0>>(rec.m_solution["z"].get())->m_value;
    if(!(rec.m_status == dips::GLOBAL))         throw std::runtime_error("failed qcp_test: rec.m_status == dips::GLOBAL");
    if(!(check_value(rec.m_lbd, -0.326992)))    throw std::runtime_error("failed qcp_test: check_value(rec.m_lbd, -0.326992)");
    if(!(check_value(rec.m_ubd, -0.326992)))    throw std::runtime_error("failed qcp_test: check_value(rec.m_ubd, -0.326992)");
    if(!(check_value(x, 0.326992)))             throw std::runtime_error("failed qcp_test: check_value(x, 0.326992)");
    if(!(check_value(y, 0.257066)))             throw std::runtime_error("failed qcp_test: check_value(y, 0.257066)");
    if(!(check_value(z, 0.415941)))             throw std::runtime_error("failed qcp_test: check_value(z, 0.415941)");

    return true;
}

//This test can take a long time
template <typename solver_type>
bool time_limit_test(const std::string& output_path = "") {
    ale::symbol_table symbols;
    std::stringstream buffer;
    dips::program_parser par(buffer, symbols);

    if (!readFileIntoBuffer(&buffer, "large_problem_2.txt")) {
        return false;
    }

    std::unique_ptr<dips::solver> solver = std::make_unique<solver_type>(symbols, output_path);
    solver->set_option("time_limit", 10);
    solver->set_option("non_convex", 2);
    dips::solve_record rec = solve_program(buffer, &par, std::move(solver));
    std::cout<<to_string(rec.m_status)<< " " << rec.m_cpu <<" " <<rec.m_lbd << " " << rec.m_ubd<<std::endl;
    if (!(rec.m_status == dips::FEASIBLE || rec.m_status == dips::UNKNOWN)) throw std::runtime_error("failed time_limit_test: rec.m_status == dips::FEASIBLE || rec.m_status == dips::UNKNOWN");

    return true;
}

template <typename solver_type>
bool tensor_ordering_test(const std::string& output_path = "") {
    ale::symbol_table symbols;
    std::stringstream buffer;
    dips::program_parser par(buffer, symbols);

    if (!readFileIntoBuffer(&buffer, "ordering_in_tensor_problem.txt")) {
        return false;
    }

    std::unique_ptr<dips::solver> solver = std::make_unique<solver_type>(symbols, output_path);
    dips::solve_record rec = solve_program(buffer, &par, std::move(solver));
    ale::tensor<double,2> x = ale::cast_parameter_symbol<ale::real<2>>(rec.m_solution["x"].get())->m_value;

    if(!(rec.m_status == dips::GLOBAL))                 throw std::runtime_error("failed tensor_ordering_test: rec.m_status == dips::GLOBAL");
    if(!(check_value(x[(size_t)0][(size_t)0],0.0)))     throw std::runtime_error("failed tensor_ordering_test: check_value(x[(size_t)0][(size_t)0],0.0)");
    if(!(check_value(x[(size_t)0][(size_t)1], 1.0)))    throw std::runtime_error("failed tensor_ordering_test: check_value(x[(size_t)0][(size_t)1],1.0)");
    if(!(check_value(x[(size_t)0][(size_t)2], 0.2)))    throw std::runtime_error("failed tensor_ordering_test: check_value(x[(size_t)0][(size_t)2],0.2)");
    if(!(check_value(x[(size_t)1][(size_t)0], 0.3)))    throw std::runtime_error("failed tensor_ordering_test: check_value(x[(size_t)1][(size_t)0],0.3)");
    if(!(check_value(x[(size_t)1][(size_t)1], 0.4)))    throw std::runtime_error("failed tensor_ordering_test: check_value(x[(size_t)1][(size_t)1],0.4)");
    if(!(check_value(x[(size_t)1][(size_t)2], 0.5)))    throw std::runtime_error("failed tensor_ordering_test: check_value(x[(size_t)1][(size_t)2],0.5)");

    return true;
}

template <typename solver_type>
bool nlp_test(const std::string& output_path = "") {
    ale::symbol_table symbols;
    std::stringstream buffer;
    dips::program_parser par(buffer, symbols);

    if (!readFileIntoBuffer(&buffer, "nlp_problem.txt")) {
        return false;
    }

    std::unique_ptr<dips::solver> solver = std::make_unique<solver_type>(symbols, output_path);
    dips::solve_record rec = solve_program(buffer, &par, std::move(solver));
    
    ale::tensor<double,1> x = ale::cast_parameter_symbol<ale::real<1>>(rec.m_solution["x"].get())->m_value;

    if(!(rec.m_status == dips::GLOBAL|| dips::FEASIBLE)) throw std::runtime_error("failed nlp_test: rec.m_status == dips::GLOBAL|| dips::FEASIBLE");
    if(!(rec.m_ubd <= 7050.0))                           throw std::runtime_error("failed nlp_test in nlp_test: rec.m_ubd <= 7050.0");
    /*
     
     if(!(check_value(x[(size_t)0],579.31)))    throw std::runtime_error("failed nlp_test");
     if(!(check_value(x[(size_t)1],1359.97)))   throw std::runtime_error("failed nlp_test");
     if(!(check_value(x[(size_t)2],5109.97)))   throw std::runtime_error("failed nlp_test");
     if(!(check_value(x[(size_t)3],182.018)))   throw std::runtime_error("failed nlp_test");
     if(!(check_value(x[(size_t)4],295.60)))    throw std::runtime_error("failed nlp_test");
     if(!(check_value(x[(size_t)5],217.98)))    throw std::runtime_error("failed nlp_test");
     if(!(check_value(x[(size_t)6],286.42)))    throw std::runtime_error("failed nlp_test");
     if(!(check_value(x[(size_t)7],395.60)))    throw std::runtime_error("failed nlp_test");
*/

    return true;
}

// checks for error in gurobi that would set (wrongly) lbd=ubd 
template <typename solver_type>
bool gap_test(const std::string& output_path = "") {
    ale::symbol_table symbols;
    std::stringstream buffer;
    dips::program_parser par(buffer, symbols);

    if (!readFileIntoBuffer(&buffer, "miqp_problem.txt")) {
        return false;
    }

    std::unique_ptr<dips::solver> solver = std::make_unique<solver_type>(symbols, output_path);
    bool abs_tol_set = solver->set_option("abs_tol", 20);
    bool rel_tol_set = solver->set_option("rel_tol", 0.9);
    if(!(abs_tol_set)) throw std::runtime_error("failed gap_test: abs_tol_set");
    if(!(rel_tol_set)) throw std::runtime_error("failed gap_test: rel_tol_set");
    
    solver->set_option("non_convex", 2);
    dips::solve_record rec = solve_program(buffer, &par, std::move(solver));
    std::cout<<to_string(rec.m_status)<< " " << rec.m_cpu <<" " <<rec.m_lbd << " " << rec.m_ubd<<std::endl;
    
    if(!(rec.m_status == dips::GLOBAL)) throw std::runtime_error("failed gap_test: rec.m_status == dips::GLOBAL");
    //MAiNGO solves the specific problem above exactly (since one the subsolver give the exact solution)
    if(rec.m_lbd == rec.m_ubd)
    {
      ale::symbol_table symbols2;
      std::stringstream buffer2;
      dips::program_parser par2(buffer2, symbols2);
      
      if (!readFileIntoBuffer(&buffer2, "nlp_problem.txt")) {
          return false;
      }
      std::unique_ptr<dips::solver> solver2 = std::make_unique<solver_type>(symbols2, "");
      bool abs_tol_set = solver2->set_option("abs_tol", 20);
      bool rel_tol_set = solver2->set_option("rel_tol", 0.9);
      if(!(abs_tol_set)) throw std::runtime_error("failed gap_test: abs_tol_set");
      if(!(rel_tol_set)) throw std::runtime_error("failed gap_test: rel_tol_set");
      rec = solve_program(buffer2, &par2, std::move(solver2));
    }
    if(!(rec.m_lbd < rec.m_ubd)) throw std::runtime_error("failed gap_test: rec.m_lbd < rec.m_ubd");

    return true;
}

template <typename solver_type>
bool solution_limit_test(const std::string& output_path = "") {
    ale::symbol_table symbols;
    std::stringstream buffer;
    dips::program_parser par(buffer, symbols);

    if (!readFileIntoBuffer(&buffer, "many_solutions_problem.txt")) {
        return false;
    }

    std::unique_ptr<dips::solver> solver = std::make_unique<solver_type>(symbols, output_path);
    bool accepted_solution_limit = solver->set_option("solution_limit", 2);
    if(!(accepted_solution_limit)) throw std::runtime_error("failed solution_limit_test: accepted_solution_limit");
    solver->set_option("non_convex", 2);
    dips::solve_record rec = solve_program(buffer, &par, std::move(solver));

    double x = ale::cast_parameter_symbol<ale::real<0>>(rec.m_solution["x"].get())->m_value;
    double y = ale::cast_parameter_symbol<ale::real<0>>(rec.m_solution["y"].get())->m_value;
    if(!(rec.m_status == dips::FEASIBLE))   throw std::runtime_error("failed solution_limit_test: rec.m_status == dips::FEASIBLE");
    if(!(rec.m_lbd < rec.m_ubd))            throw std::runtime_error("failed solution_limit_test: rec.m_lbd < rec.m_ubd");

    return true;
}

// solves the sudoku in problems/sudoku_data.txt
template <typename solver_type>
bool sudoku_test(const std::string& output_path = "") {
    ale::symbol_table symbols;
    std::stringstream buffer;
    dips::program_parser par(buffer, symbols);

    // read the basic sudoku rules into buffer
    if (!readFileIntoBuffer(&buffer, "sudoku_template.txt")) {
        return false;
    }

    sudoku_matrix sudoku;

    // read sudoku file
    std::ifstream data_file("test_data/problems/sudoku_data.txt");
    if (!data_file.is_open()) {
        std::cout << "Could not open file test_data/problems/sudoku_data.txt" << std::endl;
        return false;
    }

    for (int i = 0; i < 9; i++) {
        for (int j = 0; j < 9; j++) {
            if (data_file.eof()) {
                throw std::invalid_argument("Sudoku input data invalid.");
            }
            char c = static_cast<char>(data_file.get());
            if (c == '.') {
                sudoku[j][i] = -1;
            } else if (isdigit(c) != 0) {
                sudoku[j][i] = c - '0';
            } else {
                throw std::invalid_argument("Sudoku input data invalid.");
            }
        }
        if (data_file.get() != '\n')
            throw std::invalid_argument("Sudoku input data invalid.");
    }

    data_file.close();

    // append constraints specific to this sudoku
    for (int i = 0; i < 9; i++) {
        for (int j = 0; j < 9; j++) {
            if (sudoku[j][i] >= 0) {
                buffer << "x[" << j + 1 << "][" << i + 1 << "][" << sudoku[j][i] << "] = 1;\n";
            }
        }
    }

    std::unique_ptr<dips::solver> solver = std::make_unique<solver_type>(symbols, output_path);
    dips::solve_record rec = solve_program(buffer, &par, std::move(solver));

    auto x = ale::cast_parameter_symbol<ale::real<3>>(rec.m_solution["x"].get())->m_value;
    if(!(rec.m_status == dips::GLOBAL)) throw std::runtime_error("failed sudoku_test: rec.m_status == dips::GLOBAL");

    std::array<size_t, 3> indexes{};

    for (int i = 0; i < 9; i++) {
        for (int j = 0; j < 9; j++) {
            if (sudoku[j][i] < 0) {
                for (int k = 0; k < 9; k++) {
                    indexes[0] = j;
                    indexes[1] = i;
                    indexes[2] = k;
                    if (x[indexes.data()] > 0) {
                        sudoku[j][i] = k + 1;
                        break;
                    }
                }
            }
        }
    }

    if(!(check_sudoku(sudoku))) throw std::runtime_error("failed sudoku_test: check_sudoku(sudoku)");

    return true;
}

template <typename solver_type>
bool non_square_tensor_test(const std::string& output_path = "") {
    ale::symbol_table symbols;
    std::stringstream buffer;
    dips::program_parser par(buffer, symbols);

    if (!readFileIntoBuffer(&buffer, "non_square_tensor_problem.txt")) {
        return false;
    }

    std::unique_ptr<dips::solver> solver = std::make_unique<solver_type>(symbols, output_path);
    solver->set_option("non_convex", 2);
    dips::solve_record rec = solve_program(buffer, &par, std::move(solver));

    auto x = ale::cast_parameter_symbol<ale::real<3>>(rec.m_solution["x"].get())->m_value;

    std::array<size_t, 3> indexes{};
    std::array<double, 3> sums{};
    for (size_t a = 0; a < 2; a++) {

        for (size_t b = 0; b < 5; b++) {
            indexes = {0, a, b};
            sums.at(0) += x[indexes.data()];
        }
    }
    for (size_t b = 0; b < 5; b++) {
        indexes = {1, 1, b};
        sums.at(1) += x[indexes.data()];
    }

    for (size_t b = 0; b < 5; b++) {
        indexes = {2, 0, b};
        sums.at(2) += x[indexes.data()];
    }
    double a = ale::cast_parameter_symbol<ale::real<0>>(rec.m_solution["a"].get())->m_value;
    double b = ale::cast_parameter_symbol<ale::real<0>>(rec.m_solution["b"].get())->m_value;
    double c = ale::cast_parameter_symbol<ale::real<0>>(rec.m_solution["c"].get())->m_value;
    if(!(rec.m_status == dips::GLOBAL)) throw std::runtime_error("failed non_square_test: rec.m_status == dips::GLOBAL");
    if(!(check_value(rec.m_lbd, 0.75))) throw std::runtime_error("failed non_square_test: check_value(rec.m_lbd, 0.75)");
    if(!(check_value(rec.m_ubd, 0.75))) throw std::runtime_error("failed non_square_test: check_value(rec.m_ubd, 0.75)");
    if(!(check_value(a, 0.5)))          throw std::runtime_error("failed non_square_test: check_value(a, 0.5)");
    if(!(check_value(b, -0.5)))         throw std::runtime_error("failed non_square_test: check_value(b, -0.5)");
    if(!(check_value(c, 0.5)))          throw std::runtime_error("failed non_square_test: check_value(c, 0.5)");
    
    if(!(check_value(sums[0], a)))      throw std::runtime_error("failed non_square_test: check_value(sums[0], a)");
    if(!(check_value(sums[1], b)))      throw std::runtime_error("failed non_square_test: check_value(sums[1], b)");
    if(!(check_value(sums[2], c)))      throw std::runtime_error("failed non_square_test: check_value(sums[2], c)");

    return true;
}

template <typename solver_type>
bool exponential_test(const std::string& output_path = "") {
    ale::symbol_table symbols;
    std::stringstream buffer;
    dips::program_parser par(buffer, symbols);

    std::unique_ptr<dips::solver> solver = std::make_unique<solver_type>(symbols, output_path);
    // solver->set_option("non_convex", 2);

    if (!readFileIntoBuffer(&buffer, "exponential_problem.txt")) {
        return false;
    }

    dips::solve_record rec = solve_program(buffer, &par, std::move(solver));

    double x = ale::cast_parameter_symbol<ale::real<0>>(rec.m_solution["x"].get())->m_value;
    if(!(rec.m_status == dips::GLOBAL)) throw std::runtime_error("failed exponential_test: rec.m_status == dips::GLOBAL");
    if(!(check_value(rec.m_lbd, 2)))    throw std::runtime_error("failed exponential_test: check_value(rec.m_lbd, 2)");
    if(!(check_value(rec.m_ubd, 2)))    throw std::runtime_error("failed exponential_test: check_value(rec.m_ubd, 2)");
    if(!(check_value(x, 0,0.01)))   throw std::runtime_error("failed exponential_test: check_value(x, 0)");

    return true;
}

int main() {
    try {
        bool success = true;

// Test GAMS
#ifdef DIPS_enable_gams
        std::cout << "====================================\n"
            << "========== TESTING GAMS  =========\n"
            << "====================================\n\n" << std::endl;

        success &= simple_test<dips::gams_solver>();
        success &= simple_infeasible_test<dips::gams_solver>();
        success &= tensor_ordering_test<dips::gams_solver>();
        success &= sudoku_test<dips::gams_solver>();              // not supported in unflattened parser due to index_multiplication
        success &= non_square_tensor_test<dips::gams_solver>();
        success &= exponential_test<dips::gams_solver>();
        success &= gap_test<dips::gams_solver>();
        success &= simple_unbounded_test<dips::gams_solver>();
        success &= qcp_test<dips::gams_solver>();
        //success &= time_limit_test<dips::gams_solver>();         // exceeds max line length using both flattened and unflattened gams parser
        //success &= solution_limit_test<dips::gams_solver>();     // setting not implemented
        //success &= nlp_test<dips::gams_solver>();                // does not converge within 12h

#endif

// Test Gurobi
#ifdef DIPS_enable_gurobi
        std::cout << "====================================\n"
                  << "========== TESTING GUROBI ==========\n" 
                  << "====================================\n\n" << std::endl;

        success &= simple_test<dips::gurobi_solver>();
        success &= simple_infeasible_test<dips::gurobi_solver>();
        success &= simple_unbounded_test<dips::gurobi_solver>();
        success &= tensor_ordering_test<dips::gurobi_solver>();
        success &= qcp_test<dips::gurobi_solver>();
        success &= sudoku_test<dips::gurobi_solver>();
        success &= time_limit_test<dips::gurobi_solver>();
        success &= gap_test<dips::gurobi_solver>();
        success &= solution_limit_test<dips::gurobi_solver>();
        success &= non_square_tensor_test<dips::gurobi_solver>();
        success &= exponential_test<dips::gurobi_solver>();
#endif

// Test XPress
#ifdef DIPS_enable_xpress
        std::cout << "====================================\n"
                  << "========== TESTING XPRESS ==========\n" 
                  << "====================================\n\n" << std::endl;

        success &= sudoku_test<dips::xpress_solver>();
        success &= time_limit_test<dips::xpress_solver>();
        success &= non_square_tensor_test<dips::xpress_solver>();
        success &= tensor_ordering_test<dips::xpress_solver>();
        success &= simple_infeasible_test<dips::xpress_solver>();
        success &= simple_unbounded_test<dips::xpress_solver>();
        // success &= simple_test<dips::xpress_solver>(); // quadratic constraint type not supported
        // success &= gap_test<dips::xpress_solver>(); // non-convex objective not supported
        //success &= qcp_test<dips::xpress_solver>(); // quadratic constraint type not supported
        // success &= solution_limit_test<dips::xpress_solver>(); // non-convex objective not supported
        // success &= exponential_test<dips::xpress_solver>(); // exponential not supported

#endif

#ifdef DIPS_enable_highs
        std::cout << "====================================\n"
                  << "========== TESTING HIGHS ===========\n" 
                  << "====================================\n\n" << std::endl;

        success &= simple_infeasible_test<dips::highs_solver>();
        success &= simple_unbounded_test<dips::highs_solver>();
        success &= tensor_ordering_test<dips::highs_solver>();
        success &= sudoku_test<dips::highs_solver>();
        success &= non_square_tensor_test<dips::highs_solver>();
        // success &= simple_test<dips::highs_solver>(); // non-linear constraints not supported
        // success &= qcp_test<dips::highs_solver>(); // non-linear constraints not supported
        // success &= time_limit_test<dips::highs_solver>(); // enable once HIGHS can solve MIQP
        // success &= gap_test<dips::highs_solver>(); // enable once HIGHS can solve MIQQ
        // success &= solution_limit_test<dips::highs_solver>(); // setting not implemented
        // success &= exponential_test<dips::highs_solver>(); // exponential not supported

#endif

// Test CPlex
#ifdef DIPS_enable_cplex
        std::cout << "====================================\n"
                  << "========== TESTING CPLEX  ==========\n" 
                  << "====================================\n\n" << std::endl;

        success &= sudoku_test<dips::cplex_solver>();
        success &= non_square_tensor_test<dips::cplex_solver>();
        success &= tensor_ordering_test<dips::cplex_solver>();
        success &= simple_infeasible_test<dips::cplex_solver>();       

        // success &= simple_unbounded_test<dips::cplex_solver>(); //CPLEX thinks infeasible_or_unbounded
        // success &= gap_test<dips::cplex_solver>(); // quadratic constraints not supported
        // success &= simple_test<dips::cplex_solver>(); // quadratic constraints not supported
         success &= qcp_test<dips::cplex_solver>(); // quadratic constraints not supported
        // success &= time_limit_test<dips::cplex_solver>(); // setting not implemented
        // success &= solution_limit_test<dips::cplex_solver>(); // setting not implemented
        // success &= exponential_test<dips::highs_solver>(); // exponential not supported

#endif

// Test maingo
#ifdef DIPS_enable_maingo
        std::cout << "====================================\n"
                  << "========== TESTING MAINGO  =========\n" 
                  << "====================================\n\n" << std::endl;

        success &= simple_test<dips::maingo_solver>();
        success &= simple_infeasible_test<dips::maingo_solver>();
        success &= tensor_ordering_test<dips::maingo_solver>();
        success &= sudoku_test<dips::maingo_solver>();
        success &= non_square_tensor_test<dips::maingo_solver>();
        success &= exponential_test<dips::maingo_solver>();
        success &= gap_test<dips::maingo_solver>(); // TODO: fails with lbd=ubd because of "bug" in MAiNGO that does not forward tolerances
        // success &= simple_unbounded_test<dips::maingo_solver>(); // unbounded variables not supported
        success &= qcp_test<dips::maingo_solver>(); 
        //success &= time_limit_test<dips::maingo_solver>(); // setting not implemented & too much memory needed
        // success &= solution_limit_test<dips::maingo_solver>(); // setting not implemented
        success &= nlp_test<dips::maingo_solver>();

#endif

// Test ipopt
#ifdef DIPS_enable_ipopt
        std::cout << "====================================\n"
                  << "========== TESTING IPOPT  =========\n" 
                  << "====================================\n\n" << std::endl;
        // success &= sudoku_test<dips::ipopt_solver>(); // ipopt can not solve mixed integer problems
        success &= non_square_tensor_test<dips::ipopt_solver>();
        success &= tensor_ordering_test<dips::ipopt_solver>();
        success &= simple_test<dips::ipopt_solver>();
        success &= exponential_test<dips::ipopt_solver>();
        success &= simple_infeasible_test<dips::ipopt_solver>();
        // success &= simple_unbounded_test<dips::ipopt_solver>(); // TODO: fails with "Invalid number in NLP function or derivative detected."
        success &= qcp_test<dips::ipopt_solver>();
        success &= nlp_test<dips::ipopt_solver>();
#endif

        if (success) {
            std::cout << "\n\nAll tests finished successfully!" << std::endl;
        }
    } catch (std::exception& e) {
        std::cerr << std::endl << e.what() << std::endl;
        return -1;
    }	

    return 0;
}
