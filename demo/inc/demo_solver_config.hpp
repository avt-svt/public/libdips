#pragma once
#ifdef DIPS_enable_cplex
#include "cplex_solver.hpp"
#endif
#ifdef DIPS_enable_gams
#include "gams_solver.hpp"
#endif
#ifdef DIPS_enable_gurobi
#include "gurobi_solver.hpp"
#endif
#ifdef DIPS_enable_highs
#include "highs_solver.hpp"
#endif
#ifdef DIPS_enable_ipopt
#include "ipopt_solver.hpp"
#endif
#ifdef DIPS_enable_maingo
#include "maingo_solver.hpp"
#ifdef DIPS_use_mpi
#include "mpi.h"

#define MUTE_WORKER 1
#endif
#endif
#ifdef DIPS_enable_xpress
#include "xpress_solver.hpp"
#endif



//Preference order here:
namespace dips {
#ifdef DIPS_demos_use_maingo
using used_subsolver = dips::maingo_solver;
#elif defined(DIPS_demos_use_gams)
using used_subsolver = dips::gams_solver;
#elif defined(DIPS_demos_use_gurobi)
using used_subsolver = dips::gurobi_solver;
#elif defined(DIPS_demos_use_cplex)
using used_subsolver = dips::cplex_solver;
#elif defined(DIPS_demos_use_ipopt)
using used_subsolver = dips::ipopt_solver;
#elif defined(DIPS_demos_use_xpress)
using used_subsolver = dips::xpress_solver;
#elif defined(DIPS_demos_use_highs)
using used_subsolver = dips::highs_solver;
#else
static_assert(false, "no solver");
#endif
} //namespace dips
