#pragma once

#include "program.hpp"
#include "discretization.hpp"
#include "symbol_table.hpp"
#include "program_parser.hpp"
#include "sip_hybrid_solver.hpp"
#include "dips_util.hpp"
#include "symbol_finder.hpp"

#include <tuple>
#include <fstream>
#include <filesystem>

using namespace ale;
using namespace dips;

sip_hybrid_solver::program_info get_sip_hybrid(symbol_table& symbols, std::filesystem::path path, std::string def_base_name = "def", std::string lbp_base_name = "lbp", std::string ubp_base_name = "ubp", std::string res_base_name = "res", std::string llp_base_name = "llp") {
    sip_hybrid_solver::program_info sip_program;
    std::ifstream input;

    // set input files
    std::filesystem::path problemFile_def = path / (def_base_name + ".txt");
    std::filesystem::path problemFile_lbp = path / (lbp_base_name + ".txt");
    std::filesystem::path problemFile_ubp = path / (ubp_base_name + ".txt");
    std::filesystem::path problemFile_res = path / (res_base_name + ".txt");
    std::vector<std::filesystem::path> problemFile_llp;
    if(std::filesystem::exists(path / (llp_base_name + ".txt"))) {
        problemFile_llp.push_back(path / (llp_base_name + ".txt"));
    }
    int llp_file_cnt = 1;
    while(std::filesystem::exists(path / (llp_base_name + std::to_string(llp_file_cnt) + ".txt"))) {
        problemFile_llp.push_back(path / (llp_base_name + std::to_string(llp_file_cnt) + ".txt"));
        llp_file_cnt++;
    }

    std::vector<std::string> forbidden_expressions;
    std::vector<std::string> forbidden_keywords;
#if defined DIPS_demos_use_gams
    forbidden_keywords.insert(forbidden_keywords.end(),
      { "ms_gams_internal", "ss_gams_internal",
        "lbd_gams_internal", "ubd_gams_internal",
        "cpu_gams_internal", "mod_gams_internal",
        "obj_gams_internal", "eqobj_gams_internal" });
#ifndef DIPS_use_gams_parser_force_flattened
    forbidden_keywords.insert(forbidden_keywords.end(),
      {
        "NaN",
        "equation",
      });
    forbidden_expressions.insert(forbidden_expressions.end(),
      { "realset_.*", ".*_index", "eqcon.*", ".*_asreal" });
#endif
#endif

    // parsing problems
    std::cout << "Parsing lbp problem" << std::endl;
    input.open(problemFile_lbp, std::ios::binary);
    if(input.is_open()) {
        program_parser prog_par(input, symbols);
        prog_par.forbid_expressions(forbidden_expressions);
        prog_par.forbid_keywords(forbidden_keywords);

        sip_program.lbp = prog_par.parse();

        if(prog_par.fail()) {
            throw std::invalid_argument("Error: Encountered an error while parsing the lbp problem file");
        }
        input.close();
    } else {
        throw std::invalid_argument("Error: Could not open lbp problem file located at " + problemFile_lbp.generic_string());
    }

    std::cout << "Parsing ubp problem" << std::endl;
    input.open(problemFile_ubp, std::ios::binary);
    if(input.is_open()) {
        program_parser prog_par(input, symbols);
        prog_par.forbid_expressions(forbidden_expressions);
        prog_par.forbid_keywords(forbidden_keywords);

        sip_program.ubp = prog_par.parse();

        if(prog_par.fail()) {
            throw std::invalid_argument("Error: Encountered an error while parsing the ubp problem file");
        }
        input.close();
    } else {
        throw std::invalid_argument("Error: Could not open ubp problem file located at " + problemFile_ubp.generic_string());
    }

    std::cout << "Parsing res problem" << std::endl;
    input.open(problemFile_res, std::ios::binary);
    if(input.is_open()) {
        program_parser prog_par(input, symbols);
        prog_par.forbid_expressions(forbidden_expressions);
        prog_par.forbid_keywords(forbidden_keywords);

        sip_program.res = prog_par.parse();

        if(prog_par.fail()) {
            throw std::invalid_argument("Error: Encountered an error while parsing the res problem file");
        }
        input.close();
    } else {
        throw std::invalid_argument("Error: Could not open res problem file located at " + problemFile_res.generic_string());
    }

    std::vector<program> llp;
    if(problemFile_llp.size() == 0) {
        throw std::invalid_argument("Error: No llp file(s) found at " + (path / (llp_base_name + "*.txt")).generic_string());
    }
    for(int i = 0; i < (problemFile_llp.size()); i++) {
        program_parser prog_par(input, symbols);
        prog_par.forbid_expressions(forbidden_expressions);
        prog_par.forbid_keywords(forbidden_keywords);

        std::cout << "Parsing llp_" << i + 1 << " problem" << std::endl;
        input.open(problemFile_llp[i], std::ios::binary);
        if(input.is_open()) {
            llp.push_back(prog_par.parse());
            if(prog_par.fail()) {
                throw std::invalid_argument("Error: Encountered an error while parsing the llp problem file");
            }
            input.close();
        } else {
            throw std::invalid_argument("Error: Could not open llp problem file located at " + problemFile_llp[i].generic_string());
        }
    }

    // check consistency of variable bounds
    dips::util::check_variable_bounds(symbols, { { sip_program.lbp, sip_program.ubp, sip_program.res }, llp });

    // parsing problem definitions
    std::cout << "Parsing problem definitions" << std::endl;
    input.open(problemFile_def, std::ios::binary);
    if(input.is_open()) {
        sip_hybrid_program_definition_parser prog_def_par(input, symbols);
        prog_def_par.parse();
        if(prog_def_par.fail()) {
            throw std::invalid_argument("Error: Encountered an error while parsing the problem definitions file");
        } else {
            std::cout << "Setting eps_res and res_target\n";
            sip_program.ubp_restrict = prog_def_par.get_internal("ubp", "eps_res");
            sip_program.res_target = prog_def_par.get_internal("res", "res_target");

            std::cout << "Adding llp and discretization\n";
            if(prog_def_par.get_discretizations("lbp").size() != problemFile_llp.size() || prog_def_par.get_discretizations("ubp").size() != problemFile_llp.size() || prog_def_par.get_discretizations("res").size() != problemFile_llp.size()) {
                throw std::invalid_argument("Error: Number of llp files does not equal discretizations set in def.txt.");
            }
            for(int i = 0; i < prog_def_par.get_discretizations("lbp").size(); i++) {
                sip_program.add_llp(llp[i], prog_def_par.get_discretizations("lbp")[i], prog_def_par.get_discretizations("ubp")[i], prog_def_par.get_discretizations("res")[i]);
            }

            std::cout << "Adding info\n";
            sip_program.lbp_info = prog_def_par.get_info("lbp");
            sip_program.lbp_info_list = prog_def_par.get_info_list("lbp");
            sip_program.ubp_info = prog_def_par.get_info("ubp");
            sip_program.ubp_info_list = prog_def_par.get_info_list("ubp");
            sip_program.res_info = prog_def_par.get_info("res");
            sip_program.res_info_list = prog_def_par.get_info_list("res");
        }
        input.close();
    } else {
        throw std::invalid_argument("Error: Could not open definitions file located at " + problemFile_def.generic_string());
    }

    return sip_program;
}
