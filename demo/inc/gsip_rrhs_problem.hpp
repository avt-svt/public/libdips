#pragma once

#include "program.hpp"
#include "discretization.hpp"
#include "symbol_table.hpp"
#include "program_parser.hpp"
#include "gsip_rrhs_solver.hpp"
#include "dips_util.hpp"
#include "symbol_finder.hpp"

#include <tuple>
#include <fstream>
#include <filesystem>

using namespace ale;
using namespace dips;

gsip_rrhs_solver::program_info get_gsip_rrhs(symbol_table& symbols, std::filesystem::path path, std::string def_base_name = "def", std::string lbp_base_name = "lbp", std::string ubp_base_name = "ubp", std::string llp_base_name = "llp", std::string aux_base_name = "llp_aux") {
    gsip_rrhs_solver::program_info gsip_program;
    std::ifstream input;

    // set input files
    std::filesystem::path problemFile_def = path / (def_base_name + ".txt");
    std::filesystem::path problemFile_lbp = path / (lbp_base_name + ".txt");
    std::filesystem::path problemFile_ubp = path / (ubp_base_name + ".txt");
    std::vector<std::filesystem::path> problemFile_llp;
    if(std::filesystem::exists(path / (llp_base_name + ".txt"))) {
        problemFile_llp.push_back(path / (llp_base_name + ".txt"));
    }
    int llp_file_cnt = 1;
    while(std::filesystem::exists(path / (llp_base_name + std::to_string(llp_file_cnt) + ".txt"))) {
        problemFile_llp.push_back(path / (llp_base_name + std::to_string(llp_file_cnt) + ".txt"));
        llp_file_cnt++;
    }
    std::vector<std::filesystem::path> problemFile_aux;
    if(std::filesystem::exists(path / (aux_base_name + ".txt"))) {
        problemFile_aux.push_back(path / (aux_base_name + ".txt"));
    }
    int aux_file_cnt = 1;
    while(std::filesystem::exists(path / (aux_base_name + std::to_string(aux_file_cnt) + ".txt"))) {
        problemFile_aux.push_back(path / (aux_base_name + std::to_string(aux_file_cnt) + ".txt"));
        aux_file_cnt++;
    }

    if(llp_file_cnt != aux_file_cnt) {
        throw std::invalid_argument("Number of llps and aux are not the same");
    }
    if(llp_file_cnt > 1 || aux_file_cnt > 1) {
        throw std::invalid_argument("Currently only one llp and one aux is supported");
        ;
    }

    std::vector<std::string> forbidden_expressions;
    std::vector<std::string> forbidden_keywords;
#if defined DIPS_demos_use_gams
    forbidden_keywords.insert(forbidden_keywords.end(),
      { "ms_gams_internal", "ss_gams_internal",
        "lbd_gams_internal", "ubd_gams_internal",
        "cpu_gams_internal", "mod_gams_internal",
        "obj_gams_internal", "eqobj_gams_internal" });
#ifndef DIPS_use_gams_parser_force_flattened
    forbidden_keywords.insert(forbidden_keywords.end(),
      {
        "NaN",
        "equation",
      });
    forbidden_expressions.insert(forbidden_expressions.end(),
      { "realset_.*", ".*_index", "eqcon.*", ".*_asreal" });
#endif
#endif

    // parsing problems
    std::cout << "Parsing lbp problem" << std::endl;
    input.open(problemFile_lbp, std::ios::binary);
    if(input.is_open()) {
        program_parser prog_par(input, symbols);
        prog_par.forbid_expressions(forbidden_expressions);
        prog_par.forbid_keywords(forbidden_keywords);

        gsip_program.lbp = prog_par.parse();

        if(prog_par.fail()) {
            throw std::invalid_argument("Error: Encountered an error while parsing the lbp problem file");
        }
        input.close();
    } else {
        throw std::invalid_argument("Error: Could not open lbp problem file located at " + problemFile_lbp.generic_string());
    }

    std::cout << "Parsing ubp problem" << std::endl;
    input.open(problemFile_ubp, std::ios::binary);
    if(input.is_open()) {
        program_parser prog_par(input, symbols);
        prog_par.forbid_expressions(forbidden_expressions);
        prog_par.forbid_keywords(forbidden_keywords);

        gsip_program.ubp = prog_par.parse();

        if(prog_par.fail()) {
            throw std::invalid_argument("Error: Encountered an error while parsing the ubp problem file");
        }
        input.close();
    } else {
        throw std::invalid_argument("Error: Could not open ubp problem file located at " + problemFile_ubp.generic_string());
    }

    std::vector<program> llp;
    if(problemFile_llp.size() == 0) {
        throw std::invalid_argument("Error: No llp file(s) found at " + (path / (llp_base_name + "*.txt")).generic_string());
    }
    for(int i = 0; i < (problemFile_llp.size()); i++) {
        program_parser prog_par(input, symbols);
        prog_par.forbid_expressions(forbidden_expressions);
        prog_par.forbid_keywords(forbidden_keywords);

        std::cout << "Parsing llp_" << i + 1 << " problem" << std::endl;
        input.open(problemFile_llp[i], std::ios::binary);
        if(input.is_open()) {
            llp.push_back(prog_par.parse());
            if(prog_par.fail()) {
                throw std::invalid_argument("Error: Encountered an error while parsing the llp problem file");
            }
            input.close();
        } else {
            throw std::invalid_argument("Error: Could not open llp problem file located at " + problemFile_llp[i].generic_string());
        }
    }

    std::vector<program> aux;
    if(problemFile_aux.size() == 0) {
        throw std::invalid_argument("Error: No aux file(s) found at " + (path / (aux_base_name + "*.txt")).generic_string());
    }
    for(int i = 0; i < (problemFile_aux.size()); i++) {
        program_parser prog_par(input, symbols);
        prog_par.forbid_expressions(forbidden_expressions);
        prog_par.forbid_keywords(forbidden_keywords);

        std::cout << "Parsing aux_" << i + 1 << " problem" << std::endl;
        input.open(problemFile_aux[i], std::ios::binary);
        if(input.is_open()) {
            aux.push_back(prog_par.parse());
            if(prog_par.fail()) {
                throw std::invalid_argument("Error: Encountered an error while parsing the aux problem file");
            }
            input.close();
        } else {
            throw std::invalid_argument("Error: Could not open aux problem file located at " + problemFile_aux[i].generic_string());
        }
    }

    // check consistency of variable bounds
    dips::util::check_variable_bounds(symbols, { { gsip_program.ubp, gsip_program.lbp }, llp, aux });

    // parsing problem definitions
    std::cout << "Parsing problem definitions" << std::endl;
    input.open(problemFile_def, std::ios::binary);
    if(input.is_open()) {
        gsip_rrhs_program_definition_parser prog_def_par(input, symbols);
        prog_def_par.parse();
        if(prog_def_par.fail()) {
            throw std::invalid_argument("Error: Encountered an error while parsing the problem definitions file");
        } else {
            std::cout << "Setting eps_res, alpha, and g_u\n";
            gsip_program.ubp_restrict_u = prog_def_par.get_internal("ubp", "eps_res");
            gsip_program.ubp_restrict_l = prog_def_par.get_internal("ubp", "eps_res");
            gsip_program.aux_alpha = prog_def_par.get_internal("aux", "alpha");
            gsip_program.aux_g_u = prog_def_par.get_internal("aux", "g_u");

            std::cout << "Adding llp and discretization\n";
            if(prog_def_par.get_discretizations("lbp").size() != problemFile_llp.size() || prog_def_par.get_discretizations("ubp").size() != problemFile_llp.size()) {
                throw std::invalid_argument("Error: Number of llp files does not equal discretizations set in def.txt.");
            }
            for(int i = 0; i < prog_def_par.get_discretizations("lbp").size(); i++) {
                gsip_program.add_llp(llp[i], aux[i], prog_def_par.get_discretizations("lbp")[i], prog_def_par.get_discretizations("ubp")[i]);
            }

            std::cout << "Adding info\n";
            gsip_program.lbp_info = prog_def_par.get_info("lbp");
            gsip_program.lbp_info_list = prog_def_par.get_info_list("lbp");
            gsip_program.ubp_info = prog_def_par.get_info("ubp");
            gsip_program.ubp_info_list = prog_def_par.get_info_list("ubp");
            gsip_program.aux_info = prog_def_par.get_info("aux");
            gsip_program.aux_info_list = prog_def_par.get_info_list("aux");
        }
        input.close();
    } else {
        throw std::invalid_argument("Error: Could not open definitions file located at " + problemFile_def.generic_string());
    }

    return gsip_program;
}
