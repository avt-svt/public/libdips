#pragma once

#include "program.hpp"
#include "discretization.hpp"
#include "symbol_table.hpp"
#include "program_parser.hpp"
#include "esip_hybrid_solver.hpp"
#include "dips_util.hpp"
#include "symbol_finder.hpp"

#include <tuple>
#include <fstream>

using namespace ale;
using namespace dips;

esip_hybrid_solver::program_info get_esip_hybrid(symbol_table& symbols, std::filesystem::path path, std::string def_base_name = "def", std::string lbp_base_name = "lbp", std::string ubp_base_name = "ubp", std::string res_base_name = "res", std::string mlp_base_name = "mlp", std::string llp_base_name = "llp") {
    esip_hybrid_solver::program_info esip_program;
    std::ifstream input;

    // set input files
    std::filesystem::path problemFile_def = path / (def_base_name + ".txt");
    std::filesystem::path problemFile_ubp = path / (ubp_base_name + ".txt");
    std::filesystem::path problemFile_lbp = path / (lbp_base_name + ".txt");
    std::filesystem::path problemFile_res = path / (res_base_name + ".txt");
    std::filesystem::path problemFile_mlp = path / (mlp_base_name + ".txt");
    std::filesystem::path problemFile_llp = path / (llp_base_name + ".txt");

    std::vector<std::string> forbidden_expressions;
    std::vector<std::string> forbidden_keywords;
#if defined DIPS_demos_use_gams
    forbidden_keywords.insert(forbidden_keywords.end(),
      { "ms_gams_internal", "ss_gams_internal",
        "lbd_gams_internal", "ubd_gams_internal",
        "cpu_gams_internal", "mod_gams_internal",
        "obj_gams_internal", "eqobj_gams_internal" });
#ifndef DIPS_use_gams_parser_force_flattened
    forbidden_keywords.insert(forbidden_keywords.end(),
      {
        "NaN",
        "equation",
      });
    forbidden_expressions.insert(forbidden_expressions.end(),
      { "realset_.*", ".*_index", "eqcon.*", ".*_asreal" });
#endif
#endif

    // parsing problems
    std::vector<std::tuple<std::filesystem::path, std::filesystem::path, program&>> v = { { "lbp", problemFile_lbp, esip_program.lbp }, { "ubp", problemFile_ubp, esip_program.ubp }, { "res", problemFile_res, esip_program.res } };

    for(std::tuple<std::filesystem::path, std::filesystem::path, program&>& trip : v) {
        std::cout << "Parsing " + std::get<0>(trip).generic_string() << " problem" << std::endl;
        input.open(std::get<1>(trip), std::ios::binary);
        if(input.is_open()) {
            program_parser prog_par(input, symbols);
            prog_par.forbid_expressions(forbidden_expressions);
            prog_par.forbid_keywords(forbidden_keywords);

            std::get<2>(trip) = prog_par.parse();

            if(prog_par.fail()) {
                throw std::invalid_argument("Error: Encountered an error while parsing the" + std::get<0>(trip).generic_string() + " ulp problem file");
            }
            input.close();
        } else {
            throw std::invalid_argument("Error: Could not open ulp problem file located at " + std::get<1>(trip).generic_string());
        }
    }
    program mlp, llp;
    std::cout << "Parsing mlp problem" << std::endl;
    input.open(problemFile_mlp, std::ios::binary);
    if(input.is_open()) {
        program_parser prog_par(input, symbols);
        prog_par.forbid_expressions(forbidden_expressions);
        prog_par.forbid_keywords(forbidden_keywords);

        mlp = prog_par.parse();
        if(prog_par.fail()) {
            throw std::invalid_argument("Error: Encountered an error while parsing the mlp problem file");
        }
        input.close();
    } else {
        throw std::invalid_argument("Error: Could not open mlp problem file located at " + problemFile_mlp.generic_string());
    }

    std::cout << "Parsing llp problem" << std::endl;
    input.open(problemFile_llp, std::ios::binary);
    if(input.is_open()) {
        program_parser prog_par(input, symbols);
        prog_par.forbid_expressions(forbidden_expressions);
        prog_par.forbid_keywords(forbidden_keywords);

        llp = prog_par.parse();
        if(prog_par.fail()) {
            throw std::invalid_argument("Error: Encountered an error while parsing the llp problem file");
        }
        input.close();
    } else {
        throw std::invalid_argument("Error: Could not open llp problem file located at " + problemFile_llp.generic_string());
    }

    // check consistency of variable bounds
    dips::util::check_variable_bounds(symbols, { { esip_program.lbp, esip_program.ubp, esip_program.res, mlp, llp } });


    // parsing problem definitions
    std::cout << "Parsing problem definitions" << std::endl;
    input.open(problemFile_def, std::ios::binary);
    if(input.is_open()) {
        esip_hybrid_program_definition_parser prog_def_par(input, symbols);
        prog_def_par.parse();
        if(prog_def_par.fail()) {
            throw std::invalid_argument("Error: Encountered an error while parsing the problem definitions file");
        } else {
            std::cout << "Adding ubp discretization\n";
            assert(prog_def_par.get_ubp_discretizations().size() == 1);
            discretization ubp_disc = prog_def_par.get_ubp_discretizations().front();

            std::cout << "Adding lbp discretization\n";
            assert(prog_def_par.get_lbp_discretizations().size() == 1);
            discretization lbp_disc = prog_def_par.get_lbp_discretizations().front();

            std::cout << "Adding res discretization\n";
            assert(prog_def_par.get_res_discretizations().size() == 1);
            discretization res_disc = prog_def_par.get_res_discretizations().front();

            std::cout << "Adding mlp discretization\n";
            assert(prog_def_par.get_mlp_discretizations().size() == 1);
            discretization mlp_disc = prog_def_par.get_mlp_discretizations().front();

            std::cout << "Setting eps_res and res_target\n";
            esip_program.ubp_restrict = prog_def_par.get_internal("ubp", "eps_res");
            esip_program.res_target = prog_def_par.get_internal("res", "res_target");

            std::cout << "Adding mlp and llp\n";
            esip_program.add_mlp_and_llp(mlp, llp, lbp_disc, ubp_disc, res_disc, mlp_disc);
            std::cout << "Adding info\n";
            esip_program.lbp_info = prog_def_par.get_info("lbp");
            esip_program.lbp_info_list = prog_def_par.get_info_list("lbp");
            esip_program.ubp_info = prog_def_par.get_info("ubp");
            esip_program.ubp_info_list = prog_def_par.get_info_list("ubp");
            esip_program.res_info = prog_def_par.get_info("res");
            esip_program.res_info_list = prog_def_par.get_info_list("res");
            esip_program.mlp_info = prog_def_par.get_info("mlp");
            esip_program.mlp_info_list = prog_def_par.get_info_list("mlp");
        }
        input.close();
    } else {
        throw std::invalid_argument("Error: Could not open definitions file located at " + problemFile_def.generic_string());
    }

    return esip_program;
}
