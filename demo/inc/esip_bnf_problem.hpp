#pragma once

#include "program.hpp"
#include "discretization.hpp"
#include "symbol_table.hpp"
#include "program_parser.hpp"
#include "esip_bnf_solver.hpp"
#include "dips_util.hpp"
#include "symbol_finder.hpp"

#include <tuple>
#include <fstream>

using namespace ale;
using namespace dips;

esip_bnf_solver::program_info get_esip_bnf(symbol_table& symbols, std::filesystem::path path, std::string def_base_name = "def", std::string lbp_base_name = "lbp", std::string mlp_base_name = "mlp", std::string llp_base_name = "llp") {
    esip_bnf_solver::program_info esip_program;
    std::ifstream input;

    // set input files
    std::filesystem::path problemFile_def = path / (def_base_name + ".txt");
    std::filesystem::path problemFile_ulp = path / (lbp_base_name + ".txt");
    std::filesystem::path problemFile_mlp = path / (mlp_base_name + ".txt");
    std::filesystem::path problemFile_llp = path / (llp_base_name + ".txt");

    std::vector<std::string> forbidden_expressions;
    std::vector<std::string> forbidden_keywords;
#if defined DIPS_demos_use_gams
    forbidden_keywords.insert(forbidden_keywords.end(),
      { "ms_gams_internal", "ss_gams_internal",
        "lbd_gams_internal", "ubd_gams_internal",
        "cpu_gams_internal", "mod_gams_internal",
        "obj_gams_internal", "eqobj_gams_internal" });
#ifndef DIPS_use_gams_parser_force_flattened
    forbidden_keywords.insert(forbidden_keywords.end(),
      {
        "NaN",
        "equation",
      });
    forbidden_expressions.insert(forbidden_expressions.end(),
      { "realset_.*", ".*_index", "eqcon.*", ".*_asreal" });
#endif
#endif

    // parsing problems
    std::cout << "Parsing ulp problem" << std::endl;
    input.open(problemFile_ulp, std::ios::binary);
    if(input.is_open()) {
        program_parser prog_par(input, symbols);
        prog_par.forbid_expressions(forbidden_expressions);
        prog_par.forbid_keywords(forbidden_keywords);

        esip_program.ulp = prog_par.parse();

        if(prog_par.fail()) {
            throw std::invalid_argument("Error: Encountered an error while parsing the ulp problem file");
        }
        input.close();
    } else {
        throw std::invalid_argument("Error: Could not open ulp problem file located at " + problemFile_ulp.generic_string());
    }
    program mlp, llp;
    std::cout << "Parsing mlp problem" << std::endl;
    input.open(problemFile_mlp, std::ios::binary);
    if(input.is_open()) {
        program_parser prog_par(input, symbols);
        prog_par.forbid_expressions(forbidden_expressions);
        prog_par.forbid_keywords(forbidden_keywords);

        mlp = prog_par.parse();
        if(prog_par.fail()) {
            throw std::invalid_argument("Error: Encountered an error while parsing the mlp problem file");
        }
        input.close();
    } else {
        throw std::invalid_argument("Error: Could not open mlp problem file located at " + problemFile_mlp.generic_string());
    }

    std::cout << "Parsing llp problem" << std::endl;
    input.open(problemFile_llp, std::ios::binary);
    if(input.is_open()) {
        program_parser prog_par(input, symbols);
        prog_par.forbid_expressions(forbidden_expressions);
        prog_par.forbid_keywords(forbidden_keywords);

        llp = prog_par.parse();
        if(prog_par.fail()) {
            throw std::invalid_argument("Error: Encountered an error while parsing the llp problem file");
        }
        input.close();
    } else {
        throw std::invalid_argument("Error: Could not open llp problem file located at " + problemFile_llp.generic_string());
    }

    // check consistency of variable bounds
    dips::util::check_variable_bounds(symbols, { { esip_program.ulp, mlp, llp } });

    // parsing problem definitions
    std::cout << "Parsing problem definitions" << std::endl;
    input.open(problemFile_def, std::ios::binary);
    if(input.is_open()) {
        esip_bnf_program_definition_parser prog_def_par(input, symbols);
        prog_def_par.parse();
        if(prog_def_par.fail()) {
            throw std::invalid_argument("Error: Encountered an error while parsing the problem definitions file");
        } else {

            // TODO: is this the correct way to do this?
            std::cout << "Adding ulp discretization\n";
            assert(prog_def_par.get_ulp_discretizations().size() == 1);
            discretization ulp_disc = prog_def_par.get_ulp_discretizations().front();

            std::cout << "Adding mlp discretization\n";
            assert(prog_def_par.get_mlp_discretizations().size() == 1);
            discretization mlp_disc = prog_def_par.get_mlp_discretizations().front();
            esip_program.add_mlp_and_llp(mlp, llp, ulp_disc, mlp_disc);
            //std::cout << "Adding llp and discretization\n";
            //for (int i = 0; i < prog_def_par.get_lbp_discretizations().size(); i++) {
            //    sip_program.add_llp(llp[i], prog_def_par.get_lbp_discretizations()[i], prog_def_par.get_ubp_discretizations()[i]);
            //}

            std::cout << "Adding info\n";
            esip_program.lbp_info = prog_def_par.get_info("lbp");
            esip_program.lbp_info_list = prog_def_par.get_info_list("lbp");
            esip_program.mlp_info = prog_def_par.get_info("mlp");
            esip_program.mlp_info_list = prog_def_par.get_info_list("mlp");
        }
        input.close();
    } else {
        throw std::invalid_argument("Error: Could not open definitions file located at " + problemFile_def.generic_string());
    }

    return esip_program;
}
