#pragma once

#include "program.hpp"
#include "discretization.hpp"
#include "symbol_table.hpp"
#include "program_parser.hpp"
#include "blp_no_box_solver.hpp"
#include "dips_util.hpp"
#include "symbol_finder.hpp"

#include <tuple>
#include <fstream>
#include <filesystem>

using namespace ale;
using namespace dips;

blp_no_box_solver::program_info get_blp_no_box(symbol_table& symbols, std::filesystem::path path, std::filesystem::path subpath = "no_box", std::string def_base_name = "def", std::string skl_base_name = "skl", std::string lbp_base_name = "lbp", std::string ubp_base_name = "ubp", std::string llp_base_name = "llp", std::string llp_aux_base_name = "llp_aux") {
    blp_no_box_solver::program_info blp_program;
    std::ifstream input;

    // set input files
    std::filesystem::path problemFile_def = path / (def_base_name + ".txt");
    std::filesystem::path problemFile_skl = path / (skl_base_name + ".txt");
    std::filesystem::path problemFile_lbp = path / subpath / (lbp_base_name + ".txt");
    std::filesystem::path problemFile_ubp = path / subpath / (ubp_base_name + ".txt");
    std::vector<std::filesystem::path> problemFile_llp;
    std::vector<std::filesystem::path> problemFile_aux;
    if(std::filesystem::exists(path / subpath / (llp_base_name + ".txt"))) {
        problemFile_llp.push_back(path / subpath / (llp_base_name + ".txt"));
    }
    int llp_file_cnt = 1;
    while(std::filesystem::exists(path / subpath / (llp_base_name + std::to_string(llp_file_cnt) + ".txt"))) {
        problemFile_llp.push_back(path / subpath / (llp_base_name + std::to_string(llp_file_cnt) + ".txt"));
        llp_file_cnt++;
    }

    if(problemFile_llp.size() != 1) {
        throw std::invalid_argument("Error: number of llp file(s) found at " + (path / subpath / (llp_base_name + "*.txt")).generic_string() + " is " + std::to_string(problemFile_llp.size()) + " != 1");
    }
    if(std::filesystem::exists(path / subpath / (llp_aux_base_name + ".txt"))) {
        problemFile_aux.push_back(path / subpath / (llp_aux_base_name + ".txt"));
    }
    int aux_file_cnt = 1;
    while(std::filesystem::exists(path / subpath / (llp_aux_base_name + std::to_string(aux_file_cnt) + ".txt"))) {
        problemFile_aux.push_back(path / subpath / (llp_aux_base_name + std::to_string(aux_file_cnt) + ".txt"));
        aux_file_cnt++;
    }

    if(problemFile_aux.size() != 1) {
        throw std::invalid_argument("Error: number of aux file(s) found at " + (path / subpath / (llp_base_name + "*.txt")).generic_string() + " is " + std::to_string(problemFile_llp.size()) + " != 1");
    }


    std::vector<std::string> forbidden_expressions;
    std::vector<std::string> forbidden_keywords;
#if defined DIPS_enable_gams
    forbidden_keywords.insert(forbidden_keywords.end(),
      { "ms_gams_internal", "ss_gams_internal",
        "lbd_gams_internal", "ubd_gams_internal",
        "cpu_gams_internal", "mod_gams_internal",
        "obj_gams_internal", "eqobj_gams_internal" });
#ifndef DIPS_use_gams_parser_force_flattened
    forbidden_keywords.insert(forbidden_keywords.end(),
      {
        "NaN",
        "equation",
      });
    forbidden_expressions.insert(forbidden_expressions.end(),
      { "realset_.*", ".*_index", "eqcon.*", ".*_asreal" });
#endif
#endif

    // parsing problems
    std::cout << "Parsing skeleton (skl) problem" << std::endl;
    input.open(problemFile_skl, std::ios::binary);
    program skl;
    if(input.is_open()) {
        program_parser prog_par(input, symbols);
        prog_par.forbid_expressions(forbidden_expressions);
        prog_par.forbid_keywords(forbidden_keywords);

        skl = prog_par.parse();

        if(prog_par.fail()) {
            throw std::invalid_argument("Error: Encountered an error while parsing the skl problem file");
        }
        input.close();
        if(!skl.m_objective.empty() || !skl.m_constraints.empty() || !skl.m_relaxations.empty() || !skl.m_squashes.empty() || !skl.m_outputs.empty()) {
            throw std::invalid_argument("Error: Skeleton problem file must only contain definitions. Parsed "
                                        + std::to_string(skl.m_objective.size()) + " objective(s), "
                                        + std::to_string(skl.m_constraints.size()) + " constraint(s), "
                                        + std::to_string(skl.m_relaxations.size()) + " relaxation(s), "
                                        + std::to_string(skl.m_squashes.size()) + " squashe(s), and "
                                        + std::to_string(skl.m_outputs.size()) + " output(s)");
        }
    } else {
        throw std::invalid_argument("Error: Could not open skl problem file located at " + problemFile_skl.generic_string());
    }

    std::vector<std::tuple<std::filesystem::path, std::filesystem::path, program&>> v = { { "lbp", problemFile_lbp, blp_program.lbp }, { "ubp", problemFile_ubp, blp_program.ubp }, { "aux", problemFile_aux[0], blp_program.aux }, { "llp", problemFile_llp[0], blp_program.llp } };

    for(std::tuple<std::filesystem::path, std::filesystem::path, program&>& trip : v) {
        std::cout << "Parsing " + std::get<0>(trip).generic_string() << " problem" << std::endl;
        input.open(std::get<1>(trip), std::ios::binary);
        if(input.is_open()) {
            program_parser prog_par(input, symbols);
            prog_par.forbid_expressions(forbidden_expressions);
            prog_par.forbid_keywords(forbidden_keywords);

            std::get<2>(trip) = prog_par.parse();

            if(prog_par.fail()) {
                throw std::invalid_argument("Error: Encountered an error while parsing the" + std::get<0>(trip).generic_string() + " ulp problem file");
            }
            input.close();
        } else {
            throw std::invalid_argument("Error: Could not open ulp problem file located at " + std::get<1>(trip).generic_string());
        }
    }

    // check consistency of variable bounds
    dips::util::check_variable_bounds(symbols, { { blp_program.lbp, blp_program.ubp, blp_program.aux, blp_program.llp } });

    // parsing problem definitions
    std::cout << "Parsing problem definitions" << std::endl;
    input.open(problemFile_def, std::ios::binary);
    if(input.is_open()) {
        blp_no_box_program_definition_parser prog_def_par(input, symbols);
        prog_def_par.parse();
        if(prog_def_par.fail()) {
            throw std::invalid_argument("Error: Encountered an error while parsing the problem definitions file");
        } else {
            std::cout << "Setting aux_target, ubp_target, ulp_var, eps_aux, eps_ubp, and current_lbd\n";
            blp_program.aux_target = prog_def_par.get_internal("aux", "aux_target");
            blp_program.ubp_target = prog_def_par.get_internal("ubp", "ubp_target");
            blp_program.ul_variables = prog_def_par.get_internals_list("lbp", "u_cont_var");
            blp_program.aux_eps = prog_def_par.get_internal("aux", "eps_aux");
            blp_program.l_obj_val = prog_def_par.get_internal("aux", "l_obj_val");
            blp_program.ubp_eps = prog_def_par.get_internal("ubp", "eps_ubp");
            blp_program.current_lbd = prog_def_par.get_internal("ubp", "current_lbd");

            std::cout << "Adding llp and discretization\n";
            if(prog_def_par.get_discretizations("lbp").size() == 1) {
                blp_program.disc = prog_def_par.get_discretizations("lbp").at(0);
            } else {
                throw std::invalid_argument("Error: Need exactly one discretization for blp_no_box_solver.");
            }
            std::cout << "Adding info\n";
            blp_program.lbp_info = prog_def_par.get_info("lbp");
            blp_program.lbp_info_list = prog_def_par.get_info_list("lbp");
            blp_program.ubp_info = prog_def_par.get_info("ubp");
            blp_program.ubp_info_list = prog_def_par.get_info_list("ubp");
            blp_program.aux_info = prog_def_par.get_info("aux");
            blp_program.aux_info_list = prog_def_par.get_info_list("aux");
        }
        input.close();
    } else {
        throw std::invalid_argument("Error: Could not open definitions file located at " + problemFile_def.generic_string());
    }

    return blp_program;
}
