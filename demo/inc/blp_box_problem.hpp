#pragma once

#include "program.hpp"
#include "discretization.hpp"
#include "symbol_table.hpp"
#include "program_parser.hpp"
#include "blp_box_solver.hpp"
#include "dips_util.hpp"
#include "symbol_finder.hpp"


#include <tuple>
#include <fstream>
#include <filesystem>

using namespace ale;
using namespace dips;

blp_box_solver::program_info get_blp_box(symbol_table& symbols, std::filesystem::path path, std::filesystem::path subpath = "box", std::string def_base_name = "def", std::string skl_base_name = "skl", std::string lbp_base_name = "lbp", std::string ubp_base_name = "ubp", std::string llp_base_name = "llp", std::string aux_base_name = "llp_aux", std::string aux_v_base_name = "llp_aux_v", std::string h_max_base_name = "h_max") {
    blp_box_solver::program_info blp_program;
    std::ifstream input;

    // set input files
    std::filesystem::path problemFile_def = path / (def_base_name + ".txt");
    std::filesystem::path problemFile_skl = path / (skl_base_name + ".txt");
    std::filesystem::path problemFile_lbp = path / subpath / (lbp_base_name + ".txt");
    std::filesystem::path problemFile_ubp = path / subpath / (ubp_base_name + ".txt");

    std::vector<std::filesystem::path> problemFile_aux;
    if(std::filesystem::exists(path / subpath / (aux_base_name + ".txt"))) {
        problemFile_aux.push_back(path / subpath / (aux_base_name + ".txt"));
    }
    int aux_file_cnt = 1;
    while(std::filesystem::exists(path / (aux_base_name + std::to_string(aux_file_cnt) + ".txt"))) {
        problemFile_aux.push_back(path / (aux_base_name + std::to_string(aux_file_cnt) + ".txt"));
        aux_file_cnt++;
    }

    std::vector<std::filesystem::path> problemFile_llp;
    if(std::filesystem::exists(path / subpath / (llp_base_name + ".txt"))) {
        problemFile_llp.push_back(path / subpath / (llp_base_name + ".txt"));
    }
    int llp_file_cnt = 1;
    while(std::filesystem::exists(path / subpath / (llp_base_name + std::to_string(llp_file_cnt) + ".txt"))) {
        problemFile_llp.push_back(path / subpath / (llp_base_name + std::to_string(llp_file_cnt) + ".txt"));
        llp_file_cnt++;
    }

    std::vector<std::filesystem::path> problemFile_aux_v;
    if(std::filesystem::exists(path / subpath / (aux_v_base_name + ".txt"))) {
        problemFile_aux_v.push_back(path / subpath / (aux_v_base_name + ".txt"));
    }
    int aux_v_file_cnt = 1;
    while(std::filesystem::exists(path / subpath / (aux_v_base_name + std::to_string(aux_v_file_cnt) + ".txt"))) {
        problemFile_aux_v.push_back(path / subpath / (aux_v_base_name + std::to_string(aux_v_file_cnt) + ".txt"));
        aux_v_file_cnt++;
    }

    std::vector<std::filesystem::path> problemFile_h_max;
    if(std::filesystem::exists(path / subpath / (h_max_base_name + ".txt"))) {
        problemFile_h_max.push_back(path / subpath / (h_max_base_name + ".txt"));
    }
    int h_max_file_cnt = 1;
    while(std::filesystem::exists(path / subpath / (h_max_base_name + std::to_string(h_max_file_cnt) + ".txt"))) {
        problemFile_h_max.push_back(path / subpath / (h_max_base_name + std::to_string(h_max_file_cnt) + ".txt"));
        h_max_file_cnt++;
    }

    if(problemFile_llp.size() != problemFile_aux.size() || problemFile_llp.size() != problemFile_aux_v.size() || problemFile_llp.size() != problemFile_h_max.size() ) {
        throw std::invalid_argument("Different number of llp(s), aux, aux_v files");
    }
    if(problemFile_llp.size() != 1) {
        throw std::invalid_argument("currently number of llp(s), aux, aux_v files must be == 1");
    }

    std::vector<std::string> forbidden_expressions;
    std::vector<std::string> forbidden_keywords;
#if defined DIPS_enable_gams
    forbidden_keywords.insert(forbidden_keywords.end(),
      { "ms_gams_internal", "ss_gams_internal",
        "lbd_gams_internal", "ubd_gams_internal",
        "cpu_gams_internal", "mod_gams_internal",
        "obj_gams_internal", "eqobj_gams_internal" });
#ifndef DIPS_use_gams_parser_force_flattened
    forbidden_keywords.insert(forbidden_keywords.end(),
      {
        "NaN",
        "equation",
      });
    forbidden_expressions.insert(forbidden_expressions.end(),
      { "realset_.*", ".*_index", "eqcon.*", ".*_asreal" });
#endif
#endif

    // parsing problems
    std::cout << "Parsing skeleton (skl) problem" << std::endl;
    input.open(problemFile_skl, std::ios::binary);
    program skl;
    if(input.is_open()) {
        program_parser prog_par(input, symbols);
        prog_par.forbid_expressions(forbidden_expressions);
        prog_par.forbid_keywords(forbidden_keywords);

        skl = prog_par.parse();

        if(prog_par.fail()) {
            throw std::invalid_argument("Error: Encountered an error while parsing the skl problem file");
        }
        input.close();
        if(!skl.m_objective.empty() || !skl.m_constraints.empty() || !skl.m_relaxations.empty() || !skl.m_squashes.empty() || !skl.m_outputs.empty()) {
            throw std::invalid_argument("Error: Skeleton problem file must only contain definitions. Parsed "
                                        + std::to_string(skl.m_objective.size()) + " objective(s), "
                                        + std::to_string(skl.m_constraints.size()) + " constraint(s), "
                                        + std::to_string(skl.m_relaxations.size()) + " relaxation(s), "
                                        + std::to_string(skl.m_squashes.size()) + " squashe(s), and "
                                        + std::to_string(skl.m_outputs.size()) + " output(s)");
        }
    } else {
        throw std::invalid_argument("Error: Could not open skl problem file located at " + problemFile_skl.generic_string());
    }

    std::cout << "Parsing lbp problem" << std::endl;
    input.open(problemFile_lbp, std::ios::binary);
    if(input.is_open()) {
        program_parser prog_par(input, symbols);
        prog_par.forbid_expressions(forbidden_expressions);
        prog_par.forbid_keywords(forbidden_keywords);

        blp_program.lbp = prog_par.parse();

        if(prog_par.fail()) {
            throw std::invalid_argument("Error: Encountered an error while parsing the lbp problem file");
        }
        input.close();
    } else {
        throw std::invalid_argument("Error: Could not open lbp problem file located at " + problemFile_lbp.generic_string());
    }

    std::vector<program> llp;
    if(problemFile_llp.size() == 0) {
        throw std::invalid_argument("Error: No llp file(s) found at " + (path / (llp_base_name + "*.txt")).generic_string());
    }
    for(int i = 0; i < (problemFile_llp.size()); i++) {
        program_parser prog_par(input, symbols);
        prog_par.forbid_expressions(forbidden_expressions);
        prog_par.forbid_keywords(forbidden_keywords);

        std::cout << "Parsing llp_" << i + 1 << " problem" << std::endl;
        input.open(problemFile_llp[i], std::ios::binary);
        if(input.is_open()) {
            llp.push_back(prog_par.parse());
            if(prog_par.fail()) {
                throw std::invalid_argument("Error: Encountered an error while parsing the llp problem file");
            }
            input.close();
        } else {
            throw std::invalid_argument("Error: Could not open llp problem file located at " + problemFile_llp[i].generic_string());
        }
    }

    if(problemFile_aux.size() == 0) {
        throw std::invalid_argument("Error: No aux file(s) found at " + (path / (aux_base_name + "*.txt")).generic_string());
    }
    for(int i = 0; i < (problemFile_aux.size()); i++) {
        program_parser prog_par(input, symbols);
        prog_par.forbid_expressions(forbidden_expressions);
        prog_par.forbid_keywords(forbidden_keywords);

        std::cout << "Parsing aux_" << i + 1 << " problem" << std::endl;
        input.open(problemFile_aux[i], std::ios::binary);
        if(input.is_open()) {
            blp_program.aux.push_back(prog_par.parse());
            if(prog_par.fail()) {
                throw std::invalid_argument("Error: Encountered an error while parsing the aux problem file");
            }
            input.close();
        } else {
            throw std::invalid_argument("Error: Could not open aux problem file located at " + problemFile_aux[i].generic_string());
        }
    }

    if(problemFile_aux_v.size() == 0) {
        throw std::invalid_argument("Error: No aux_v file(s) found at " + (path / (aux_v_base_name + "*.txt")).generic_string());
    }
    for(int i = 0; i < (problemFile_aux_v.size()); i++) {
        program_parser prog_par(input, symbols);
        prog_par.forbid_expressions(forbidden_expressions);
        prog_par.forbid_keywords(forbidden_keywords);

        std::cout << "Parsing aux_v_" << i + 1 << " problem" << std::endl;
        input.open(problemFile_aux_v[i], std::ios::binary);
        if(input.is_open()) {
            blp_program.aux_v.push_back(prog_par.parse());
            if(prog_par.fail()) {
                throw std::invalid_argument("Error: Encountered an error while parsing the aux_v problem file");
            }
            input.close();
        } else {
            throw std::invalid_argument("Error: Could not open aux_v problem file located at " + problemFile_aux_v[i].generic_string());
        }
    }

    if(problemFile_h_max.size() == 0) {
        throw std::invalid_argument("Error: No h_max file(s) found at " + (path / (h_max_base_name + "*.txt")).generic_string());
    }
    for(int i = 0; i < (problemFile_h_max.size()); i++) {
        program_parser prog_par(input, symbols);
        prog_par.forbid_expressions(forbidden_expressions);
        prog_par.forbid_keywords(forbidden_keywords);

        std::cout << "Parsing h_max_" << i + 1 << " problem" << std::endl;
        input.open(problemFile_h_max[i], std::ios::binary);
        if(input.is_open()) {
            blp_program.h_max.push_back(prog_par.parse());
            if(prog_par.fail()) {
                throw std::invalid_argument("Error: Encountered an error while parsing the h_max problem file");
            }
            input.close();
        } else {
            throw std::invalid_argument("Error: Could not open h_max problem file located at " + problemFile_h_max[i].generic_string());
        }
    }

    std::cout << "Parsing ubp problem" << std::endl;
    input.open(problemFile_ubp, std::ios::binary);
    if(input.is_open()) {
        program_parser prog_par(input, symbols);
        prog_par.forbid_expressions(forbidden_expressions);
        prog_par.forbid_keywords(forbidden_keywords);

        blp_program.ubp = prog_par.parse();

        if(prog_par.fail()) {
            throw std::invalid_argument("Error: Encountered an error while parsing the ubp problem file");
        }
        input.close();
    } else {
        throw std::invalid_argument("Error: Could not open ubp problem file located at " + problemFile_ubp.generic_string());
    }

    // check consistency of variable bounds
    dips::util::check_variable_bounds(symbols, { { skl, blp_program.lbp, blp_program.ubp }, llp, blp_program.aux, blp_program.aux_v, blp_program.h_max });

    // parsing problem definitions
    std::cout << "Parsing problem definitions" << std::endl;
    input.open(problemFile_def, std::ios::binary);
    if(input.is_open()) {
        blp_box_program_definition_parser prog_def_par(input, symbols);
        prog_def_par.parse();
        if(prog_def_par.fail()) {
            throw std::invalid_argument("Error: Encountered an error while parsing the problem definitions file");
        } else {
            std::cout << "Setting blp parameters and infos\n";

            // TODO: implement multiple llps
            blp_program.lbp_variable = prog_def_par.get_internals_list("lbp", "u_cont_var");
            blp_program.box_parameter = prog_def_par.get_internals_list("lbp", "box_parameter");
            blp_program.h_max_names = prog_def_par.get_internals_list("lbp", "l_obj_max");
            blp_program.aux_hstar = prog_def_par.get_internals_list("aux", "l_obj_star");
            blp_program.aux_eps_h2 = prog_def_par.get_internals_list("aux", "eps2_l_obj");
            blp_program.ubp_lbd = prog_def_par.get_internal("ubp", "current_lbd");
            blp_program.ubp_eps_h = prog_def_par.get_internal("ubp", "eps_l_obj");

            std::cout << "Adding llp and discretization\n";
            if(prog_def_par.get_discretizations("lbp").size() != problemFile_llp.size()) {
                throw std::invalid_argument("Error: Number of llp files does not equal discretizations set in def.txt.");
            }
            for(int i = 0; i < prog_def_par.get_discretizations("lbp").size(); i++) {
                blp_program.add_llp(llp[i], prog_def_par.get_discretizations("lbp")[i]);
            }

            std::cout << "Adding info\n";
            blp_program.lbp_info = prog_def_par.get_info("lbp");
            blp_program.lbp_info_list = prog_def_par.get_info_list("lbp");
            blp_program.ubp_info = prog_def_par.get_info("ubp");
            blp_program.ubp_info_list = prog_def_par.get_info_list("ubp");
            blp_program.aux_info = prog_def_par.get_info("aux");
            blp_program.aux_info_list = prog_def_par.get_info_list("aux");
        }
        input.close();
    } else {
        throw std::invalid_argument("Error: Could not open definitions file located at " + problemFile_def.generic_string());
    }

    return blp_program;
}
