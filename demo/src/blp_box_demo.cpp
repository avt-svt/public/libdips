#include "blp_box_problem.hpp"
#include "blp_box_solver.hpp"
#include "dips_util.hpp"          // for getInputPath, getOutputPath, check_objective_value
#include "solver.hpp"             // for report
#include "symbol_table.hpp"       // for symbol_table
#include "demo_solver_config.hpp" // for used_subsolver

#include <iostream>   // for operator<<, char_traits, ostream, cout, endl, basic_ostream, ofstream, basic_ios::rdbuf, basic_ostream<>::__ostream_type
#include <filesystem> // for create_directories, remove_all
#include <exception>  // for exception
#include <fstream>    // for basic_ofstream<>::__filebuf_type
#include <memory>     // for allocator, shared_ptr, __shared_ptr_access, make_shared
#include <string>     // for string, operator+, basic_string

int main(int argc, char *argv[]) {

#ifdef DIPS_use_mpi
    MPI_Init(NULL, NULL);
    int _rank;
    int _nProcs;
    MPI_Comm_rank(MPI_COMM_WORLD, &_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &_nProcs);
    // Mute cout for workers to avoid multiple outputs
#if MUTE_WORKER
    std::ostringstream mutestream;
    std::streambuf *coutBuf = std::cout.rdbuf();
    std::streambuf *cerrBuf = std::cerr.rdbuf();
    if(_rank != 0) {
        std::cout.rdbuf(mutestream.rdbuf());
        std::cerr.rdbuf(mutestream.rdbuf());
    }
#endif
#endif

    std::cout << "Initializing:\n";
    std::cout << std::boolalpha << '\n';
    ale::symbol_table symbols;
    dips::solve_record rec;
    try {
        std::filesystem::path input_path = dips::util::getInputPath(argc, argv, "./blp_input/");
        std::filesystem::path output_path = dips::util::getOutputPath(argc, argv, "./blp_output_box/");

        // TODO: maingo should create its on folders
#ifdef DIPS_use_mpi
        if(_rank == 0) {
#endif
            std::filesystem::remove_all(output_path);
            std::filesystem::create_directories(output_path);
#ifdef DIPS_use_mpi
        }
        MPI_Barrier(MPI_COMM_WORLD);
#endif

        dips::blp_box_solver::program_info blp_program = get_blp_box(symbols, input_path);

        std::cout << "Initializing solver\n";
        std::shared_ptr<dips::blp_box_solver> blp = std::make_shared<dips::blp_box_solver>(symbols, dips::used_subsolver::make_solver, output_path.generic_string());


        std::cout << "Passing settings to solver. Note that some settings might be changed or overwritten by the algorithm.\n";
#ifdef DIPS_demos_use_maingo
        blp->set_option("settings_ubp", (input_path / "settings.txt").generic_string());
        blp->set_option("settings_lbp", (input_path / "settings.txt").generic_string());
        blp->set_option("settings_lbp_llp", (input_path / "settings.txt").generic_string());
        blp->set_option("settings_lbp_llp_aux", (input_path / "settings.txt").generic_string());
        blp->set_option("settings_lbp_llp_aux_v", (input_path / "settings.txt").generic_string());
#elif defined DIPS_demos_use_gams
        blp->set_option("settings_ubp", std::to_string(1));
        blp->set_option("settings_lbp", std::to_string(2));
        blp->set_option("settings_lbp_llp", std::to_string(3));
        blp->set_option("settings_lbp_llp_aux", std::to_string(4));
        blp->set_option("settings_lbp_llp_aux_v", std::to_string(5));
#endif
        //blp->set_option("feas_tol"         , 0.);     // TODO: add options

        std::cout << "\nSolving program:\n";
        rec = blp->solve(blp_program);

        // output for final status
        std::cout << "\n\n===================================================================\n\n"
                  << "Reporting final status\n\n"
                  << "===================================================================\n";
        report(rec);

        // create final output file
#ifdef DIPS_use_mpi
        if(_rank == 0) {
#endif
            std::ofstream final_output(output_path / "final_output.txt");
            std::streambuf *backup = std::cout.rdbuf();
            std::cout.rdbuf(final_output.rdbuf());
            report(rec);
            std::cout.rdbuf(backup); // reassign cout to its former buffer before that buffer no longer exists
            final_output.close();    // close file output
#ifdef DIPS_use_mpi
        }
        MPI_Barrier(MPI_COMM_WORLD);
#endif
        // check if user-set and computed objective value are similiar
        if(blp_program.lbp_info.find("objective") != blp_program.lbp_info.end()) {
            if(!dips::util::check_objective_value(rec, std::stod(blp_program.lbp_info.find("objective")->second), blp->get_option("abs_tol"), blp->get_option("rel_tol"))) {
                throw std::runtime_error("Error: User-set and computed optimal objective value not similiar.");
            }
        }
    } catch(std::exception &e) {
        std::cerr << std::endl
                  << e.what() << std::endl;
#ifdef DIPS_use_mpi
        // Turn on worker output again
#if MUTE_WORKER
        std::cout.rdbuf(coutBuf);
        std::cerr.rdbuf(cerrBuf);
#endif
        MPI_Finalize();
#endif
        return (-1);
    } catch(...) {
        std::cerr << "encountered unknown fatal error." << std::endl;
#ifdef DIPS_use_mpi
        // Turn on worker output again
#if MUTE_WORKER
        std::cout.rdbuf(coutBuf);
        std::cerr.rdbuf(cerrBuf);
#endif
        MPI_Finalize();
#endif
        return (-1);
    }

#ifdef DIPS_use_mpi
    // Turn on worker output again
#if MUTE_WORKER
    std::cout.rdbuf(coutBuf);
    std::cerr.rdbuf(cerrBuf);
#endif
    MPI_Finalize();
#endif
    if(rec.m_status == GLOBAL) {
        return 0;
    } else {
        return -1;
    }
}
