#include "gsip_rrhs_problem.hpp"  // for get_gsip
#include "gsip_rrhs_solver.hpp"   // for gsip_rrhs_solver, gsip_rrhs_solver::program_info
#include "dips_util.hpp"          // for getInputPath, getOutputPath, check_objective_value
#include "solver.hpp"             // for report
#include "symbol_table.hpp"       // for symbol_table
#include "demo_solver_config.hpp" // for used_subsolver

#include <iostream>   // for operator<<, char_traits, ostream, cout, endl, basic_ostream, ofstream, basic_ios::rdbuf, basic_ostream<>::__ostream...
#include <filesystem> // for create_directories, remove_all
#include <exception>  // for exception
#include <fstream>    // for basic_ofstream<>::__filebuf_type
#include <memory>     // for allocator, shared_ptr, __shared_ptr_access, make_shared
#include <string>     // for string, operator+, basic_string

int main(int argc, char *argv[]) {

#ifdef DIPS_use_mpi
    MPI_Init(NULL, NULL);
    int _rank;
    int _nProcs;
    MPI_Comm_rank(MPI_COMM_WORLD, &_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &_nProcs);
    // Mute cout for workers to avoid multiple outputs
#if MUTE_WORKER
    std::ostringstream mutestream;
    std::streambuf *coutBuf = std::cout.rdbuf();
    std::streambuf *cerrBuf = std::cerr.rdbuf();
    if(_rank != 0) {
        std::cout.rdbuf(mutestream.rdbuf());
        std::cerr.rdbuf(mutestream.rdbuf());
    }
#endif
#endif

    std::cout << "Initializing:\n";
    std::cout << std::boolalpha << '\n';
    ale::symbol_table symbols;
    dips::solve_record rec;
    try {
        std::filesystem::path input_path = dips::util::getInputPath(argc, argv, "./gsip_input/");
        std::filesystem::path output_path = dips::util::getOutputPath(argc, argv, "./gsip_output_rrhs/");

        // TODO: maingo should create its on folders
#ifdef DIPS_use_mpi
        if(_rank == 0) {
#endif
            std::filesystem::remove_all(output_path);
            std::filesystem::create_directories(output_path);
#ifdef DIPS_use_mpi
        }
        MPI_Barrier(MPI_COMM_WORLD);
#endif

        dips::gsip_rrhs_solver::program_info gsip_program = get_gsip_rrhs(symbols, input_path);

        std::cout << "Initializing solver\n";
        std::shared_ptr<dips::gsip_rrhs_solver> gsip = std::make_shared<dips::gsip_rrhs_solver>(symbols, dips::used_subsolver::make_solver, output_path.generic_string());


        std::cout << "Passing settings to solver. Note that some settings might be changed or overwritten by the algorithm.\n";
#ifdef DIPS_demos_use_maingo
        gsip->set_option("settings_lbp", (input_path / "settings_lbp.txt").generic_string());
        gsip->set_option("settings_lbp_llp", (input_path / "settings_llp.txt").generic_string());
        gsip->set_option("settings_ubp", (input_path / "settings_ubp.txt").generic_string());
        gsip->set_option("settings_ubp_llp", (input_path / "settings_llp.txt").generic_string());
        gsip->set_option("settings_lbp_aux", (input_path / "settings_aux.txt").generic_string());
#elif defined DIPS_demos_use_gams
        gsip->set_option("settings_lbp", (std::string) "1");
        gsip->set_option("settings_lbp_llp", (std::string) "2");
        gsip->set_option("settings_lbp_aux", (std::string) "4");
        gsip->set_option("settings_ubp", (std::string) "3");
        gsip->set_option("settings_ubp_llp", (std::string) "2");
#endif
        gsip->set_option("feas_tol", 0.00e+00);

        gsip->set_option("abs_tol", 1.00e-02);
        gsip->set_option("rel_tol", 1.00e-02);

        gsip->set_option("abs_tol_lbp", 1.00e-03);
        gsip->set_option("rel_tol_lbp", 1.00e-03);
        gsip->set_option("abs_tol_lbp_llp", 1.00e-04);
        gsip->set_option("rel_tol_lbp_llp", 1.00e-04);

        gsip->set_option("abs_tol_lbp_aux", 1.00e-05);
        gsip->set_option("rel_tol_lbp_aux", 1.00e-05);

        gsip->set_option("abs_tol_ubp", 1.00e-03);
        gsip->set_option("rel_tol_ubp", 1.00e-03);
        gsip->set_option("abs_tol_ubp_llp", 1.00e-04);
        gsip->set_option("rel_tol_ubp_llp", 1.00e-04);

        gsip->set_option("init_res_u", 1.00e+00);
        gsip->set_option("red_res_u", 2.00e+00);
        gsip->set_option("min_eps_res_u", 1.00e-09);

        gsip->set_option("init_res_l", 1.00e+00);
        gsip->set_option("red_res_l", 2.00e+00);
        gsip->set_option("min_eps_res_l", 1.00e-09);

        gsip->set_option("init_alpha", 2.50e-01);
        gsip->set_option("red_alpha", 1.20e+00);
        gsip->set_option("min_alpha", 1.00e-09);
        gsip->set_option("tol_llp_slater", 1.00e-05);

        gsip->set_option("red_tol_lbp_llp", 1.25e+00);
        gsip->set_option("red_tol_ubp_llp", 1.25e+00);

        gsip->set_option("max_iter", 1.00e+03);
        gsip->set_option("max_time", 1.20e+03);

        gsip->set_option("discr_all", false);

        std::cout << "\nSolving program:\n";
        rec = gsip->solve(gsip_program);

        // output for final status
        std::cout << "\n\n===================================================================\n\n"
                  << "Reporting final status\n\n"
                  << "===================================================================\n";
        report(rec);

        // create final output file
#ifdef DIPS_use_mpi
        if(_rank == 0) {
#endif
            std::ofstream final_output(output_path / "final_output.txt");
            std::streambuf *backup = std::cout.rdbuf();
            std::cout.rdbuf(final_output.rdbuf());
            report(rec);
            std::cout.rdbuf(backup); // reassign cout to its former buffer before that buffer no longer exists
            final_output.close();    // close file output
#ifdef DIPS_use_mpi
        }
        MPI_Barrier(MPI_COMM_WORLD);
#endif
        // check if user-set and computed objective value are similiar
        if(gsip_program.lbp_info.find("objective") != gsip_program.lbp_info.end()) {
            if(!dips::util::check_objective_value(rec, std::stod(gsip_program.lbp_info.find("objective")->second), gsip->get_option("abs_tol"), gsip->get_option("rel_tol"))) {
                throw std::runtime_error("Error: User-set and computed optimal objective value not similiar.");
            }
        }
    } catch(std::exception &e) {
        std::cerr << std::endl
                  << e.what() << std::endl;
#ifdef DIPS_use_mpi
        // Turn on worker output again
#if MUTE_WORKER
        std::cout.rdbuf(coutBuf);
        std::cerr.rdbuf(cerrBuf);
#endif
        MPI_Finalize();
#endif
        return (-1);
    } catch(...) {
        std::cerr << "encountered unknown fatal error." << std::endl;
#ifdef DIPS_use_mpi
        // Turn on worker output again
#if MUTE_WORKER
        std::cout.rdbuf(coutBuf);
        std::cerr.rdbuf(cerrBuf);
#endif
        MPI_Finalize();
#endif
        return (-1);
    }

#ifdef DIPS_use_mpi
    // Turn on worker output again
#if MUTE_WORKER
    std::cout.rdbuf(coutBuf);
    std::cerr.rdbuf(cerrBuf);
#endif
    MPI_Finalize();
#endif
    if(rec.m_status == GLOBAL) {
        return 0;
    } else {
        return -1;
    }
}
