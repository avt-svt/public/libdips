#include "esip_bnf_problem.hpp"   // for get_esip
#include "esip_bnf_solver.hpp"    // for esip_bnf_solver, esip_bnf_solver::program_info
#include "dips_util.hpp"          // for getInputPath, getOutputPath, check_objective_value
#include "solver.hpp"             // for report
#include "symbol_table.hpp"       // for symbol_table
#include "demo_solver_config.hpp" // for used_subsolver

#include <iostream>   // for operator<<, char_traits, ostream, cout, endl, basic_ostream, ofstream, basic_ios::rdbuf, basic_ostream<>::__ostream_...
#include <filesystem> // for create_directories, remove_all
#include <exception>  // for exception
#include <fstream>    // for basic_ofstream<>::__filebuf_type
#include <memory>     // for allocator, shared_ptr, __shared_ptr_access, make_shared
#include <string>     // for string, operator+, basic_string

int main(int argc, char *argv[]) {

#ifdef DIPS_use_mpi
    MPI_Init(NULL, NULL);
    int _rank;
    int _nProcs;
    MPI_Comm_rank(MPI_COMM_WORLD, &_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &_nProcs);
    // Mute cout for workers to avoid multiple outputs
#if MUTE_WORKER
    std::ostringstream mutestream;
    std::streambuf *coutBuf = std::cout.rdbuf();
    std::streambuf *cerrBuf = std::cerr.rdbuf();
    if(_rank != 0) {
        std::cout.rdbuf(mutestream.rdbuf());
        std::cerr.rdbuf(mutestream.rdbuf());
    }
#endif
#endif

    std::cout << "Initializing:\n";
    std::cout << std::boolalpha << '\n';
    ale::symbol_table symbols;
    dips::solve_record rec;
    try {
        std::filesystem::path input_path = dips::util::getInputPath(argc, argv, "./esip_input/");
        std::filesystem::path output_path = dips::util::getOutputPath(argc, argv, "./esip_output_bnf/");

        // TODO: maingo should create its on folders
#ifdef DIPS_use_mpi
        if(_rank == 0) {
#endif
            std::filesystem::remove_all(output_path);
            std::filesystem::create_directories(output_path);
#ifdef DIPS_use_mpi
        }
        MPI_Barrier(MPI_COMM_WORLD);
#endif

        dips::esip_bnf_solver::program_info esip_program = get_esip_bnf(symbols, input_path);

        std::cout << "Initializing solver\n";
        std::shared_ptr<dips::esip_bnf_solver> esip = std::make_shared<dips::esip_bnf_solver>(symbols, dips::used_subsolver::make_solver, output_path.generic_string());


        std::cout << "Passing settings to solver. Note that some settings might be changed or overwritten by the algorithm.\n";
#ifdef DIPS_demos_use_maingo
        esip->set_option("settings_ulp", (input_path / "settings_ulp.txt").generic_string());
        esip->set_option("settings_minmax_ulp", (input_path / "settings_minmax_ulp.txt").generic_string());
        esip->set_option("settings_minmax_llp", (input_path / "settings_minmax_llp.txt").generic_string());
#elif defined DIPS_demos_use_gams
        esip->set_option("settings_ulp", (std::string) "1");
        esip->set_option("settings_minmax_ulp", (std::string) "2");
        esip->set_option("settings_minmax_llp", (std::string) "3");
#endif
        esip->set_option("feas_tol", 1e-4);
        esip->set_option("max_iter", 100.0);
        //TODO: 1e-8 will not converge due to accuracy problems in maingo (we need
        //inequality tolerance in maingo to be lower, i.e.,  deltaIneq  1e-8

        std::cout << "\nSolving program:\n";
        rec = esip->solve(esip_program);

        // output for final status
        std::cout << "\n\n===================================================================\n\n"
                  << "Reporting final status\n\n"
                  << "===================================================================\n";
        report(rec);

        // create final output file
#ifdef DIPS_use_mpi
        if(_rank == 0) {
#endif
            std::ofstream final_output(output_path / "final_output.txt");
            std::streambuf *backup = std::cout.rdbuf();
            std::cout.rdbuf(final_output.rdbuf());
            report(rec);
            std::cout.rdbuf(backup); // reassign cout to its former buffer before that buffer no longer exists
            final_output.close();    // close file output
#ifdef DIPS_use_mpi
        }
        MPI_Barrier(MPI_COMM_WORLD);
#endif
        // check if user-set and computed objective value are similiar
        if(esip_program.lbp_info.find("objective") != esip_program.lbp_info.end()) {
            if(!dips::util::check_objective_value(rec, std::stod(esip_program.lbp_info.find("objective")->second), esip->get_option("abs_tol"), esip->get_option("rel_tol"))) {
                throw std::runtime_error("Error: User-set and computed optimal objective value not similiar.");
            }
        }
    } catch(std::exception &e) {
        std::cerr << std::endl
                  << e.what() << std::endl;
#ifdef DIPS_use_mpi
        // Turn on worker output again
#if MUTE_WORKER
        std::cout.rdbuf(coutBuf);
        std::cerr.rdbuf(cerrBuf);
#endif
        MPI_Finalize();
#endif
        return (-1);
    } catch(...) {
        std::cerr << "encountered unknown fatal error." << std::endl;
#ifdef DIPS_use_mpi
        // Turn on worker output again
#if MUTE_WORKER
        std::cout.rdbuf(coutBuf);
        std::cerr.rdbuf(cerrBuf);
#endif
        MPI_Finalize();
#endif
        return (-1);
    }

#ifdef DIPS_use_mpi
    // Turn on worker output again
#if MUTE_WORKER
    std::cout.rdbuf(coutBuf);
    std::cerr.rdbuf(cerrBuf);
#endif
    MPI_Finalize();
#endif
    if(rec.m_status == GLOBAL) {
        return 0;
    } else {
        return -1;
    }
}
