definitions:
set{index} ubp_disc_index := {};
real[0, 2] ubp_y_disc := 0;
real[0] ubp_z_disc;
real eps_res := 1;

objective:
- 4 * x[1] - 2/3 * (x[3] + x[5]);

constraints:
forall i in ubp_disc_index: -eps_res >= x[1] + ubp_z_disc[i] * ubp_y_disc[i][1] + x[2] * ubp_y_disc[i][2] + x[3] * ubp_y_disc[i][1] ^ 2 + x[4] * ubp_y_disc[i][1] * ubp_y_disc[i][2] + x[5] * ubp_y_disc[i][2] ^ 2 - 3 - (ubp_y_disc[i][1] ^ 2 - ubp_y_disc[i][2] ^ 2) ^ 2;

