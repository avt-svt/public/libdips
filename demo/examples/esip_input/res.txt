definitions:
set{index} res_disc_index := {};
real[0, 2] res_y_disc := 0;
real[0] res_z_disc;
real eta in [-100,100];
real f_res := 0;


objective:
- eta;

constraints:
- 4 * x[1] - 2/3 * (x[3] + x[5]) - f_res <= 0;

forall i in res_disc_index: -eta >= x[1] + res_z_disc[i] * res_y_disc[i][1] + x[2] * res_y_disc[i][2] + x[3] * res_y_disc[i][1] ^ 2 + x[4] * res_y_disc[i][1] * res_y_disc[i][2] + x[5] * res_y_disc[i][2] ^ 2 - 3 - (res_y_disc[i][1] ^ 2 - res_y_disc[i][2] ^ 2) ^ 2;

