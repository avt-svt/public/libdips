definitions:
set {index} ubp_k_disc:= {};
real[0] ubp_y_disc := 0;

real eps_res := 0;

objective:
obj(x);

constraints:
forall k in ubp_k_disc :
    forall u in {1 .. num_gu} :
        min( gu(x, ubp_y_disc[k])[u] , - max(l in {1 .. num_gl} : gl(x, ubp_y_disc[k])[l]) ) <= -eps_res;
