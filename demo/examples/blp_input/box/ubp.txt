definitions:
real eps_l_obj := 0;
real current_lbd := 0;

objective:
u_obj(xcont,ycont);

constraints:
forall i in {1 .. num_gu} :
    gu(xcont,ycont)[i] <= 0;
forall i in {1 .. num_gl} :
    gl(xcont,ycont)[i] <= 0;
forall i in {1 .. num_vil} :
    vil(ycont)[i] <= 0;
l_obj(xcont,ycont) <= l_obj_star + eps_l_obj;
current_lbd <= u_obj(xcont,ycont);
