definitions:
real l_obj_star := 0;
real alpha := 0;

objective:
max( i in {1 .. num_gl} : gl(xcont,ycont)[i]);

constraints:
l_obj(xcont,ycont) <= alpha * l_obj_star;
forall i in {1 .. num_vil} :
    vil(ycont)[i] <= 0;