# SIP
# min_{x in X} obj(x)
#         s.t. forall y in Y: gu(x,y) <= 0
#              X    := {x in X^0}
#              Y    := {y in Y^0}

definitions:
real[2] x in [-1000, 1000];

set{index} lbp_k_disc := {};
real[0] lbp_y_disc := 0;

index num_gu := 1;

real obj (real[2] x) := -inv(pow((x[1] - 4), 2) + pow((x[2] - 4), 2) + 0.1) - inv(pow((x[1] - 1), 2) + pow((x[2] - 1), 2) + 0.2) - inv(pow((x[1] - 8), 2) + pow((x[2] - 8), 2) + 0.2);
real[num_gu] gu(real[2] x, real y) := ( 0.1 - pow((x[1] - y), 2) - pow((x[2] - y), 2) );

objective:
obj(x);

constraints:
forall k in lbp_k_disc :
    forall u in {1 .. num_gu} :
        gu(x, lbp_y_disc[k])[u] <= 0;
