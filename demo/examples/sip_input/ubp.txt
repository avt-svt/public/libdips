definitions:
set{index} ubp_k_disc := {};
real[0] ubp_y_disc := 0;

real eps_res := 1;

objective:
obj(x);

constraints:
forall k in ubp_k_disc :
    forall u in {1 .. num_gu} :
        gu(x, ubp_y_disc[k])[u] <= - eps_res;
