definitions:
real y2 in [0, 10];

objective:
- max(u in {1 .. num_gu} : gu(x, y2)[u]);
