definitions:
real y1 in [-10, 0];

objective:
- max(u in {1 .. num_gu} : gu(x, y1)[u]);
